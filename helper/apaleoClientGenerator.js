const { codegen } = require('swagger-axios-codegen');

const generate = (remoteUrl, fileName, include = []) => {
  codegen({
    methodNameMode: 'operationId',
    outputDir: '.generated',
    fileName: `${fileName}.ts`,
    include,
    remoteUrl,
  });
};

// generate('https://webhook.apaleo.com/swagger/notification-v1/swagger.json', 'notification', ['Subscriptions']);
generate('https://api.apaleo.com/swagger/booking-v1/swagger.json', 'reservation', [
  {
    Reservation: [
      'BookingReservationsByIdGet',
      'BookingReservationsGet',
      'BookingReservationsByIdPatch',
      'BookingReservationsByIdAvailable-unitsGet',
    ],
  },
  { Booking: ['BookingBookingsByIdPatch', 'BookingBookingsByIdGet', 'BookingBookingsPost', 'BookingBookings$forcePost'] },
  {
    ReservationActions: [
      'BookingReservation-actionsByIdAssign-unitPut',
      'BookingReservation-actionsByIdAssign-unitByUnitIdPut',
      'BookingReservation-actionsByIdCheckoutPut',
      'BookingReservation-actionsByIdAmendPut',
      'BookingReservation-actionsByIdAmend$forcePut',
      'BookingReservation-actionsByIdCancelPut',
      'BookingReservation-actionsByIdCheckinPut',
    ],
  },
]);
generate('https://api.apaleo.com/swagger/booking-v1/swagger.json', 'reservationPost', [
  {
    Booking: ['BookingBookingsPost', 'BookingBookings$forcePost'],
  },
]);
generate('https://api.apaleo.com/swagger/rateplan-v1/swagger.json', 'rateplan', [
  {
    RatePlan: ['RateplanRate-plansByIdGet', 'RateplanRate-plansGet'],
  },
]);
generate('https://api.apaleo.com/swagger/inventory-v1/swagger.json', 'property', [
  { Property: ['InventoryPropertiesByIdGet', 'InventoryPropertiesGet'] },
  { Unit: ['InventoryUnitsGet', 'InventoryUnitsByIdPatch', 'InventoryUnitsByIdGet'] },
  { UnitGroup: ['InventoryUnit-groupsGet'] },
]);
generate('https://api.apaleo.com/swagger/finance-v1/swagger.json', 'finance', [
  { Folio: ['FinanceFoliosGet', 'FinanceFoliosByIdPatch', 'FinanceFoliosByIdGet'] },
  { Invoice: ['FinanceInvoicesGet', 'FinanceInvoicesPost', 'FinanceInvoicesByIdPdfGet', 'FinanceInvoicesByIdGet'] },
  { InvoiceAction: ['FinanceInvoice-actionsByIdCancelPut', 'FinanceInvoice-actionsByIdPayPut'] },
  { FolioActions: ['FinanceFolio-actionsByFolioIdCancellation-feePost'] },
  {
    FolioPayments: [
      'FinanceFoliosByFolioIdPaymentsPost',
      'FinanceFoliosByFolioIdPaymentsBy-payment-accountPost',
      'FinanceFoliosByFolioIdPaymentsBy-authorizationPost',
    ],
  },
]);
generate('https://api.apaleo.com/swagger/logs-v1/swagger.json', 'logs', [{ FinanceLogs: ['LogsFinanceFolioGet'] }]);
generate('https://api.apaleo.com/swagger/logs-v0-nsfw/swagger.json', 'reservationLogs', [{ BookingLogs: ['LogsBookingReservationGet'] }]);
generate('https://api.apaleo.com/swagger/availability-v1/swagger.json', 'availability', [
  {
    Availability: ['AvailabilityUnit-groupsGet'],
  },
]);
generate('https://api.apaleo.com/swagger/availability-v0-nsfw/swagger.json', 'availabilityNSFW', [
  {
    Availability: ['AvailabilityUnit-groupsByIdPatch'],
  },
]);
