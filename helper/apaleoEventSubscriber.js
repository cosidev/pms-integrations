const axios = require('axios').default;
const { keys } = require('./credentials');

const stage = 'live';

const getHeader = async () => {
  const credentials = Buffer.from(`${keys[stage].clientId}:${keys[stage].clientSecreet}`).toString('base64');
  try {
    const headers = {
      'Authorization': `Basic ${credentials}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    const result = await axios.post('https://identity.apaleo.com/connect/token', 'grant_type=client_credentials', { headers });
    const token = result.data.access_token;
    return {
      Authorization: `Bearer ${token}`,
    };
  } catch (e) {
    console.log(e);
  }
}

const addWebhook = async (endpointUrl, topics, propertyIds) => {
  try {
    const headers = await getHeader();

    const webhokData = { endpointUrl, topics, propertyIds };
    const webhookData = await axios.post('https://webhook.apaleo.com/v1/subscriptions', webhokData, { headers });
    console.log('Webhook', webhookData.data);
  } catch (e) {
    console.log(e);
  }
};

const removeWebhook = async (id) => {
  try {
    const headers = await getHeader();
    const webhookData = await axios.delete(`https://webhook.apaleo.com/v1/subscriptions/${id}`, { headers });
    console.log('Webhook', webhookData.data);
  } catch (e) {
    console.log(e);
  }
}

const getWebhooks = async () => {
  try {
    const headers = await getHeader();
    const result = await axios.get('https://webhook.apaleo.com/v1/subscriptions', { headers });
    console.log(result.data);
  } catch (e) {
    console.log(e);
  }
}

const main = async () => {
  await removeWebhook('ab4a1a18-757c-4527-b4c4-df9eb72e75c4');
  await addWebhook('https://lmwfrzxola.execute-api.eu-central-1.amazonaws.com/prod/apaleo/event?key=2FQvfHLhj8KFE9W8EfOn7frLHuvSMg0g', ['Reservation', 'Folio'], ['DE_BE_001', 'DE_BE_002', 'CZ_PR_002', 'DE_MU_001']);
  await getWebhooks();
};

main();
