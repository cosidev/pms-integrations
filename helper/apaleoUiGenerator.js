const axios = require('axios').default;
const { keys } = require('./credentials');

const stage = 'live';

const getHeader = async () => {
  const credentials = Buffer.from(`${keys[stage].clientId}:${keys[stage].clientSecreet}`).toString('base64');
  try {
    const headers = {
      'Authorization': `Basic ${credentials}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    const result = await axios.post('https://identity.apaleo.com/connect/token', 'grant_type=client_credentials', { headers });
    const token = result.data.access_token;
    return {
      Authorization: `Bearer ${token}`,
    };
  } catch (e) {
    console.log(e);
  }
}

const request = async (url, requestMethod, data = undefined) => {
  try {
    const baseUrl = 'https://integration.apaleo.com/integration/v1';
    const headers = await getHeader();
    const result = await requestMethod(`${baseUrl}${url}`, data || { headers }, { headers });
    console.log(result.data);
  } catch (e) {
    console.log(e);
    console.log(e.response.data);
  }
}

const createIntegration = async (target, data) => request(`/ui-integrations/${target}`, axios.post, data);
const getIntegrations = async () => request('/ui-integrations', axios.get);
const deleteIntegration = async (target, id) => request(`/ui-integrations/${target}/${id}`, axios.delete);
const testIntegration = async (target, id) => request(`/ui-integrations/${target}/${id}/$test`, axios.get);

const main = async () => {
  await getIntegrations();
  const integrationData = {
    code: 'reservationDetails',
    Label: 'Details',
    sourceUrl: 'https://lmwfrzxola.execute-api.eu-central-1.amazonaws.com/prod/apaleoExtensions/auth?path=reservationDetails&key=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGFsZW9VaUludGVncmF0aW9uIjp0cnVlLCJpYXQiOjE1NzQxODMxMTN9.bJoGWqtO6Een-CFtmL9qml8PXaz7BC9I-IZuSVxDq6A',
    sourceType: 'Private',
  };
  // await deleteIntegration('ReservationDetailsTab', 'COSIGROUPLIVE-RESERVATIONDETAILS');
  // await createIntegration('PropertyMenuApps', integrationData);
  await createIntegration('ReservationDetailsTab', integrationData);
  // await testIntegration('ReservationDetailsTab', 'COSIGROUPTEST-RESERVATIONDETAILS');
};

main();
