# PMS Integrations

Custom solutions for apaleo.

## Overivew
![Overview](overview.png?raw=true "Overview")

```
# Deployment (aws credentials need to be setup)
sls deploy

# Local development
yarn install

# Install local dynamodb (only needed once)
sls dynamodb install

yarn start

# Start dynamo db (separate terminal)
sls dynamodb start
```
