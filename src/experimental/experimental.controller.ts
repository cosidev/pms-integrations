import { Controller, Get, Post, Body, HttpException } from '@nestjs/common';
import Axios from 'axios';
import { KeyAuth } from '../common/auth/keyAuth.decorator';
import { generalConfig } from '../config';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import logger from '../LoggingService';
import { DynamoClient } from '../common/aws';

@Controller('experimental')
export class ExperimentalController {
  constructor(private readonly apaleoClient: ApaleoClient, private readonly dynamoClient: DynamoClient) {}

  @Get('units')
  @KeyAuth(generalConfig.experimental.apiKey)
  async getUnits() {
    let { units = [] } = await this.apaleoClient.getUnits({ propertyId: 'CZ_PR_002', expand: ['property'] });
    let { units: katerUnits = [] } = await this.apaleoClient.getUnits({ propertyId: 'DE_BE_002', expand: ['property'] });
    units = [...units, ...katerUnits];

    return units.map(unit => ({
      id: unit.id,
      name: unit.name,
      property: unit.property.name,
    }));
  }

  @Post('front')
  async frontWebhook(@Body() webhook: any) {
    const validUntilDate = new Date();
    validUntilDate.setDate(validUntilDate.getDate() + 10);
    const validUntil = Math.floor(validUntilDate.getTime() / 1000);
    await this.dynamoClient.putItem(generalConfig.aws.dataDynamoTableName, {
      dataKey: 'front',
      sKey: `webhook_${webhook.data.id}`,
      validUntil,
      ...webhook,
    });
    return { message: 'processed' };
  }

  @Post('open')
  @KeyAuth(generalConfig.experimental.apiKey)
  async openUnit(@Body() unitData: any) {
    const unitId = unitData.unitId;
    if (!unitId) {
      throw new HttpException('Invalid data', 400);
    }
    try {
      const accessPointId = await this.glutzRequest('eAccess.getAccessPoint', [{ unitId }]);
      const devices: any[] = await this.glutzRequest('eAccess.getModel', [
        'Devices',
        { deviceType: 111, accessPointId },
        ['id', 'deviceId', 'deviceType', 'deviceid', 'accessPointId'],
      ]);
      if (devices.length === 0) {
        throw new HttpException('Device not found', 404);
      }
      const device = devices[0];
      const method = 'eAccess.deviceOperation';
      const params = ['OpenDoor', { deviceid: device.deviceid, action: 1, outputs: 1, hasMotor: true }];
      await this.glutzRequest(method, params);
      return true;
    } catch (e) {
      throw new HttpException('Error with glutz communication', 400);
    }
  }

  private async glutzRequest(method, params) {
    const body = {
      jsonrpc: '2.0',
      id: 1,
      params,
      method,
    };
    const auth = {
      username: 'cosi',
      password: generalConfig.glutzInterfacePass,
    };
    const conf = {
      auth,
      dataType: 'json',
      headers: { 'Content-Type': 'application/json-rpc' },
    };
    const { data } = await Axios.post('https://pin.cosi-group.com/rpc', JSON.stringify(body), conf);
    if (!data || data.result === '0') {
      logger.debugLog('Invalid glutz server response', { glutzRequest: { params, method, data } });
      throw new Error('Invalid response');
    }
    return data.result;
  }
}
