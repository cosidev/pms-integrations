import { Module } from '@nestjs/common';
import { ExperimentalController } from './experimental.controller';
import { ApaleoModule } from '../apaleo/apaleo.module';
import { CommonModule } from '../common/common.module';

@Module({
  controllers: [ExperimentalController],
  imports: [ApaleoModule, CommonModule],
})
export class ExperimentalModule {}
