import { ApaleoReservation, Language, CancellationStatus } from './types';

export default class ReservationService {
  getFormattedRoomName(name: string) {
    const separatorIndex = name.indexOf('-');
    if (separatorIndex > 0) {
      return name.substr(0, separatorIndex);
    }
    return name;
  }

  // Needed for sag interface
  getReservationNumber(reservationId: string): number {
    // tslint:disable-next-line: radix
    return parseInt([...reservationId].map(c => c.charCodeAt(0)).join('').substr(0, 7));
  }

  getPreferredLanguage(reservation: ApaleoReservation): Language {
    const { booker, primaryGuest } = reservation;

    if (booker && booker.preferredLanguage) {
      const lang = booker.preferredLanguage.toLowerCase();
      if (['de', 'en'].includes(lang)) {
        return lang as Language;
      }
    }

    if (booker && booker.address && booker.address.countryCode) {
      const { countryCode = '' } = booker.address;

      return countryCode.toLowerCase() === 'de' ? 'de' : 'en';
    }

    if (primaryGuest.nationalityCountryCode) {
      const countryCode = primaryGuest.nationalityCountryCode;

      return countryCode.toLowerCase() === 'de' ? 'de' : 'en';
    }
    return 'en';
  }

  getCancellationStatus(reservation: ApaleoReservation): CancellationStatus {
    let cancellationStatus: CancellationStatus;
    const isFlexValid = new Date() < new Date(reservation.cancellationFee.dueDateTime);

    const isDirectIbe = ['Direct', 'Ibe'].includes(reservation.channelCode);
    const isCancelAllowed =
      reservation.actions &&
      reservation.actions.find(each => each.action === 'Cancel' && each.isAllowed);

    const isCancelable = isCancelAllowed && isDirectIbe;
    const isFlex = reservation.cancellationFee.code === 'FLEX';
    const isNonRef = reservation.cancellationFee.code === 'NONREF';

    if (isFlex && isFlexValid && isCancelable) {
      cancellationStatus = CancellationStatus.FlexFree;
    } else if (isFlex  && !isFlexValid && isCancelable) {
      cancellationStatus = CancellationStatus.FlexCharge;
    } else if (isNonRef && isCancelable) {
      cancellationStatus = CancellationStatus.NonRef;
    } else if (isCancelAllowed && !isDirectIbe) {
      cancellationStatus = CancellationStatus.Redirect;
    } else {
      cancellationStatus = CancellationStatus.NonCancelable;
    }

    return cancellationStatus;
  }
}
