import createHttpError from 'http-errors';
import MailService from '../MailService';
import sendGrid from '../sendGrid';
import ApaleoEventHandler from '../apaleo/ApaleoEventHandler';
import { ReservationEvent, MailType, ManualMailEvent } from '../types';
import CmsClient from '../CmsClient';
import logger from '../LoggingService';
import JWTService from '../JWTService';
import { applyMiddleware } from '../middleware';
import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import NewMailService from '../customerCommunication/mail.service';

export const onMailEvent = applyMiddleware(async event => {
  const eventData = event.Records[0].Sns;
  const reservationEvent: ReservationEvent = JSON.parse(eventData.Message);
  const cmsClient = new CmsClient();
  const mailService = new MailService(sendGrid, cmsClient);

  const app = await NestFactory.createApplicationContext(AppModule);
  const newMailService = app.get(NewMailService);

  switch (reservationEvent.type) {
    case MailType.Created:
      await newMailService.sendBookingConfirmation(reservationEvent);
      break;
    case MailType.CheckIn:
      await newMailService.sendCheckInInfos(reservationEvent);
      break;
    case MailType.CheckInDay:
      await newMailService.sendCheckInDayInfos(reservationEvent);
      break;
    case MailType.Invoice:
      await newMailService.sendInvoice(reservationEvent);
      break;
    case MailType.NeededGuestData:
      await newMailService.sendLegalRequirementsMail(reservationEvent);
      break;
    case MailType.ChangeInvoiceAddress:
      await mailService.sendChangeInvoiceAddress(reservationEvent);
      break;
    case MailType.BreakfastSelectionReminder:
      await mailService.sendDailyBreakfastReminder(reservationEvent);
      break;
  }
});

export const onMailAction = applyMiddleware(async event => {
  const eventData: ManualMailEvent = event.body;
  logger.infoLog('Manual mail event', eventData);
  if (!eventData.type && !eventData.reservationId) {
    throw createHttpError.BadRequest;
  }
  try {
    const jwt = new JWTService();
    const payload = jwt.verifyToken(eventData.token) as any;
    if (!payload.manualMail) {
      throw createHttpError.BadRequest;
    }
    const eventHandler = new ApaleoEventHandler();
    await eventHandler.processManualMailEvent(eventData);
    logger.infoLog('Processed manual mail event');
    return { message: 'Processed' };
  } catch (e) {
    logger.errorLog('Could not process manual mail event', { e, event });
    throw createHttpError.BadGateway;
  }
});
