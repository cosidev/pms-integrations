import logger from '../LoggingService';
import { ReservationEvent } from '../types';
import { ReservationExportHandler } from '../ReservationExportHandler';
import { applyMiddleware } from '../middleware';

export const onAllReservationExport = applyMiddleware((async (event) => {
  logger.infoLog('Exporting all reservations');
  const handler = new ReservationExportHandler();
  await handler.exportAllReservations();
  logger.infoLog('Exported all reservations');
}));

export const onAllUnitsExport = applyMiddleware((async (event) => {
  logger.infoLog('Exporting all Units');
  const handler = new ReservationExportHandler();
  await handler.exportAllUnits();
}));

export const onAllPropertiesExport = applyMiddleware((async (event) => {
  logger.infoLog('Exporting all Properties');
  const handler = new ReservationExportHandler();
  await handler.exportAllProperties();
}));

export const onChangedReservation = applyMiddleware((async (event) => {
  const eventData = event.Records[0].Sns;
  logger.infoLog('Export changed reservation', eventData);
  const reservationEvent: ReservationEvent = JSON.parse(eventData.Message);
  const handler = new ReservationExportHandler();
  await handler.exportChangedReservation(reservationEvent);
  logger.infoLog('Exported changed reservation');
}));
