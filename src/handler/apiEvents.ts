import createHttpError from 'http-errors';
import { HousekeepingType, HousekeepingRequest, GuestDataRequest, GuestPaymentRequest } from '../types';
import { generalConfig } from '../config';
import HousekeepingService from '../housekeeping/HousekeepingService';
import logger from '../LoggingService';
import GuestDataHandler from '../GuestDataHandler';
import { applyKeyAuthMiddleware, applyMiddleware } from '../middleware';
import GuestPaymentHandler from '../GuestPaymentHandler';

export const onGuestDataAction = applyMiddleware(async (event) => {
  const request: GuestDataRequest = event.body;
  const guestDataHandler = new GuestDataHandler();
  const data = await guestDataHandler.handleRequest(request);
  return data;
});

export const onGuestPaymentAction = applyMiddleware(async (event) => {
  const request: GuestPaymentRequest = event.body;
  const handler = new GuestPaymentHandler();
  const { headers: { origin, Origin } } = event;
  const data = await handler.handleRequest({ ...request, host: origin || Origin });
  return data;
});

export const onGuestPaymentRedirect = (async (event) => {
  // ToDo add env based location
  let redirectUrl = 'http://localhost:4000/payment';
  try {
    const handler = new GuestPaymentHandler();
    redirectUrl = await handler.handleRedirectRequest(event);
  } catch (e) {
    logger.errorLog('Could not handle payment redirect request', { e });
  }

  return {
    statusCode: 302,
    headers: {
      Location: redirectUrl,
    },
  };
});

export const onHousekeepingAction = applyKeyAuthMiddleware(generalConfig.housekeepingApiKey, async (event) => {
  try {
    const eventData: HousekeepingRequest = event.body;
    const housekeeping = new HousekeepingService();
    let data;
    let requestDate;
    switch (eventData.type) {
      case HousekeepingType.DailyInfos:
        requestDate = new Date(eventData.date);
        data = await housekeeping.getDailyInfos(requestDate, [eventData.propertyId]);
        break;
      case HousekeepingType.WeeklyInfos:
        requestDate = new Date(eventData.date);
        data = await housekeeping.getWeeklyInfos(requestDate, [eventData.propertyId]);
        break;
      case HousekeepingType.UnitCleaned:
        data = await housekeeping.handleCleanedUnit(eventData.unitId);
        break;
      case HousekeepingType.CheckOutInfos:
        requestDate = new Date(eventData.date);
        data = await housekeeping.getCheckedOutByDay(requestDate, [eventData.propertyId]);
        break;
    }
    return { data };
  } catch (e) {
    logger.errorLog('Could not handle housekeeping request', { error: e, errorMessage: e.message, errorTrace: e.stack });
    throw new createHttpError.BadGateway();
  }
});
