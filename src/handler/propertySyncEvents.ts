import logger from '../LoggingService';
import { PropertyDetailsService } from '../common/propertyDetails.service';
import { applyMiddleware } from '../middleware';
import { DynamoClient } from '../common/aws';
import { PropertyDetails } from '../common/types/property';
import JWTService from '../JWTService';
import createHttpError from 'http-errors';

export const onDatoCMSWebhookSent = applyMiddleware(async event => {
  const jwt = new JWTService();
  const { key } = event.queryStringParameters;
  const payload = jwt.verifyToken(key) as any;

  if (!payload?.datoCMS) {
    throw createHttpError.Unauthorized;
  }

  logger.infoLog('DatoCMS Property ' + event.body?.event_type + ' webhook recieved.');
  const {
    entity: { attributes },
  } = event.body;

  const dynamo = new DynamoClient();
  const propertyDetailsService = new PropertyDetailsService(dynamo);

  const camelCasedAttributes = datoCMSWebhookCamelCaser(attributes);

  await propertyDetailsService.createOrUpdateDetails(camelCasedAttributes);

  logger.infoLog('DatoCMS Property webhook processed.');
});

function datoCMSWebhookCamelCaser(snakeCaseValues: any) {
  if (!snakeCaseValues.property_id) {
    logger.errorLog('Invalid property id from datoCMS', snakeCaseValues);
    throw new Error('Invalid property id from datoCMS');
  }

  const camelCaseValues: PropertyDetails = {
    title: snakeCaseValues.title,
    propertyId: snakeCaseValues.property_id,
    bookingMail: snakeCaseValues.booking_mail,
    bookingPage: snakeCaseValues.booking_page,
    checkInAndOutDetails: { en: snakeCaseValues.check_in_and_out_details?.en, de: snakeCaseValues.check_in_and_out_details?.de },
    additionalInformation: { en: snakeCaseValues.additional_information?.en, de: snakeCaseValues.additional_information?.de },
    parking: { en: snakeCaseValues.parking?.en, de: snakeCaseValues.parking?.de },
    breakfastRecommendations: { en: snakeCaseValues.breakfast_recommendations?.en, de: snakeCaseValues.breakfast_recommendations?.de },
    specialHints: { en: snakeCaseValues.special_hints?.en, de: snakeCaseValues.special_hints?.de },
    recommendations: { en: snakeCaseValues.recommendations?.en, de: snakeCaseValues.recommendations?.de },
    emailFooter: { en: snakeCaseValues.email_footer?.en, de: snakeCaseValues.email_footer?.de },
    directions: { en: snakeCaseValues.directions?.en, de: snakeCaseValues.directions?.de },
    rooms: { en: snakeCaseValues.rooms?.en, de: snakeCaseValues.rooms?.de },
    signature: { en: snakeCaseValues.signature?.en, de: snakeCaseValues.signature?.de },
    availableServices: JSON.parse(snakeCaseValues.available_services),
  };

  return camelCaseValues;
}
