import ApaleoEventHandler from '../apaleo/ApaleoEventHandler';
import createHttpError from 'http-errors';
import { DashButtonEvent } from '../types';
import { applyMiddleware } from '../middleware';
import JWTService from '../JWTService';

export const onAwsCheckoutButtonPress = applyMiddleware(async (event: DashButtonEvent) => {
  const apaleoEventHandler = new ApaleoEventHandler();
  await apaleoEventHandler.processCheckOutEvent(event?.placementInfo?.attributes?.propertyId, event?.placementInfo?.attributes?.unitId, event);
});

export const onCheckoutButtonPressed = applyMiddleware(async event => {
  const jwt = new JWTService();
  const payload = jwt.verifyToken(event.body.key) as any;

  if (!payload.propertyId || !payload.unitId) {
    throw createHttpError.BadRequest;
  }

  const apaleoEventHandler = new ApaleoEventHandler();
  await apaleoEventHandler.processCheckOutEvent(payload.propertyId, payload.unitId, event.headers);
});
