import { NestFactory } from '@nestjs/core';
import logger from '../LoggingService';
import { applyMiddleware } from '../middleware';
import ApaleoEventHandler from '../apaleo/ApaleoEventHandler';
import PragueBreakfastHandler from '../PragueBreakfastHandler';
import { PragueExportHandler } from '../PragueExportHandler';
import { MunicoPinCodeHandler } from '../MunicoPinCodeHandler';
import { ApaleoPaymentHandler } from '../ApaleoPaymentHandler';
import { generalConfig } from '../config';
import { AppModule } from '../app.module';
import { GlutzCheckinService } from '../keySystem/glutz/GlutzCheckIn.service';
import { OverbookingsReport } from '../overbookings/OverbookingsReport';

export const sendCheckInInfos = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new ApaleoEventHandler();
  logger.infoLog('Processing checkIns');
  await eventHandler.processCheckIns();
});

export const sendInvoices = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new ApaleoEventHandler();
  logger.infoLog('Processing checkOuts');
  await eventHandler.processTodayCheckOuts();
});

export const sendMunicoPinCodes = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  logger.infoLog('Processing munico pin infos');
  const handler = new MunicoPinCodeHandler();
  await handler.processTodayPinCodes();
});

export const sendCheckInDayInfos = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new ApaleoEventHandler();
  await eventHandler.processCheckInDayInfos();
});

export const addPayments = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const paymentHandler = new ApaleoPaymentHandler();
  await paymentHandler.addCheckOutPayments();
  await paymentHandler.addCheckInPayments();
});

export const sendPragueExport = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const exporter = new PragueExportHandler();
  await exporter.exportGuestData();
});

export const validateTravelpurpose = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new ApaleoEventHandler();
  await eventHandler.processTravelPurposeValidation();
});

export const sendBreakfastReminders = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new ApaleoEventHandler();
  await eventHandler.processPragueBreakfastEmails();
});

export const sendBreakfastProviderMail = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const eventHandler = new PragueBreakfastHandler();
  await eventHandler.sendProviderInformation();
});

export const glutzDeviceLogExport = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const app = await NestFactory.createApplicationContext(AppModule);
  const service = app.get(GlutzCheckinService);
  await service.checkInReservations();
});

export const sendOverbookingsReport = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const app = await NestFactory.createApplicationContext(AppModule);
  const service = app.get(OverbookingsReport);
  await service.sendDailyOverbookingsReport();
});

export const disableUnitGroupsAvailability = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const app = await NestFactory.createApplicationContext(AppModule);
  const service = app.get(OverbookingsReport);
  await service.disableUnitGroupsAvailability();
});

export const disableOverbookingsBeforeArrival = applyMiddleware(async () => {
  if (!generalConfig.prod) {
    return;
  }
  const app = await NestFactory.createApplicationContext(AppModule);
  const service = app.get(OverbookingsReport);
  await service.disableOverbookingsBeforeArrival();
});
