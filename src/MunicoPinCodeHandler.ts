import { isToday } from 'date-fns';
import dateformat from 'dateformat';
import { ApaleoClient } from './apaleo/ApaleoClient';
import ReservationService from './ReservationService';
import { DynamoClient } from './DynamoClient';
import { DynamoClient as Dynamo } from './common/aws/dynamoClient';
import { ApaleoReservation } from './types';
import { ReservationDetailsService } from './common/reservation/reservationDetails.service';
import { GuestExperienceService } from './GuestExperienceService';
import { ReservationRefundDetailsService } from './common/reservation/reservationRefundDetails.service';

const detailsKey = 'munico_pin_code';

export class MunicoPinCodeHandler {
  private apaleoClient: ApaleoClient;
  private guestExperienceService: GuestExperienceService;
  private reservationService: ReservationService;
  private reservationDetailsService: ReservationDetailsService;
  private dynamoClient: DynamoClient;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.guestExperienceService = new GuestExperienceService();
    this.reservationService = new ReservationService();
    this.dynamoClient = new DynamoClient();
    const dynamo = new Dynamo();
    const refundService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundService);
  }

  async processTodayPinCodes() {
    // tslint:disable-next-line: max-line-length
    const { reservations = [] } = await this.apaleoClient.getReservationsByDay(new Date(), ['Confirmed', 'InHouse'], 'stay', {
      propertyIds: 'DE_MU_001', expand: ['assignedUnits'], sort: ['unitname:asc'],
    });
    const { units = [] } = await this.apaleoClient.getUnits({ propertyId: 'DE_MU_001' });

    // Pregenerate random code for units without checkIn / stay tonight
    const filteredUnits = units.filter(unit => !reservations.find(res => res.assignedUnits[0].unit.id === unit.id));
    let msg = '';
    for (const unit of filteredUnits) {
      let pinCode = await this.getSavedPinCode(unit.id);
      if (!pinCode) {
        pinCode = this.getRandomCode();
        await this.dynamoClient.addItem(unit.id, detailsKey, { pinCode });
        const roomName = this.reservationService.getFormattedRoomName(unit.name);
        msg += `Room = ${roomName} || PIN Code = *${pinCode}* || No Guest \n`;
      }
    }

    const checkIns = reservations.filter(res => isToday(new Date(res.arrival)));
    for (const reservation of checkIns) {
      const unit = reservation.assignedUnits[0].unit;
      const savedCode = await this.getSavedPinCode(unit.id);
      if (savedCode) {
        await this.dynamoClient.deleteItem(unit.id, detailsKey);
      }
      const roomName = this.reservationService.getFormattedRoomName(unit.name);
      const guestName = `${reservation.primaryGuest.firstName} ${reservation.primaryGuest.lastName}`;
      const reservationDetails = await this.reservationDetailsService.getDetails(reservation.id);
      const isBooking = reservation.channelCode === 'BookingCom';
      msg += `Room = ${roomName} || PIN Code = *${reservationDetails.pinCodes.pinComfortCode}* || Guest = ${guestName} || Reservation id = ${reservation.id} ${isBooking ? 'Booking (Check payment)' : ''} \n`;
    }
    if (msg.length > 0) {
      const today = new Date();
      await this.guestExperienceService.createGuestExperienceTask({
        sender: `Munico PIN Codes ${dateformat(today, 'dd-mm-yyyy')}`,
        subject: 'Munico PIN Codes today',
        message: msg,
      });
    }
  }

  async generate(reservation: ApaleoReservation, unitId: string) {
    // Use pregenerated code if is same day reservation
    if (isToday(new Date(reservation.arrival))) {
      const pinCode = await this.getSavedPinCode(unitId);
      if (pinCode) {
        await this.dynamoClient.deleteItem(unitId, detailsKey);
        return pinCode;
      }
    }
    return this.getRandomCode();
  }

  private async getSavedPinCode(unitId: string) {
    const response = await this.dynamoClient.getItem(unitId, detailsKey);
    if (response.Item) {
      return response.Item.pinCode;
    }
    return undefined;
  }

  private getRandomCode() {
    return `${Math.floor(1000 + Math.random() * 9000)}`;
  }
}
