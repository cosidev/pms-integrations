import { Injectable } from '@nestjs/common';
import { DynamoDB } from 'aws-sdk';
import dateformat from 'dateformat';
import { generalConfig } from '../config';
import { WorkingHoursDto } from './dto';

const housekeepingTable = generalConfig.aws.housekeepingDynamoTableName;

@Injectable()
export default class WorkingHoursService {
  private client: DynamoDB.DocumentClient;
  constructor() {
    this.client = new DynamoDB.DocumentClient({
      region: generalConfig.aws.dynamoRegion,
      endpoint: generalConfig.aws.dynamoEndpoint,
    });
  }

  async getUserHours(userId: string) {
    const result = await this.client.query({
      TableName: housekeepingTable,
      KeyConditionExpression: 'pKey = :pKey and begins_with(sKey, :sKey)',
      ExpressionAttributeValues: {
        ':pKey': userId,
        ':sKey': 'hours_',
      },
      ScanIndexForward: false,
      Limit: 50,
      ExpressionAttributeNames: {
        '#date': 'date',
        '#start': 'start',
        '#end': 'end',
      },
      ProjectionExpression: '#date,#start,#end,propertyId,breakStart,breakEnd',
    }).promise();
    return result.Items;
  }

  async addUserHours(userId: string, hours: WorkingHoursDto) {
    const date = dateformat(hours.date, 'yyyy-mm-dd');
    await this.client.put({
      TableName: housekeepingTable,
      Item: {
        pKey: userId,
        sKey: `hours_${date}_${hours.propertyId}`,
        ...hours,
      },
    }).promise();
    return hours;
  }
}
