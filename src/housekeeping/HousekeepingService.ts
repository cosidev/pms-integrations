import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { differenceInCalendarDays } from 'date-fns';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import { BookingModel, ReservationItemModel } from '../apaleo/generated/reservation';
import ReservationService from '../ReservationService';
import logger from '../LoggingService';
import { User } from '../auth/types';
import { SpecialWish } from '../types';
import { ReservationDetailsService } from '../common/reservation/reservationDetails.service';
import { DynamoClient } from '../common/aws';
import { ReservationRefundDetailsService } from '../common/reservation/reservationRefundDetails.service';

interface HousekeepingReservationMap {
  [key: string]: {
    reservationId: string;
    guests: number;
    additionalInfo: string;
    services: string[];
    checkInTime: string;
    checkOutTime: string;
    specialWishes: SpecialWish[];
    status: string;
  };
}

interface DayData {
  date: Date;
  counts: {
    [propertyId: string]: {
      propertyName: string;
      checkIns: number;
      checkOuts: number;
      stays: number;
    };
  };
}

interface BookingMap {
  [key: string]: BookingModel;
}

@Injectable()
export default class HousekeepingService {
  private apaleoClient: ApaleoClient;
  private reservationService: ReservationService;
  private reservationDetailsService: ReservationDetailsService;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.reservationService = new ReservationService();
    const dynamo = new DynamoClient();
    const refundDetailsService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundDetailsService);
  }

  async getUserProperties(user: User, role: string) {
    if (!user.propertyRoles) {
      return [];
    }
    const { properties = [] } = await this.apaleoClient.getProperties();
    return await Promise.all(
      properties
        .filter(property => user.propertyRoles[property.id] && user.propertyRoles[property.id].includes(role))
        .map(async property => {
          const { units = [] } = await this.apaleoClient.getUnits({ propertyId: property.id });
          return {
            id: property.id,
            name: property.name,
            units: units.map(unit => ({
              id: unit.id,
              name: this.reservationService.getFormattedRoomName(unit.name),
            })),
          };
        }),
    );
  }

  async getDailyInfos(day: Date, propertyIds: string[]) {
    try {
      const format = (date: Date) => dateformat(date, 'dd.mm.yyyy');

      // tslint:disable-next-line: max-line-length
      const { reservations = [] } = await this.apaleoClient.getReservationsByDay(day, ['Confirmed', 'InHouse', 'CheckedOut'], 'stay', {
        propertyIds,
      });

      const checkIns = reservations.filter(item => format(item.arrival) === format(day));
      const checkOuts = reservations.filter(item => format(item.departure) === format(day));
      const stays = reservations.filter(item => format(item.departure) !== format(day) && format(item.arrival) !== format(day));

      const bookings: BookingMap = {};
      for (const reservation of [...checkIns, ...stays]) {
        bookings[reservation.id] = await this.apaleoClient.getBookingById(reservation.bookingId);
      }

      const formattedArrivals = await this.formatReservations(checkIns, bookings);
      const formattedDepartures = await this.formatReservations(checkOuts);
      const formattedStays = await this.formatReservations(stays, bookings);

      const { units } = await this.apaleoClient.getUnits({ propertyId: propertyIds[0] });
      const unitList = units.map(unit => {
        const roomName = this.reservationService.getFormattedRoomName(unit.name);
        const checkIn = formattedArrivals[roomName];
        const checkOut = formattedDepartures[roomName];

        let stay = formattedStays[roomName];
        if (stay && checkIn) {
          if (stay.reservationId === checkIn.reservationId) {
            stay = undefined;
          }
        }
        if (stay && checkOut) {
          if (stay.reservationId === checkOut.reservationId) {
            stay = undefined;
          }
        }
        return {
          id: unit.id,
          title: roomName,
          occupied: unit.status.isOccupied,
          condition: unit.status.condition,
          checkIn,
          checkOut,
          stay,
        };
      });
      return unitList;
    } catch (e) {
      logger.errorLog('Could not load daily infos data', { error: e, errorMessage: e.message, errorTrace: e.stack });
      throw e;
    }
  }

  async getWeeklyInfos(day: Date, propertyIds: string[]) {
    try {
      day.setUTCHours(0, 0, 0);
      const twoWeeksLater = new Date();
      twoWeeksLater.setDate(day.getDate() + 14);
      twoWeeksLater.setUTCHours(23, 59);
      const weekData: DayData[] = [];
      const format = (date: Date) => dateformat(date, 'dd.mm.yyyy');
      // tslint:disable-next-line: max-line-length
      const { reservations = [] } = await this.apaleoClient.getReservationsbyTime(
        ['Confirmed', 'InHouse', 'CheckedOut'],
        'stay',
        day,
        twoWeeksLater,
        { propertyIds },
      );
      for (let i = 0; i <= 14; i++) {
        const nextDay = new Date();
        nextDay.setDate(day.getDate() + i);
        const formattedNextDay = format(nextDay);
        const checkIns = reservations.filter(item => format(item.arrival) === formattedNextDay);
        const checkOuts = reservations.filter(item => format(item.departure) === formattedNextDay);
        const stays = reservations.filter(
          item => differenceInCalendarDays(new Date(item.arrival), nextDay) < 0 && differenceInCalendarDays(new Date(item.departure), nextDay) > 0,
        );
        const counts = {};
        propertyIds.forEach(
          propertyId =>
            (counts[propertyId] = {
              checkIns: checkIns.filter(checkIn => checkIn.property.id === propertyId).length,
              checkOuts: checkOuts.filter(checkOut => checkOut.property.id === propertyId).length,
              stays: stays.filter(stay => stay.property.id === propertyId).length,
            }),
        );
        weekData.push({ date: nextDay, counts });
      }
      return weekData;
    } catch (e) {
      logger.errorLog('Could not load weekly infos data', { error: e, errorMessage: e.message, errorTrace: e.stack });
      throw e;
    }
  }

  async handleCleanedUnit(unitId: string, user?: User) {
    const data: any = {};
    try {
      await this.apaleoClient.markUnitAsCleaned(unitId);
      const name = user ? `${user.firstName} ${user.lastName}` : 'anonymous';
      logger.infoLog(`${unitId} was marked as cleaned by ${name}!`, { unitId });
      data.success = true;
    } catch (e) {
      logger.errorLog('Could not mark unit as cleaned', { e, unitId });
      data.success = false;
    }
    return data;
  }

  async getCheckedOutByDay(day: Date, propertyIds: string[]) {
    try {
      const expand = ['assignedUnits'];
      const { reservations = [] } = await this.apaleoClient.getReservationsByDay(day, ['CheckedOut'], 'Departure', { expand, propertyIds });
      const data = reservations.map(reservation => ({
        title: this.reservationService.getFormattedRoomName(reservation.assignedUnits[0].unit.name),
        checkedOutAt: reservation.checkOutTime,
      }));
      return data;
    } catch (e) {
      logger.errorLog('Could not load checked out reservations', { e, day });
      return { data: [] };
    }
  }

  private async formatReservations(reservations?: ReservationItemModel[], bookings?: BookingMap): Promise<HousekeepingReservationMap> {
    const formattedReservations = {};
    await Promise.all(
      reservations.map(async reservation => {
        const details = await this.reservationDetailsService.getDetails(reservation.id);
        try {
          const roomName = this.reservationService.getFormattedRoomName(reservation.unit.name);
          formattedReservations[roomName] = {
            name: `${reservation.primaryGuest.firstName} ${reservation.primaryGuest.lastName}`,
            reservationId: reservation.id,
            guests: reservation.adults,
            childrenAges: reservation.childrenAges || [],
            additionalInfo: bookings ? this.parseBookingComment(bookings[reservation.id].bookerComment) : '',
            services: reservation.services ? reservation.services.map(service => service.service.name) : [],
            checkInTime: dateformat(reservation.arrival, 'dd.mm.yyyy'),
            checkOutTime: dateformat(reservation.departure, 'dd.mm.yyyy'),
            specialWishes: details.specialWishes,
            status: reservation.status,
          };
        } catch (e) {
          logger.errorLog(`Could not format reservation for housekeeping ${reservation.id}`, {
            housekeepingReservation: reservation,
            errorMessage: e.message,
            e,
          });
        }
      }),
    );
    return formattedReservations;
  }

  private parseBookingComment(comment?: string) {
    if (!comment) {
      return comment;
    }
    let stripedComment = comment;
    stripedComment = stripedComment.replace(/\*\* THIS RESERVATION HAS BEEN PRE-PAID \*\*Guest has paid: EUR ([0-9]*\.[0-9]*)?[0-9]*/, '');
    stripedComment = stripedComment.replace(/BOOKING NOTE : Payment charge is EUR ([0-9]*\.[0-9]*)?[0-9]*/, '');
    stripedComment = stripedComment.replace('Booker has a genius status', '');
    // Try to retrieve check in times => set from booking.com
    const timeExpression = '([0-1]?[0-9]|2[0-3]):[0-5][0-9]/g';
    const checkInExpression = `between ${timeExpression} and ${timeExpression}`;
    const res = stripedComment.match(new RegExp(checkInExpression));
    if (res && res[0]) {
      const times = [...res[0].matchAll(new RegExp(timeExpression))];
      const from = times[0][0];
      const to = times[1][0];
      if (from && to) {
        return `Ankunft voraussichtlich zwischen ${from} und ${to}.`;
      }
    }
    return stripedComment;
  }
}
