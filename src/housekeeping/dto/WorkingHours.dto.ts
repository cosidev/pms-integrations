import { IsString, IsDateString } from 'class-validator';

export default class WorkingHoursDto {
  @IsDateString()
  date: string;

  @IsString()
  propertyId: string;

  @IsDateString()
  start: string;
  @IsDateString()
  end: string;

  @IsDateString()
  breakStart: string;
  @IsDateString()
  breakEnd: string;
}
