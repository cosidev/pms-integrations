import { IsString } from 'class-validator';

export default class DamageReportResolvedDto {
  @IsString()
  userId: string;

  @IsString()
  id: string;

  @IsString()
  detailsId: string;
}
