import { IsString } from 'class-validator';

export default class AreaCodesDto {
  @IsString()
  propertyId: string;

  @IsString()
  areaCode: string;
}
