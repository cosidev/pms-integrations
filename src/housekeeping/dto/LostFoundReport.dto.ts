import { IsString, ArrayMaxSize, Length, IsOptional } from 'class-validator';

export default class LostFoundReportDto {
  @IsOptional()
  unitId?: string;

  @IsString()
  areaCode: string;

  @IsString()
  propertyId: string;

  @IsString()
  propertyName: string;

  @IsString()
  @Length(3, 500)
  description: string;

  @IsString({ each: true })
  @ArrayMaxSize(10)
  pictures: string[];
}
