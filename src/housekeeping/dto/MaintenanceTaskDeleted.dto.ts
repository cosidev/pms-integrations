import { IsString } from 'class-validator';

export default class MaintenanceTaskDeletedDto {
  @IsString()
  propertyId: string;

  @IsString()
  id: string;
}
