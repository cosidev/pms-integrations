import { IsString } from 'class-validator';

export default class UnitCleanedDto {
  @IsString()
  unitId: string;
}
