import { IsString, IsBooleanString } from 'class-validator';

export default class MaintenanceQueryDto {
  @IsString()
  propertyId: string;

  @IsBooleanString()
  resolved: boolean;
}
