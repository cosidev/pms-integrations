import { IsString, IsNumber, IsOptional, Length } from 'class-validator';

export default class MaintenanceTaskResolvedDto {
  @IsString()
  areaCode: string;

  @IsString()
  propertyId: string;

  @IsString()
  propertyName: string;

  @IsString()
  @Length(3, 500)
  description: string;

  @IsNumber()
  dueDate: number;

  @IsString()
  id: string;

  @IsString()
  detailsId: string;

  @IsOptional()
  @IsNumber()
  frequency?: number;
}
