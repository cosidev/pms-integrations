import { IsString, IsDate, IsNumber, Length, IsOptional, IsDateString, IsBoolean } from 'class-validator';

export default class MaintenanceDto {
  @IsString()
  areaCode: string;

  @IsOptional()
  @IsString()
  unitId?: string;

  @IsString()
  propertyId: string;

  @IsString()
  propertyName: string;

  @IsString()
  @Length(3, 500)
  description: string;

  @IsDateString()
  dueDate: Date;

  @IsOptional()
  @IsNumber()
  frequency?: number;
}
