import { IsString, IsDateString } from 'class-validator';

export default class DayInfosDto {
  @IsDateString()
  date: string;

  @IsString()
  propertyId: string;
}
