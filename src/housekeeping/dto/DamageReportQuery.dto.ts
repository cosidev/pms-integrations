import { IsString, IsBooleanString } from 'class-validator';

export default class DamageReportQueryDto {
  @IsString()
  propertyId: string;

  @IsBooleanString()
  resolved: boolean;
}
