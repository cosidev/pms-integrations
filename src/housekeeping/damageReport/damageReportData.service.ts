import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { generalConfig } from '../../config';
import { DamageReportDto, DamageReportResolvedDto } from '../dto';
import { Report } from '../types';
import { DynamoClient } from '../../common/aws';

const housekeepingTable = generalConfig.aws.housekeepingDynamoTableName;

@Injectable()
export default class DamageReportDataService {
  constructor(private readonly dynamoClient: DynamoClient) {}

  async getUserDamageReports(userId: string, propertyId: string): Promise<Report[]> {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        KeyConditionExpression: 'pKey = :userId and begins_with(sKey, :sKey)',
        ExpressionAttributeValues: {
          ':userId': userId,
          ':sKey': `damage_${propertyId}`,
        },
        ScanIndexForward: false,
        Limit: 50,
        ProjectionExpression: 'propertyId,propertyName,description,unitId,areaCode,pictures,createdAt,resolved,sKey,GSI1sKey,GSI1pKey',
      })
      .promise();

    return this.formatDamageReportItems(result.Items);
  }

  async getPropertyDamageReports(propertyId: string, resolved: boolean) {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        IndexName: 'GSI1',
        KeyConditionExpression: 'GSI1pKey = :pKey and begins_with(GSI1sKey, :sKey)',
        ExpressionAttributeValues: {
          ':pKey': propertyId,
          ':sKey': `damage_${resolved ? 'resolved' : 'unresolved'}`,
        },
        ScanIndexForward: true,
        Limit: 50,
        ProjectionExpression: 'propertyId,propertyName,description,unitId,areaCode,pictures,createdAt,resolved,pKey,sKey,GSI1sKey,GSI1pKey',
      })
      .promise();

    return this.formatDamageReportItems(result.Items);
  }

  async saveDamageReport(userId: string, data: DamageReportDto): Promise<void> {
    const now = new Date();
    const createdAt = Math.floor(now.getTime() / 1000);
    const date = dateformat(now, 'yyyy-mm-dd HH:MM:ss');

    await this.dynamoClient.putItem(housekeepingTable, {
      ...data,
      pKey: userId,
      sKey: `damage_${data.propertyId}_${date}`,
      resolved: false,
      userId,
      createdAt,
      GSI1pKey: data.propertyId,
      GSI1sKey: `damage_unresolved_${date}`,
    });
  }

  async resolveDamageReport(damageReport: DamageReportResolvedDto): Promise<void> {
    const key = {
      pKey: damageReport.userId,
      sKey: damageReport.id,
    };
    const values = { resolved: true, GSI1sKey: damageReport.detailsId.replace('unresolved', 'resolved') };

    await this.dynamoClient.updateItem(housekeepingTable, key, values);
  }

  private formatDamageReportItems(items: any[]): Report[] {
    const reports: Report[] = items.map(item => ({
      id: item.sKey,
      detailsId: item.GSI1sKey,
      userId: item.pKey,
      createdAt: item.createdAt,
      description: item.description,
      pictures: item.pictures,
      propertyId: item.GSI1pKey,
      propertyName: item.propertyName,
      resolved: item.resolved,
      unitId: item.unitId,
      areaCode: item.areaCode,
    }));

    return reports;
  }
}
