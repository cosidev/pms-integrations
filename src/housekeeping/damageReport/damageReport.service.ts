import { Injectable } from '@nestjs/common';
import { v4 } from 'uuid';
import { generalConfig } from '../../config';
import { S3Service } from '../../common/s3.service';
import { DamageReportDto, DamageReportResolvedDto, DamageReportQueryDto } from '../dto';
import { SlackService } from '../../SlackService';
import { Report } from '../types';
import DamageReportDataService from './damageReportData.service';

const housekeepingBucket = generalConfig.aws.housekeepingBucketName;
const folderName = 'damageReport';

@Injectable()
export default class DamageReportService {
  constructor(
    private readonly s3Service: S3Service,
    private readonly dataService: DamageReportDataService,
    private readonly slackService: SlackService,
  ) {}

  async getUploadPictureUrl(unitPath: string) {
    const key = `${folderName}/${unitPath}/${v4()}.jpeg`;
    const uploadUrl = await this.s3Service.getPresignedUrl(housekeepingBucket, key, 600, 'image/jpeg', 'public-read');
    return { uploadUrl };
  }

  async getUserDamageReports(userId: string, propertyId: string): Promise<Report[]> {
    return this.dataService.getUserDamageReports(userId, propertyId);
  }

  async getPropertyDamageReports(data: DamageReportQueryDto) {
    return this.dataService.getPropertyDamageReports(data.propertyId, data.resolved);
  }

  async saveDamageReport(userId: string, data: DamageReportDto) {
    await this.dataService.saveDamageReport(userId, data);
    await this.slackService.sendOperationsTask(
      `Damage has been reported in the room ${data.areaCode} (${data.unitId}) in ${data.propertyName}.
    Description: ${data.description}`,
      data.pictures,
    );
    return data;
  }

  async resolveDamageReport(damageReport: DamageReportResolvedDto) {
    await this.dataService.resolveDamageReport(damageReport);
  }
}
