import { CommonModule } from './../common/common.module';
import { Test, TestingModule } from '@nestjs/testing';
import { HousekeepingController } from './housekeeping.controller';
import HousekeepingService from './HousekeepingService';
import WorkingHoursService from './WorkingHours.service';
import DamageReportService from './damageReport/damageReport.service';
import DamageReportDataService from './damageReport/damageReportData.service';
import LostFoundReportService from './lostFound/lostFoundReport.service';
import LostFoundReportDataService from './lostFound/lostFoundReportData.service';
import MaintenanceService from './maintenance/maintenance.service';
import MaintenanceDataService from './maintenance/maintenanceData.service';
import { HousekeepingModule } from './housekeeping.module';

describe('Housekeeping Controller', () => {
  let controller: HousekeepingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CommonModule, HousekeepingModule],
      controllers: [HousekeepingController],
      providers: [
        HousekeepingService,
        WorkingHoursService,
        DamageReportService,
        DamageReportDataService,
        LostFoundReportService,
        LostFoundReportDataService,
        MaintenanceService,
        MaintenanceDataService,
      ],
    }).compile();

    controller = module.get<HousekeepingController>(HousekeepingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
