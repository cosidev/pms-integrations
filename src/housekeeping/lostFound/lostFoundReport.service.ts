import { Injectable } from '@nestjs/common';
import { v4 } from 'uuid';
import dateformat from 'dateformat';
import { generalConfig } from '../../config';
import { S3Service } from '../../common/s3.service';
import { LostFoundReportDto } from '../dto';
import { SlackService } from '../../SlackService';
import LostFoundReportDataService from './lostFoundReportData.service';

const housekeepingBucket = generalConfig.aws.housekeepingBucketName;
const folderName = 'lostFoundReport';

@Injectable()
export default class LostFoundReportService {
  constructor(
    private readonly s3Service: S3Service,
    private readonly dataService: LostFoundReportDataService,
    private readonly slackService: SlackService,
  ) {}

  async getUploadPictureUrl(unitId: string) {
    const key = `${folderName}/${unitId}/${v4()}.jpeg`;
    const uploadUrl = await this.s3Service.getPresignedUrl(housekeepingBucket, key, 600, 'image/jpeg', 'public-read');
    return { uploadUrl };
  }

  async getUserLostFoundReports(userId: string, propertyId: string) {
    return this.dataService.getUserLostFoundReports(userId, propertyId);
  }

  async getPropertyLostFoundReports(propertyId: string) {
    return this.dataService.getPropertyLostFoundReports(propertyId);
  }

  async saveLostFoundReport(userId: string, data: LostFoundReportDto) {
    const lastSeqNumber = await this.dataService.getLatestSequenceNumber(data.propertyId);
    let seqNumber = 1;
    if (lastSeqNumber) {
      seqNumber = lastSeqNumber + 1;
    }

    await this.dataService.saveLostFoundReport(userId, {
      ...data,
      seqNumber,
    });
    await this.dataService.addSequenceNumber(data.propertyId, seqNumber);

    const message = data.unitId
      ? `Lost items have been reported in the room ${data.areaCode} (${data.unitId}) in ${data.propertyName}.
        Description: ${data.description}`
      : `Lost items have been reported in ${data.propertyName}.`;
    await this.slackService.sendOperationsTask(message, data.pictures);

    return data;
  }
}
