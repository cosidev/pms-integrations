import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { LostFoundReport } from '../types';
import { DynamoClient } from '../../common/aws';
import { generalConfig } from '../../config';

const housekeepingTable = generalConfig.aws.housekeepingDynamoTableName;

@Injectable()
export default class LostFoundReportDataService {
  constructor(private readonly dynamoClient: DynamoClient) {}

  async getUserLostFoundReports(userId: string, propertyId: string) {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        KeyConditionExpression: 'pKey = :userId and begins_with(sKey, :sKey)',
        ExpressionAttributeValues: {
          ':userId': userId,
          ':sKey': `lostfound_${propertyId}`,
        },
        ScanIndexForward: false,
        Limit: 50,
        ProjectionExpression: 'propertyId,propertyName,description,unitId,areaCode,pictures,createdAt,resolved,seqNumber,sKey,GSI1sKey',
      })
      .promise();

    return this.formatLostFoundReports(result.Items);
  }

  async getPropertyLostFoundReports(propertyId: string) {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        IndexName: 'GSI1',
        KeyConditionExpression: 'GSI1pKey = :pKey and begins_with(GSI1sKey, :sKey)',
        ExpressionAttributeValues: {
          ':pKey': propertyId,
          ':sKey': 'lostfound_unresolved',
        },
        ScanIndexForward: false,
        Limit: 50,
        ProjectionExpression: 'propertyId,propertyName,description,unitId,areaCode,pictures,createdAt,resolved,seqNumber,sKey,GSI1sKey',
      })
      .promise();

    return this.formatLostFoundReports(result.Items);
  }

  async getLatestSequenceNumber(propertyId: string): Promise<number | undefined> {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        KeyConditionExpression: 'pKey = :pKey and begins_with(sKey, :sKey)',
        ExpressionAttributeValues: {
          ':pKey': propertyId,
          ':sKey': 'seqnumber_',
        },
        ScanIndexForward: false,
        Limit: 1,
        ProjectionExpression: 'seqNumber',
      })
      .promise();

    if (result.Items.length === 0) {
      return undefined;
    }
    return result.Items[0].seqNumber;
  }

  async addSequenceNumber(propertyId: string, seqNumber: number): Promise<void> {
    const now = new Date();
    const createdAt = Math.floor(now.getTime() / 1000);
    await this.dynamoClient.putItem(housekeepingTable, {
      pKey: propertyId,
      sKey: `seqnumber_${createdAt}`,
      seqNumber,
    });
  }

  async saveLostFoundReport(userId: string, data: Partial<LostFoundReport>): Promise<void> {
    const now = new Date();
    const createdAt = Math.floor(now.getTime() / 1000);
    const date = dateformat(now, 'yyyy-mm-dd HH:MM:ss');

    const item: any = {
      ...data,
      pKey: userId,
      sKey: `lostfound_${data.propertyId}_${date}`,
      GSI1pKey: data.propertyId,
      GSI1sKey: `lostfound_unresolved_${date}`,
      resolved: false,
      seqNumber: data.seqNumber,
      createdAt,
      userId,
    };

    await this.dynamoClient.putItem(housekeepingTable, item);
  }

  private formatLostFoundReports(items: any[]): LostFoundReport[] {
    const reports: LostFoundReport[] = items.map(item => ({
      id: item.sKey,
      detailsId: item.GSI1sKey,
      userId: item.pKey,
      createdAt: item.createdAt,
      description: item.description,
      pictures: item.pictures,
      propertyId: item.GSI1pKey,
      propertyName: item.propertyName,
      resolved: item.resolved,
      unitId: item.unitId,
      areaCode: item.areaCode,
      seqNumber: item.seqNumber,
    }));
    return reports;
  }
}
