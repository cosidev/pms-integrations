import { Controller, Post, Delete, Req, Body, UseGuards, Get, applyDecorators, Param, Query } from '@nestjs/common';
import createHttpError from 'http-errors';
import { HousekeepingRequest, HousekeepingType } from '../types';
import HousekeepingService from './HousekeepingService';
import logger from '../LoggingService';
import { KeyAuth } from '../common/auth/keyAuth.decorator';
import { generalConfig } from '../config';
import {
  DayInfosDto,
  UnitCleanedDto,
  WeekInfosDto,
  WorkingHoursDto,
  DamageReportDto,
  DamageReportResolvedDto,
  LostFoundReportDto,
  MaintenanceDto,
  MaintenanceQueryDto,
  DamageReportQueryDto,
  MaintenanceTaskResolvedDto,
  MaintenanceTaskDeletedDto,
  AreaCodesDto,
} from './dto';
import { AuthGuard } from '@nestjs/passport';
import { PropertyGuard } from '../auth/guards/Property.guard';
import { Roles } from '../auth/roles';
import WorkingHoursService from './WorkingHours.service';
import DamageReportService from './damageReport/damageReport.service';
import LostFoundReportService from './lostFound/lostFoundReport.service';
import MaintenanceService from './maintenance/maintenance.service';

enum HousekeepingRoles {
  Cleaner = 'Cleaner',
  Maintenance = 'Maintenance',
}

const CleanerGuard = () => applyDecorators(UseGuards(AuthGuard('jwt'), PropertyGuard), Roles(HousekeepingRoles.Cleaner));
const MaintenanceGuard = () => applyDecorators(UseGuards(AuthGuard('jwt'), PropertyGuard), Roles(HousekeepingRoles.Maintenance));

@Controller('housekeeping')
export class HousekeepingController {
  constructor(
    private readonly housekeepingService: HousekeepingService,
    private readonly hoursService: WorkingHoursService,
    private readonly damageReportService: DamageReportService,
    private readonly lostFoundReportService: LostFoundReportService,
    private readonly maintenanceService: MaintenanceService,
  ) {}

  @Post()
  @KeyAuth(generalConfig.housekeepingApiKey)
  async index(@Req() event) {
    try {
      const eventData: HousekeepingRequest = event.body;
      let data;
      let requestDate;
      switch (eventData.type) {
        case HousekeepingType.DailyInfos:
          requestDate = new Date(eventData.date);
          data = await this.housekeepingService.getDailyInfos(requestDate, [eventData.propertyId]);
          break;
        case HousekeepingType.WeeklyInfos:
          requestDate = new Date(eventData.date);
          data = await this.housekeepingService.getWeeklyInfos(requestDate, [eventData.propertyId]);
          break;
        case HousekeepingType.UnitCleaned:
          data = await this.housekeepingService.handleCleanedUnit(eventData.unitId);
          break;
        case HousekeepingType.CheckOutInfos:
          requestDate = new Date(eventData.date);
          data = await this.housekeepingService.getCheckedOutByDay(requestDate, [eventData.propertyId]);
          break;
      }
      return data;
    } catch (e) {
      logger.errorLog('Could not handle housekeeping request', { error: e, errorMessage: e.message, errorTrace: e.stack });
      throw new createHttpError.BadGateway();
    }
  }

  @Get('properties')
  @CleanerGuard()
  properties(@Req() req) {
    return this.housekeepingService.getUserProperties(req.user, HousekeepingRoles.Cleaner);
  }

  @Post('daily')
  @CleanerGuard()
  dailyInfos(@Body() data: DayInfosDto) {
    return this.housekeepingService.getDailyInfos(new Date(data.date), [data.propertyId]);
  }

  @Post('weekly')
  @CleanerGuard()
  async weeklyInfos(@Body() data: WeekInfosDto) {
    return this.housekeepingService.getWeeklyInfos(new Date(data.date), data.propertyIds);
  }

  @Post('checkOuts')
  @CleanerGuard()
  async checkOutInfos(@Body() data: DayInfosDto) {
    return this.housekeepingService.getCheckedOutByDay(new Date(data.date), [data.propertyId]);
  }

  @Post('unitCleaned')
  @CleanerGuard()
  async unitCleaned(@Body() data: UnitCleanedDto, @Req() request) {
    return this.housekeepingService.handleCleanedUnit(data.unitId, request.user);
  }

  @Get('hours')
  @CleanerGuard()
  async getWorkingHours(@Req() req) {
    return this.hoursService.getUserHours(req.user.id);
  }

  @Post('hours')
  @CleanerGuard()
  async addWorkingHours(@Req() req, @Body() data: WorkingHoursDto) {
    return this.hoursService.addUserHours(req.user.id, data);
  }

  @Get('damageReport/:propertyId')
  @CleanerGuard()
  getUserDamageReports(@Req() req, @Param('propertyId') propertyId: string) {
    // ToDo extend property guard to support param authorization
    return this.damageReportService.getUserDamageReports(req.user.id, propertyId);
  }

  @Get('damageReport')
  @MaintenanceGuard()
  getPropertyDamageReports(@Query() data: DamageReportQueryDto) {
    data.resolved = (data.resolved as any) === 'true';
    return this.damageReportService.getPropertyDamageReports(data);
  }

  @Get('damageReport/uploadUrl/:unitId')
  @CleanerGuard()
  getDamagePictureUrl(@Param('unitId') unitId: string) {
    return this.damageReportService.getUploadPictureUrl(unitId);
  }

  @Post('damageReport')
  @CleanerGuard()
  addDamageReport(@Req() req, @Body() data: DamageReportDto) {
    return this.damageReportService.saveDamageReport(req.user.id, data);
  }

  @Post('damageReport/resolved')
  @MaintenanceGuard()
  resolveDamageReport(@Body() data: DamageReportResolvedDto) {
    return this.damageReportService.resolveDamageReport(data);
  }

  @Get('lostFoundReport/:propertyId')
  @CleanerGuard()
  getLostFoundReportsByUserId(@Req() req, @Param('propertyId') propertyId: string) {
    return this.lostFoundReportService.getUserLostFoundReports(req.user.id, propertyId);
  }

  @Get('lostFoundReport/maintenance/:propertyId')
  @MaintenanceGuard()
  getPropertyLostFoundReports(@Param('propertyId') propertyId: string) {
    return this.lostFoundReportService.getPropertyLostFoundReports(propertyId);
  }

  @Get('lostFoundReport/uploadUrl/:unitId')
  @CleanerGuard()
  getLostFoundPictureUrl(@Param('unitId') unitId: string) {
    return this.lostFoundReportService.getUploadPictureUrl(unitId);
  }

  @Post('lostFoundReport')
  @CleanerGuard()
  addLostFoundReport(@Req() req, @Body() data: LostFoundReportDto) {
    return this.lostFoundReportService.saveLostFoundReport(req.user.id, data);
  }

  @Get('maintenance')
  @MaintenanceGuard()
  getPropertyMaintenanceTasks(@Query() data: MaintenanceQueryDto) {
    data.resolved = (data.resolved as any) === 'true';
    return this.maintenanceService.getPropertyMaintenanceTasks(data);
  }

  @Post('maintenance')
  @MaintenanceGuard()
  saveMaintenanceTask(@Req() req, @Body() data: MaintenanceDto) {
    return this.maintenanceService.saveMaintenanceTask(req.user.id, data);
  }

  @Post('maintenance/resolved')
  @MaintenanceGuard()
  resolveMaintenanceTask(@Req() req, @Body() data: MaintenanceTaskResolvedDto) {
    return this.maintenanceService.resolveMaintenanceTask(req.user.id, data);
  }

  @Delete('maintenance')
  @MaintenanceGuard()
  deleteMaintenanceTask(@Body() data: MaintenanceTaskDeletedDto) {
    return this.maintenanceService.deleteMaintenanceTask(data);
  }

  @Get('areaCodes/:propertyId')
  @MaintenanceGuard()
  getPropertyAreaCodes(@Param('propertyId') propertyId: string) {
    return this.maintenanceService.getPropertyAreaCodes(propertyId);
  }

  @Post('areaCodes')
  @MaintenanceGuard()
  savePropertyAreaCode(@Body() data: AreaCodesDto) {
    return this.maintenanceService.savePropertyAreaCode(data);
  }
}
