import { MaintenanceDto, MaintenanceQueryDto, MaintenanceTaskResolvedDto, MaintenanceTaskDeletedDto, AreaCodesDto } from '../dto';
import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { MaintenanceTask } from '../types';
import { DynamoClient } from '../../common/aws';
import { generalConfig } from '../../config';

const housekeepingTable = generalConfig.aws.housekeepingDynamoTableName;

@Injectable()
export default class MaintenanceDataService {
  constructor(private readonly dynamoClient: DynamoClient) {}

  async getPropertyMaintenanceTasks(propertyId: string, resolved: boolean) {
    const result = await this.dynamoClient
      .query({
        TableName: housekeepingTable,
        IndexName: 'GSI1',
        KeyConditionExpression: 'GSI1pKey = :pKey and begins_with(GSI1sKey, :sKey)',
        ExpressionAttributeValues: {
          ':pKey': propertyId,
          ':sKey': `maintenance_${resolved ? 'resolved' : 'unresolved'}`,
        },
        ScanIndexForward: true,
        Limit: 50,
        ProjectionExpression: 'propertyId,propertyName,areaCode,description,dueDate,frequency,createdAt,resolved,pKey,sKey,GSI1sKey,GSI1pKey',
      })
      .promise();

    return this.formatMaintenanceTasks(result.Items);
  }

  async saveMaintenanceTask(userId: string, data: MaintenanceDto): Promise<void> {
    const now = new Date();
    const formattedDueDate = dateformat(data.dueDate, 'yyyy-mm-dd HH:MM:ss');
    const formattedDate = dateformat(now, 'yyyy-mm-dd HH:MM:ss');
    const dueDate = Math.floor(new Date(data.dueDate).getTime() / 1000);

    await this.dynamoClient.putItem(housekeepingTable, {
      ...data,
      pKey: data.propertyId,
      sKey: `maintenance_${data.propertyId}_${formattedDate}`,
      GSI1pKey: data.propertyId,
      GSI1sKey: `maintenance_unresolved_${formattedDueDate}`,
      resolved: false,
      createdAt: Math.floor(now.getTime() / 1000),
      userId,
      dueDate,
    });
  }

  async resolveMaintenanceTask(task: MaintenanceTaskResolvedDto): Promise<void> {
    const key = { pKey: task.propertyId, sKey: task.id };
    const values = { resolved: true, GSI1sKey: task.detailsId.replace('unresolved', 'resolved') };

    await this.dynamoClient.updateItem(housekeepingTable, key, values);
  }

  async deleteMaintenanceTask(data: MaintenanceTaskDeletedDto) {
    await this.dynamoClient
      .delete({
        TableName: housekeepingTable,
        Key: { pKey: data.propertyId, sKey: data.id },
      })
      .promise();
  }

  async savePropertyAreaCode(data: AreaCodesDto): Promise<void> {
    await this.dynamoClient
      .update({
        TableName: housekeepingTable,
        Key: {
          pKey: data.propertyId,
          sKey: 'area_codes',
        },
        UpdateExpression: 'add areaCodes :areaCodes',

        ExpressionAttributeValues: {
          ':areaCodes': this.dynamoClient.createSet([data.areaCode]),
        },

        ReturnValues: 'NONE',
      })
      .promise();
  }

  async getPropertyAreaCodes(propertyId: string): Promise<{ areaCodes: string[] }> {
    return await this.dynamoClient.getItem(housekeepingTable, { pKey: propertyId, sKey: 'area_codes' });
  }

  private formatMaintenanceTasks(items: any[]): MaintenanceTask[] {
    const tasks: MaintenanceTask[] = items.map(item => ({
      id: item.sKey,
      detailsId: item.GSI1sKey,
      userId: item.userId,
      createdAt: item.createdAt,
      description: item.description,
      propertyId: item.GSI1pKey,
      propertyName: item.propertyName,
      resolved: item.resolved,
      areaCode: item.areaCode,
      dueDate: item.dueDate,
      frequency: item.frequency,
    }));
    return tasks;
  }
}
