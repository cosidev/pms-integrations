import { Injectable } from '@nestjs/common';
import addDays from 'date-fns/addDays';
import { MaintenanceDto, MaintenanceQueryDto, MaintenanceTaskResolvedDto, MaintenanceTaskDeletedDto, AreaCodesDto } from '../dto';
import MaintenanceDataService from './maintenanceData.service';

@Injectable()
export default class MaintenanceService {
  constructor(private readonly dataService: MaintenanceDataService) {}

  async getPropertyMaintenanceTasks(data: MaintenanceQueryDto) {
    return this.dataService.getPropertyMaintenanceTasks(data.propertyId, data.resolved);
  }

  async saveMaintenanceTask(userId: string, data: MaintenanceDto) {
    return this.dataService.saveMaintenanceTask(userId, data);
  }

  async resolveMaintenanceTask(userId: string, task: MaintenanceTaskResolvedDto) {
    await this.dataService.resolveMaintenanceTask(task);

    if (task.frequency) {
      const newDueDate = addDays(new Date(), task.frequency);
      const newTask = { ...task, dueDate: newDueDate };
      await this.dataService.saveMaintenanceTask(userId, newTask);
    }
    return task;
  }

  async deleteMaintenanceTask(data: MaintenanceTaskDeletedDto) {
    await this.dataService.deleteMaintenanceTask(data);
  }

  async getPropertyAreaCodes(propertyId: string) {
    return this.dataService.getPropertyAreaCodes(propertyId);
  }

  async savePropertyAreaCode(data: AreaCodesDto) {
    return this.dataService.savePropertyAreaCode(data);
  }
}
