import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { HousekeepingController } from './housekeeping.controller';
import HousekeepingService from './HousekeepingService';
import WorkingHoursService from './WorkingHours.service';
import DamageReportService from './damageReport/damageReport.service';
import DamageReportDataService from './damageReport/damageReportData.service';
import LostFoundReportService from './lostFound/lostFoundReport.service';
import LostFoundReportDataService from './lostFound/lostFoundReportData.service';
import MaintenanceService from './maintenance/maintenance.service';
import MaintenanceDataService from './maintenance/maintenanceData.service';

@Module({
  imports: [CommonModule],
  controllers: [HousekeepingController],
  providers: [
    HousekeepingService,
    WorkingHoursService,
    DamageReportService,
    DamageReportDataService,
    LostFoundReportService,
    LostFoundReportDataService,
    MaintenanceService,
    MaintenanceDataService,
  ],
})
export class HousekeepingModule { }
