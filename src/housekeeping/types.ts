export interface Report {
  id: string;
  detailsId: string;
  userId: string;
  unitId?: string;
  areaCode: string;
  propertyId: string;
  propertyName: string;
  description: string;
  pictures: string[];
  createdAt: number;
  resolved: boolean;
}

export interface LostFoundReport extends Report {
  seqNumber: number;
}

export interface MaintenanceTask {
  id: string;
  detailsId: string;
  userId: string;
  areaCode: string;
  propertyId: string;
  propertyName: string;
  description: string;
  dueDate: number;
  frequency: number;
  createdAt: number;
  resolved: boolean;
}
