import sendgrid from '@sendgrid/mail';

const sgApiKey = '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';
sendgrid.setApiKey(sgApiKey);

export const templateIds = {
  de: {
    bookingConfirmation: 'd-3cac1079bad24a0490489467a6b90205',
    bookingCanceled: 'd-5f8c19da35ad43789b8954abbd02456a',
    checkInInfos: 'd-49bbc4435bcc4ef7b1202717c640af08',
    checkInDayInfos: 'd-010200a8cf5f493bbc84e44e93e0eb6a',
    checkOutInfos: 'd-280920eb577343a6832b189c38ab88b3',
    invoices: 'd-f12de5242b3c44c0b56a297e71d746ef',
    neededGuestData: 'd-82778d97c2c8465fad50b297c5c76af0',
    changeInvoiceAddress: 'd-26edb362664e47149d70ddf8eb94db20',
    breakfastSelection: 'd-ee15c135d79c457ab7bcd78ec7a5663f',
  },
  en: {
    bookingConfirmation: 'd-88034716d8c54347b7ca119fa5e8f7e1',
    bookingCanceled: 'd-3771a901f7d74b28b1c49feeab63bcc3',
    checkInInfos: 'd-c4b0345f55b14313b5a2265be2baea9f',
    checkInDayInfos: 'd-fd2ca7e29d6c41b2914e48f4bff5379a',
    checkOutInfos: 'd-280920eb577343a6832b189c38ab88b3',
    invoices: 'd-7ae0df589a8d4eb1b2ba7d7da8dedb78',
    neededGuestData: 'd-b5b17d75836b442bab3e5d77db673d48',
    changeInvoiceAddress: 'd-8b800d4983af445daf525120d592fed6',
    breakfastSelection: 'd-f7390ff8b2834f37be5ce79ba2db3c7c',
  },
};

export default sendgrid;
