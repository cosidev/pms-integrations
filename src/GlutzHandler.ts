import axios from 'axios';
import logger from './LoggingService';
import { generalConfig } from './config';
import { ApaleoReservation } from './types';

interface GlutzReservation {
  reservationId: string;
  name: string;
  unitIds: string[];
  from: string;
  to: string;
}

interface GlutzUser {
  reservationId?: string;
  backupUnitId?: string;
}

interface CodeConfig {
  id?: string;
  code?: number;
  description?: string;
  userId?: string;
  validFrom?: string;
  validTo?: string;
}

export default class GlutzHandler {
  async generateCode(reservation: GlutzReservation): Promise<number> {
    const reservationId = reservation.reservationId;
    logger.infoLog(`Generating merchants key code for reservation ${reservationId}`, { glutzReservation: reservation });
    try {
      const user = await this.createUser(`${reservation.name} [${reservationId}]`, { reservationId });
      for (const unitId of reservation.unitIds) {
        const ap = await this.getAccessPointId(unitId);
        await this.addUserToAccesspoint(ap, user);
      }
      // Generate code
      const code = this.getRandomCode();
      const codeResponse = await this.setCode({
        code,
        description: reservationId,
        userId: user,
        validFrom: reservation.from,
        validTo: reservation.to,
      });
      logger.infoLog(`Got code ${code} response for reservation ${reservationId}`, { codeResponse });
      return code;
    } catch (e) {
      logger.errorLog(`Error while generating merchants pin`, { glutzReservation: reservation });
      throw e;
    }
  }

  async removeCode(reservation: ApaleoReservation) {
    const reservationId = reservation.id;
    logger.infoLog(`Deleting merchants key code for reservation ${reservationId}`);
    try {
      const user = await this.getUser({ reservationId });
      await this.deleteUser(user);
      logger.infoLog(`Deleted merchants key code for reservation ${reservationId}`);
    } catch (e) {
      logger.errorLog(`Error while deleting merchants pin code (${reservationId})`, { errorMessage: e.message });
      throw e;
    }
  }

  async getBackupCode(unitId: string, entryUnitIds: string[], roomNumber: string, validTo?: string) {
    const logInfo = `${roomNumber} (${unitId})`;
    logger.infoLog(`Retrieving backup code for room ${logInfo}`);

    const user = await this.getBackupUser(unitId, entryUnitIds, roomNumber);
    const codes = (await this.getUserCodes(user)) || [];

    if (validTo) {
      logger.infoLog(`Invalidating current backup codes for room ${logInfo}`);
      await this.invalidateBackupCodes(codes, user, validTo);
      return this.createBackupCode(user);
    }

    const validBackupCode = codes.find(c => !c.validTo);
    if (validBackupCode) {
      logger.infoLog(`Found existing valid backup code for room ${logInfo}`);
      return validBackupCode.code;
    }

    return this.createBackupCode(user);
  }

  private async getBackupUser(unitId: string, entryUnitIds: string[], roomNumber: string): Promise<string> {
    try {
      const user = await this.getUser({ backupUnitId: unitId });
      return user;
    } catch (e) {
      logger.infoLog(`No backup user for room ${roomNumber} ${unitId} found`);
      const user = await this.createUser(`Backup ${roomNumber} (${unitId})`, { backupUnitId: unitId });
      const accessPointUpdates = [unitId, ...entryUnitIds].map(async unit => {
        const ap = await this.getAccessPointId(unit);
        return this.addUserToAccesspoint(ap, user);
      });
      await Promise.all(accessPointUpdates);
      return user;
    }
  }

  private async invalidateBackupCodes(codes: any[], user: string, validTo: string): Promise<void> {
    const codeUpdates = codes.map(c => {
      if (!c.validTo) {
        return this.setCode({
          id: c.id,
          userId: user,
          validTo,
        });
      }
    });
    await Promise.all(codeUpdates);
  }

  private async createBackupCode(user: string) {
    const code = this.getRandomCode();
    await this.setCode({
      code,
      userId: user,
      validTo: null,
    });
    return code;
  }

  private getRandomCode(): number {
    return Math.floor(10000 + Math.random() * 9000);
  }

  private rpcRequest = async (params, method) => {
    const body = {
      jsonrpc: '2.0',
      id: 1,
      params,
      method,
    };
    const auth = {
      username: 'cosi',
      password: generalConfig.glutzInterfacePass,
    };
    const conf = {
      auth,
      dataType: 'json',
      headers: { 'Content-Type': 'application/json-rpc' },
    };
    const { data } = await axios.post('https://pin.cosi-group.com/rpc', JSON.stringify(body), conf);
    if (!data || data.result === '0') {
      logger.debugLog('Invalid glutz server response', { glutzRequest: { params, method, data } });
      throw new Error('Invalid response');
    }
    return data.result;
  };

  private async createUser(label: string, user: GlutzUser) {
    return this.rpcRequest([{ label, ...user }], 'eAccess.createUser');
  }

  private setCode(codeConfig: CodeConfig) {
    return this.rpcRequest(['Codes', { actionProfileId: '2', ...codeConfig }], 'eAccess.setModel');
  }

  private getUserCodes(userId: string) {
    return this.rpcRequest(['Codes', { userId }, ['id', 'userId', 'code', 'validFrom', 'validTo']], 'eAccess.getModel');
  }

  private getAccessPointId(unitId) {
    return this.rpcRequest([{ unitId }], 'eAccess.getAccessPoint');
  }

  private addUserToAccesspoint(accessPointId, userId) {
    return this.rpcRequest([[accessPointId], userId], 'eAccess.addAccessPointsToUser');
  }

  private getUser(filter: GlutzUser) {
    return this.rpcRequest([filter], 'eAccess.getUser');
  }

  private deleteUser(id: string) {
    return this.rpcRequest([id], 'eAccess.deleteUser');
  }
}
