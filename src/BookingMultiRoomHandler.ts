import { ApaleoClient } from './apaleo/ApaleoClient';
import { ReservationModel } from './apaleo/generated/reservation';
import { GuestExperienceService } from './GuestExperienceService';

export class BookingMultiRoomHandler {
  constructor(
    private readonly apaleoClient: ApaleoClient,
    private readonly guestExperienceService: GuestExperienceService,
  ) { }

  async handleNewReservation(reservation: ReservationModel) {
    const booking = await this.apaleoClient.getBookingById(reservation.bookingId);
    const trigger = 'The multi-room booking from Booking.com did not provide the information on how to distribute the children to the rooms.';
    const isMultiRoom = booking.bookerComment ? booking.bookerComment.includes(trigger) : false;
    if (isMultiRoom) {
      await this.guestExperienceService.createGuestExperienceTask({
        sender: `Multiroom Booking ${reservation.id}`,
        subject: `Booking.com multi room booking`,
        message: `${reservation.id} has a multi-room booking. Please check children distribution`,
      });
    }

    return reservation;
  }
}
