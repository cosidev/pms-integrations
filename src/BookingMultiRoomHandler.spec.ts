import axios from 'axios';
import { Test, TestingModule } from '@nestjs/testing';
import { NEW_PRAGUE_RESERVATION } from './apaleo/__mocks__/prague.mock';
import { AppModule } from './app.module';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { GuestExperienceService } from './GuestExperienceService';
import { BookingMultiRoomHandler } from './BookingMultiRoomHandler';
import { BookingModel } from './apaleo/generated/reservation';

describe('BookingMultiRoomHandler', () => {
  let apaleoClient: ApaleoClient;
  let guestExperienceService: GuestExperienceService;
  let bookingMultiRoomHandler: BookingMultiRoomHandler;

  beforeAll(() => {
    jest.spyOn(axios, 'post')
      .mockImplementation(() => new Promise(resolve => resolve()));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
      ],
    }).compile();

    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    guestExperienceService = new GuestExperienceService();
    bookingMultiRoomHandler = new BookingMultiRoomHandler(apaleoClient, guestExperienceService);
  });

  afterAll(() => {
    jest.resetAllMocks();
  });

  it('should return the reservation immediately if booker comment doesn\'t contain multi-room trigger', async () => {
    jest.spyOn(apaleoClient, 'getBookingById')
      .mockImplementation(() => new Promise(resolve => resolve({
        bookerComment: '',
      } as BookingModel)));
    jest.spyOn(guestExperienceService, 'createGuestExperienceTask')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await bookingMultiRoomHandler.handleNewReservation(NEW_PRAGUE_RESERVATION);

    expect(apaleoClient.getBookingById).toHaveBeenCalledTimes(1);
    expect(guestExperienceService.createGuestExperienceTask).not.toHaveBeenCalled();
  });

  it('should createGuestExperienceTask if reservation is multi-room', async () => {
    jest.spyOn(apaleoClient, 'getBookingById')
      .mockImplementation(() => new Promise(resolve => resolve({
        bookerComment: 'The multi-room booking from Booking.com did not provide the information on how to distribute the children to the rooms.',
      } as BookingModel)));
    jest.spyOn(guestExperienceService, 'createGuestExperienceTask')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await bookingMultiRoomHandler.handleNewReservation(NEW_PRAGUE_RESERVATION);

    expect(guestExperienceService.createGuestExperienceTask).toHaveBeenCalled();
  });
});
