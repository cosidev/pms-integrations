import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { NEW_RESERVATION, OLD_RESERVATION, RESERVATION, PAST_TIME_SLICES, FUTURE_TIME_SLICES } from './apaleo/__mocks__';
import { AirBnBReservationUpdateHandler } from './AirBnBReservationUpdateHandler';
import { SlackService } from './SlackService';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { DynamoClient } from './common/aws/dynamoClient';
import { generalConfig } from './config';
import { ReservationModel } from './apaleo/generated/reservation';

jest.mock('./LoggingService.ts');
jest.mock('./SlackService.ts');

describe('AirBnBReservationUpdateHandler', () => {
  let apaleoClient: ApaleoClient;
  let dynamoClient: DynamoClient;
  let slack: SlackService;
  let airBnBReservationUpdateHandler: AirBnBReservationUpdateHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    dynamoClient = module.get<DynamoClient>(DynamoClient);
    slack = module.get<SlackService>(SlackService);
    airBnBReservationUpdateHandler = new AirBnBReservationUpdateHandler(dynamoClient, apaleoClient, slack);
  });

  describe('handleAmmendedReservation', () => {
    it('should immediately return the reservation if initialGrossAmount is equal to new', async () => {
      const DYNAMO_S_KEY = 'airbnb_commission';
      const dynamoFetchResult: any = {
        newGrossAmount: RESERVATION.totalGrossAmount.amount,
      };

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementation(() => new Promise(resolve => resolve()));

      const amendedReservation = await airBnBReservationUpdateHandler.handleAmmendedReservation(RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, { dataKey: RESERVATION.id, sKey: DYNAMO_S_KEY });

      expect(amendedReservation).toBe(RESERVATION);
      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(0);
    });

    it('should not call sendAmmendedGrossAmount if dynamodb response is empty', async () => {
      const DYNAMO_S_KEY = 'airbnb_commission';

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementation(() => new Promise(resolve => resolve()));

      const amendedReservation = await airBnBReservationUpdateHandler.handleAmmendedReservation(RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, { dataKey: RESERVATION.id, sKey: DYNAMO_S_KEY });

      expect(amendedReservation).toBe(RESERVATION);
      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(0);
    });

    it('should sendAmmendedGrossAmount to slack if initialGrossAmount is not equal to new', async () => {
      const dynamoFetchResult: any = {
        newGrossAmount: 100,
      };

      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(dynamoClient, 'updateItem').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(apaleoClient, 'getReservationById').mockImplementation(() => new Promise(resolve => resolve(NEW_RESERVATION)));
      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementation(() => new Promise(resolve => resolve()));

      await airBnBReservationUpdateHandler.handleAmmendedReservation(RESERVATION);

      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(1);
    });

    it('should trigger updatePriceOfFutureTimeslices if stay has been amended', async () => {
      const dynamoFetchResult: any = {
        newGrossAmount: 100,
      };

      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(dynamoClient, 'updateItem').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(apaleoClient, 'getReservationById').mockImplementation(() => new Promise(resolve => resolve(RESERVATION)));
      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementation(() => new Promise(resolve => resolve()));

      const amendedReservation = await airBnBReservationUpdateHandler.handleAmmendedReservation(RESERVATION);
      expect(amendedReservation.timeSlices).toEqual([...PAST_TIME_SLICES, ...FUTURE_TIME_SLICES]);
      expect(dynamoClient.updateItem).toHaveBeenCalledTimes(1);
      expect(apaleoClient.forceAmmendReservation).toHaveBeenCalledTimes(1);
      expect(apaleoClient.getReservationById).toHaveBeenCalledTimes(1);
    });

    it('should exit if there are no future timeslices', async () => {
      const dynamoFetchResult: any = {
        newGrossAmount: 100,
      };

      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(dynamoClient, 'updateItem').mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(apaleoClient, 'getReservationById').mockImplementation(() => new Promise(resolve => resolve(RESERVATION)));
      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementation(() => new Promise(resolve => resolve()));

      RESERVATION.timeSlices = RESERVATION.timeSlices.filter(timeSlice => !timeSlice.actions[0].isAllowed);

      await airBnBReservationUpdateHandler.handleAmmendedReservation(RESERVATION);
      expect(dynamoClient.updateItem).not.toHaveBeenCalled();
      expect(apaleoClient.forceAmmendReservation).not.toHaveBeenCalled();
      expect(apaleoClient.getReservationById).not.toHaveBeenCalled();
    });

    it('should exit if not Airbnb booking', async () => {
      jest.spyOn(dynamoClient, 'getItem');

      const reservation = {
        ...RESERVATION,
        source: 'Booking.com',
      } as ReservationModel;
      await airBnBReservationUpdateHandler.handleAmmendedReservation(reservation);

      expect(dynamoClient.getItem).not.toHaveBeenCalled();
      expect(slack.sendAmmendedGrossAmount).not.toHaveBeenCalled();
    });
  });

  describe('handleNewReservation', () => {
    it('should exit if not Airbnb booking', async () => {
      jest.spyOn(dynamoClient, 'getItem');

      const reservation = {
        ...RESERVATION,
        source: 'Booking.com',
      } as ReservationModel;
      await airBnBReservationUpdateHandler.handleNewReservation(reservation);

      expect(dynamoClient.getItem).not.toHaveBeenCalled();
      expect(slack.sendAmmendedGrossAmount).not.toHaveBeenCalled();
    });

    it('should exit if reservation exists', async () => {
      const DYNAMO_S_KEY = 'airbnb_commission';
      const dynamoFetchResult: any = {
        newGrossAmount: 100,
      };

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));

      const amendedReservation = await airBnBReservationUpdateHandler.handleNewReservation(RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, { dataKey: NEW_RESERVATION.id, sKey: DYNAMO_S_KEY });

      expect(amendedReservation).toBe(RESERVATION);
      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(0);
    });

    it('should successfully amend a new reservation', async () => {
      const DYNAMO_S_KEY = 'airbnb_commission';
      const dynamoFetchResult: any = null;

      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'getReservationById').mockImplementation(() => new Promise(resolve => resolve(NEW_RESERVATION)));

      jest.spyOn(dynamoClient, 'getItem').mockImplementation(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(dynamoClient, 'updateItem').mockImplementation(() => new Promise(resolve => resolve()));

      await airBnBReservationUpdateHandler.handleNewReservation(OLD_RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, { dataKey: OLD_RESERVATION.id, sKey: DYNAMO_S_KEY });

      expect(dynamoClient.updateItem).toHaveBeenCalledWith(
        generalConfig.aws.dataDynamoTableName,
        { dataKey: OLD_RESERVATION.id, sKey: DYNAMO_S_KEY },
        { commission: 20, newGrossAmount: 158, oldGrossAmount: 138 },
      );
    });
  });
});
