import { Pool } from 'pg';
import { ApaleoClient } from './apaleo/ApaleoClient';
import logger from './LoggingService';
import { generalConfig } from './config';
import { ApaleoReservation } from './types';

interface CheckOutButtonEvent {
  id?: string;
  reservationId: string;
  unitId: string;
  unitGroupId: string;
  timestamp: Date;
}

export default class DashButtonExportHandler {

  apaleoClient: ApaleoClient;
  database: Pool;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.database = new Pool(generalConfig.exportDatabase);
  }

  async exportDashButtonCheckOut(reservation: ApaleoReservation) {
    const insert = `INSERT INTO checkout_button_events (unit_id, unit_group_id, timestamp, reservation_id)
    VALUES ($1, $2, $3, $4) RETURNING id`;
    try {
      await this.database.connect();
      const values = [
        reservation.unit.id,
        reservation.unitGroup.id,
        new Date(),
        reservation.id,
      ];
      await this.database.query<CheckOutButtonEvent>(insert, values);
    } catch (e) {
      logger.errorLog(`Could not insert dash button checkout`, { error: e, errorMessage: e.message, errorTrace: e.stack });
    }
  }
}
