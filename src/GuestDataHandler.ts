import { format } from 'date-fns';
import { ApaleoClient } from './apaleo/ApaleoClient';
import JWTService from './JWTService';
import logger from './LoggingService';
import ReservationService from './ReservationService';
import {
  GuestDataRequest,
  GuestDataType,
  PayloadData,
  Guest,
  GuestDataAddData,
  AddInvoiceAddress,
  SpecialRequestData,
  BreakfastSelection,
  CmsProperty,
} from './types';
import { Operation } from './apaleo/generated/finance';
import { generalConfig } from './config';
import { ReservationModel } from './apaleo/generated/reservation';
import { ReservationDetailsService } from './common/reservation/reservationDetails.service';
import { PropertyDetailsService } from './common/propertyDetails.service';
import { DynamoClient } from './common/aws';
import { ReservationRefundDetailsService } from './common/reservation/reservationRefundDetails.service';
import { OverbookingsService } from './overbookings/overbookings.service';
import { pragueBreakfastProperties } from './util/properties';

const CZECK_PROPERTIES = ['CZ_PR_002', 'CZ_PR_003', 'CZ_PR_004'];
const GERMAN_PROPERTIES = ['DE_BE_001', 'DE_BE_002', 'DE_LE_001', 'DE_MU_001', 'DE_B_PR_01'];

export default class GuestDataHandler {
  private jwtService: JWTService;
  private apaleoClient: ApaleoClient;
  private reservationService: ReservationService;
  private reservationDetailsService: ReservationDetailsService;
  private propertyDetailsService: PropertyDetailsService;
  private overbookingsService: OverbookingsService;

  constructor() {
    this.jwtService = new JWTService();
    this.apaleoClient = new ApaleoClient();
    this.reservationService = new ReservationService();
    const dynamo = new DynamoClient();
    const refundService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundService);
    this.propertyDetailsService = new PropertyDetailsService(dynamo);
    this.overbookingsService = new OverbookingsService(this.apaleoClient);
  }

  async handleRequest(request: GuestDataRequest) {
    try {
      let payload: PayloadData;

      switch (request.type) {
        case GuestDataType.GetGuestData:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.getGetGuestData(payload.reservationId);
        case GuestDataType.AddGuestData:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.addGuestData(payload.reservationId, request.data);
        case GuestDataType.AddSpecialRequest:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.addSpecialRequest(payload.reservationId, request.data);
        case GuestDataType.AddInvoiceAddress:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.addInvoiceAddress(payload.reservationId, payload.bookingId, request.data);
        case GuestDataType.GetLastValidInvoice:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.apaleoClient.getLastValidInvoicePDF(request.data?.invoiceId);
        case GuestDataType.AddPreferredLanguage:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.addPreferredLanguage(payload.bookingId, request.data);
        case GuestDataType.CancelTrip:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          return this.cancelReservation(payload.reservationId);
        case GuestDataType.SaveBreakfastSelection:
          payload = this.jwtService.verifyToken(request.key) as PayloadData;
          const { selectedBreakfast } = request.data;
          return this.saveBreakfastData(payload.reservationId, selectedBreakfast);
        case GuestDataType.CheckInData:
          return this.getCheckInData(request.key, new Date(request.day));
      }
    } catch (e) {
      logger.errorLog('Could not handle guest data request', { e, guestDataRequest: request });
      throw e;
    }
  }

  private async getCheckInData(key: string, date: Date) {
    if (key !== generalConfig.pragueReceptionApiKey) {
      throw new Error('Invalid api key');
    }
    const propertyIds = ['CZ_PR_002'];
    const expand = ['assignedUnits'];
    // tslint:disable-next-line: max-line-length
    const { reservations = [] } = await this.apaleoClient.getReservationsByDay(date, ['Confirmed', 'InHouse'], 'Arrival', { propertyIds, expand });
    const data = [];
    for (const reservation of reservations) {
      const booking = await this.apaleoClient.getBookingById(reservation.bookingId);
      const details = await this.reservationDetailsService.getDetails(reservation.id);
      const token = this.jwtService.getToken({ reservationId: reservation.id, bookingId: reservation.bookingId });
      try {
        data.push({
          reservationId: reservation.id,
          name: `${reservation.primaryGuest.firstName || ''} ${reservation.primaryGuest.lastName || ''}`,
          arrival: reservation.arrival,
          departure: reservation.departure,
          balance: reservation.balance,
          additionalInfo: booking.bookerComment || '',
          room: this.reservationService.getFormattedRoomName(reservation.assignedUnits[0].unit.name),
          guestDataUrl: `https://guests.cosi-prague.com/guest-legal-info?key=${token}`,
          paymentUrl: `https://guests.cosi-prague.com/guest-payment?key=${token}`,
          pinComfortCode: details.pinCodes.pinComfortCode,
        });
      } catch (e) {
        logger.errorLog(`Could not format check in entry for reservation ${reservation.id}`, {
          checkInReservation: reservation,
          errorMessage: e.message,
          e,
        });
      }
    }
    return { data };
  }

  private async getGetGuestData(reservationId: string) {
    const expand = ['booker', 'actions'];
    const reservation = await this.apaleoClient.getReservationById(reservationId, expand);
    const [cmsProperty, rateplan, lastValidInvoice, details] = await Promise.all([
      this.propertyDetailsService.getDetails(reservation.property.id),
      this.apaleoClient.getRateplanById(reservation.ratePlan.id),
      this.apaleoClient.getLastValidInvoice(reservationId),
      this.reservationDetailsService.getDetails(reservationId),
    ]);

    const bookerAddress = reservation.booker.address;
    const invoiceAddress = {
      name: reservation.booker.firstName || '',
      surname: reservation.booker.lastName || '',
      companyName: reservation.booker.company ? reservation.booker.company.name : '',
      taxId: reservation.booker.company ? reservation.booker.company.taxId : '',
      address: bookerAddress?.addressLine1 ? bookerAddress.addressLine1 : '',
      postalCode: bookerAddress?.postalCode ? bookerAddress.postalCode : '',
      city: bookerAddress?.city ? bookerAddress.city : '',
      country: bookerAddress?.countryCode ? bookerAddress.countryCode : '',
    };

    const children = reservation.childrenAges ? reservation.childrenAges.length : 0;
    const totalGuests = reservation.adults + children;

    const { additionalGuests = [], primaryGuest = [] }: any = reservation;
    additionalGuests.unshift(primaryGuest);

    const guests: Guest[] = [...new Array(totalGuests)].map((_, i) => {
      const { firstName, lastName, identificationNumber, company, nationalityCountryCode, address, birthDate, email, phone } =
        additionalGuests[i] || {};

      return {
        surname: lastName || '',
        name: firstName || '',
        birthday: birthDate || null,
        citizenship: nationalityCountryCode || '',
        passportNumber: identificationNumber || '',
        visaNumber: company ? company.taxId : '',
        address: address ? address.addressLine1 : '',
        email: email || '',
        phone: phone || '',
        note: undefined,
      };
    });
    const propertyName = reservation.property.name || '';
    const language = this.reservationService.getPreferredLanguage(reservation);
    const cancellationStatus = this.reservationService.getCancellationStatus(reservation);

    const breakfastServiceIds = ['CZ_PR_002-BFS', 'CZ_PR_002-BFS1', 'CZ_PR_003-BFS15', 'CZ_PR_003-BFS', 'CZ_PR_004-BFS15', 'CZ_PR_004-BFS'];
    const hasBreakfast: boolean = rateplan.includedServices ? rateplan.includedServices.some(x => breakfastServiceIds.includes(x.serviceId)) : false;

    const balance = { ...reservation.balance };
    if (reservation.guaranteeType === 'CreditCard') {
      balance.amount = 0;
    }

    return {
      bookedOn: format(new Date(reservation.created), 'dd/MM/y, H:mm'),
      checkIn: format(new Date(reservation.arrival), 'dd/MM/y'),
      checkOut: format(new Date(reservation.departure), 'dd/MM/y'),
      roomType: reservation.unitGroup ? reservation.unitGroup.name : '',
      roomName: reservation.unit ? this.reservationService.getFormattedRoomName(reservation.unit.name) : undefined,
      cancellationFee: reservation.cancellationFee,
      cancellationTime: reservation.cancellationTime && format(new Date(reservation.cancellationTime), 'dd/MM/y, H:mm'),
      guarenteeType: reservation.guaranteeType,
      paymentAccount: reservation.paymentAccount,
      ratePlan: reservation.ratePlan,
      channelName: reservation.channelCode === 'ChannelManager' ? reservation.source : reservation.channelCode,
      source: reservation.source,
      status: reservation.status,
      cancellationStatus,
      pinComfortCode: details.pinCodes.pinComfortCode,
      pinAccessCode: details.pinCodes.pinAccessCode,
      specialWishes: details.specialWishes,
      availableSpecialWishes: cmsProperty.availableServices,
      availableModules: this.availableModules(reservation, cmsProperty, hasBreakfast),
      hasBreakfast,
      breakfastSelection: details.breakfastSelection,
      parkingInfo: cmsProperty.parking,
      breakfastRecommendations: cmsProperty.breakfastRecommendations,
      termsUrl: cmsProperty.bookingPage + '/agb',
      balance,
      reservationId: reservation.id,
      totalGuests,
      travelPurpose: reservation.travelPurpose,
      from: reservation.arrival,
      to: reservation.departure,
      guests,
      invoiceAddress,
      invoice: lastValidInvoice,
      propertyName,
      isGermanProperty: GERMAN_PROPERTIES.includes(reservation.property.id),
      isCzeckProperty: CZECK_PROPERTIES.includes(reservation.property.id),
      language,
    };
  }

  private async addSpecialRequest(reservationId: string, data: SpecialRequestData) {
    const specialWishes = data.specialWishes;
    try {
      await this.reservationDetailsService.updateDetails(reservationId, { specialWishes });
      logger.infoLog(`Saved special requests for reservation ${reservationId}`);
      return { success: true };
    } catch (e) {
      logger.errorLog(`Could not save special request for CMS res ${reservationId}`, { e });
      throw e;
    }
  }

  private async addGuestData(reservationId: string, data: GuestDataAddData) {
    const request: Operation[] = [];
    const primaryGuest = data.guests.shift();
    logger.infoLog(`Saving guest data for reservation ${reservationId}`, { guestData: data });
    let success = false;
    try {
      request.push({
        from: '',
        op: 'replace',
        path: '/travelPurpose',
        value: data.travelPurpose as any,
      });
      request.push({
        from: '',
        op: 'replace',
        path: '/primaryGuest',
        value: this.formatGuestObj(primaryGuest),
      });
      // Additional guests
      const additionalGuests = [];
      data.guests.forEach(guest => {
        if (guest.surname) {
          additionalGuests.push(this.formatGuestObj(guest));
        }
      });
      request.push({
        from: '',
        op: 'add',
        path: '/additionalGuests',
        value: additionalGuests,
      });

      await this.apaleoClient.updateReservationbyId(reservationId, request);
      logger.infoLog(`Saved guest data for reservation ${reservationId}`, { guestDataRequest: request });
      success = true;
    } catch (e) {
      logger.errorLog(`Could not save guest data ${reservationId}`, { e, data, request });
      throw e;
    }
    return { success };
  }

  private async addInvoiceAddress(reservationId: string, bookingId: string, data: { address: AddInvoiceAddress; folioId?: string }) {
    const value = {
      firstName: data.address?.name,
      lastName: data.address?.surname,
      company: {
        name: data.address?.companyName,
        taxId: data.address?.taxId,
      },
      address: {
        addressLine1: data.address?.address,
        postalCode: data.address?.postalCode,
        city: data.address?.city,
        countryCode: data.address?.country,
      },
    };
    let response = { success: false, invoice: null };

    try {
      // Invoice already has been created so correct its address
      if (data.folioId) {
        const op: Operation[] = [
          {
            from: '',
            op: 'replace',
            path: 'debitor',
            value,
          },
        ];
        await this.apaleoClient.updateFolioById(data.folioId, op);
        const newLastValidInvoice = await this.apaleoClient.getLastValidInvoice(reservationId);

        response = { ...response, invoice: newLastValidInvoice };
        logger.infoLog(`Corrected address for folio ${data.folioId}`, { invoiceDataRequest: value, folioId: data.folioId });
      }

      // Update booker's address no matter if an invoice has been created or not
      await this.apaleoClient.updateBookingById(bookingId, '/booker', value);
      logger.infoLog(`Updated booker's address for booking ${bookingId}`, { invoiceDataRequest: value });
      response.success = true;
    } catch (e) {
      logger.errorLog(`Could not save invoice address ${bookingId}`, { e, data });
      throw e;
    }
    return response;
  }

  private async addPreferredLanguage(bookingId: string, preferredLanguage: string) {
    let success = false;
    try {
      await this.apaleoClient.updateBookingById(bookingId, '/booker/preferredLanguage', preferredLanguage);
      logger.infoLog(`Saved preferred language for ${bookingId}`, { addPreferredLanguage: preferredLanguage });
      success = true;
    } catch (e) {
      logger.errorLog(`Could not save preferred language for ${bookingId}`, { e, preferredLanguage });
      throw e;
    }
    return { success };
  }

  private async cancelReservation(reservationId: string) {
    let success = false;
    const guestData = await this.getGetGuestData(reservationId);
    if (!['Direct', 'Ibe'].includes(guestData.channelName)) {
      return { success };
    } else {
      try {
        await this.apaleoClient.cancelReservation(reservationId);
        await this.overbookingsService.processCancelledReservation(reservationId);
        logger.infoLog(`Cancelled trip for ${reservationId}`);
        success = true;
      } catch (e) {
        logger.errorLog(`Could not cancel trip for ${reservationId}`, { e });
        throw e;
      }
      return { success, guestData };
    }
  }

  private async saveBreakfastData(reservationId: string, breakfastSelection: BreakfastSelection[]) {
    try {
      await this.reservationDetailsService.updateDetails(reservationId, { breakfastSelection });
      return { success: true };
    } catch (e) {
      logger.errorLog(`Could not save breakfast data for reservation ${reservationId}`, { e });
      throw e;
    }
  }

  private formatBirthday(date: Date | string) {
    return date ? format(new Date(date), 'yyyy-MM-dd') : null;
  }

  private formatGuestObj(guest: Guest) {
    return {
      lastName: guest.surname,
      firstName: guest.name,
      birthDate: this.formatBirthday(guest.birthday),
      identificationNumber: guest.passportNumber,
      nationalityCountryCode: guest.citizenship,
      company: {
        taxId: guest.visaNumber,
      },
      address: {
        addressLine1: guest.address,
      },
      email: guest.email,
      phone: guest.phone,
    };
  }

  private availableModules(reservation: ReservationModel, cmsProperty: CmsProperty, hasBreakfast: boolean) {
    const modules = ['home', 'personal-info'];

    const isProd = generalConfig.prod;

    const propertyId = reservation.property?.id;

    const canceled = reservation.status === 'Canceled';

    const isCzeck = CZECK_PROPERTIES.includes(propertyId);
    const isGermany = GERMAN_PROPERTIES.includes(propertyId);

    const hasAdyenMerchantAccount = (generalConfig.adyen.merchantAccount[propertyId] as boolean) && !canceled;

    if (isCzeck || isGermany || !isProd) {
      modules.push('guest-legal-info');
    }

    if ((pragueBreakfastProperties && hasBreakfast) || !isProd) {
      modules.push('breakfast');
    }

    if (hasAdyenMerchantAccount || !isProd) {
      modules.push('guest-payment');
    }

    if (cmsProperty.availableServices?.length > 0 || !isProd) {
      modules.push('special-request');
    }

    if ((cmsProperty.breakfastRecommendations && !hasBreakfast) || !isProd) {
      modules.push('breakfast-recommendations');
    }
    if (cmsProperty.parking || !isProd) {
      modules.push('parking-info');
    }
    if (!canceled || !isProd) {
      modules.push('invoice');
    }

    return modules;
  }
}
