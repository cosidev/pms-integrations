import { Client, Config, CheckoutAPI } from '@adyen/api-library';
import axios from 'axios';
import { generalConfig } from './config';
import { DynamoClient } from './DynamoClient';

export default class AdyenClient {
  client: Client;
  checkOutApi: CheckoutAPI;
  dynamoClient: DynamoClient;

  constructor() {
    const config = new Config();
    config.apiKey = generalConfig.adyen.apiKey;
    config.checkoutEndpoint = generalConfig.adyen.checkoutURL;

    this.client = new Client({
      config,
      httpClient: {
        async request(endpoint, json, config) {
          const response = await axios({
            method: 'POST',
            url: endpoint,
            data: JSON.parse(json),
            headers: {
              'X-API-Key': config.apiKey,
              'Content-type': 'application/json',
            },
          });
          return response.data;
        },
        async post(endpoint, json, config) {
          const response = await axios({
            method: 'POST',
            url: endpoint,
            data: json,
            headers: {
              'X-API-Key': config.apiKey,
              'Content-type': 'application/json',
            },
          });
          return response.data;
        },
      },
    });
    this.client.setEnvironment(generalConfig.prod ? 'LIVE' : 'TEST', '122cca5de27a5380-ApaleoGmbH');
    this.checkOutApi = new CheckoutAPI(this.client);
    this.dynamoClient = new DynamoClient();
  }

  paymentMethods(request: ICheckout.PaymentMethodsRequest) {
    return this.checkOutApi.paymentMethods(request);
  }

  payment(request: ICheckout.PaymentRequest) {
    return this.checkOutApi.payments(request);
  }

  paymentsDetails(request: ICheckout.DetailsRequest) {
    return this.checkOutApi.paymentsDetails(request);
  }

  async originKey(host: string): Promise<string> {
    const originKeys = await this.dynamoClient.getItem('adyenData', 'originKeys');

    if (originKeys.Item && originKeys.Item.value && originKeys.Item.value[host]) {
      return originKeys.Item.value[host];
    } else {
      const { data: newOriginKeyData } = await axios({
        method: 'POST',
        url: this.client.config.checkoutEndpoint + '/originKeys',
        data: { originDomains: [host] },
        headers: {
          'X-API-Key': this.client.config.apiKey,
          'Content-type': 'application/json',
        },
      });
      const newOriginKeys =
        originKeys.Item && newOriginKeyData
          ? { ...newOriginKeyData.originKeys, ...originKeys.Item.value }
          : newOriginKeyData
          ? { ...newOriginKeyData.originKeys }
          : originKeys.Item
          ? originKeys.Item.Value
          : {};

      await this.dynamoClient.addItem('adyenData', 'originKeys', { value: newOriginKeys });

      return newOriginKeys[host];
    }
  }
}
