import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { ApaleoClient } from './apaleo/ApaleoClient';
import CmsClient from './CmsClient';
import ReservationService from './ReservationService';
import SagKeyhandler from './SagKeyHandler';
import GlutzHandler from './GlutzHandler';
import { ApaleoReservation } from './types';
import logger from './LoggingService';
import { MunicoPinCodeHandler } from './MunicoPinCodeHandler';
import { ReservationDetails } from './common/types/reservation';
import { ReservationDetailsService } from './common/reservation/reservationDetails.service';
import { DynamoClient as Dynamo } from './common/aws';
import { DynamoClient } from './DynamoClient';
import { ReservationRefundDetailsService } from './common/reservation/reservationRefundDetails.service';
import { glutzProperties } from './util/properties';
import { ReservationEventLogger, EnumReservationEvents } from './ReservationEventLogger';
import RemoteLockService from './keySystem/remoteLock/remoteLock.service';
import RemoteLockApiService from './keySystem/remoteLock/remoteLockApi.service';

@Injectable()
export class PinCodeHandler {
  apaleoClient: ApaleoClient;
  cmsClient: CmsClient;
  reservationService: ReservationService;
  reservationDetailsService: ReservationDetailsService;
  sagHandler: SagKeyhandler;
  glutzHandler: GlutzHandler;
  municoHandler: MunicoPinCodeHandler;
  remoteLockService: RemoteLockService;
  dynamoClient: DynamoClient;
  reservationEventLogger: ReservationEventLogger;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.cmsClient = new CmsClient();
    this.reservationService = new ReservationService();
    this.dynamoClient = new DynamoClient();
    const dynamo = new Dynamo();
    const refundDetailsService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundDetailsService);
    this.sagHandler = new SagKeyhandler();
    this.glutzHandler = new GlutzHandler();
    this.municoHandler = new MunicoPinCodeHandler();
    this.reservationEventLogger = new ReservationEventLogger(dynamo);
    this.remoteLockService = new RemoteLockService(new RemoteLockApiService());
  }

  async generateCodes(reservation: ApaleoReservation, resDetails: ReservationDetails) {
    try {
      const unit = await this.apaleoClient.assignRoomToReservation(reservation.id);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.UnitAssigned,
        reservationId: reservation.id,
        message: 'Unit assigned to reservation',
        additionalData: {
          rooms: unit,
        },
      });
      const room = this.reservationService.getFormattedRoomName(unit.timeSlices[0].unit.name);
      const unitId = unit.timeSlices[0].unit.id;
      const guestName = `${reservation.primaryGuest.firstName} ${reservation.primaryGuest.lastName}`;
      const d = d => dateformat(d, 'dd.mm.yyyy');
      logger.infoLog(`Generating codes for room ${room} => ${d(reservation.arrival)} - ${d(reservation.departure)} (${reservation.id})`);
      if (!resDetails.pinCodes.pinComfortCode) {
        let pinCodes = {};
        if (reservation.property.id === 'DE_BE_001') {
          pinCodes = await this.getSagUpdateValues(reservation, room, guestName);
        } else if (reservation.property.id === 'DE_MU_001') {
          pinCodes = await this.getMunicoUpdateValues(reservation, unitId);
        } else if (reservation.property.id === 'AT_VI_001') {
          pinCodes = await this.getStraussUpdateValues(reservation, room);
        } else {
          pinCodes = await this.getGlutzUpdateValues(reservation, unitId, guestName, room);
        }
        logger.infoLog(`Updating pin codes for room ${room} ${reservation.id}`, { pinCodes });
        resDetails.pinCodes = pinCodes;
        await this.reservationDetailsService.updateDetails(reservation.id, { pinCodes });
        await this.reservationEventLogger.logEvent({
          event: EnumReservationEvents.PinGenerated,
          reservationId: reservation.id,
          message: `Generated pin codes for room ${room}`,
          additionalData: {
            pinCodes,
          },
        });
        return resDetails;
      }
    } catch (e) {
      logger.errorLog(`Could not generate keys for reservation ${reservation.id} ${reservation.property.id}`, { errorMessage: e.message });
      throw e;
    }
  }

  private async getStraussUpdateValues(reservation: ApaleoReservation, room: string) {
    const pinComfortCode = await this.remoteLockService.generateCode({
      arrival: dateformat(reservation.arrival, 'yyyy-mm-dd'),
      departure: dateformat(reservation.departure, 'yyyy-mm-dd'),
      id: reservation.id,
      unitName: room,
    });
    return { pinComfortCode };
  }

  private async getMunicoUpdateValues(reservation: ApaleoReservation, unitId: string) {
    const pinComfortCode = await this.municoHandler.generate(reservation, unitId);
    return { pinComfortCode };
  }

  private async getSagUpdateValues(reservation: ApaleoReservation, room: string, guestName: string) {
    const reservationNumber = this.reservationService.getReservationNumber(reservation.id);
    let roomNumber = room;
    if (roomNumber === '098' || roomNumber === '099') {
      roomNumber = roomNumber.substr(1, roomNumber.length);
    }
    // tslint:disable-next-line: max-line-length
    const { codes, extendedCodes } = await this.sagHandler.generate({
      roomNumber,
      name: guestName,
      arrival: reservation.arrival,
      departure: reservation.departure,
      reservationNumber,
    });
    const updateValues = {
      pinAccessCode: codes.pinAccessCode,
      pinComfortCode: codes.pinComfortCode,
      mainDoorCode: extendedCodes.pinComfortCode,
      earlyCheckInCode: extendedCodes.pinAccessCode,
    };
    return updateValues;
  }

  async removeCode(reservation: ApaleoReservation) {
    let removed = false;
    if (reservation.property.id === 'DE_BE_001') {
      // Disabled
      await this.removeSagCode(reservation);
      removed = true;
    } else if (reservation.property.id === 'DE_MU_001') {
      // Nothing to remove
      removed = true;
    } else if (reservation.property.id === 'AT_VI_001') {
      // Nothing to remove
      await this.remoteLockService.removeCode(reservation.id);
      removed = true;
    } else if (glutzProperties.includes(reservation.property.id)) {
      await this.removeGlutzCode(reservation);
      removed = true;
    }

    if (removed) {
      await this.reservationDetailsService.updateDetails(reservation.id, { pinCodes: {} });
    }
  }

  private async getGlutzUpdateValues(reservation: ApaleoReservation, unitId: string, guestName: string, room: string) {
    let codeConfig;
    const propertyId = reservation.property.id;
    if (['CZ_PR_002', 'CZ_PR_003', 'CZ_PR_004'].includes(propertyId)) {
      codeConfig = {
        reservationId: reservation.id,
        name: guestName,
        unitIds: [unitId, ...this.getEntryUnitIds(room, propertyId)],
        from: dateformat(reservation.arrival, "yyyy-mm-dd'T'HH:MM:00"),
        to: dateformat(reservation.departure, "yyyy-mm-dd'T'12:MM:00"),
      };
    }
    if (reservation.property.id === 'DE_BE_002') {
      codeConfig = {
        reservationId: reservation.id,
        name: guestName,
        unitIds: [unitId, ...this.getEntryUnitIds(room, propertyId)],
        from: dateformat(reservation.arrival, "yyyy-mm-dd'T'HH:MM:00"),
        to: dateformat(reservation.departure, "yyyy-mm-dd'T'11:MM:00"),
      };
    }
    const pinComfortCode = await this.glutzHandler.generateCode(codeConfig);
    return { pinComfortCode };
  }

  async getBackupCode(unitId: string, propertyId: string) {
    if (!glutzProperties.includes(propertyId)) {
      return undefined;
    }
    const response = await this.dynamoClient.getItem(unitId, 'glutz_backup_code');
    if (response.Item) {
      return response.Item.backupCode;
    }

    const unit = await this.apaleoClient.getUnitById(unitId);
    const room = this.reservationService.getFormattedRoomName(unit.name);
    const entryUnits = this.getEntryUnitIds(room, propertyId);
    const backupCode = await this.glutzHandler.getBackupCode(unitId, entryUnits, room);
    await this.dynamoClient.addItem(unitId, 'glutz_backup_code', { backupCode });
    return backupCode;
  }

  async invalidateBackupCode(unitId: string, propertyId: string, validTo: Date) {
    if (!glutzProperties.includes(propertyId)) {
      return undefined;
    }

    const unit = await this.apaleoClient.getUnitById(unitId);
    const room = this.reservationService.getFormattedRoomName(unit.name);
    const entryUnits = this.getEntryUnitIds(room, propertyId);
    const invalidationDate = dateformat(validTo, "yyyy-mm-dd'T'HH:MM:00");
    const backupCode = await this.glutzHandler.getBackupCode(unitId, entryUnits, room, invalidationDate);
    await this.dynamoClient.addItem(unitId, 'glutz_backup_code', { backupCode });
    return backupCode;
  }

  private getEntryUnitIds(room: string, propertyId: string) {
    if (!glutzProperties.includes(propertyId)) {
      return [];
    }

    if (['CZ_PR_002', 'CZ_PR_003', 'CZ_PR_004'].includes(propertyId)) {
      return [`${propertyId}-ENTRY`];
    }

    if (propertyId === 'DE_BE_002') {
      const entryUnitId = room.substr(0, 2) === '46' ? 'DE_BE_002-ENTRY46' : 'DE_BE_002-ENTRY47';
      return [entryUnitId];
    }

    return [];
  }

  async removeSagCode(reservation: ApaleoReservation) {
    const params = {
      reservationNumber: this.reservationService.getReservationNumber(reservation.id),
      roomNumber: this.reservationService.getFormattedRoomName(reservation.unit.name),
    };
    logger.infoLog('Sag code deletion params', { sagDeletionParams: params });
    return this.sagHandler.remove(params);
  }

  async removeGlutzCode(reservation: ApaleoReservation) {
    return this.glutzHandler.removeCode(reservation);
  }
}
