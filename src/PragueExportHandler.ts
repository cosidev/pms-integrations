import dateformat from 'dateformat';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { SlackService } from './SlackService';
import { EnumReservationItemModelTravelPurpose, GuestModel, ReservationItemModel, PersonAddressModel } from './apaleo/generated/reservation';
import { getAlpha3CountryCode } from './util/countries';
import logger from './LoggingService';

export class PragueExportHandler {

  apaleoClient: ApaleoClient;
  slack: SlackService;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.slack = new SlackService();
  }

  async exportGuestData() {
    const properties = [
      { propertyId: 'CZ_PR_002', ubyportId: '100107775245' },
      { propertyId: 'CZ_PR_003', ubyportId: '100307775245' },
      { propertyId: 'CZ_PR_004', ubyportId: '100207775245' },
    ];
    await Promise.all(properties.map(property => this.createAndSendExport(property.propertyId, property.ubyportId)));
  }

  async createAndSendExport(propertyId: string, ubyportId: string) {
    const today = new Date();
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    yesterday.setUTCHours(23, 59);

    const startDate = new Date();
    // Go back 5 days on wednesday and 2 days on friday
    const prevDays = startDate.getDay() === 3 ? 5 : 2;
    startDate.setDate(today.getDate() - prevDays);
    startDate.setUTCHours(0, 0);
    // tslint:disable-next-line: max-line-length
    const { reservations = [] } = await this.apaleoClient.getReservationsbyTime(['InHouse', 'CheckedOut', 'Confirmed'], 'Arrival', startDate, yesterday, { propertyIds: [propertyId] });
    let exportData = `A|2|${ubyportId}|ZVTTK|COSI PRAGUE MALA STRANA, S.R.O.|+420 228 880 181|Praha|Praha 1|Mala Strana|Mostecka|273|21|11800|${dateformat(new Date(), 'yyyy.mm.dd HH:mm:ss')}||\r\n`;
    const format = d => dateformat(d, 'dd.mm.yyyy');
    const addressField = (x: string, add?: string) => x ? `${x.replace(/\r?\n|\r/g, ' ')}${add || ''} ` : '';
    const formatAddress = (address: PersonAddressModel) => `${addressField(address.postalCode)}${addressField(address.city, ',')}${addressField(address.addressLine1)}${addressField(address.addressLine2)}`;
    const guestFormat = (reservation: ReservationItemModel, guest: GuestModel) => {
      const validationErrors = [];
      const addError = (key: string) => {
        validationErrors.push(key);
        return '';
      };
      const formatedGuest = [
        'U',
        format(reservation.arrival),
        format(reservation.departure),
        guest.lastName || addError('lastName'),
        guest.firstName || addError('firstName'),
        '',
        guest.birthDate ? format(guest.birthDate) : addError('birthday'),
        '',
        '',
        guest.nationalityCountryCode ? getAlpha3CountryCode(guest.nationalityCountryCode) : addError('nationalaityCountryCode'),
        guest.address ? formatAddress(guest.address) : addError('address'),
        guest.identificationNumber || addError('idNumber'),
        guest.company ? guest.company.taxId : addError('visaNumber'),
        reservation.travelPurpose === EnumReservationItemModelTravelPurpose.Business ? 'Business' : '10',
        '',
        '',
      ];
      return { formatedGuest, validationErrors };
    };
    const errors = [];
    reservations.forEach(reservation => {
      const guests = [reservation.primaryGuest, ...reservation.additionalGuests || []];
      guests.forEach(guest => {
        // Skip czech people
        if (guest.nationalityCountryCode !== 'CZ') {
          const name = `${guest.firstName} ${guest.lastName}`;
          try {
            const { formatedGuest, validationErrors } = guestFormat(reservation, guest);
            if (validationErrors.length > 0) {
              const errMsg = `Found error/s for reservation ${reservation.id} ${name}: ${validationErrors.join(', ')}`;
              if (validationErrors.length === 1 && validationErrors[0] === 'visaNumber') {
                formatedGuest[12] = '0000';
                exportData += formatedGuest.join('|') + '\r\n';
              }
              errors.push(errMsg);
            } else {
              exportData += formatedGuest.join('|') + '\r\n';
            }
          } catch (e) {
            logger.errorLog(`Could not process guest for prague export ${reservation.id} ${name}`, { e, guest });
            // tslint:disable-next-line: max-line-length
            errors.push(`Found error/s for reservation ${reservation.id} ${name}: Unknown`);
          }
        }
      });
    });
    await this.slack.sendPragueExportData(exportData, propertyId, ubyportId);
    await this.slack.sendPragueMessage(errors.length > 0 ? errors.join('\n') : 'No errors found');
  }
}
