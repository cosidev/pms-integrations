import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import ApaleoExtensionsService from './apaleoExtensions.service';
import { ApaleoExtensionsController } from './apaleoExtensions.controller';
import { ApaleoModule } from '../apaleo/apaleo.module';

@Module({
  imports: [CommonModule, ApaleoModule],
  controllers: [ApaleoExtensionsController],
  providers: [ApaleoExtensionsService],
})
export class ApaleoExtensionsModule { }
