import { QueryInput, ExpressionAttributeValueMap, QueryOutput } from 'aws-sdk/clients/dynamodb';
import { Request, AWSError } from 'aws-sdk';
import { Test, TestingModule } from '@nestjs/testing';
import { ApaleoExtensionsModule } from './apaleoExtensions.module';
import { DynamoClient } from './../common/aws/dynamoClient';
import JWTService from '../JWTService';
import ApaleoExtensionsService from './apaleoExtensions.service';
import { ReservationRefundDetailsService } from './../common/reservation/reservationRefundDetails.service';
import { PropertyDetails } from '../common/types/property';
import { PropertyDetailsService } from './../common/propertyDetails.service';
import { RefundStatusUpdateDto } from './dto';
import { generalConfig } from './../config';

describe('ApaleoExtensionsService', () => {
  let jwtService: JWTService;
  let dynamoClient: DynamoClient;
  let propertyDetailsService: PropertyDetailsService;
  let apaleoExtensionsService: ApaleoExtensionsService;
  let reservationRefundDetailsService: ReservationRefundDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApaleoExtensionsModule],
    }).compile();

    dynamoClient = module.get<DynamoClient>(DynamoClient);
    jwtService = module.get<JWTService>(JWTService);
    apaleoExtensionsService = module.get<ApaleoExtensionsService>(ApaleoExtensionsService);
    propertyDetailsService = module.get<PropertyDetailsService>(PropertyDetailsService);
    reservationRefundDetailsService = module.get<ReservationRefundDetailsService>(ReservationRefundDetailsService);
  });

  it('should handle getExtensionsUrl', async (): Promise<void> => {
    jest.spyOn(jwtService, 'getToken');

    const PATH = 'reservationDetails';
    const { expTimestamp, url } = await apaleoExtensionsService.getExtensionsUrl(PATH);

    expect(url.includes(PATH)).toBeTruthy();
    expect(jwtService.getToken).toHaveBeenCalledWith(
      {
        apaleoExtensions: true,
        manualMail: true,
      },
      { expiresIn: '3h' },
    );
  });

  it('should handle updateRefundStatus', async (): Promise<void> => {
    jest.spyOn(reservationRefundDetailsService, 'updateDetails').mockImplementation(() => new Promise(resolve => resolve()));

    const RESERVATION_ID = 'YTG56RB9-1';
    await apaleoExtensionsService.updateRefundStatus(RESERVATION_ID, {} as RefundStatusUpdateDto);

    expect(reservationRefundDetailsService.updateDetails).toHaveBeenCalledWith(RESERVATION_ID, {});
  });

  it('should handle getPropertyDetails', async (): Promise<void> => {
    const propertyDetails: PropertyDetails = {} as PropertyDetails;
    jest.spyOn(propertyDetailsService, 'getDetails').mockImplementation(() => new Promise(resolve => resolve(propertyDetails)));

    const PROPERTY_ID = '12345';
    await apaleoExtensionsService.getPropertyDetails(PROPERTY_ID);

    expect(propertyDetailsService.getDetails).toHaveBeenCalledWith(PROPERTY_ID);

    try {
      jest
        .spyOn(propertyDetailsService, 'getDetails')
        .mockImplementation(() => new Promise((resolve, reject) => reject(`Could not find property details ${PROPERTY_ID}`)));

      await apaleoExtensionsService.getPropertyDetails(PROPERTY_ID);
    } catch (e) {
      expect(e).toEqual('Could not find property details 12345');
    }
  });

  it('should handle getReservationEventLogs', async (): Promise<void> => {
    generalConfig.aws.dataDynamoTableName = 'communication-dev-data';
    const returnValueMock = ({
      promise() {
        return Promise.resolve({ Items: [] });
      },
    } as unknown) as Request<QueryOutput, AWSError>;
    jest.spyOn(dynamoClient, 'query').mockReturnValue(returnValueMock);

    const RESERVATION_ID = 'YTG56RB9-1';
    await apaleoExtensionsService.getReservationEventLogs(RESERVATION_ID);

    const input: QueryInput = {
      TableName: 'communication-dev-data',
      KeyConditionExpression: 'dataKey = :dataKey and begins_with(sKey, :sKey)',
      ExpressionAttributeValues: {
        ':dataKey': RESERVATION_ID,
        ':sKey': 'reservation_events_log_',
      } as ExpressionAttributeValueMap,
      ScanIndexForward: false,
      ExpressionAttributeNames: {
        '#timeStamp': 'timeStamp',
      },
      ProjectionExpression: `reservationId,event,message,#timeStamp,additionalData`,
    };
    expect(dynamoClient.query).toHaveBeenCalledWith(input);
  });
});
