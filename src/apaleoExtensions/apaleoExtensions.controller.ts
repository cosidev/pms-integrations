import { Controller, Get, Query, SetMetadata, Post, Body, Param } from '@nestjs/common';
import ApaleoExtensionsService from './apaleoExtensions.service';
import { generalConfig } from '../config';
import { KeyAuth } from '../common/auth/keyAuth.decorator';
import { JwtAuth } from '../common/auth/jwtAuth.decorator';
import { PathDto, SpecialWishUpdateDto, RefundStatusUpdateDto } from './dto';

@Controller('apaleoExtensions')
export class ApaleoExtensionsController {
  constructor(private readonly extensionsService: ApaleoExtensionsService) { }

  @Get('auth')
  @KeyAuth(generalConfig.apaleo.uiGenerationKey)
  @SetMetadata('customResponse', true)
  auth(@Query() data: PathDto) {
    return this.extensionsService.getExtensionsUrl(data.path);
  }

  @Get('reservationDetails/:id')
  @JwtAuth('apaleoExtensions')
  reservationDetails(@Param('id') id: string) {
    return this.extensionsService.getReservationDetails(id);
  }

  @Post('reservationDetails/:id/specialWish')
  @JwtAuth('apaleoExtensions')
  updateSpecialWish(@Param('id') id: string, @Body() data: SpecialWishUpdateDto) {
    return this.extensionsService.updateSpecialWishes(id, data);
  }

  @Post('reservationDetails/:id/refundStatus')
  @JwtAuth('apaleoExtensions')
  updateRefundStatus(@Param('id') id: string, @Body() data: RefundStatusUpdateDto) {
    return this.extensionsService.updateRefundStatus(id, data);
  }

  @Post('reservationDetails/:id/invalidatePinCodes')
  @JwtAuth('apaleoExtensions')
  invalidatePinCodes(@Param('id') id: string) {
    return this.extensionsService.invalidatePinCodes(id);
  }

  @Post('reservationDetails/:id/invalidateBackupCode')
  @JwtAuth('apaleoExtensions')
  invalidateBackupCode(@Param('id') id: string) {
    return this.extensionsService.invalidateBackupCode(id);
  }

  @Get('propertyDetails/:id')
  @JwtAuth('apaleoExtensions')
  propertyDetails(@Param('id') id: string) {
    return this.extensionsService.getPropertyDetails(id);
  }

  @Get('reservationLogs/:id')
  @JwtAuth('apaleoExtensions')
  reservationEventLogs(@Param('id') reservationId: string) {
    return this.extensionsService.getReservationEventLogs(reservationId);
  }
}
