import { CommissionReturnStatus } from '../../common/types/reservation';
import { IsOptional, IsString, IsBoolean, IsEnum } from 'class-validator';

// tslint:disable-next-line: max-classes-per-file
export default class RefundstatusUpdateDto {
  @IsBoolean()
  forcedRefundByOta: boolean;

  @IsBoolean()
  guestRebooked: boolean;

  @IsBoolean()
  guestVoucher: boolean;

  @IsOptional()
  @IsString()
  rebookingReservationId?: string;

  @IsBoolean()
  refundedByUs: boolean;

  @IsEnum(CommissionReturnStatus)
  commissionReturnStatus: CommissionReturnStatus;
}
