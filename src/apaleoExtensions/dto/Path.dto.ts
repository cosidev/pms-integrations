import { IsString } from 'class-validator';

export default class PathDto {
  @IsString()
  path: string;
}
