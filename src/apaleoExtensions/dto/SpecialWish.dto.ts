import { IsString, IsOptional, IsNumber, ValidateNested, IsArray } from 'class-validator';
import { Type } from 'class-transformer';

class SpecialWish {
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  count: number;
}

// tslint:disable-next-line: max-classes-per-file
export default class SpecialWishUpdateDto {
  @IsArray()
  @ValidateNested()
  @Type(() => SpecialWish)
  specialWishes: SpecialWish[];
}
