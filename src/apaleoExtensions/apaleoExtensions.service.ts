import { generalConfig } from './../config';
import { DynamoClient } from './../common/aws/dynamoClient';
import { Injectable } from '@nestjs/common';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import JWTService from '../JWTService';
import { ReservationDetailsService } from '../common/reservation/reservationDetails.service';
import { PinCodeHandler } from '../PinCodeHandler';
import { SpecialWishUpdateDto } from './dto';
import RefundstatusUpdateDto from './dto/RefundStatusUpdate.dto';
import { ReservationRefundDetailsService } from '../common/reservation/reservationRefundDetails.service';
import { PropertyDetailsService } from '../common/propertyDetails.service';

@Injectable()
export default class ApaleoExtensionsService {
  constructor(
    private readonly jwtService: JWTService,
    private readonly apaleoClient: ApaleoClient,
    private readonly reservationDetailsService: ReservationDetailsService,
    private readonly reservationRefundDetailsService: ReservationRefundDetailsService,
    private readonly propertyDetailsService: PropertyDetailsService,
    private readonly pinCodeHandler: PinCodeHandler,
    private readonly dynamoClient: DynamoClient,
  ) { }

  async getExtensionsUrl(path: string) {
    const expTimestamp = new Date();
    expTimestamp.setHours(expTimestamp.getHours() + 3);
    const jwtToken = this.jwtService.getToken({ apaleoExtensions: true, manualMail: true }, { expiresIn: '3h' });
    return { url: `https://apaleo.cosi-group.com/${path}?token=${jwtToken}`, expTimestamp };
  }

  async getReservationDetails(reservationId: string) {
    const [details, apaleoReservation] = await Promise.all([
      this.reservationDetailsService.getDetails(reservationId),
      this.apaleoClient.getReservationById(reservationId),
    ]);
    const cmsProperty = await this.propertyDetailsService.getDetails(apaleoReservation.property.id);
    if (apaleoReservation.unit) {
      const backupCode = await this.pinCodeHandler.getBackupCode(apaleoReservation.unit.id, apaleoReservation.property.id);
      details.pinCodes.backupCode = backupCode;
    }
    return {
      reservation: details,
      availableServices: cmsProperty.availableServices,
      manageTripUrl: `https://cosi-trip.com?key=${this.jwtService.getToken({ reservationId, bookingId: apaleoReservation.bookingId })}`,
    };
  }

  async invalidatePinCodes(reservationId: string) {
    const apaleoReservation = await this.apaleoClient.getReservationById(reservationId);
    await this.pinCodeHandler.removeCode(apaleoReservation);
    return this.reservationDetailsService.getDetails(reservationId);
  }

  async invalidateBackupCode(reservationId: string) {
    const apaleoReservation = await this.apaleoClient.getReservationById(reservationId);
    let backupCode;
    if (apaleoReservation.unit) {
      const { unit, property, departure } = apaleoReservation;
      backupCode = await this.pinCodeHandler.invalidateBackupCode(unit.id, property.id, departure);
    }
    const details = await this.reservationDetailsService.getDetails(reservationId);
    details.pinCodes.backupCode = backupCode;
    return details;
  }

  async updateSpecialWishes(reservationId: string, data: SpecialWishUpdateDto) {
    const { specialWishes } = data;
    return this.reservationDetailsService.updateDetails(reservationId, { specialWishes });
  }

  async updateRefundStatus(reservationId: string, data: RefundstatusUpdateDto) {
    return this.reservationRefundDetailsService.updateDetails(reservationId, data);
  }

  async getPropertyDetails(propertyId: string) {
    return {
      property: await this.propertyDetailsService.getDetails(propertyId),
    };
  }

  async getReservationEventLogs(reservationId: string) {
    const dataTable = generalConfig.aws.dataDynamoTableName;
    const eventLogs = await this.dynamoClient.query({
      TableName: dataTable,
      KeyConditionExpression: 'dataKey = :dataKey and begins_with(sKey, :sKey)',
      ExpressionAttributeValues: {
        ':dataKey': reservationId,
        ':sKey': 'reservation_events_log_',
      },
      ScanIndexForward: false,
      ExpressionAttributeNames: {
        '#timeStamp': 'timeStamp',
      },
      ProjectionExpression: `reservationId,event,message,#timeStamp,additionalData`,
    }).promise();

    return {
      eventsLog: eventLogs.Items,
    };
  }
}
