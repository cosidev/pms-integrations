import { Injectable } from '@nestjs/common';
import { generalConfig } from './config';
import { DynamoClient } from './common/aws/dynamoClient';
import { isValid } from 'date-fns';

export enum EnumReservationEvents {
  'ReservationCreated' = 'reservationCreated',
  'PinGenerated' = 'pinGenerated',
  'InvoiceGenerated' = 'invoiceGenerated',
  'UnitAssigned' = 'unitAssigned',
  'BookingConfirmation' = 'bookingConfirmation',
  'CheckIn' = 'checkIn',
  'CheckInDay' = 'checkInDay',
}

type TimeStamp = Date | string;
interface EventLog {
  reservationId: string;
  message: string;
  additionalData?: any;
  timeStamp?: TimeStamp;
  event: EnumReservationEvents;
}

@Injectable()
export class ReservationEventLogger {
  constructor(private readonly dynamoClient: DynamoClient) {}

  public async logEvent(data: EventLog): Promise<void> {
    const log: EventLog = {
      ...data,
      timeStamp: this.parseTimestamp(data.timeStamp),
    };

    await this.createLog(log);
  }

  private parseTimestamp(timestamp: TimeStamp): string {
    if (timestamp instanceof Date) {
      return new Date(timestamp).toISOString();
    }

    if (typeof timestamp === 'string' && isValid(new Date(timestamp))) {
      return new Date(timestamp).toISOString();
    }

    return new Date().toISOString();
  }

  private async createLog(log: EventLog): Promise<void> {
    const dataTable = generalConfig.aws.dataDynamoTableName;
    await this.dynamoClient.putItem(dataTable, {
      dataKey: log.reservationId,
      sKey: `reservation_events_log_${log.timeStamp}`,
      event: log.event,
      ...log,
    });
  }
}
