import { RESERVATION } from './apaleo/__mocks__';
import { ReservationModel } from './apaleo/generated/reservation';
import { HrsEmailHandler } from './HrsEmailHandler';
import { GuestExperienceService } from './GuestExperienceService';

jest.mock('./LoggingService.ts');
jest.mock('./SlackService.ts');

describe('HrsEmailHandler', () => {
  let hrsEmailHandler: HrsEmailHandler;
  let guestExperienceService: GuestExperienceService;

  beforeEach(async () => {
    guestExperienceService = new GuestExperienceService();
    hrsEmailHandler = new HrsEmailHandler(guestExperienceService);
  });

  describe('handleNewReservation', () => {
    it('should exit if not HRS booking', async () => {
      jest.spyOn(guestExperienceService, 'createGuestExperienceTask')
        .mockImplementation(() => new Promise(resolve => resolve()));

      const reservation = {
        ...RESERVATION,
        source: 'Booking.com',
      } as ReservationModel;
      await hrsEmailHandler.handleNewReservation(reservation);

      expect(guestExperienceService.createGuestExperienceTask).not.toBeCalled();
    });

    it('should create guest experience task for HRS bookings', async () => {
      jest.spyOn(guestExperienceService, 'createGuestExperienceTask')
        .mockImplementation(() => new Promise(resolve => resolve()));

      const hrsReservation = Object.assign({}, RESERVATION, { source: 'HRS' });
      await hrsEmailHandler.handleNewReservation(hrsReservation);

      expect(guestExperienceService.createGuestExperienceTask).toBeCalled();
      expect(guestExperienceService.createGuestExperienceTask).toBeCalledWith({
        sender: `HRS Reservation ${hrsReservation.id}`,
        message: `A new HRS reservation just got in. Please ask for email address. ${hrsReservation.id}`,
        subject: `New HRS reservation ${hrsReservation.id}`,
      });
    });
  });
});
