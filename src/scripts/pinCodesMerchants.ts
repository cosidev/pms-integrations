import dotenv from 'dotenv';
dotenv.config();
import dateformat from 'dateformat';
import DatoClient from '../CmsClient';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import ReservationService from '../ReservationService';
import GlutzHandler from '../GlutzHandler';

const pinCodes = async () => {
  const client = new ApaleoClient();
  const cms = new DatoClient();
  const resService = new ReservationService();
  const glutz = new GlutzHandler();

  const today = new Date();
  today.setUTCHours(0, 0, 0);
  const threeDaysEnd = new Date();
  threeDaysEnd.setDate(threeDaysEnd.getDate() + 3);
  threeDaysEnd.setUTCHours(23, 59);
  const { reservations = [] } = await client
    .getReservationsbyTime(['Confirmed', 'InHouse'], 'Arrival', today, threeDaysEnd, { propertyIds: ['CZ_PR_002'], expand: ['booker', 'assignedUnits'] });
  for (const reservation of reservations) {
    try {
      let cmsReservation = await cms.getReservationInfos(reservation.id);
      if (!cmsReservation) {
        cmsReservation = await cms.createReservation(reservation);
      }
      let room;
      let unitId;
      if (!reservation.assignedUnits || reservation.assignedUnits.length === 0) {
        const unit = await client.assignRoomToReservation(reservation.id);
        unitId = unit.timeSlices[0].unit.id;
        room = resService.getFormattedRoomName(unit.timeSlices[0].unit.name);
      } else {
        room = resService.getFormattedRoomName(reservation.assignedUnits[0].unit.name);
        unitId = reservation.assignedUnits[0].unit.id;
      }
      const guestName = `${reservation.primaryGuest.firstName} ${reservation.primaryGuest.lastName}`;
      const guest = `${guestName}, ${reservation.booker.phone || ''}`;
      const d = (d) => dateformat(d, 'dd.mm.yyyy');
      console.log(`Room ${room} => ${d(reservation.arrival)} - ${d(reservation.departure)} (${guest}, ${reservation.id}) ${cmsReservation.pinComfortCode}  ${cmsReservation.pinAccessCode} ${cmsReservation.checkinInfosSent ? 'Sent' : 'Not sent'}`);
      if (!cmsReservation.pinComfortCode) {
        console.log('Generating codes');
        const code = await glutz.generateCode({
          reservationId: reservation.id,
          name: guestName,
          unitIds: [unitId, 'CZ_PR_002-ENTRY'],
          from: dateformat(reservation.arrival, 'yyyy-mm-dd\'T\'HH:MM:00'),
          to: dateformat(reservation.departure, 'yyyy-mm-dd\'T\'12:MM:00'),
        });
        const updateValues = { pinComfortCode: `${code}` };
        await cms.updateEntry(cmsReservation.id, updateValues);
        console.log('Updated codes', updateValues);
      }
    } catch (e) {
      console.log(e);
      console.log(reservation);
    }
  }
  process.exit(0);
};

pinCodes();
