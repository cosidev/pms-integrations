import dotenv from 'dotenv';
dotenv.config();
import { ApaleoClient } from '../apaleo/ApaleoClient';
import { Pool } from 'pg';
import { generalConfig } from '../config';

const exportCheck = async () => {
  const client = new ApaleoClient();
  const db = new Pool(generalConfig.exportDatabase);
  try {
    const { reservations, count } = await client.getReservations({});
    await db.connect();
    const select = 'SELECT * FROM reservations WHERE reservation_id = $1';
    console.log(count);
    for (const reservation of reservations) {
      const res = await db.query(select, [reservation.id]);
      if (res.rowCount === 0) {
        console.log(`Could not find reservation ${reservation.id} (${reservation.property.id}) in reservations table. Created at: ${reservation.created}`);
      }
    }
  } catch (e) {
    console.log(e);
    if (e.response && e.response.status === 422) {
      console.log(e.response.data.messages);
    }
  }
  process.exit(0);
};

exportCheck();
