import dotenv from 'dotenv';
dotenv.config();
import { ApaleoClient } from '../apaleo/ApaleoClient';

const pinCodes = async () => {
  const client = new ApaleoClient();

  const saturday = new Date();
  saturday.setDate(saturday.getDate() - 3);
  const threeDaysEnd = new Date();
  threeDaysEnd.setDate(threeDaysEnd.getDate() + 3);
  threeDaysEnd.setUTCHours(23, 59);
  const { reservations = [] } = await client
    .getReservationsbyTime(['Confirmed', 'InHouse'], 'Creation', saturday, threeDaysEnd, { propertyIds: ['CZ_PR_002'], expand: ['booker', 'assignedUnits'] });
  for (const reservation of reservations) {
    console.log(`Checking reservation ${reservation.id} for guest data`);
    if (reservation.primaryGuest.birthDate) {
      console.log('Found birthday', reservation.primaryGuest.birthDate, ' forprimary guest');
    }
    if (reservation.adults > 1 && reservation.additionalGuests && reservation.additionalGuests[1].birthDate) {
      console.log('Found birthday', reservation.primaryGuest.birthDate, ' for second guest');
    }
  }
  console.log(`found ${reservations.length} reservations`);
  process.exit(0);
};

pinCodes();
