import dotenv from 'dotenv';
dotenv.config();
import dateformat from 'dateformat';
import DatoClient from '../CmsClient';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import ReservationService from '../ReservationService';
import SagKeyHandler from '../SagKeyHandler';

const pinCodes = async () => {
  const client = new ApaleoClient();
  const cms = new DatoClient();
  const resService = new ReservationService();
  const sag = new SagKeyHandler();

  const today = new Date();
  today.setUTCHours(0, 0, 0);
  const threeDaysEnd = new Date();
  threeDaysEnd.setDate(threeDaysEnd.getDate() + 3);
  threeDaysEnd.setUTCHours(23, 59);
  const { reservations = [] } = await client
    .getReservationsbyTime(['Confirmed', 'InHouse'], 'Arrival', today, threeDaysEnd, { propertyIds: ['DE_BE_001'], expand: ['booker', 'assignedUnits'] });
  for (const reservation of reservations) {
    try {
      let cmsReservation = await cms.getReservationInfos(reservation.id);
      if (!cmsReservation) {
        cmsReservation = await cms.createReservation(reservation);
      }
      let room;
      if (!reservation.assignedUnits || reservation.assignedUnits.length === 0) {
        const unit = await client.assignRoomToReservation(reservation.id);
        room = resService.getFormattedRoomName(unit.timeSlices[0].unit.name);
      } else {
        room = resService.getFormattedRoomName(reservation.assignedUnits[0].unit.name);
      }
      const guestName = `${reservation.primaryGuest.firstName} ${reservation.primaryGuest.lastName}`;
      const guest = `${guestName}, ${reservation.booker.phone || ''}`;
      const d = (d) => dateformat(d, 'dd.mm.yyyy');
      console.log(`Room ${room} => ${d(reservation.arrival)} - ${d(reservation.departure)} (${guest}, ${reservation.id}) ${cmsReservation.pinComfortCode}  ${cmsReservation.pinAccessCode} ${cmsReservation.checkinInfosSent ? 'Sent' : 'Not sent'} ${reservation.balance.amount}${reservation.balance.currency}`);
      if (!cmsReservation.pinComfortCode) {
        console.log('Generating codes');
        let updateValues = {};
        if (room === '098' || room === '099') {
          const staticCodes = { '099': '1738', '098': '4346' };
          updateValues = { pinAccessCode: staticCodes[room], pinComfortCode: staticCodes[room] };
        } else {
          const reservationNumber = resService.getReservationNumber(reservation.id);
          // tslint:disable-next-line: max-line-length
          updateValues = await sag.generate({ roomNumber: room, name: guestName, arrival: reservation.arrival, departure: reservation.departure, reservationNumber });
        }
        await cms.updateEntry(cmsReservation.id, updateValues);
        console.log('Updated codes', updateValues);
      }
    } catch (e) {
      console.log(e);
      console.log(reservation);
    }
  }
  process.exit(0);
};

pinCodes();
