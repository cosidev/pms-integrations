import dotenv from 'dotenv';
dotenv.config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { OverbookingsService } from '../overbookings/overbookings.service';

const main = async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  const s = app.get(OverbookingsService);
  await s.seedVirtualAvailability();
};

main();
