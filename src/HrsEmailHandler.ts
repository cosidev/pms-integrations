import { GuestExperienceService } from './GuestExperienceService';
import { ReservationModel } from './apaleo/generated/reservation';

export class HrsEmailHandler {
  constructor(
    private readonly guestExperienceService: GuestExperienceService,
  ) { }

  async handleNewReservation(reservation: ReservationModel) {
    if (!this.shouldHandle(reservation)) {
      return reservation;
    }

    await this.guestExperienceService.createGuestExperienceTask({
      sender: `HRS Reservation ${reservation.id}`,
      message: `A new HRS reservation just got in. Please ask for email address. ${reservation.id}`,
      subject: `New HRS reservation ${reservation.id}`,
    });

    return reservation;
  }

  private shouldHandle(reservation: ReservationModel): boolean {
    return reservation.source === 'HRS';
  }
}
