import { FrontClient, Handle } from './FrontClient';
import { ApaleoReservation } from './types';

const taskInbox = 'cha_2c3ns';

export interface IGuestExperienceTask {
  sender: string;
  subject: string;
  message: string;
}

export class GuestExperienceService {
  frontClient: FrontClient;
  constructor() {
    this.frontClient = new FrontClient();
  }

  async createGuestExperienceTask(task: IGuestExperienceTask) {
    await this.frontClient.receiveCustomMessage(taskInbox, {
      sender: {
        name: task.sender,
        handle: task.sender,
      },
      subject: task.subject,
      body: task.message,
    });
  }

  async createContact(reservation: ApaleoReservation) {
    const handles: Handle[] = [{
      handle: reservation.booker.email,
      source: 'email',
    }];
    if (reservation.booker.phone) {
      handles.push({
        handle: reservation.booker.phone,
        source: 'phone',
      });
    }
    await this.frontClient.createContact({
      handles,
      description: `Guest ${reservation.id}`,
      name: `${reservation.booker.firstName} ${reservation.booker.lastName}`,
    });
  }
}
