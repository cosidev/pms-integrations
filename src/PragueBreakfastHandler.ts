import dateformat from 'dateformat';
import sendgrid from './sendGrid';
import logger from './LoggingService';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { ReservationItemModel } from './apaleo/generated/reservation';
import ReservationService from './ReservationService';
import { ReservationDetailsService } from './common/reservation/reservationDetails.service';
import { DynamoClient } from './common/aws';
import { ReservationRefundDetailsService } from './common/reservation/reservationRefundDetails.service';
import { pragueBreakfastProperties } from './util/properties';

export default class PragueBreakfastHandler {
  private apaleoClient: ApaleoClient;
  private reservationService: ReservationService;
  private reservationDetailsService: ReservationDetailsService;
  private sendGridClient;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.reservationService = new ReservationService();
    const dynamo = new DynamoClient();
    const refundDetailsService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundDetailsService);
    this.sendGridClient = sendgrid;
  }

  async sendProviderInformation() {
    logger.infoLog('Sending prague breakfast provider information');
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    const reservations = await this.getBreakfastReservations(tomorrow, { expand: ['booker', 'assignedUnits'] });
    logger.infoLog(`Got ${reservations.length} breakfast reservations for tomorrow`);

    const dayFormat = (d: Date) => dateformat(d, 'dd-mm-yyyy');
    let breakfastSelections = '';
    await Promise.all(
      reservations.map(async reservation => {
        try {
          const { childrenAges = [], booker } = reservation;
          const name = `${booker.firstName} ${booker.lastName}`;
          const totalGuests = reservation.adults + childrenAges.length;
          const roomName = this.reservationService.getFormattedRoomName(reservation.assignedUnits[0].unit.name);
          const details = await this.reservationDetailsService.getDetails(reservation.id);
          const daySelection = details.breakfastSelection.find(selection => dayFormat(selection.date) === dayFormat(tomorrow));
          if (!daySelection) {
            breakfastSelections += `Room: ${roomName} | Guest name: ${name} | Total guests: ${totalGuests} <br>`;
          } else {
            const selections = Object.keys(daySelection.selection)
              .map(key => `${key} = ${daySelection.selection[key]}`)
              .join(', ');
            breakfastSelections += `Room: ${roomName} | Guest name: ${name} | Total guests: ${totalGuests} | Selections: ${selections} <br>`;
          }
        } catch (e) {
          logger.errorLog(`Could not format reservation ${reservation.id} for prague breakfast `, { errorMessage: e.message });
        }
      }),
    );
    breakfastSelections = breakfastSelections.length > 0 ? breakfastSelections : 'No guests with breakfast tomorrow';
    await this.sendGridClient.send({
      templateId: 'd-203162bc15e647d796734dcfde0d3aff',
      dynamicTemplateData: { breakfastSelections },
      from: {
        email: 'booking@cosi-prague.com',
        name: 'Cosi Group',
      },
      to: ['puor@malostranska-beseda.cz', 'luca.demmel@cosi-group.com'],
    });
  }

  async getBreakfastReservations(date: Date, additionalFilter?: any): Promise<ReservationItemModel[]> {
    const filter = { propertyIds: pragueBreakfastProperties, ...additionalFilter };
    const breakfastServiceIds = ['CZ_PR_002-BFS', 'CZ_PR_002-BFS1', 'CZ_PR_003-BFS15', 'CZ_PR_003-BFS', 'CZ_PR_004-BFS15', 'CZ_PR_004-BFS'];
    const { reservations = [] } = await this.apaleoClient.getReservationsByDay(date, ['Confirmed', 'InHouse'], 'Stay', filter);
    const breakfastReservations = [];
    for (const reservation of reservations) {
      const rateplan = await this.apaleoClient.getRateplanById(reservation.ratePlan.id);
      const hasBreakfast: boolean = rateplan.includedServices
        ? rateplan.includedServices.some(x => breakfastServiceIds.includes(x.serviceId))
        : false;
      if (hasBreakfast) {
        breakfastReservations.push(reservation);
      }
    }

    return breakfastReservations;
  }
}
