import Axios from 'axios';
import { generalConfig } from './config';
import logger from './LoggingService';

export class UrlShortener {
  async shorten(url: string): Promise<string> {
    const body = {
      group_guid: 'Bk1vbE93o0y',
      long_url: url,
    };
    const result = await Axios.post(`https://api-ssl.bitly.com/v4/shorten`, body, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${generalConfig.bitlyToken}`,
      },
    });
    if (result.status === 201) {
      return result.data.id;
    }
    logger.errorLog(`Could not generate short url`, { url, status: result.status, bitlyResponse: result.data });
    throw new Error('Could not shorten url');
  }
}
