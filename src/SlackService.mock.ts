export const UNIT_GROUPS = [
  {
    unitGroup:
    {
      id: 'DE_DEV_001-DELUX',
      code: 'DELUX',
      name: 'Deluxe',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet',
    },
    physicalCount: 3,
    houseCount: 3,
    soldCount: 3,
    occupancy: 120,
    availableCount: 0,
    sellableCount: -2,
    allowedOverbookingCount: 0,
    maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
    block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    propertyName: 'Dev Hotel',
  },
  {
    unitGroup:
    {
      id: 'DE_DEV_001-SUPERIOR',
      code: 'SUPERIOR',
      name: 'SUPERIOR',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    physicalCount: 3,
    houseCount: 3,
    soldCount: 0,
    occupancy: 150,
    availableCount: 3,
    sellableCount: -3,
    allowedOverbookingCount: 0,
    maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
    block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    propertyName: 'The Staging Hotel',
  },
];
