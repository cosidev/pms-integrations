import { Test, TestingModule } from '@nestjs/testing';
import addDays from 'date-fns/addDays';
import { ApaleoModule } from './apaleo/apaleo.module';
import { DynamoClient } from './common/aws/dynamoClient';
import { ReservationEventLogger, EnumReservationEvents } from './ReservationEventLogger';

jest.mock('./LoggingService.ts');

interface EventLog {
  reservationId: string;
  message: string;
  additionalData?: any;
  timeStamp?: Date | string;
  event: EnumReservationEvents;
}

function createTestCase(log: EventLog) {
  const updateLog = { ...log };
  if (!log.timeStamp) {
    updateLog.timeStamp = new Date().toISOString();
  } else {
    updateLog.timeStamp = new Date(log.timeStamp).toISOString();
  }

  return {
    log: updateLog,
    updateValues: {
      dataKey: log.reservationId,
      sKey: `reservation_events_log_${updateLog.timeStamp}`,
      ...updateLog,
    },
  };
}

describe('ReservationEventLogger', () => {
  let dynamoClient: DynamoClient;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApaleoModule],
    }).compile();

    dynamoClient = module.get<DynamoClient>(DynamoClient);
  });

  it('should create logs for all supported reservation events', async (): Promise<void> => {
    jest.spyOn(dynamoClient, 'putItem').mockImplementation(() => new Promise(resolve => resolve()));

    const testCases = [
      createTestCase({
        event: EnumReservationEvents.BookingConfirmation,
        reservationId: 'EF61DG78Q-1',
        message: 'Booking confirmation mail sent',
      }),
      createTestCase({
        event: EnumReservationEvents.CheckIn,
        reservationId: 'EF61DG78Q-1',
        message: 'checkIn information mail sent',
      }),
      createTestCase({
        event: EnumReservationEvents.CheckInDay,
        reservationId: 'EF61DG78Q-1',
        message: 'CheckInDay mail sent',
      }),
      createTestCase({
        event: EnumReservationEvents.InvoiceGenerated,
        reservationId: 'EF61DG78Q-1',
        message: 'Invoice generated for reservation',
      }),
      createTestCase({
        event: EnumReservationEvents.PinGenerated,
        reservationId: 'EF61DG78Q-1',
        message: 'Pincode generated for reservation',
        additionalData: {
          pinCodes: ['12252'],
        },
      }),
      createTestCase({
        event: EnumReservationEvents.ReservationCreated,
        reservationId: 'EF61DG78Q-1',
        message: 'Reservation created',
        timeStamp: addDays(new Date(), -1),
      }),
      createTestCase({
        event: EnumReservationEvents.UnitAssigned,
        reservationId: 'EF61DG78Q-1',
        message: 'Unit assigned to reservation',
        additionalData: {
          rooms: [],
        },
      }),
    ];

    for (const { log, updateValues } of testCases) {
      await new ReservationEventLogger(dynamoClient).logEvent(log);

      expect(dynamoClient.putItem).toHaveBeenCalled();
      expect(dynamoClient.putItem).toHaveBeenCalledWith(undefined, updateValues);
    }

    expect(dynamoClient.putItem).toHaveBeenCalledTimes(testCases.length);

    const supportedEvents = {
      ...EnumReservationEvents,
    };
    expect(Object.keys(supportedEvents).length).toEqual(testCases.length);
  });
});
