import * as logzio from 'logzio-nodejs';
import { generalConfig } from './config';

class LoggingService {

  logger: any;

  constructor() {
    this.logger = logzio.createLogger({
      token: generalConfig.logzToken,
      type: 'pms-integrations',
      host: 'listener-eu.logz.io',
      port: 8071,
      protocol: 'https',
    });
  }

  infoLog(msg: string, data?: any) {
    this.log(msg, 'INFO', data);
  }

  errorLog(msg: string, data) {
    this.log(msg, 'ERROR', data);
  }

  debugLog(msg: string, data) {
    this.log(msg, 'DEBUG', data);
  }

  log(msg: string, lvl: string, data) {
    // tslint:disable-next-line: no-console
    console.log(msg, data || null);
    if (generalConfig.isOffline) {
      return;
    }
    this.logger.log({
      message: msg,
      stage: generalConfig.env,
      level: lvl,
      data,
    });
  }

  endLogRequest() {
    return new Promise((res, rej) => {
      this.logger.sendAndClose((err: any, data: any) => {
        if (!err) {
          res();
          return 0;
        }
        rej(err);
      });
    });
  }
}

export default new LoggingService();
