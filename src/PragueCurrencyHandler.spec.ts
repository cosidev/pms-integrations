import axios from 'axios';
import { Test, TestingModule } from '@nestjs/testing';
import { NEW_PRAGUE_RESERVATION, OLD_PRAGUE_RESERVATION } from './apaleo/__mocks__/prague.mock';
import { generalConfig } from './config';
import { AppModule } from './app.module';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { SlackService } from './SlackService';
import { DynamoClient } from './common/aws/dynamoClient';
import PragueCurrencyHandler from './PragueCurrencyHandler';
import { ReservationModel } from './apaleo/generated/reservation';

jest.mock('./LoggingService.ts');
jest.mock('./SlackService.ts');

describe('PragueCurrencyHandler', () => {
  let apaleoClient: ApaleoClient;
  let dynamoClient: DynamoClient;
  let slack: SlackService;
  let pragueCurrencyHandler: PragueCurrencyHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    dynamoClient = module.get<DynamoClient>(DynamoClient);
    slack = module.get<SlackService>(SlackService);
    pragueCurrencyHandler = new PragueCurrencyHandler(dynamoClient, apaleoClient, slack);
  });

  describe('handleNewReservation', () => {
    it('should disregard non-prague reservations', async () => {
      jest.spyOn(dynamoClient, 'getItem');

      const reservation = {
        ...NEW_PRAGUE_RESERVATION,
        property: {
          id: 'DE_DEV_001',
        },
      } as ReservationModel;
      await pragueCurrencyHandler.handleReservation(reservation);
      expect(dynamoClient.getItem).not.toHaveBeenCalled();
    });

    it('should disregard non-booking.com reservations', async () => {
      jest.spyOn(dynamoClient, 'getItem');

      const reservation = {
        ...NEW_PRAGUE_RESERVATION,
        source: 'Airbnb',
      } as ReservationModel;
      await pragueCurrencyHandler.handleReservation(reservation);
      expect(dynamoClient.getItem).not.toHaveBeenCalled();
    });

    it('should exit if reservation exists', async () => {
      const DYNAMO_S_KEY = 'prague_currency_conversion';
      const dynamoFetchResult: any = {
        newGrossAmount: 100,
      };

      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(dynamoFetchResult)));

      const amendedReservation = await pragueCurrencyHandler.handleReservation(NEW_PRAGUE_RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, {
        dataKey: NEW_PRAGUE_RESERVATION.id,
        sKey: DYNAMO_S_KEY,
      });

      expect(amendedReservation).toBe(NEW_PRAGUE_RESERVATION);
      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(0);
    });

    it('should successfully amend the totalGrossAmount on timeslices based on exchange rate', async () => {
      const RATE = 25;
      jest.spyOn(axios, 'get').mockImplementationOnce(
        () =>
          new Promise(resolve =>
            resolve({
              data: {
                success: true,
                quotes: {
                  EURCZK: RATE,
                },
              },
            }),
          ),
      );

      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'updateReservationbyId').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'getReservationById').mockImplementationOnce(() => new Promise(resolve => resolve(NEW_PRAGUE_RESERVATION)));

      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(null)));
      jest.spyOn(dynamoClient, 'putItem').mockImplementationOnce(() => new Promise(resolve => resolve()));

      await pragueCurrencyHandler.handleReservation(OLD_PRAGUE_RESERVATION);

      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(
        'https://www.apilayer.net/api/live?access_key=5e8bfab3107f440d31ab4ad44f41efe4&source=EUR&currencies=CZK',
      );
      expect(apaleoClient.forceAmmendReservation).toHaveBeenCalledTimes(1);

      const existingTimesliceAmount = OLD_PRAGUE_RESERVATION.timeSlices[0].totalGrossAmount.amount;
      expect(apaleoClient.forceAmmendReservation).toHaveBeenCalledWith(
        OLD_PRAGUE_RESERVATION.id,
        expect.objectContaining({
          timeSlices: [
            {
              ratePlanId: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
              totalGrossAmount: {
                amount: existingTimesliceAmount * RATE,
                currency: 'CZK',
              },
            },
          ],
        }),
      );
    });

    it('should successfully amend the commissionAmount based on exchange rate', async () => {
      const RATE = 25;
      jest.spyOn(axios, 'get').mockImplementationOnce(
        () =>
          new Promise(resolve =>
            resolve({
              data: {
                success: true,
                quotes: {
                  EURCZK: RATE,
                },
              },
            }),
          ),
      );

      jest.spyOn(apaleoClient, 'forceAmmendReservation').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'updateReservationbyId').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'getReservationById').mockImplementationOnce(() => new Promise(resolve => resolve(NEW_PRAGUE_RESERVATION)));

      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(null)));
      jest.spyOn(dynamoClient, 'putItem').mockImplementationOnce(() => new Promise(resolve => resolve()));

      await pragueCurrencyHandler.handleReservation(OLD_PRAGUE_RESERVATION);

      const updateValues = [
        {
          op: 'add',
          path: '/commission/commissionAmount',
          value: {
            amount: OLD_PRAGUE_RESERVATION.commission.commissionAmount.amount * RATE,
            currency: 'CZK',
          },
        },
      ];
      expect(apaleoClient.updateReservationbyId).toHaveBeenCalledWith(OLD_PRAGUE_RESERVATION.id, updateValues);
    });

    it('should gracefully handle CZK exchange rate retrieval failure', async () => {
      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(null)));
      jest.spyOn(axios, 'get').mockImplementationOnce(
        () =>
          new Promise(resolve =>
            resolve({
              data: {
                success: false,
              },
            }),
          ),
      );

      try {
        await pragueCurrencyHandler.handleReservation(OLD_PRAGUE_RESERVATION);
      } catch (error) {
        expect(error).toEqual(new Error('Could not get CZK rate from currency layer'));
      }
    });
  });

  describe('handleAmmendedReservation', () => {
    it('should disregard non-prague reservations', async () => {
      jest.spyOn(dynamoClient, 'getItem');

      const reservation = {
        ...NEW_PRAGUE_RESERVATION,
        property: {
          id: 'DE_DEV_001',
        },
      } as ReservationModel;
      await pragueCurrencyHandler.handleAmmendedReservation(reservation);
      expect(dynamoClient.getItem).not.toHaveBeenCalled();
    });

    it('should return the reservation if initialGrossAmount is equal to new', async () => {
      const DYNAMO_S_KEY = 'prague_currency_conversion';
      const dynamoFetchResult: any = {
        newGrossAmount: NEW_PRAGUE_RESERVATION.totalGrossAmount.amount,
      };

      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(dynamoFetchResult)));
      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementationOnce(() => new Promise(resolve => resolve()));

      await pragueCurrencyHandler.handleAmmendedReservation(NEW_PRAGUE_RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, {
        dataKey: NEW_PRAGUE_RESERVATION.id,
        sKey: DYNAMO_S_KEY,
      });

      expect(slack.sendAmmendedGrossAmount).not.toHaveBeenCalled();
    });

    it('should not call sendAmmendedGrossAmount if dynamodb response is empty', async () => {
      const DYNAMO_S_KEY = 'prague_currency_conversion';

      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementationOnce(() => new Promise(resolve => resolve()));

      await pragueCurrencyHandler.handleAmmendedReservation(NEW_PRAGUE_RESERVATION);

      expect(dynamoClient.getItem).toHaveBeenCalledWith(generalConfig.aws.dataDynamoTableName, {
        dataKey: NEW_PRAGUE_RESERVATION.id,
        sKey: DYNAMO_S_KEY,
      });

      expect(slack.sendAmmendedGrossAmount).not.toHaveBeenCalled();
    });

    it('should sendAmmendedGrossAmount to slack if initialGrossAmount is not equal to new', async () => {
      const dynamoFetchResult: any = {
        newGrossAmount: 190,
      };

      jest.spyOn(slack, 'sendAmmendedGrossAmount').mockImplementationOnce(() => new Promise(resolve => resolve()));
      jest.spyOn(dynamoClient, 'getItem').mockImplementationOnce(() => new Promise(resolve => resolve(dynamoFetchResult)));

      await pragueCurrencyHandler.handleAmmendedReservation(NEW_PRAGUE_RESERVATION);

      expect(slack.sendAmmendedGrossAmount).toHaveBeenCalledTimes(1);
    });
  });
});
