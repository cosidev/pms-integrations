import { ReservationListModel } from './../../apaleo/generated/reservation';
import { ReservationItemModel } from '../../apaleo/generated/reservation';

export const APALEO_RESERVATIONS_PER_MONTH: ReservationItemModel[] = [
  {
    id: 'MIYJRCIL-4',
    bookingId: 'MIYJRCIL',
    status: 'CheckedOut',
    checkInTime: '2020-03-18T11:34:52+01:00',
    checkOutTime: '2020-03-18T11:35:07+01:00',
    property:
    {
      id: 'DE_B_PR_01',
      code: 'DE_B_PR_01',
      name: 'The Staging Hotel',
    },
    totalGrossAmount: { amount: 506, currency: 'EUR' },
    arrival: '2020-02-23T11:34:52+01:00',
    departure: '2020-03-01T11:00:00+01:00',
    created: '2020-03-18T11:34:46+01:00',
    modified: '2020-03-18T11:35:07+01:00',
    adults: 1,
    channelCode: 'Direct',
    primaryGuest: { lastName: 'd' },
    booker: { lastName: 'd' },
    guaranteeType: 'CreditCard',
    cancellationFee:
    {
      code: 'FLEX',
      name: 'Flexible 24h',
      description: 'Flexible 24h',
      dueDateTime: '2020-03-18T11:34:46+01:00',
      fee: { amount: 95, currency: 'EUR' },
    },
    noShowFee: { code: 'NOSHOW', fee: { amount: 95, currency: 'EUR' } },
    balance: { amount: -95, currency: 'EUR' },
    allFoliosHaveInvoice: false,
    hasCityTax: false,
    commission: {
      commissionAmount: {
        amount: 158.4,
      },
    },
    externalCode: '2310332407|988',
  } as unknown as ReservationItemModel,
  {
    id: 'MIYJRCIL-1',
    bookingId: 'MIYJRCIL',
    status: 'CheckedOut',
    checkInTime: '2020-03-18T11:34:52+01:00',
    checkOutTime: '2020-03-18T11:35:07+01:00',
    property:
    {
      id: 'DE_B_PR_01',
      code: 'DE_B_PR_01',
      name: 'The Staging Hotel',
    },
    totalGrossAmount: { amount: 0, currency: 'EUR' },
    arrival: '2020-03-10T11:34:52+01:00',
    departure: '2020-03-12T11:00:00+01:00',
    created: '2020-03-18T11:34:46+01:00',
    modified: '2020-03-18T11:35:07+01:00',
    adults: 1,
    channelCode: 'Direct',
    primaryGuest: { lastName: 'd' },
    booker: { lastName: 'd' },
    guaranteeType: 'CreditCard',
    cancellationFee:
    {
      code: 'FLEX',
      name: 'Flexible 24h',
      description: 'Flexible 24h',
      dueDateTime: '2020-03-18T11:34:46+01:00',
      fee: { amount: 95, currency: 'EUR' },
    },
    noShowFee: { code: 'NOSHOW', fee: { amount: 95, currency: 'EUR' } },
    balance: { amount: -95, currency: 'EUR' },
    allFoliosHaveInvoice: false,
    hasCityTax: false,
    commission: {
      commissionAmount: {
        amount: 10,
      },
    },
    externalCode: '3176848876|988',
  } as unknown as ReservationItemModel,
  {
    id: 'MIYJRCIL-10',
    bookingId: 'MIYJRCIL',
    status: 'CheckedOut',
    checkInTime: '2020-03-18T11:34:52+01:00',
    checkOutTime: '2020-03-18T11:35:07+01:00',
    property:
    {
      id: 'DE_B_PR_01',
      code: 'DE_B_PR_01',
      name: 'The Staging Hotel',
    },
    totalGrossAmount: { amount: 0, currency: 'EUR' },
    arrival: '2020-03-10T11:34:52+01:00',
    departure: '2020-03-12T11:00:00+01:00',
    created: '2020-03-18T11:34:46+01:00',
    modified: '2020-03-18T11:35:07+01:00',
    adults: 1,
    channelCode: 'Direct',
    primaryGuest: { lastName: 'd' },
    booker: { lastName: 'd' },
    guaranteeType: 'CreditCard',
    cancellationFee:
    {
      code: 'FLEX',
      name: 'Flexible 24h',
      description: 'Flexible 24h',
      dueDateTime: '2020-03-18T11:34:46+01:00',
      fee: { amount: 95, currency: 'EUR' },
    },
    noShowFee: { code: 'NOSHOW', fee: { amount: 95, currency: 'EUR' } },
    balance: { amount: -95, currency: 'EUR' },
    allFoliosHaveInvoice: false,
    hasCityTax: false,
    commission: {
      commissionAmount: {
        amount: 10,
      },
    },
    externalCode: '3176841876|988',
  } as unknown as ReservationItemModel,
  {
    id: 'MIYJRCIL-12',
    bookingId: 'MIYJRCIL',
    status: 'CheckedOut',
    checkInTime: '2020-03-18T11:34:52+01:00',
    checkOutTime: '2020-03-18T11:35:07+01:00',
    property:
    {
      id: 'DE_B_PR_01',
      code: 'DE_B_PR_01',
      name: 'The Staging Hotel',
    },
    totalGrossAmount: { amount: 0, currency: 'EUR' },
    arrival: '2020-03-10T11:34:52+01:00',
    departure: '2020-03-12T11:00:00+01:00',
    created: '2020-03-18T11:34:46+01:00',
    modified: '2020-03-18T11:35:07+01:00',
    adults: 1,
    channelCode: 'Direct',
    primaryGuest: { lastName: 'd' },
    booker: { lastName: 'd' },
    guaranteeType: 'CreditCard',
    cancellationFee:
    {
      code: 'FLEX',
      name: 'Flexible 24h',
      description: 'Flexible 24h',
      dueDateTime: '2020-03-18T11:34:46+01:00',
      fee: { amount: 95, currency: 'EUR' },
    },
    noShowFee: { code: 'NOSHOW', fee: { amount: 95, currency: 'EUR' } },
    balance: { amount: -95, currency: 'EUR' },
    allFoliosHaveInvoice: false,
    hasCityTax: false,
    commission: {
      commissionAmount: {
        amount: 10,
      },
    },
    externalCode: '2169804161|15',
  } as unknown as ReservationItemModel,
];

export const APALEO_RESERVATIONS_BY_TIME: ReservationListModel = {
  reservations: APALEO_RESERVATIONS_PER_MONTH,
  count: APALEO_RESERVATIONS_PER_MONTH.length,
};
