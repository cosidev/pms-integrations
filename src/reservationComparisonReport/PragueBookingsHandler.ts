import { generalConfig } from './../config';
import { DynamoClient } from './../common/aws/dynamoClient';
import { Injectable } from '@nestjs/common';
import logger from '../LoggingService';

interface PragueReservationData {
  oldGrossAmount: number;
  newGrossAmount: number;

  [key: string]: any;
}

interface PragueBookings {
  [key: string]: PragueReservationData;
}

interface IQueryKeys {
  [key: string]: string;
}

@Injectable()
export class PragueBookingsHandler {
  constructor(
    private readonly dynamoClient: DynamoClient,
  ) { }

  public async getPragueBookings(propertyId: string, reservationIds: string[]): Promise<PragueBookings> {
    if (!this.isPragueProperty(propertyId) || !reservationIds.length) {
      return {};
    }

    const pragueBookingsData = await this.fetchReservationsGross(reservationIds);

    const formattedBookings: PragueBookings = {};
    pragueBookingsData.forEach(booking => {
      formattedBookings[booking.dataKey] = {
        oldGrossAmount: booking.oldGrossAmount,
        newGrossAmount: booking.newGrossAmount,
      };
    });

    return formattedBookings;
  }

  public isPragueProperty(propertyId: string): boolean {
    const PRAGUE_PROPERTIES = ['CZ_PR_002', 'CZ_PR_003', 'CZ_PR_004'];

    if (!PRAGUE_PROPERTIES.includes(propertyId)) {
      return false;
    }

    return true;
  }

  private async fetchReservationsGross(reservationIds: string[]): Promise<PragueReservationData[]> {
    const table = generalConfig.aws.dataDynamoTableName;
    const requestItems = this.buildQueryKeys(reservationIds);
    let reservations = [];

    try {
      while (requestItems.length) {
        const batchRequestItems = requestItems.splice(0, 90);
        logger.infoLog('Fetching records from dynamodb for batch', batchRequestItems);
        const reservationResponse = await this.dynamoClient.batchGetItems<PragueReservationData>(
          table, batchRequestItems,
        ) || [];

        reservations = [...reservations, ...reservationResponse];
      }

      return reservations;
    } catch (error) {
      logger.errorLog(
        `Could not retrieve batch reservations for ${requestItems}`, { error },
      );
      throw new Error('Could not retrieve batch reservations for specified requestItems');
    }
  }

  private buildQueryKeys(reservationIds: string[]): IQueryKeys[] {
    return reservationIds.map(id => {
      return {
        dataKey: id,
        sKey: 'prague_currency_conversion',
      };
    });
  }
}
