import { lastDayOfMonth, startOfMonth, isSameDay } from 'date-fns';
import { Injectable } from '@nestjs/common';
import { AirbnbBookingsHandler } from './AirbnbBookingsHandler';
import { PragueBookingsHandler } from './PragueBookingsHandler';
import {
  ReservationsDifferentOnApaleo,
  ApaleoReservationsMap,
  IsSameReservation,
  ValidateArrivalAndDeparture,
  GeneratedReport,
  IApaleoReservationField,
  CreateApaleoReservationsMap,
  ApaleoReservationsWithFolioCharges,
} from './reservationComparisonReport.types';
import { ChannelReservationFormatter, ChannelReservation } from './ChannelReservationFormatter';
import { ApaleoClient } from './../apaleo/ApaleoClient';
import { ComparisonReportDto } from './dto';
import logger from '../LoggingService';
import { ReservationItemModel, ReservationListModel } from '../apaleo/generated/reservation';
import { EnumBookingReservationModelChannelCode } from './../apaleo/generated/reservation';

interface IndexSignature {
  [key: string]: string[];
}

@Injectable()
export class ReservationComparisonReportService {
  constructor(
    private readonly apaleoClient: ApaleoClient,
    private readonly channelReservationFormatter: ChannelReservationFormatter,
    private readonly pragueBookingsHandler: PragueBookingsHandler,
    private readonly airbnbBookingsHandler: AirbnbBookingsHandler,
  ) { }

  public async generateReport(data: ComparisonReportDto): Promise<GeneratedReport> {
    const { propertyId, source } = data;
    const reservationWithDeparture = data.reservations.find((reservation) => {
      if (!reservation) {
        return false;
      }

      return !!reservation.departure;
    });

    if (!reservationWithDeparture || !reservationWithDeparture.departure) {
      return {
        sameReservations: [],
        reservationsDifferentOnApaleo: [],
        reservationsAbsentOnApaleo: [],
        apaleoReservationsAbsentInCSV: [],
      };
    }

    const { departure } = reservationWithDeparture;
    const apaleoReservations: ReservationItemModel[] = await this.fetchReservationByFilter(departure, data);
    const channelReservations: ChannelReservation[] = this.channelReservationFormatter.format(data);
    const report: GeneratedReport = await this.compareReservations(channelReservations, apaleoReservations, propertyId, source);

    return report;
  }

  private async fetchReservationByFilter(departure: string, data: ComparisonReportDto): Promise<ReservationItemModel[]> {
    const { channelCode } = data;
    const {
      filter, status, dateFilter, firstDayOfMonth, lastDayOfMonthValue,
    } = this.buildQueryFilter(departure, data);

    try {
      logger.infoLog(`Fetching reservations between ${firstDayOfMonth} and ${lastDayOfMonth}`, {
        ...filter,
        status,
        dateFilter,
      });
      const reservations: ReservationListModel = await this.apaleoClient.getReservationsbyTime(
        status, dateFilter, firstDayOfMonth, lastDayOfMonthValue, filter,
      );

      return reservations.reservations || [];
    } catch (error) {
      logger.errorLog(
        `Could not retrieve reservations for ${channelCode} between ${firstDayOfMonth} and ${lastDayOfMonthValue}`, { error },
      );
    }
  }

  private buildQueryFilter(departure: string, data: ComparisonReportDto) {
    const { channelCode, propertyId, source, reservations } = data;
    const STATUS = [];
    const DATE_FILTER = 'Departure';

    const departureToDate = new Date(departure);
    departureToDate.setUTCHours(0, 0, 0);

    const firstDayOfMonth = startOfMonth(departureToDate);
    const lastDayOfMonthValue = lastDayOfMonth(departureToDate);
    lastDayOfMonthValue.setUTCHours(23, 59);

    const filter: IndexSignature = {
      channelCode: [channelCode],
      propertyIds: [propertyId],
    };

    if (source) {
      filter.source = [source];
    }

    if (this.pragueBookingsHandler.isPragueProperty(propertyId)) {
      filter.channelCode.push(EnumBookingReservationModelChannelCode.ChannelManager);
    }

    if (this.airbnbBookingsHandler.isAirbnbBooking(source)) {
      const propertyIds = this.airbnbBookingsHandler.getPropertyIds(reservations);
      filter.propertyIds = propertyIds;
    }

    return {
      filter,
      status: STATUS,
      dateFilter: DATE_FILTER,
      firstDayOfMonth,
      lastDayOfMonthValue,
    };
  }

  private async compareReservations(
    channelReservations: ChannelReservation[],
    apaleoReservations: ReservationItemModel[],
    propertyId: string,
    source: string,
  ): Promise<GeneratedReport> {
    const sameReservations: ChannelReservation[] = [];
    const reservationsAbsentOnApaleo: ChannelReservation[] = [];
    const reservationsDifferentOnApaleo: ReservationsDifferentOnApaleo[] = [];

    const { apaleoReservationsMap, reservationIds } = this.createApaleoReservationsMap(apaleoReservations, source);
    const apaleoReservationsWithFolioCharges = await this.appendFolioChargesToReservations(reservationIds, apaleoReservationsMap, propertyId);

    for (const channelReservation of channelReservations) {
      const { channelExternalCode } = channelReservation;
      const apaleoReservation: ApaleoReservationsWithFolioCharges = apaleoReservationsWithFolioCharges[channelExternalCode];

      if (!apaleoReservation) {
        reservationsAbsentOnApaleo.push(channelReservation);
        continue;
      }

      const { isSameReservation, errors } = this.isSameReservation(apaleoReservation, channelReservation);
      if (isSameReservation) {
        sameReservations.push(channelReservation);
      } else {
        reservationsDifferentOnApaleo.push({
          channelReservation,
          apaleoReservation,
          errors,
        });
      }

      delete apaleoReservationsMap[channelExternalCode];
    }

    return {
      sameReservations,
      reservationsDifferentOnApaleo: Object.values(reservationsDifferentOnApaleo),
      reservationsAbsentOnApaleo,
      apaleoReservationsAbsentInCSV: Object.values(apaleoReservationsMap),
    };
  }

  private createApaleoReservationsMap(
    apaleoReservations: ReservationItemModel[],
    source: string,
  ): CreateApaleoReservationsMap {
    const apaleoReservationsMap: ApaleoReservationsMap = {};
    const reservationIds = [];

    for (const reservation of apaleoReservations) {
      if (source && reservation.source !== source) {
        continue;
      }

      const formattedExternalCode =
        this.formatApaleoExternalCode(reservation.externalCode || '');
      const identifier = formattedExternalCode || reservation.id;
      const existingReservation = apaleoReservationsMap[identifier];
      if (existingReservation) {
        const duplicateReservation = this.selectApaleoFields(reservation);

        apaleoReservationsMap[identifier] = {
          ...existingReservation,
          totalGrossAmount: existingReservation.totalGrossAmount + duplicateReservation.totalGrossAmount,
          commissionAmount: existingReservation.commissionAmount + duplicateReservation.commissionAmount,
        };
      } else {
        apaleoReservationsMap[identifier] = this.selectApaleoFields(reservation);
      }

      reservationIds.push(reservation.id);
    }

    return {
      apaleoReservationsMap,
      reservationIds,
    };
  }

  private async appendFolioChargesToReservations(
    reservationIds: string[],
    apaleoReservationsMap: ApaleoReservationsMap,
    propertyId: string,
  ): Promise<Partial<ApaleoReservationsWithFolioCharges>> {
    const apaleoReservationsWithFolioCharges: Partial<ApaleoReservationsWithFolioCharges> = {};

    const pragueBookings = await this.pragueBookingsHandler.getPragueBookings(propertyId, reservationIds);
    const formattedFolios = await this.getFolios(reservationIds);

    Object.entries(apaleoReservationsMap).forEach(([key, value]) => {
      const totalFolioCharge = formattedFolios[value.id] || 0;
      apaleoReservationsWithFolioCharges[key] = {
        ...value,
        totalFolioCharge,
      };

      const pragueBooking = pragueBookings[value.id];
      if (pragueBooking) {
        const { oldGrossAmount } = pragueBooking;
        if (apaleoReservationsWithFolioCharges[key].totalGrossAmount === totalFolioCharge) {
          apaleoReservationsWithFolioCharges[key].totalFolioCharge = oldGrossAmount;
        }

        apaleoReservationsWithFolioCharges[key].totalGrossAmount = oldGrossAmount;
      }
    });

    return apaleoReservationsWithFolioCharges;
  }

  private async getFolios(reservationIds: string[]): Promise<any> {
    let folios = [];

    try {
      logger.infoLog(`Fetching folios for`, reservationIds);
      const foliosResponse = await this.apaleoClient.getFolios({
        expand: ['reservation', 'charges'],
        reservationIds,
      });

      folios = foliosResponse.folios || [];
    } catch (error) {
      logger.errorLog(
        `Could not retrieve folios for ${reservationIds}`, { error },
      );
    }

    const formattedFolios = {};
    folios.forEach((folio: any) => {
      const ACCUMULATOR = 0;

      const chargeReducer = (accumulator: number, charge: any) => accumulator + charge?.amount?.grossAmount;
      const charges: any[] = folio?.charges || [];
      const totalFolioCharge = charges.reduce(chargeReducer, ACCUMULATOR);

      formattedFolios[folio.reservation.id] = totalFolioCharge;
    });

    return formattedFolios;
  }

  private formatApaleoExternalCode(externalCode: string): string {
    const EXTERNAL_CODE_SEP = '|';
    const formattedExternalCode = (externalCode).split(EXTERNAL_CODE_SEP)[0];
    return formattedExternalCode;
  }

  private selectApaleoFields(apaleoReservations: ReservationItemModel): IApaleoReservationField {
    const {
      id, bookingId, status = '', totalGrossAmount, arrival = '', departure = '',
      primaryGuest, commission, externalCode = '',
    } = apaleoReservations;

    return {
      id,
      bookingId,
      status,
      totalGrossAmount: totalGrossAmount?.amount,
      arrival: arrival as Partial<string>,
      departure: departure as Partial<string>,
      primaryGuest: `${primaryGuest.firstName || ''} ${primaryGuest.lastName || ''}`.trim(),
      commissionAmount: commission?.commissionAmount?.amount,
      externalCode,
    };
  }

  private isSameReservation(
    apaleoReservation: ApaleoReservationsWithFolioCharges, channelReservation: ChannelReservation,
  ): IsSameReservation {
    const { channelCommissionAmount, channelTotalGrossAmount } = channelReservation;
    const { totalGrossAmount, commissionAmount, status, totalFolioCharge } = apaleoReservation;
    let apaleoCommissionAmount = commissionAmount;
    let apaleoTotalGrossAmount = totalGrossAmount;

    const CANCELED = 'canceled';
    if (status.toLowerCase() === CANCELED) {
      if (channelCommissionAmount === 0) {
        apaleoCommissionAmount = 0;
      }

      if (channelTotalGrossAmount === 0) {
        apaleoTotalGrossAmount = 0;
      }
    } else {
      apaleoTotalGrossAmount = totalFolioCharge;
    }

    const isCommissionValid = channelCommissionAmount - apaleoCommissionAmount < 1;
    const totalGrossInvalid = apaleoTotalGrossAmount !== channelTotalGrossAmount &&
      (apaleoTotalGrossAmount - channelTotalGrossAmount > 1 &&
        apaleoTotalGrossAmount - channelTotalGrossAmount > -1);

    const { arrivalIsSameDay, departureIsSameDay } =
      this.validateArrivalAndDeparture(channelReservation, apaleoReservation);
    const validations = {
      totalGrossInvalid,
      commissionInvalid: !isCommissionValid,
      arrivalInvalid: !arrivalIsSameDay,
      departureInvalid: !departureIsSameDay,
    };

    const errors: string[] = Object.keys(validations).filter(error => validations[error]);
    if (errors.length) {
      return {
        isSameReservation: false,
        errors,
      };
    }

    return {
      isSameReservation: true,
      errors,
    };
  }

  private validateArrivalAndDeparture(
    channelReservation: ChannelReservation, apaleoReservation: IApaleoReservationField,
  ): ValidateArrivalAndDeparture {
    const TIME_ZONE_SEP = 'T';
    const {
      channelArrival, channelDeparture,
    } = channelReservation;

    const {
      arrival: arrivalOnApaleo, departure: departureOnApaleo,
    } = apaleoReservation;

    const arrivalOnApaleoWithoutTimeZone = String(arrivalOnApaleo).split(TIME_ZONE_SEP)[0];
    const departureOnApaleoWithoutTimeZone = String(departureOnApaleo).split(TIME_ZONE_SEP)[0];

    const arrivalIsSameDay = isSameDay(new Date(channelArrival), new Date(arrivalOnApaleoWithoutTimeZone));
    const departureIsSameDay = isSameDay(new Date(channelDeparture), new Date(departureOnApaleoWithoutTimeZone));

    return {
      arrivalIsSameDay,
      departureIsSameDay,
    };
  }
}
