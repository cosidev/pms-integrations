import { ChannelReservation } from './ChannelReservationFormatter';

export interface ApaleoReservationsMap {
  [key: string]: IApaleoReservationField;
}

export interface CreateApaleoReservationsMap {
  apaleoReservationsMap: ApaleoReservationsMap;
  reservationIds: string[];
}

export interface ApaleoReservationsWithFolioCharges extends IApaleoReservationField {
  totalFolioCharge: number;
}

export interface ReservationsDifferentOnApaleo {
  channelReservation: ChannelReservation;
  apaleoReservation: ApaleoReservationsWithFolioCharges;
  errors: string[];

  [key: string]: object;
}

export interface GeneratedReport {
  sameReservations: ChannelReservation[];
  reservationsDifferentOnApaleo: ReservationsDifferentOnApaleo[];
  reservationsAbsentOnApaleo: ChannelReservation[];
  apaleoReservationsAbsentInCSV: IApaleoReservationField[];
}

export interface ValidateArrivalAndDeparture {
  arrivalIsSameDay: boolean;
  departureIsSameDay: boolean;
}

export interface IsSameReservation {
  isSameReservation: boolean;
  errors: string[];
}

export interface IApaleoReservationField {
  id: string;
  bookingId: string;
  status: string;
  totalGrossAmount: number;
  arrival: string;
  departure: string;
  primaryGuest: string;
  commissionAmount: number;
  externalCode: string;
}

export enum BookingSource {
  'Airbnb' = 'Airbnb',
}
