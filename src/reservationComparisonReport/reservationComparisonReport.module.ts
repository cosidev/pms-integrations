import { AirbnbBookingsHandler } from './AirbnbBookingsHandler';
import { DynamoClient } from './../common/aws/dynamoClient';
import { PragueBookingsHandler } from './PragueBookingsHandler';
import { ChannelReservationFormatter } from './ChannelReservationFormatter';
import { ReservationComparisonReportService } from './reservationComparisonReport.service';
import { ApaleoModule } from './../apaleo/apaleo.module';
import { Module } from '@nestjs/common';
import { ReservationComparisonReportController } from './reservationComparisonReport.controller';

@Module({
    imports: [ApaleoModule],
    controllers: [ReservationComparisonReportController],
    providers: [
        ReservationComparisonReportService,
        ChannelReservationFormatter,
        PragueBookingsHandler,
        DynamoClient,
        AirbnbBookingsHandler,
    ],
})
export class ReservationComparisonReportModule { }
