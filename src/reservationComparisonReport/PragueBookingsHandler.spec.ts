import { ReservationComparisonReportModule } from './reservationComparisonReport.module';
import { DynamoClient } from './../common/aws/dynamoClient';
import { PragueBookingsHandler } from './PragueBookingsHandler';
import { Test, TestingModule } from '@nestjs/testing';
import { BATCH_GET_RESPONSE, FORMATTED_DYNAMO_RESPONSE } from './__mocks__/prague';

jest.mock('../LoggingService.ts');

describe('PragueBookingsHandler', () => {
  let dynamoClient: DynamoClient;
  let pragueBookingsHandler: PragueBookingsHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ReservationComparisonReportModule],
    }).compile();

    dynamoClient = module.get<DynamoClient>(DynamoClient);
    pragueBookingsHandler = new PragueBookingsHandler(dynamoClient);
  });

  it('should successfully validate a prague property', (): void => {
    const isValid = pragueBookingsHandler.isPragueProperty('CZ_PR_003');

    expect(isValid).toBeTruthy();
  });

  it('should fail prague property validation if property is invalid', (): void => {
    const isValid = pragueBookingsHandler.isPragueProperty('DE_DE_003');

    expect(isValid).toBeFalsy();
  });

  it('should return an empty object if property is not a prague property', async (): Promise<void> => {
    const pragueBookings = await pragueBookingsHandler.getPragueBookings('DE_DE_003', []);

    expect(pragueBookings).toEqual({});
  });

  it('should return an empty object if reservationIds are empty', async (): Promise<void> => {
    const pragueBookings = await pragueBookingsHandler.getPragueBookings('CZ_PR_003', []);

    expect(pragueBookings).toEqual({});
  });

  it('should fetch and format gross information from dynamodb', async (): Promise<void> => {
    jest.spyOn(dynamoClient, 'batchGetItems')
      .mockImplementation(() => new Promise(resolve => resolve(BATCH_GET_RESPONSE)));

    const RESERVATION_IDS = ['VV2Q459-1', 'EV9R452-1'];
    const pragueBookings = await pragueBookingsHandler.getPragueBookings('CZ_PR_003', RESERVATION_IDS);

    expect(pragueBookings).toEqual(FORMATTED_DYNAMO_RESPONSE);
    expect(RESERVATION_IDS).toEqual(Object.keys(FORMATTED_DYNAMO_RESPONSE));
  });

  it('should return an empty object if information is not on dynamodb', async (): Promise<void> => {
    jest.spyOn(dynamoClient, 'batchGetItems')
      .mockImplementation(() => new Promise(resolve => resolve([])));

    const pragueBookings = await pragueBookingsHandler.getPragueBookings('CZ_PR_003', ['IV2Q459-1', '2V9R452-1']);

    expect(pragueBookings).toEqual({});
  });

  it('should correctly handle error during data retrieval', async (): Promise<void> => {
    jest.spyOn(dynamoClient, 'batchGetItems')
      .mockImplementation(() => new Promise((_, reject) => reject()));

    try {
      await pragueBookingsHandler.getPragueBookings('CZ_PR_003', ['IV2Q459-1', '2V9R452-1']);
    } catch (e) {
      expect(e).toEqual(new Error(
        'Could not retrieve batch reservations for specified requestItems',
      ));
    }
  });
});
