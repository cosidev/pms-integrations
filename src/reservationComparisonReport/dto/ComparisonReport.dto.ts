import { BookingSource } from '../reservationComparisonReport.types';
import { IsArray, IsEnum, IsString, IsOptional } from 'class-validator';
import { EnumBookingReservationModelChannelCode } from '../../apaleo/generated/reservation';

export default class ComparisonReportDto {
  @IsEnum(EnumBookingReservationModelChannelCode)
  channelCode: EnumBookingReservationModelChannelCode;

  @IsEnum(BookingSource)
  @IsOptional()
  source: BookingSource;

  @IsString()
  propertyId: string;

  @IsArray()
  reservations: any[];
}
