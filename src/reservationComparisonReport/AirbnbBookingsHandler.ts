import { Injectable } from '@nestjs/common';

@Injectable()
export class AirbnbBookingsHandler {
  public getPropertyIds(reservations: any[]): string[] {
    const propertyIds = {};

    reservations.forEach(reservation => {
      const propertyId = reservation.property_name;

      propertyIds[propertyId] = true;
    });

    return Object.keys(propertyIds);
  }

  public isAirbnbBooking(source: string): boolean {
    const AIRBNB = 'airbnb';

    if (source && source.toLowerCase() === AIRBNB) {
      return true;
    }

    return false;
  }
}
