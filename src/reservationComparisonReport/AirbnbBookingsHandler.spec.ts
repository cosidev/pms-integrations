import { AIRBNB_RESERVATIONS } from './__mocks__/airbnb';
import { AirbnbBookingsHandler } from './AirbnbBookingsHandler';
import { Test, TestingModule } from '@nestjs/testing';

describe('AirbnbBookingsHandler', () => {
  let airbnbBookingsHandler: AirbnbBookingsHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AirbnbBookingsHandler],
    }).compile();

    airbnbBookingsHandler = module.get<AirbnbBookingsHandler>(AirbnbBookingsHandler);
  });

  it('should compute a list of reservation propertyIds', (): void => {
    const propertyIds = airbnbBookingsHandler.getPropertyIds(AIRBNB_RESERVATIONS);
    expect(propertyIds).toEqual([
      'DE_LE_001',
      'DE_BE_001',
      'DE_MU_001',
    ]);
  });

  it('should validate airbnb bookings', (): void => {
    let isAirbnbBooking = airbnbBookingsHandler.isAirbnbBooking('airbnb');
    expect(isAirbnbBooking).toBeTruthy();

    isAirbnbBooking = airbnbBookingsHandler.isAirbnbBooking('Booking.com');
    expect(isAirbnbBooking).toBeFalsy();
  });
});
