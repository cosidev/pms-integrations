import { Injectable } from '@nestjs/common';
import { ComparisonReportDto } from './dto';

export interface ChannelReservation {
  channelExternalCode: string;
  channelArrival: string;
  channelDeparture: string;
  channelTotalGrossAmount: number;
  channelCommissionAmount: number;
  currency?: string;
  bookerName?: string;
  guestName?: string;
}

@Injectable()
export class ChannelReservationFormatter {
  public format(data: ComparisonReportDto): ChannelReservation[] {
    const reservations = data.reservations;

    return reservations.map(reservation => {
      const {
        reservation_number,
        arrival = '',
        departure = '',
        final_amount = 0,
        commission_amount,
        currency,
        booker_name = '',
        guest_name = '',
      } = reservation;

      const commissionAmountTofixed = (commission_amount && commission_amount.toFixed(2));

      return {
        channelExternalCode: String(reservation_number),
        channelArrival: arrival,
        channelDeparture: departure,
        channelTotalGrossAmount: final_amount,
        channelCommissionAmount: Number(commissionAmountTofixed),
        currency,
        bookerName: booker_name,
        guestName: guest_name || '',
      };
    });
  }
}
