import { AirbnbBookingsHandler } from './AirbnbBookingsHandler';
import { DynamoClient } from './../common/aws/dynamoClient';
import { PragueBookingsHandler } from './PragueBookingsHandler';
import { ChannelReservationFormatter } from './ChannelReservationFormatter';
import { ReservationComparisonReportService } from './reservationComparisonReport.service';
import { ApaleoModule } from './../apaleo/apaleo.module';
import { Test, TestingModule } from '@nestjs/testing';
import { ReservationComparisonReportController } from './reservationComparisonReport.controller';

describe('ReservationComparisonReport Controller', () => {
  let controller: ReservationComparisonReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApaleoModule],
      controllers: [ReservationComparisonReportController],
      providers: [
        ReservationComparisonReportService,
        ChannelReservationFormatter,
        PragueBookingsHandler,
        DynamoClient,
        AirbnbBookingsHandler,
      ],
    }).compile();

    controller = module.get<ReservationComparisonReportController>(ReservationComparisonReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
