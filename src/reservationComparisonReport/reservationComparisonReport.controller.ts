import { ReservationComparisonReportService } from './reservationComparisonReport.service';
import { JwtAuth } from './../common/auth/jwtAuth.decorator';
import { Controller, Body, Post } from '@nestjs/common';
import ComparisonReportDto from './dto/ComparisonReport.dto';

@Controller('reservationComparisonReport')
export class ReservationComparisonReportController {
  constructor(private readonly reportService: ReservationComparisonReportService) { }

  @Post('/')
  @JwtAuth('reservationComparisonReport')
  report(@Body() data: ComparisonReportDto) {
    return this.reportService.generateReport(data);
  }
}
