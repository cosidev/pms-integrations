import { BookingSource } from './reservationComparisonReport.types';
import { FORMATTED_BOOKING_COM_RESERVATIONS } from './__mocks__/bookingComFormattedReservations';
import { ChannelReservationFormatter } from './ChannelReservationFormatter';
import { BOOKING_COM_RESERVATIONS } from './__mocks__/bookingComReservation';
import { Test, TestingModule } from '@nestjs/testing';
import { EnumBookingReservationModelChannelCode } from '../apaleo/generated/reservation';

interface ChannelReservation {
  channelExternalCode: string;
  channelArrival: string;
  channelDeparture: string;
  channelTotalGrossAmount: number;
  channelCommissionAmount: number;
  currency?: string;
  bookerName?: string;
  guestName?: string;
}

describe('ChannelReservationFormatter', () => {
  let channelReservationFormatter: ChannelReservationFormatter;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ChannelReservationFormatter],
    }).compile();

    channelReservationFormatter = module.get<ChannelReservationFormatter>(ChannelReservationFormatter);
  });

  it('should accurately format booking.com reservations', (): void => {
    const formattedData: ChannelReservation[] = channelReservationFormatter.format({
      channelCode: EnumBookingReservationModelChannelCode.BookingCom,
      propertyId: '',
      reservations: BOOKING_COM_RESERVATIONS,
      source: '' as BookingSource,
    });

    expect(formattedData).toEqual(FORMATTED_BOOKING_COM_RESERVATIONS);
  });

  it('should return an empty array if the csv record is empty', (): void => {
    const formattedData: ChannelReservation[] = channelReservationFormatter.format({
      channelCode: EnumBookingReservationModelChannelCode.Direct,
      propertyId: '',
      reservations: [],
      source: '' as BookingSource,
    });

    expect(formattedData).toEqual([]);
  });
});
