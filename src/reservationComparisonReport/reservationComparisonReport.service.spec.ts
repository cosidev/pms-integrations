import { Test, TestingModule } from '@nestjs/testing';
import { ReservationComparisonReportModule } from './reservationComparisonReport.module';
import { AirbnbBookingsHandler } from './AirbnbBookingsHandler';
import { DynamoClient } from './../common/aws/dynamoClient';
import {
  FORMATTED_BOOKING_COM_RESERVATIONS,
  RESERVATION_COMPARISON_REPORT,
  BOOKING_COM_RESERVATIONS,
  APALEO_RESERVATIONS_BY_TIME,
  BATCH_GET_RESPONSE,
  APALEO_PRAGUE_BOOKINGS_BY_TIME,
  FORMATTED_DYNAMO_RESPONSE,
  PRAGUE_RESERVATIONS,
  PRAGUE_REPORT,
} from './__mocks__/reservationComparisonReport';
import { ApaleoClient } from './../apaleo/ApaleoClient';
import { ReservationComparisonReportService } from './reservationComparisonReport.service';
import { EnumBookingReservationModelChannelCode } from '../apaleo/generated/reservation';
import { PragueBookingsHandler } from './PragueBookingsHandler';
import { ComparisonReportDto } from './dto';
import { BookingSource } from './reservationComparisonReport.types';
import { ChannelReservationFormatter } from './ChannelReservationFormatter';

jest.mock('../LoggingService.ts');

describe('ReservationComparisonReportService', () => {
  let dynamoClient: DynamoClient;
  let apaleoClient: ApaleoClient;
  let pragueBookingsHandler: PragueBookingsHandler;
  let channelReservationFormatter: ChannelReservationFormatter;
  let airbnbBookingsHandler: AirbnbBookingsHandler;
  let reservationComparisonReportService: ReservationComparisonReportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ReservationComparisonReportModule],
    }).compile();

    dynamoClient = module.get<DynamoClient>(DynamoClient);
    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    channelReservationFormatter = module.get<ChannelReservationFormatter>(ChannelReservationFormatter);
    airbnbBookingsHandler = module.get<AirbnbBookingsHandler>(AirbnbBookingsHandler);
    pragueBookingsHandler = new PragueBookingsHandler(dynamoClient);
    reservationComparisonReportService = new ReservationComparisonReportService(
      apaleoClient, channelReservationFormatter, pragueBookingsHandler, airbnbBookingsHandler,
    );
  });

  describe('report generation', () => {
    it('should generate an accurate report after comparing reservations from channel with apaleo reservation', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_RESERVATIONS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: BOOKING_COM_RESERVATIONS,
        propertyId: '',
        source: '' as BookingSource,
      };

      const report = await reservationComparisonReportService.generateReport(postData);

      expect(report).toEqual(RESERVATION_COMPARISON_REPORT);
      expect(report.sameReservations.length).toBe(1);
    });

    it('should generate an empty report if channel reservation is empty', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_RESERVATIONS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: [],
        propertyId: '',
        source: '' as BookingSource,
      };

      const report = await reservationComparisonReportService.generateReport(postData);

      expect(report.sameReservations).toEqual([]);
      expect(report.reservationsDifferentOnApaleo).toEqual([]);
      expect(report.apaleoReservationsAbsentInCSV).toEqual([]);
      expect(report.reservationsAbsentOnApaleo).toEqual([]);
    });

    it('should mark report as reservationsAbsentOnApaleo if reservations from apaleo are empty', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve({
          reservations: [],
          count: 0,
        })));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: BOOKING_COM_RESERVATIONS,
        propertyId: '',
        source: '' as BookingSource,
      };

      const report = await reservationComparisonReportService.generateReport(postData);

      expect(report.sameReservations).toEqual([]);
      expect(report.reservationsDifferentOnApaleo).toEqual([]);
      expect(report.apaleoReservationsAbsentInCSV).toEqual([]);
      expect(report.reservationsAbsentOnApaleo).toEqual(FORMATTED_BOOKING_COM_RESERVATIONS);
    });
  });

  describe('prague property reports', () => {
    it('should skip batchGetItems call if prague property validation fails', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_PRAGUE_BOOKINGS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));
      jest.spyOn(dynamoClient, 'batchGetItems')
        .mockImplementation(() => new Promise(resolve => resolve(BATCH_GET_RESPONSE)));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: BOOKING_COM_RESERVATIONS,
        propertyId: 'DE_DE_003',
        source: '' as BookingSource,
      };

      await reservationComparisonReportService.generateReport(postData);

      expect(dynamoClient.batchGetItems).not.toHaveBeenCalled();
    });

    it('should fetch oldGrossAmount from dynamodb', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_PRAGUE_BOOKINGS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));
      jest.spyOn(dynamoClient, 'batchGetItems')
        .mockImplementation(() => new Promise(resolve => resolve(BATCH_GET_RESPONSE)));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: BOOKING_COM_RESERVATIONS,
        propertyId: 'CZ_PR_003',
        source: '' as BookingSource,
      };

      await reservationComparisonReportService.generateReport(postData);

      expect(dynamoClient.batchGetItems).toHaveBeenCalled();
    });

    it('should generate report with oldGrossAmounts from dynamodb', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_PRAGUE_BOOKINGS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));
      jest.spyOn(pragueBookingsHandler, 'getPragueBookings')
        .mockImplementation(() => new Promise(resolve => resolve(FORMATTED_DYNAMO_RESPONSE)));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: PRAGUE_RESERVATIONS,
        propertyId: 'CZ_PR_003',
        source: '' as BookingSource,
      };

      const report = await reservationComparisonReportService.generateReport(postData);

      const RESERVATION_IDS = ['VV2Q459-1', 'EV9R452-1', 'MIYJRCIL-10'];
      expect(pragueBookingsHandler.getPragueBookings).toHaveBeenCalledWith(
        postData.propertyId,
        RESERVATION_IDS,
      );
      expect(report).toEqual(PRAGUE_REPORT);
    });

    it('should fail if oldGrossAmounts from dynamodb is empty', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve(APALEO_PRAGUE_BOOKINGS_BY_TIME)));
      jest.spyOn(apaleoClient, 'getFolios')
        .mockImplementation(() => new Promise(resolve => resolve([])));
      jest.spyOn(pragueBookingsHandler, 'getPragueBookings')
        .mockImplementation(() => new Promise(resolve => resolve({})));

      const postData: ComparisonReportDto = {
        channelCode: EnumBookingReservationModelChannelCode.BookingCom,
        reservations: PRAGUE_RESERVATIONS,
        propertyId: 'CZ_PR_003',
        source: '' as BookingSource,
      };

      const report = await reservationComparisonReportService.generateReport(postData);

      const RESERVATION_IDS = ['VV2Q459-1', 'EV9R452-1', 'MIYJRCIL-10'];
      expect(pragueBookingsHandler.getPragueBookings).toHaveBeenCalledWith(
        postData.propertyId,
        RESERVATION_IDS,
      );
      expect(report).toEqual(PRAGUE_REPORT);
    });
  });
});
