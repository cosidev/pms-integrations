import { DynamoDB } from 'aws-sdk';
import { generalConfig } from './config';

export class DynamoClient {

  client: DynamoDB.DocumentClient;
  defaultTable: string;

  constructor() {
    const { aws } = generalConfig;
    this.client = new DynamoDB.DocumentClient({
      region: aws.dynamoRegion,
      endpoint: aws.dynamoEndpoint,
    });
    this.defaultTable = aws.dataDynamoTableName;
  }

  async getItem(dataKey: string, sKey: string) {
    return this.client.get({
      TableName: this.defaultTable,
      Key: { dataKey, sKey },
    }).promise();
  }

  async addItem(dataKey: string, sKey: string, values: any) {
    await this.client.put({
      TableName: this.defaultTable,
      Item: {
        dataKey,
        sKey,
        ...values,
      },
    }).promise();
  }

  async deleteItem(dataKey: string, sKey: string) {
    return this.client.delete({
      TableName: this.defaultTable,
      Key: { dataKey, sKey },
    }).promise();
  }
}
