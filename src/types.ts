import { ReservationModel, ReservationItemModel } from './apaleo/generated/reservation';

export enum MailType {
  Created = 'created',
  CheckIn = 'checkIn',
  CheckInDay = 'checkInDay',
  CheckOut = 'checkOut',
  Canceled = 'canceled',
  Invoice = 'invoice',
  InvoiceStatusChanged = 'invoiceStatusChanged',
  FolioChanged = 'folioChanged',
  ModifiedGuestData = 'modifiedGuestData',
  NeededGuestData = 'neededGuestData',
  BreakfastSelectionReminder = 'breakfastSelectionReminder',
  ChangeInvoiceAddress = 'changeInvoiceAddress',
}

export interface ManualEvent {
  reservationId: string;
  type: MailType;
}

export interface ManualMailEvent extends ManualEvent {
  email?: string;
  token: string;
}

export enum HousekeepingType {
  DailyInfos = 'dailyInfos',
  WeeklyInfos = 'weeklyInfos',
  Overview = 'overview',
  UnitCleaned = 'unitCleaned',
  CheckOutInfos = 'checkOutInfos',
}

export interface HousekeepingDateRequest {
  type: HousekeepingType.DailyInfos | HousekeepingType.DailyInfos | HousekeepingType.WeeklyInfos | HousekeepingType.CheckOutInfos;
  date: Date;
  propertyId: string;
}

export interface HousekeepingUnitCleanRequest {
  type: HousekeepingType.UnitCleaned;
  unitId: string;
}

export type HousekeepingRequest = HousekeepingDateRequest | HousekeepingUnitCleanRequest;

export interface ReservationNight {
  from: Date;
  to: Date;
  serviceDate: Date;
  rate: number;
  unitId: string;
  unitGroupId: string;
}

export type Language = 'en' | 'de';

export interface ReservationGuest {
  title: string;
  firstName: string;
  lastName: string;
  fullName: string;
  citizenship: string;
  idNumber: string;
  address: string;
  email: string;
}

export interface Reservation {
  reservationId: string;
  channelCode: string;
  source: string;
  status: string;
  bookingId: string;
  checkInTime: string;
  checkOutTime: string;
  language: Language;
  cancelationRules: string;
  cancellationTime?: Date;
  ratePlanCode: string;
  roomType: string;
  travelPurpose: string;
  comissionAmount: number;
  comissionCurrency: string;
  createdAt: Date;
  checkIn: Date;
  checkOut: Date;
  nights: ReservationNight[];
  booker: {
    male: boolean;
    female: boolean;
    email: string;
    firstName: string;
    lastName: string;
  };
  primaryGuest: ReservationGuest;
  additionalGuests: ReservationGuest[];
  unit: {
    unitId: string;
    unitGroupId: string;
    name: string;
    description: string;
  };
  adults: number;
  children: number;
  guestComment: string;
  property: {
    id: string;
    name: string;
    addressLine1: string;
    addressLine2: string;
    postalCode: string;
    city: string;
    countryCode: string;
    isBerlin: boolean;
    bookingEmail: string;
    bookingPage: string;
    additionalInformation: string;
    footer: string;
    signature: string;
    cmsId: string;
    breakfastRecommendations: string;
    parking: string;
  };
  totalGrossAmount: number;
  totalGrossCurrency: string;
}

export interface ReservationCreatedEvent {
  type: MailType.Created;
  data: Reservation;
  showCancelationRules: boolean;
  tripManagementUrl: string;
}

interface ReservationBaseCheckInEvent {
  data: Reservation;
  pinAccessCode: number;
  pinComfortCode: number;
  mainDoorCode?: number;
  earlyCheckInCode?: number;
  roomDirection: string;
  checkInOutInfos: string;
  roomNumber: string;
}

export interface ReservationCheckInEvent extends ReservationBaseCheckInEvent {
  type: MailType.CheckIn;
}

export interface ReservationCheckInDayEvent extends ReservationBaseCheckInEvent {
  type: MailType.CheckInDay;
}

export interface ReservationInvoiceStatusChangedEvent {
  type: MailType.InvoiceStatusChanged;
  data: Reservation;
}

export interface FolioChangedEvent {
  type: MailType.FolioChanged;
  data: Reservation;
}

export interface ReservationModifiedGuestDataEvent {
  type: MailType.ModifiedGuestData;
  data: Reservation;
}

export interface ReservationCheckOutEvent {
  type: MailType.CheckOut;
  data: Reservation;
}

export interface ReservationCanceledEvent {
  type: MailType.Canceled;
  data: Reservation;
  cancellationFee: number;
  cancellationCurrency: string;
}

export interface ReservationInvoiceEvent {
  type: MailType.Invoice;
  data: Reservation;
  invoices: string[];
}

export interface ReservationNeededGuestDataEvent {
  type: MailType.NeededGuestData;
  data: Reservation;
  neededDataUrl: string;
}

export interface ReservationBreakfastSelection {
  type: MailType.BreakfastSelectionReminder;
  data: Reservation;
  breakfastUrl: string;
}

export interface ReservationChangeInvoiceAddressEvent {
  type: MailType.ChangeInvoiceAddress;
  data: Reservation;
  changeInvoiceAddressUrl: string;
}

// tslint:disable-next-line: max-line-length
export type ReservationEvent =
  | ReservationCreatedEvent
  | ReservationCheckInEvent
  | ReservationCheckOutEvent
  | ReservationCanceledEvent
  | ReservationInvoiceEvent
  | ReservationCheckInDayEvent
  | ReservationInvoiceStatusChangedEvent
  | ReservationModifiedGuestDataEvent
  | ReservationNeededGuestDataEvent
  | ReservationChangeInvoiceAddressEvent
  | ReservationBreakfastSelection;

export type ApaleoReservation = ReservationModel | ReservationItemModel;

interface DatoInfoItem {
  id: string;
  updatedAt: string;
  createdAt: string;
}

interface CmsRoom {
  /** cms id */
  id: string;
  /** pms id */
  rid: string;
  directions: string;
  checkInCheckOutDetailsPerRoom?: string;
}

export interface Translated<T> {
  de: T;
  en: T;
}

export interface CmsProperty extends DatoInfoItem {
  propertyId: string;
  bookingMail: string;
  bookingPage: string;
  signature: Translated<string>;
  additionalInformation: Translated<string>;
  emailFooter: Translated<string>;
  checkInAndOutDetails: Translated<string>;
  rooms: Translated<{ [key: string]: CmsRoom }>;
  availableServices?: string[];
  parking: Translated<string>;
  breakfastRecommendations: Translated<string>;
}

export interface SpecialWish {
  name: string;
  count?: number;
}

export interface BreakfastSelection {
  date: Date;
  selection: {
    [key: string]: number;
  };
}

export interface CmsReservation extends DatoInfoItem {
  reservationId: string;
  bookingConfirmationSent: boolean;
  invoiceSent: boolean;
  checkinInfosSent: boolean;
  checkoutInfosSent: boolean;
  cancelConfirmationSent: boolean;
  pinAccessCode?: string;
  pinComfortCode?: string;
  allergies: string;
  bookerName: string;
  specialWishes: SpecialWish[];
  breakfastSelection: BreakfastSelection[];
}

export interface DashButtonEvent {
  deviceInfo: {
    deviceId: string;
    type: string;
    remainingLife: number;
  };
  deviceEvent: {
    buttonClicked: {
      clickType: string;
      reportedTime: Date;
    };
  };
  placementInfo: {
    projectName: string;
    placementName: string;
    attributes: {
      propertyId: string;
      unitId: string;
      room: string;
    };
  };
}

export interface FlicButtonHeaders {
  'User-Agent': string;
  'button-battery-level': string;
  'button-name': string;
  'Content-Type': string;
  Host: string;
  Connection: string;
  'Content-Length': string;
}

export interface Guest {
  surname: string;
  name: string;
  birthday: Date;
  citizenship: string;
  passportNumber: string;
  visaNumber: string;
  address: string;
  note: string;
  phone: string;
  email: string;
}

export interface GuestDataAddData {
  travelPurpose: string;
  guests: Guest[];
}

export interface SpecialRequestData {
  specialWishes: SpecialWish[];
  reservationId: SpecialWish[];
}

export interface SaveBreakfastData {
  selectedBreakfast: BreakfastSelection[];
}

export interface AddInvoiceAddress {
  name: string;
  surname: string;
  companyName: string;
  taxId: string;
  address: string;
  postalCode: string;
  city: string;
  country: string;
}

export enum GuestDataType {
  GetGuestData = 'getGuestData',
  AddGuestData = 'addGuestData',
  AddSpecialRequest = 'addSpecialRequest',
  AddInvoiceAddress = 'addInvoiceAddress',
  AddPreferredLanguage = 'addPreferredLanguage',
  CancelTrip = 'cancelTrip',
  CheckInData = 'getCheckInData',
  SaveBreakfastSelection = 'saveBreakfastSelection',
  GetLastValidInvoice = 'getLastValidInvoice',
}

export interface GuestDataGetDataRequest {
  type: GuestDataType.GetGuestData;
  key: string;
}

export interface GuestDataAddDataRequest {
  type: GuestDataType.AddGuestData;
  key: string;
  data: GuestDataAddData;
}
export interface GuestDataAddInvoiceRequest {
  type: GuestDataType.AddInvoiceAddress;
  key: string;
  data: { address: AddInvoiceAddress; folioId: string };
}
export interface GuestDataAddPreferredLanguage {
  type: GuestDataType.AddPreferredLanguage;
  key: string;
  data: string;
}
export interface GuestDataGetLastValidInvoice {
  type: GuestDataType.GetLastValidInvoice;
  key: string;
  data: { invoiceId: string };
}
export interface GuestDataCancelTrip {
  type: GuestDataType.CancelTrip;
  key: string;
}
export interface GuestDataAddSpecialRequest {
  type: GuestDataType.AddSpecialRequest;
  key: string;
  data: SpecialRequestData;
}

export interface GuestDataCheckInRequest {
  type: GuestDataType.CheckInData;
  key: string;
  day: string;
}

export interface GuestDataSelectBreakfastRequest {
  type: GuestDataType.SaveBreakfastSelection;
  key: string;
  data: SaveBreakfastData;
}

export type GuestDataRequest =
  | GuestDataGetDataRequest
  | GuestDataAddDataRequest
  | GuestDataCheckInRequest
  | GuestDataAddInvoiceRequest
  | GuestDataAddPreferredLanguage
  | GuestDataCancelTrip
  | GuestDataAddSpecialRequest
  | GuestDataSelectBreakfastRequest
  | GuestDataGetLastValidInvoice;

export interface PayloadData {
  reservationId: string;
  bookingId: string;
}

export enum GuestPaymentRequestType {
  GetOriginKey = 'getOriginKey',
  GetPaymentMethods = 'getPaymentMethods',
  SubmitPayment = 'submitPayment',
  SubmitPaymentDetails = 'submitPaymentDetails',
}

export interface GuestPaymentRequest {
  type: GuestPaymentRequestType;
  key: string;
}

export enum CancellationStatus {
  FlexFree = 'flexFree',
  FlexCharge = 'flexCharge',
  NonRef = 'nonRef',
  Redirect = 'redirect',
  NonCancelable = 'nonCancelable',
}
