import { Module } from '@nestjs/common';
import { APP_GUARD, APP_PIPE, APP_INTERCEPTOR, APP_FILTER } from '@nestjs/core';
import { ApaleoModule } from './apaleo/apaleo.module';
import { ApaleoExtensionsModule } from './apaleoExtensions/apaleoExtensions.module';
import { KeyAuthGuard } from './common/auth/keyAuth.guard';
import { JwtAuthGuard } from './common/auth/jwtAuth.guard';
import { HousekeepingModule } from './housekeeping/housekeeping.module';
import { InputValidationPipe } from './common/validation.pipe';
import { AuthModule } from './auth/auth.module';
import { ResponseInterceptor } from './common/response.interceptor';
import { HttpExceptionFilter } from './common/httpException.filter';
import { ExperimentalModule } from './experimental/experimental.module';
import { ReservationComparisonReportModule } from './reservationComparisonReport/reservationComparisonReport.module';
import { MidstayReservationsModule } from './midstayReservations/midstayReservations.module';
import { KeySystemModule } from './keySystem/KeySystem.module';
import { OverbookingsManagerModule } from './overbookings/overbookings.module';
import JWTService from './JWTService';
import { CustomerCommunicationModule } from './customerCommunication/customerCommunication.module';

@Module({
  imports: [
    ApaleoModule,
    HousekeepingModule,
    AuthModule,
    ApaleoExtensionsModule,
    ExperimentalModule,
    ReservationComparisonReportModule,
    MidstayReservationsModule,
    KeySystemModule,
    OverbookingsManagerModule,
    CustomerCommunicationModule,
  ],
  providers: [
    JWTService,
    { provide: APP_GUARD, useClass: KeyAuthGuard },
    { provide: APP_GUARD, useClass: JwtAuthGuard },
    { provide: APP_PIPE, useClass: InputValidationPipe },
    { provide: APP_INTERCEPTOR, useClass: ResponseInterceptor },
    { provide: APP_FILTER, useClass: HttpExceptionFilter },
  ],
})
export class AppModule {}
