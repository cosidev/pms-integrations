import { v4 } from 'uuid';
import { ApaleoClient } from './apaleo/ApaleoClient';
import logger from './LoggingService';
import JWTService from './JWTService';
import { GuestPaymentRequestType, PayloadData, ApaleoReservation } from './types';
import AdyenClient from './AdyenClient';
import { generalConfig } from './config';
import { DynamoClient } from './DynamoClient';
import { EnumFolioItemModelAllowedActions, CreateAuthorizationPaymentRequest } from './apaleo/generated/finance';

export default class GuestPaymentHandler {
  jwtService: JWTService;
  adyenClient: AdyenClient;
  apaleoClient: ApaleoClient;
  dynamoClient: DynamoClient;

  constructor() {
    this.jwtService = new JWTService();
    this.apaleoClient = new ApaleoClient();
    this.adyenClient = new AdyenClient();
    this.dynamoClient = new DynamoClient();
  }

  async handleRequest(request: any) {
    try {
      const payload = this.jwtService.verifyToken(request.key) as PayloadData;
      const reservation = await this.apaleoClient.getReservationById(payload.reservationId);
      const reservationData = {
        id: reservation.id,
        balance: reservation.balance,
        amount: {
          value: Math.abs(reservation.balance.amount) * 100,
          currency: reservation.balance.currency,
        },
      };
      let adyenData: any;
      switch (request.type) {
        case GuestPaymentRequestType.GetOriginKey:
          adyenData = await this.getOriginKey(request.host);
          return { adyenData };
        case GuestPaymentRequestType.GetPaymentMethods:
          adyenData = await this.getPaymentMethods(reservation);
          return { reservationData, adyenData };
        case GuestPaymentRequestType.SubmitPayment:
          adyenData = await this.submitPayment(request.data, reservation);
          return { adyenData };
        case GuestPaymentRequestType.SubmitPaymentDetails:
          adyenData = await this.submitPaymentDetails(request.data);
          return { adyenData };
      }
    } catch (e) {
      logger.errorLog('Could not handle guest payment request', {
        e,
        message: e.message,
        GuestPaymentRequest: request,
      });
      throw e;
    }
  }

  private getParamsFromUrl(url: string) {
    const decodedUrl: string = decodeURI(url);
    const params: string[] = decodedUrl.split('&');
    const obj = {};
    if (params && params.length) {
      params.forEach(param => {
        const keyValuePair = param.split('=');
        const key: string = keyValuePair[0];
        const value: string = keyValuePair[1];
        obj[key] = value;
      });
    }
    return obj;
  }

  async handleRedirectRequest(request: any): Promise<string> {
    let params: any = request.queryStringParameters;
    if (request.httpMethod === 'POST') {
      params = {
        ...params,
        ...this.getParamsFromUrl(request.body),
      };
    }
    const redirectURL = `${generalConfig.adyen.redirectURL}/guest-payment?key=${params.key}`;

    const payload = this.jwtService.verifyToken(params.key) as PayloadData;
    const reservation = await this.apaleoClient.getReservationById(payload.reservationId);

    const paymentData = await this.dynamoClient.getItem(reservation.id, 'adyen_payment_data');
    const detailsResponse = await this.adyenClient.paymentsDetails({
      paymentData: paymentData.Item && paymentData.Item.value,
      details: {
        MD: params.MD,
        PaRes: params.PaRes,
      },
    });

    await this.dynamoClient.deleteItem(reservation.id, 'adyen_payment_data');
    if (detailsResponse.resultCode === 'Authorised') {
      await this.addApaleoPayment(detailsResponse, reservation);
      return `${redirectURL}&resultCode=Authorised`;
    }

    return `${redirectURL}&resultCode=${detailsResponse.resultCode}`;
  }

  private async getOriginKey(host: string): Promise<string> {
    return this.adyenClient.originKey(host);
  }

  private async getPaymentMethods(reservation: ApaleoReservation): Promise<ICheckout.PaymentMethodsResponse> {
    const merchantAccount = generalConfig.adyen.merchantAccount[reservation.property.id];
    return this.adyenClient.paymentMethods({
      amount: {
        currency: reservation.balance.currency,
        value: Math.abs(reservation.balance.amount) * 100,
      },
      merchantAccount,
      channel: 'Web',
      blockedPaymentMethods: ['bcmc'],
      allowedPaymentMethods: ['scheme'],
    });
  }

  private async submitPayment(request: any, reservation: ApaleoReservation): Promise<ICheckout.PaymentResponse> {
    const merchantAccount = generalConfig.adyen.merchantAccount[reservation.property.id];

    const additionalData: any = {
      'metadata.flowType': 'CaptureOnly',
      'metadata.accountId': generalConfig.apaleo.accountId,
      'metadata.propertyId': reservation.property.id,
    };

    const response = await this.adyenClient.payment({
      ...request,
      merchantAccount,
      deliveryDate: (reservation.arrival as unknown) as string,
      paymentMethod: request.paymentMethod,
      enableOneClick: false,
      enableRecurring: true,
      shopperReference: v4(),
      shopperInteraction: 'Ecommerce',
      recurringProcessingModel: 'UnscheduledCardOnFile',
      amount: {
        currency: reservation.balance.currency,
        value: Math.abs(reservation.balance.amount) * 100,
      },
      returnUrl: `${generalConfig.adyen.returnURL}/guestPaymentRedirect?key=${this.jwtService.getToken({
        reservationId: reservation.id,
      })}`,
      reference: reservation.id,
      additionalData,
      // Enable below to force threeD payment
      // additionalData: { executeThreeD: true, allow3DS2: true },
      // channel: 'Web',
    });

    if (response.resultCode === 'Authorised') {
      await this.addApaleoPayment(response, reservation);
      return response;
    }

    if (response.resultCode === 'RedirectShopper') {
      await this.dynamoClient.addItem(reservation.id, 'adyen_payment_data', { value: response.paymentData });
      return response;
    }

    return response;
  }

  private async submitPaymentDetails(request) {
    const details = await this.adyenClient.paymentsDetails(request);
    return details;
  }

  private async addApaleoPayment(paymentResponse: ICheckout.PaymentResponse, reservation: ApaleoReservation) {
    const { folios = [] } = await this.apaleoClient.getFoliosByReservationId(reservation.id, {
      excludeClosed: true,
      expand: ['allowedActions'],
    });
    for (const folio of folios) {
      const { allowedActions = [] } = folio;
      if (!allowedActions.includes(EnumFolioItemModelAllowedActions.AddPayment) || folio.balance.amount >= 0) {
        continue;
      }
      const payment: CreateAuthorizationPaymentRequest = {
        amount: {
          amount: Math.abs(folio.balance.amount),
          currency: folio.balance.currency,
        },
        transactionReference: paymentResponse.pspReference,
      };
      try {
        await this.apaleoClient.addPaymentByAuthorization(folio.id, payment);
        logger.infoLog(`Submitted payment request to ${reservation.id} (${folio.id})`, { payment });
      } catch (e) {
        logger.errorLog(`Could not submit payment request to ${reservation.id} (${folio.id})`, { e, payment });
      }
    }
  }
}
