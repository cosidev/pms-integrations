import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { BookingCreatedModel } from './../apaleo/generated/reservation';
import { RatePlanItemModel } from './../apaleo/generated/rateplan';
import {
  RESERVATION_DATA,
  buildDate,
  RESERVATION_CREATION_ERROR,
  PROPERTIES_LIST,
  FORMATTED_PROPERTIES_LIST,
  DIRECT_RESERVATION_DATA,
} from './__mocks__/midstayReservations';
import { CreateReservationDto, EnumBookingSource } from './dto/CreateReservation.dto';
import { MidstayReservationsModule } from './midstayReservations.module';
import { ApaleoClient } from './../apaleo/ApaleoClient';
import { MidstayReservationsService } from './midstayReservations.service';
import { BookingService } from '../apaleo/generated/reservation';
import { ReservationEventLogger } from './../ReservationEventLogger';

jest.mock('../LoggingService.ts');

describe('MidstayReservationsService', () => {
  let midstayReservationService: MidstayReservationsService;
  let apaleoClient: ApaleoClient;
  let reservationEventLogger: ReservationEventLogger;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [MidstayReservationsModule],
    }).compile();

    midstayReservationService = module.get<MidstayReservationsService>(MidstayReservationsService);
    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    reservationEventLogger = module.get<ReservationEventLogger>(ReservationEventLogger);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('reservation creator', () => {
    it('should handle the creation of midstay reservations', async (): Promise<void> => {
      jest.spyOn(reservationEventLogger, 'logEvent')
        .mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'addMidstayReservation')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));
      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [
            {
              id: '0005_FLEX_MS',
            } as RatePlanItemModel,
          ],
          count: 1,
        })));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.HomeLike,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      await midstayReservationService.createReservation(postData);
      expect(apaleoClient.addMidstayReservation).toHaveBeenCalledWith(RESERVATION_DATA, false);

      const ratePlanFilter = {
        propertyId: postData.propertyId,
        ratePlanCodes: ['0005_FLEX_MS'],
      };
      expect(apaleoClient.getRateplans).toHaveBeenCalledWith(ratePlanFilter);
    });

    it('should enforce restrictions if ignoreRestrictions is false', async (): Promise<void> => {
      jest.spyOn(reservationEventLogger, 'logEvent')
        .mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(apaleoClient, 'addMidstayReservation');
      jest.spyOn(apaleoClient, 'getApiConfig')
        .mockImplementation(() => new Promise(resolve => resolve()));

      jest.spyOn(BookingService, 'bookingBookingsPost')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));
      jest.spyOn(BookingService, 'bookingBookings$forcePost')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));

      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [
            {
              id: '0005_FLEX_MS',
            } as RatePlanItemModel,
          ],
          count: 1,
        })));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.HomeLike,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      await midstayReservationService.createReservation(postData);

      expect(apaleoClient.addMidstayReservation).toHaveBeenCalledWith(RESERVATION_DATA, false);
      expect(BookingService.bookingBookingsPost).toHaveBeenCalled();
      expect(BookingService.bookingBookings$forcePost).not.toHaveBeenCalled();
    });

    it('should bypass restrictions if ignoreRestrictions is true', async (): Promise<void> => {
      jest.spyOn(reservationEventLogger, 'logEvent')
        .mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(BookingService, 'bookingBookingsPost')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));
      jest.spyOn(BookingService, 'bookingBookings$forcePost')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));

      jest.spyOn(apaleoClient, 'addMidstayReservation');
      jest.spyOn(apaleoClient, 'getApiConfig')
        .mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [
            {
              id: '0005_FLEX_MS',
            } as RatePlanItemModel,
          ],
          count: 1,
        })));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.HomeLike,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: true,
      };

      await midstayReservationService.createReservation(postData);

      expect(apaleoClient.addMidstayReservation).toHaveBeenCalledWith(RESERVATION_DATA, true);
      expect(BookingService.bookingBookings$forcePost).toHaveBeenCalled();
      expect(BookingService.bookingBookingsPost).not.toHaveBeenCalled();
    });

    it('should gracefully handle failure to create midstay reservations', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'addMidstayReservation')
        .mockImplementation(() => new Promise((resolve, reject) => reject(RESERVATION_CREATION_ERROR)));
      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [
            {
              id: '0005_FLEX_MS',
            } as RatePlanItemModel,
          ],
          count: 1,
        })));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.HomeLike,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).toHaveBeenCalledTimes(1);
      } catch (e) {
        expect(e).toEqual(new HttpException(
          RESERVATION_CREATION_ERROR.response.data.messages, HttpStatus.BAD_REQUEST,
        ));
      }
    });

    it('should throw if rate plan does not exist', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'addMidstayReservation')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));
      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise((_, reject) => reject()));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.HomeLike,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.getRateplans).toHaveBeenCalledTimes(1);
      } catch (e) {
        expect(e).toEqual(new HttpException('Could not retrieve rate plan', HttpStatus.NOT_FOUND));
      }

      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [],
          count: 0,
        })));

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.getRateplans).toHaveBeenCalledTimes(1);
      } catch (e) {
        expect(e).toEqual(new HttpException('Could not retrieve rate plan', HttpStatus.NOT_FOUND));
      }
    });

    it('should fail validation if arrival is invalid', async (): Promise<void> => {
      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: '',
          email: '',
        },
        commissionAmount: 0,
        currency: '',
        arrival: '',
        departure: buildDate(2),
        source: EnumBookingSource.Others,
        totalPrice: 0,
        adults: 0,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).not.toHaveBeenCalled();
      } catch (e) {
        expect(e).toEqual(new RangeError('Invalid time value'));
      }
    });

    it('should fail if departure is invalid', async (): Promise<void> => {
      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: '',
          email: '',
        },
        commissionAmount: 0,
        currency: '',
        arrival: buildDate(0),
        departure: '',
        source: EnumBookingSource.Others,
        totalPrice: 200,
        adults: 0,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).not.toHaveBeenCalled();
      } catch (e) {
        expect(e).toEqual(new RangeError('Invalid time value'));
      }
    });

    it('should fail if arrival is equal to departure', async (): Promise<void> => {
      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: '',
          email: '',
        },
        commissionAmount: 0,
        currency: '',
        arrival: buildDate(0),
        departure: buildDate(0),
        source: EnumBookingSource.Others,
        totalPrice: 0,
        adults: 0,
        propertyId: '',
        unitGroupCode: '',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).not.toHaveBeenCalled();
      } catch (e) {
        expect(e).toEqual(new Error('Departure must be in the future after arrival'));
      }
    });

    it('should fail if arrival is greater than departure', async (): Promise<void> => {
      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: '',
          email: '',
        },
        commissionAmount: 0,
        currency: '',
        arrival: buildDate(0),
        departure: buildDate(-1),
        source: EnumBookingSource.Others,
        totalPrice: 0,
        adults: 0,
        propertyId: '',
        unitGroupCode: '',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).not.toHaveBeenCalled();
      } catch (e) {
        expect(e).toEqual(new Error('Departure must be in the future after arrival'));
      }
    });

    it('should fail if arrival is less than departure', async (): Promise<void> => {
      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: '',
          email: '',
        },
        commissionAmount: 0,
        currency: '',
        arrival: buildDate(2),
        departure: buildDate(1),
        source: EnumBookingSource.Others,
        totalPrice: 0,
        adults: 0,
        propertyId: '',
        unitGroupCode: '',
        ignoreRestrictions: false,
      };

      try {
        await midstayReservationService.createReservation(postData);
        expect(apaleoClient.addMidstayReservation).not.toHaveBeenCalled();
      } catch (e) {
        expect(e).toEqual(new Error('Departure must be in the future after arrival'));
      }
    });

    it('should set commission to 0 if booking source is OTHERS(Bookings on Direct channel)', async (): Promise<void> => {
      jest.spyOn(reservationEventLogger, 'logEvent')
        .mockImplementation(() => new Promise(resolve => resolve()));
      jest.spyOn(apaleoClient, 'addMidstayReservation')
        .mockImplementation(() => new Promise(resolve => resolve({} as Promise<BookingCreatedModel>)));
      jest.spyOn(apaleoClient, 'getRateplans')
        .mockImplementation(() => new Promise(resolve => resolve({
          ratePlans: [
            {
              id: '0005_FLEX_MS',
            } as RatePlanItemModel,
          ],
          count: 1,
        })));

      const postData: CreateReservationDto = {
        primaryGuest: {
          firstName: '',
          lastName: 'West',
          email: '',
        },
        commissionAmount: 50,
        currency: 'CZK',
        arrival: buildDate(0),
        departure: buildDate(2),
        source: EnumBookingSource.Others,
        totalPrice: 300,
        adults: 2,
        propertyId: 'DE_BE_001',
        unitGroupCode: '0005',
        ignoreRestrictions: false,
      };

      await midstayReservationService.createReservation(postData);
      expect(apaleoClient.addMidstayReservation).toHaveBeenCalledWith(DIRECT_RESERVATION_DATA, false);
    });
  });

  describe('properties handler', () => {
    it('should retrieve all properties', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getProperties')
        .mockImplementation(() => new Promise(resolve => resolve({
          properties: PROPERTIES_LIST,
          count: 1,
        })));

      const properties = await midstayReservationService.getProperties();
      expect(apaleoClient.getProperties).toHaveBeenCalledTimes(1);

      expect(properties).toEqual(FORMATTED_PROPERTIES_LIST);
    });
  });

  describe('unitgroups handler', () => {
    it('should retrieve trigger getUnitGroupsByPropertyId', async (): Promise<void> => {
      jest.spyOn(apaleoClient, 'getUnitGroupsByPropertyId')
        .mockImplementation(() => new Promise(resolve => resolve({
          unitGroups: [],
          count: 0,
        })));

      await midstayReservationService.getUnitGroupsByPropertyId('DE_PQ_1');
      expect(apaleoClient.getUnitGroupsByPropertyId).toHaveBeenCalledTimes(1);
    });
  });
});
