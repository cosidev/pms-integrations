import { ReservationEventLogger } from './../ReservationEventLogger';
import { ApaleoModule } from './../apaleo/apaleo.module';
import { Module } from '@nestjs/common';
import { MidstayReservationsController } from './midstayReservations.controller';
import { MidstayReservationsService } from './midstayReservations.service';
import { DynamoClient } from '../common/aws';

@Module({
    imports: [ApaleoModule],
    controllers: [MidstayReservationsController],
    providers: [MidstayReservationsService, ReservationEventLogger, DynamoClient],
})
export class MidstayReservationsModule { }
