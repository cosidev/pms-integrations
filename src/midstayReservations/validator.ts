import { registerDecorator, ValidationOptions } from 'class-validator';
import { isValid } from 'date-fns';

export function IsValidDate(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'isValidDate',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          return isValid(new Date(value));
        },
      },
    });
  };
}
