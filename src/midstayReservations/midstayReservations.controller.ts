import { JwtAuth } from './../common/auth/jwtAuth.decorator';
import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { MidstayReservationsService } from './midstayReservations.service';
import { CreateReservationDto } from './dto/CreateReservation.dto';

@Controller('midstayReservations')
export class MidstayReservationsController {
  constructor(private readonly midstayReservationsService: MidstayReservationsService) {}

  @Post('/create')
  @JwtAuth('midstayReservations')
  createReservation(@Body() data: CreateReservationDto) {
    return this.midstayReservationsService.createReservation(data);
  }

  @Get('/properties')
  @JwtAuth('midstayReservations')
  getProperties() {
    return this.midstayReservationsService.getProperties();
  }

  @Get('/unitGroups/:propertyId')
  @JwtAuth('midstayReservations')
  getUnitGroups(@Param('propertyId') propertyId: string) {
    return this.midstayReservationsService.getUnitGroupsByPropertyId(propertyId);
  }
}
