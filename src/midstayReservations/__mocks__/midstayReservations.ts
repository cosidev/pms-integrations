import { format } from 'date-fns';
import { PropertyItem } from './../midstayReservations.interface';
import { PropertyItemModel } from './../../apaleo/generated/property';
import {
  CreateBookingModel,
  CreateReservationModel,
  EnumCreateReservationModelChannelCode,
} from './../../apaleo/generated/reservation';

export function buildDate(numberOfDays: number) {
  const DATE_FORMAT: string = 'yyyy-MM-dd';

  const date = new Date();
  date.setDate(date.getDate() + numberOfDays);

  return format(date, DATE_FORMAT);
}

export const RESERVATION_DATA: CreateBookingModel = {
  booker: {
    email: '',
    firstName: '',
    lastName: 'West',
  },
  bookerComment: '',
  comment: '',
  reservations: [
    {
      adults: 2,
      arrival: buildDate(0),
      channelCode: 'ChannelManager',
      source: 'HomeLike',
      commission: {
        commissionAmount: {
          amount: 50,
          currency: 'CZK',
        },
      },
      departure: buildDate(2),
      primaryGuest: {
        email: '',
        firstName: '',
        lastName: 'West',
      },
      timeSlices: [
        {
          ratePlanId: '0005_FLEX_MS',
          totalAmount: {
            amount: 150,
            currency: 'CZK',
          },
        },
        {
          ratePlanId: '0005_FLEX_MS',
          totalAmount: {
            amount: 150,
            currency: 'CZK',
          },
        },
      ],
    } as CreateReservationModel,
  ],
} as CreateBookingModel;

export const DIRECT_RESERVATION_DATA: CreateBookingModel = {
  booker: {
    email: '',
    firstName: '',
    lastName: 'West',
  },
  bookerComment: '',
  comment: '',
  reservations: [
    {
      adults: 2,
      arrival: buildDate(0),
      channelCode: EnumCreateReservationModelChannelCode.Direct,
      commission: {
        commissionAmount: {
          amount: 0,
          currency: 'CZK',
        },
      },
      departure: buildDate(2),
      primaryGuest: {
        email: '',
        firstName: '',
        lastName: 'West',
      },
      timeSlices: [
        {
          ratePlanId: '0005_FLEX_MS',
          totalAmount: {
            amount: 150,
            currency: 'CZK',
          },
        },
        {
          ratePlanId: '0005_FLEX_MS',
          totalAmount: {
            amount: 150,
            currency: 'CZK',
          },
        },
      ],
    } as CreateReservationModel,
  ],
} as CreateBookingModel;

export const RESERVATION_CREATION_ERROR = {
  response: {
    data: {
      messages: [
        'Reservations[0].TimeSlices[0].RatePlanId: The RatePlanId field is required.',
      ],
    },
  },
};

export const PROPERTIES_LIST = [
  {
    id: 'DE_PQ_01',
    code: 'DE_PQ_01',
    isTemplate: false,
    name: 'The Dancing River',
    companyName: 'COSI Hospitality GmbH',
    commercialRegisterEntry: 'unbekannt',
    taxId: 'unbekannt',
    location: {
      addressLine1: 'BS 500',
      addressLine2: '',
      postalCode: '10005',
      city: 'Berlin',
      countryCode: 'DE',
    },
    paymentTerms: {
      en: 'total in advance',
    },
    timeZone: 'Europe/Berlin',
    currencyCode: 'EUR',
    created: '2019-01-17T13:00:24+02:00' as unknown as Date,
  } as PropertyItemModel,
];

export const FORMATTED_PROPERTIES_LIST: PropertyItem[] = [
  {
    id: 'DE_PQ_01',
    code: 'DE_PQ_01',
    name: 'The Dancing River',
    currencyCode: 'EUR',
  },
];
