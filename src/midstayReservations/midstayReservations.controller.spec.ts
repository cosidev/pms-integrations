import { MidstayReservationsModule } from './midstayReservations.module';
import { Test, TestingModule } from '@nestjs/testing';
import { MidstayReservationsController } from './midstayReservations.controller';

describe('MidstayReservations Controller', () => {
  let controller: MidstayReservationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [MidstayReservationsModule],
    }).compile();

    controller = module.get<MidstayReservationsController>(MidstayReservationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
