export interface PropertyItem {
  id: string;
  code: string;
  name: string;
  currencyCode: string;
}
