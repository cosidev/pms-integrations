import {
  IsString, IsOptional, IsEmail, MinLength,
} from 'class-validator';

export class PrimaryGuest {
  @IsString()
  @IsOptional()
  firstName: string;

  @MinLength(1)
  @IsString()
  lastName: string;

  @IsEmail()
  @IsOptional()
  email: string;
}
