import { PrimaryGuest } from './PrimaryGuest.dto';
import {
  IsString, IsNumber, Min, ValidateNested, IsEnum, IsBoolean,
} from 'class-validator';
import { Type } from 'class-transformer';
import { IsValidDate } from '../validator';

export enum EnumBookingSource {
  'Airbnb' = 'Airbnb',
  'Flatio' = 'Flatio',
  'HomeLike' = 'HomeLike',
  'HousingAnywhere' = 'HousingAnywhere',
  'Others' = 'Others',
}

export class CreateReservationDto {
  @ValidateNested()
  @Type(() => PrimaryGuest)
  primaryGuest: PrimaryGuest;

  @IsNumber()
  commissionAmount: number;

  @IsString()
  currency: string;

  @IsValidDate()
  arrival: string;

  @IsValidDate()
  departure: string;

  @IsString()
  unitGroupCode: string;

  @IsString()
  propertyId: string;

  @IsEnum(EnumBookingSource)
  source: EnumBookingSource;

  @IsNumber()
  totalPrice: number;

  @IsNumber()
  @Min(1)
  adults: number;

  @IsBoolean()
  ignoreRestrictions: boolean;
}
