import { differenceInDays, format } from 'date-fns';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { RatePlanItemModel } from './../apaleo/generated/rateplan';
import { PropertyItem } from './midstayReservations.interface';
import { ApaleoClient } from './../apaleo/ApaleoClient';
import { CreateReservationDto, EnumBookingSource } from './dto/CreateReservation.dto';
import {
  BookingCreatedModel,
  CreateBookingModel,
  CommissionModel,
  CreateReservationModel,
  CreateReservationTimeSliceModel,
  GuestModel,
  BookerModel,
  EnumCreateReservationModelChannelCode,
} from '../apaleo/generated/reservation';
import { PropertyItemModel, UnitGroupItemModel } from './../apaleo/generated/property';
import { ReservationEventLogger, EnumReservationEvents } from './../ReservationEventLogger';
import logger from '../LoggingService';

@Injectable()
export class MidstayReservationsService {
  constructor(
    private readonly apaleoClient: ApaleoClient,
    private readonly reservationEventLogger: ReservationEventLogger,
  ) { }

  public async getProperties(): Promise<PropertyItem[]> {
    logger.infoLog('Fetching properties');
    const propertyData = await this.apaleoClient.getProperties();
    const properties: PropertyItemModel[] = propertyData.properties || [];

    return properties.map((property): PropertyItem => {
      const { id, code, name, currencyCode } = property;
      return {
        id,
        code,
        name,
        currencyCode,
      };
    });
  }

  public async getUnitGroupsByPropertyId(propertyId: string): Promise<UnitGroupItemModel[]> {
    logger.infoLog('Fetching Unit Groups on', propertyId);
    const { unitGroups = [] } = await this.apaleoClient.getUnitGroupsByPropertyId(propertyId);
    return unitGroups;
  }

  public async createReservation(data: CreateReservationDto): Promise<BookingCreatedModel> {
    const { arrival, departure, ignoreRestrictions } = data;
    const { isValid, validationErrors } = this.validateDates(arrival, departure);
    if (!isValid) {
      logger.errorLog('Date validation failed. Errors:', validationErrors);
      throw new HttpException(validationErrors, HttpStatus.BAD_REQUEST);
    }

    const reservation: CreateBookingModel = await this.formatBookingData(data);
    try {
      logger.infoLog('Creating new reservation', {
        reservation,
      });
      const createdReservation = await this.apaleoClient.addMidstayReservation(reservation, ignoreRestrictions);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.ReservationCreated,
        reservationId: createdReservation.id,
        message: 'Reservation created',
      });
      return createdReservation;
    } catch (error) {
      const statusCode = error.response?.status;
      if (statusCode === HttpStatus.FORBIDDEN) {
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }

      logger.errorLog('Could not create reservation', { reservation });
      throw new HttpException(error?.response?.data?.messages, HttpStatus.BAD_REQUEST);
    }
  }

  private async formatBookingData(data: CreateReservationDto): Promise<CreateBookingModel> {
    const reservation = await this.formatReservationData(data);

    return {
      booker: this.formatGuestInfo<BookerModel>(data),
      comment: '',
      bookerComment: '',
      reservations: [
        reservation,
      ],
    } as CreateBookingModel;
  }

  private formatGuestInfo<T>(data: CreateReservationDto): T {
    const { primaryGuest } = data;

    return primaryGuest as unknown as T;
  }

  private async formatReservationData(data: CreateReservationDto): Promise<CreateReservationModel> {
    const {
      commissionAmount, currency, source, arrival, departure, adults,
    } = data;

    const { arrivalDate, departureDate } = this.formatArrivalAndDeparture(arrival, departure);
    const timeSlices = await this.generateTimeSlices(arrivalDate, departureDate, data);

    const reservationData = {
      primaryGuest: this.formatGuestInfo<GuestModel>(data),
      adults,
      arrival: arrivalDate,
      departure: departureDate,
      channelCode: 'ChannelManager',
      source,
      timeSlices,
      commission: {
        commissionAmount: {
          amount: commissionAmount,
          currency,
        },
      } as CommissionModel,
    } as CreateReservationModel;

    if (source === EnumBookingSource.Others) {
      reservationData.channelCode = EnumCreateReservationModelChannelCode.Direct;
      reservationData.commission.commissionAmount.amount = 0;
      delete (reservationData.source);
    }

    return reservationData;
  }

  private formatArrivalAndDeparture(arrival: string, departure: string) {
    const DATE_FORMAT: string = 'yyyy-MM-dd';

    const arrivalDate: string = format(new Date(arrival), DATE_FORMAT);
    const departureDate: string = format(new Date(departure), DATE_FORMAT);

    return {
      arrivalDate,
      departureDate,
    };
  }

  private async generateTimeSlices(
    arrivalDate: string, departureDate: string, data: CreateReservationDto,
  ): Promise<CreateReservationTimeSliceModel[]> {
    const { propertyId, unitGroupCode } = data;
    const ratePlan = await this.getRatePlan(propertyId, unitGroupCode);

    if (!ratePlan) {
      throw new HttpException('Could not retrieve rate plan', HttpStatus.NOT_FOUND);
    }

    const { totalPrice, currency } = data;

    const numberOfNights: number = differenceInDays(new Date(departureDate), new Date(arrivalDate));
    const pricePerNight: number = Number((totalPrice / numberOfNights).toFixed(2));

    const derivedTotal = pricePerNight * numberOfNights;
    const differenceInTotal = derivedTotal - totalPrice;

    const timeSliceDistribution: any[] = new Array(numberOfNights);
    const lastTimeSliceIndex = timeSliceDistribution.length - 1;
    const timeSlices: CreateReservationTimeSliceModel[] = timeSliceDistribution
      .fill(0)
      .map((_, index): CreateReservationTimeSliceModel => {
        let costPerNight = pricePerNight;
        if (lastTimeSliceIndex === index) {
          costPerNight = costPerNight - differenceInTotal;
        }

        return {
          ratePlanId: ratePlan.id,
          totalAmount: {
            amount: costPerNight,
            currency,
          },
        };
      });

    return timeSlices;
  }

  private async getRatePlan(propertyId: string, unitGroupCode: string): Promise<RatePlanItemModel> {
    const filter = {
      propertyId,
      ratePlanCodes: [this.buildRatePlanCode(unitGroupCode)],
    };

    try {
      const { ratePlans = [] } = await this.apaleoClient.getRateplans(filter);
      return ratePlans[0];
    } catch (error) {
      logger.errorLog('Could not retrieve rate plan', { filter });
      throw new HttpException('Could not retrieve rate plan', HttpStatus.NOT_FOUND);
    }
  }

  private buildRatePlanCode(unitGroupCode: string): string {
    return `${unitGroupCode}_FLEX_MS`;
  }

  private validateDates(arrival: string, departure: string) {
    const arrivalDifference: number = differenceInDays(
      new Date(arrival).setUTCHours(0, 0, 0), new Date(),
    );
    const numberOfNights: number = differenceInDays(
      new Date(departure), new Date(arrival),
    );

    let isValid = true;
    const errors = {
      arrivalInvalid: arrivalDifference < 0 ? 'Arrival date must be the current date or in the future' : '',
      departureInvalid: numberOfNights <= 0 ? 'Departure must be in the future after arrival' : '',
    };

    const validationErrors = Object.values(errors).filter((error): boolean => {
      if (error) {
        isValid = false;
        return true;
      }

      return false;
    });

    return {
      isValid,
      validationErrors,
    };
  }
}
