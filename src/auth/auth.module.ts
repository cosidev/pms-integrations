import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { JwtModule } from '@nestjs/jwt';
import { generalConfig } from '../config';
import { JwtStrategy } from './jwt.strategy';
import { PropertyGuard } from './guards/Property.guard';

@Module({
  imports: [
    JwtModule.register({
      secret: generalConfig.authJwtSecret,
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, UserService, JwtStrategy, PropertyGuard],
})
export class AuthModule { }
