import { CanActivate, Injectable, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { User } from '../types';

interface IPropertyBody {
  propertyId?: string;
  propertyIds?: string[];
}

@Injectable()
export class PropertyGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const { propertyId, propertyIds = [] }: IPropertyBody = request.body;
    const user: User = request.user;
    let hasPropertyRoles = true;

    if (request.params.propertyId) {
      const userPropertyRoles = user.propertyRoles[request.params.propertyId];
      if (!userPropertyRoles) {
        return false;
      }
    }

    if (request.query.propertyId) {
      const userPropertyRoles = user.propertyRoles[request.query.propertyId];
      if (!userPropertyRoles) {
        return false;
      }
    }

    if (!propertyId && propertyIds.length === 0) {
      return true;
    }
    if (propertyId) {
      propertyIds.push(propertyId);
    }

    // Check if all given properties contain necessary roles

    propertyIds.forEach(id => {
      const userPropertyRoles = user.propertyRoles[id];
      if (!userPropertyRoles || !userPropertyRoles.some(role => roles.includes(role))) {
        hasPropertyRoles = false;
      }
    });

    return hasPropertyRoles;
  }
}
