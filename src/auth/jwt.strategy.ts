import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { generalConfig } from '../config';
import { UserService } from './user.service';
import { InvalidCredentialsException } from './auth.exceptions';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: generalConfig.authJwtSecret,
    });
  }

  async validate(payload: any) {
    if (!payload.id) {
      throw new InvalidCredentialsException();
    }
    const user = await this.userService.getUserById(payload.id);
    return user;
  }
}
