import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { hash, compare } from 'bcrypt';
import { v4 } from 'uuid';
import { LoginDto, RegisterDto } from './dto';
import { UserService } from './user.service';
import { User } from './types';
import { InvalidCredentialsException, UserAlreadyExistsException, UserNotFoundException } from './auth.exceptions';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService, private readonly jwtService: JwtService) { }
  async login(data: LoginDto) {
    const user = await this.userService.getUserByEmail(data.email);
    if (!user) {
      throw new UserNotFoundException();
    }

    const validPassword = await compare(data.password, user.password);
    if (validPassword) {
      return {
        token: this.createToken(user),
        user: this.stripUser(user),
      };
    }
    throw new InvalidCredentialsException();
  }

  async register(data: RegisterDto): Promise<User> {
    const userExists = await this.userService.getUserByEmail(data.email);
    if (userExists) {
      throw new UserAlreadyExistsException();
    }
    const user: User = {
      ...data,
      id: v4(),
      password: await hash(data.password, 12),
    };
    await this.userService.saveUser(user);
    return this.stripUser(user);
  }

  createToken(user: User) {
    return this.jwtService.sign({ id: user.id, email: user.email });
  }

  stripUser(user: User) {
    delete user.password;
    delete user.sKey;
    return user;
  }
}
