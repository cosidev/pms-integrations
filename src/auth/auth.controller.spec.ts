import { PropertyGuard } from './guards/Property.guard';
import { JwtStrategy } from './jwt.strategy';
import { UserService } from './user.service';
import { generalConfig } from './../config';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';

describe('Auth Controller', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: generalConfig.authJwtSecret,
        }),
      ],
      controllers: [AuthController],
      providers: [AuthService, UserService, JwtStrategy, PropertyGuard],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
