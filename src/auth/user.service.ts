import { Injectable } from '@nestjs/common';
import { DynamoDB } from 'aws-sdk';
import { generalConfig } from '../config';
import { User } from './types';

const detailsKey = 'details';
const authTable = generalConfig.aws.authDynamoTableName;

@Injectable()
export class UserService {
  client: DynamoDB.DocumentClient;

  constructor() {
    this.client = new DynamoDB.DocumentClient({
      region: generalConfig.aws.dynamoRegion,
      endpoint: generalConfig.aws.dynamoEndpoint,
    });
  }

  async getUserById(id: string): Promise<User> {
    const result = await this.client.get({
      TableName: authTable,
      Key: { id, sKey: detailsKey },
    }).promise();
    return result.Item as User;
  }

  async getUserByEmail(email: string): Promise<User> {
    const result = await this.client.query({
      TableName: authTable,
      IndexName: 'emailUser',
      KeyConditionExpression: 'email = :value',
      ExpressionAttributeValues: {
        ':value': email,
      },
      Limit: 1,
    }).promise();
    return result.Items[0] as User;
  }

  async saveUser(user: User) {
    await this.client.put({
      TableName: authTable,
      Item: {
        id: user.id,
        sKey: detailsKey,
        ...user,
      },
    }).promise();
    return user;
  }

  async deleteUser(user: User) {
    return this.client.delete({
      TableName: authTable,
      Key: { id: user.id, sKey: detailsKey },
    });
  }
}
