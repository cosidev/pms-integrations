import axios from 'axios';
import logger from './LoggingService';
import { generalConfig } from './config';

interface Kcr {
  roomNumber: string;
  arrival: Date;
  departure: Date;
  name: string;
  reservationNumber: number;
}

interface Ker {
  roomNumber: string;
  reservationNumber: number;
}

interface KeyResponse {
  codes: {
    pinAccessCode: string;
    pinComfortCode: string;
  };
  extendedCodes: {
    pinAccessCode: string;
    pinComfortCode: string;
  };
}

const INTERFACE_API = 'https://sag.cosi-group.com';

export default class SagKeyhandler {
  async generate(info: Kcr): Promise<KeyResponse> {
    try {
      return this.request('generate', info);
    } catch (e) {
      logger.errorLog('Could not generate keys', { e, errorMessage: e.message, trace: e.trace });
      throw e;
    }
  }

  async remove(info: Ker) {
    try {
      return this.request('remove', info);
    } catch (e) {
      logger.errorLog('Could not delete keys', { e, errorMessage: e.message, trace: e.trace });
      throw e;
    }
  }

  async request(path: string, info: Kcr | Ker) {
    const { data } = await axios.post(`${INTERFACE_API}/${path}`, JSON.stringify(info), {
      headers: {
        Authorization: `Bearer ${generalConfig.sagInterfaceToken}`,
        'Content-Type': 'application/json',
      },
      timeout: 5000,
      timeoutErrorMessage: 'Connection timeout',
    });
    if (!data || !data.success) {
      throw new Error('Invalid response from sag-interface');
    }
    return data.data;
  }
}
