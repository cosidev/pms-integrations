import { Server } from 'http';
import * as serverlessExpress from 'aws-serverless-express';
import lumigo from '@lumigo/tracer';
import express from 'express';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { Context } from 'aws-lambda';
import { generalConfig } from './config';

const tracer = lumigo({ token: generalConfig.lumigoToken });

let lambdaProxy: Server;

async function bootstrap() {
  const expressServer = express();
  expressServer.disable('x-powered-by');
  const nestApp = await NestFactory.create(AppModule, new ExpressAdapter(expressServer));
  nestApp.enableCors();
  await nestApp.init();

  return serverlessExpress.createServer(expressServer);
}

export const handler = tracer.trace(async (event: any, context: Context) => {
  if (!lambdaProxy) {
    lambdaProxy = await bootstrap();
  }
  return serverlessExpress.proxy(lambdaProxy, event, context, 'PROMISE').promise;
});
