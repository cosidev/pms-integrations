import { generalConfig } from './config';
import { isAfter, isSameDay } from 'date-fns';
import { DesiredStayDetailsModel, DesiredTimeSliceModel, ReservationModel, TimeSliceModel } from './apaleo/generated/reservation';
import { DynamoClient } from './common/aws/dynamoClient';
import logger from './LoggingService';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { SlackService } from './SlackService';

interface AirBnBReservationData {
  oldGrossAmount: number;
  commission: number;
  newGrossAmount: number;
}

const table = generalConfig.aws.dataDynamoTableName;
const sKey = 'airbnb_commission';
const defaultExpands = ['booker', 'assignedUnits', 'timeSlices'];

/**
 * Adds airbnb commission to total gross amount on new reservations
 * Updates total gross amount of future timeslices when airbnb stay is amended
 */
export class AirBnBReservationUpdateHandler {
  constructor(
    private readonly dynamoClient: DynamoClient,
    private readonly apaleoClient: ApaleoClient,
    private readonly slackService: SlackService,
  ) {}

  async handleNewReservation(reservation: ReservationModel) {
    if (!this.shouldHandle(reservation)) {
      return reservation;
    }

    const reservationResponse = await this.dynamoClient.getItem<AirBnBReservationData>(table, { dataKey: reservation.id, sKey });

    if (reservationResponse) {
      return reservation;
    }

    await this.addCommissionToReservation(reservation);

    const newReservation = await this.apaleoClient.getReservationById(reservation.id, defaultExpands);
    const updateValues: AirBnBReservationData = {
      oldGrossAmount: reservation.totalGrossAmount.amount,
      commission: reservation.commission.commissionAmount.amount,
      newGrossAmount: newReservation.totalGrossAmount.amount,
    };
    await this.dynamoClient.updateItem<AirBnBReservationData>(table, { dataKey: reservation.id, sKey }, updateValues);

    return newReservation;
  }

  async handleAmmendedReservation(reservation: ReservationModel) {
    if (!this.shouldHandle(reservation)) {
      return reservation;
    }

    const { id, totalGrossAmount, commission } = reservation;
    const reservationResponse = await this.dynamoClient.getItem<AirBnBReservationData>(table, { dataKey: id, sKey });
    if (!reservationResponse) {
      return reservation;
    }

    const { newGrossAmount: initialGrossAmount } = reservationResponse;
    if (initialGrossAmount === totalGrossAmount.amount) {
      return reservation;
    }

    await this.slackService.sendAmmendedGrossAmount(
      `${id} (AirBnB) has a new total gross amount. Old = ${initialGrossAmount} Current: ${totalGrossAmount.amount}`,
    );

    const { futureTimeSlices, totalNights, pastTimeSlices, sumOfPastTimeslices } = this.postedAndNonPostedTimeslicesData(reservation);
    if (!futureTimeSlices.length) {
      return reservation;
    }

    const commissionAmount = commission?.commissionAmount?.amount || 0;
    const newTotalGrossAmount = futureTimeSlices[0].totalGrossAmount.amount * totalNights + commissionAmount;
    const pricePerNonPostedTimeslice = (newTotalGrossAmount - sumOfPastTimeslices) / futureTimeSlices.length;

    const amendedReservation = await this.updatePriceOfFutureTimeslices(reservation, pastTimeSlices, futureTimeSlices, pricePerNonPostedTimeslice);
    const updateValues: AirBnBReservationData = {
      oldGrossAmount: reservation.totalGrossAmount.amount,
      commission: reservation.commission.commissionAmount.amount,
      newGrossAmount: amendedReservation.totalGrossAmount.amount,
    };
    await this.dynamoClient.updateItem<AirBnBReservationData>(table, { dataKey: id, sKey }, updateValues);

    return amendedReservation;
  }

  private async updatePriceOfFutureTimeslices(
    reservation: ReservationModel,
    pastTimeSlices: TimeSliceModel[],
    futureTimeSlices: TimeSliceModel[],
    pricePerNonPostedTimeslice: number,
  ) {
    const timeSlices: DesiredTimeSliceModel[] = [];

    pastTimeSlices.forEach(timeslice => {
      timeSlices.push({
        ratePlanId: timeslice.ratePlan.id,
        totalGrossAmount: {
          amount: timeslice.totalGrossAmount.amount,
          currency: timeslice.totalGrossAmount.currency,
        },
      });
    });

    futureTimeSlices.forEach(timeslice => {
      timeSlices.push({
        ratePlanId: timeslice.ratePlan.id,
        totalGrossAmount: {
          amount: pricePerNonPostedTimeslice,
          currency: timeslice.totalGrossAmount.currency,
        },
      });
    });

    const request: DesiredStayDetailsModel = {
      arrival: (reservation.arrival as unknown) as string,
      departure: (reservation.departure as unknown) as string,
      adults: reservation.adults,
      childrenAges: reservation.childrenAges,
      requote: false,
      timeSlices,
    };

    logger.infoLog(`Updating future timeslices on ${reservation.id}`, {
      ammendRequest: request,
    });
    await this.apaleoClient.forceAmmendReservation(reservation.id, request);
    logger.infoLog(`Updated future timeslices on ${reservation.id}`);

    const amendedReservation = await this.apaleoClient.getReservationById(reservation.id, defaultExpands);

    return amendedReservation;
  }

  private async addCommissionToReservation(reservation: ReservationModel) {
    const timeSlices: DesiredTimeSliceModel[] = [];
    const nights = reservation.timeSlices.length;
    const commissionPerNight = reservation.commission.commissionAmount.amount / nights;
    reservation.timeSlices.forEach(slice => {
      timeSlices.push({
        ratePlanId: slice.ratePlan.id,
        totalGrossAmount: {
          amount: slice.totalGrossAmount.amount + commissionPerNight,
          currency: slice.totalGrossAmount.currency,
        },
      });
    });

    const request: DesiredStayDetailsModel = {
      arrival: (reservation.arrival as unknown) as string,
      departure: (reservation.departure as unknown) as string,
      adults: reservation.adults,
      childrenAges: reservation.childrenAges,
      requote: true,
      timeSlices,
    };

    logger.infoLog(`Updating airbnb commission on ${reservation.id}`, {
      ammedRequest: request,
    });
    await this.apaleoClient.forceAmmendReservation(reservation.id, request);
    logger.infoLog(`Updated airbnb commission on ${reservation.id}`);
  }

  private shouldHandle(reservation: ReservationModel) {
    if (reservation.source !== 'Airbnb') {
      return false;
    }

    const firstOfMarch = new Date('03/01/2020');
    const arrival = new Date(reservation.arrival);
    return isSameDay(firstOfMarch, arrival) || isAfter(arrival, firstOfMarch);
  }

  private postedAndNonPostedTimeslicesData(reservation: ReservationModel) {
    const { timeSlices = [] } = reservation;

    const totalNights = timeSlices.length;
    let sumOfPastTimeslices = 0;
    const pastTimeSlices: TimeSliceModel[] = [];
    const futureTimeSlices: TimeSliceModel[] = [];

    timeSlices.forEach((timeSlice): void => {
      const { actions = [] } = timeSlice;

      const isTimesliceInThePast = actions.some(timeSliceAction => {
        const AMEND_ACTION = 'Amend';
        if (timeSliceAction.action === AMEND_ACTION && timeSliceAction.isAllowed) {
          return false;
        }

        return true;
      });

      if (isTimesliceInThePast) {
        sumOfPastTimeslices += timeSlice.totalGrossAmount.amount;
        pastTimeSlices.push(timeSlice);
      } else {
        futureTimeSlices.push(timeSlice);
      }
    });

    return {
      totalNights,
      futureTimeSlices,
      pastTimeSlices,
      sumOfPastTimeslices,
    };
  }
}
