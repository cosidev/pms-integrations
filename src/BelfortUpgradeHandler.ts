import dateformat from 'dateformat';
import { ReservationModel } from './apaleo/generated/reservation';
import { ApaleoClient } from './apaleo/ApaleoClient';
import logger from './LoggingService';

const UPGRADE_UNIT_GROUPS = ['DE_BE_001-0006', 'DE_BE_001-0001'];

/** Handles automatic upgrading for doubleroom bookings at Belfort */
export default class BelfortUpgradeHandler {
  apaleoClient: ApaleoClient;

  constructor() {
    this.apaleoClient = new ApaleoClient();
  }

  async handleReservation(reservation: ReservationModel): Promise<ReservationModel> {
    if (!this.isInDateRange(reservation)) {
      return reservation;
    }
    if (this.shouldAssignRoom(reservation)) {
      logger.infoLog(`Assigning room to reservation ${reservation.id} for upgrading feature`);
      await this.apaleoClient.assignRoomToReservation(reservation.id);
      return reservation;
    }
    if (!this.isDoubleRoom(reservation)) {
      return reservation;
    }

    logger.infoLog(`Trying to upgrade ${reservation.id}`);
    const { units = [] } = await this.apaleoClient.getAvailableUnitsForReservation(reservation.id, {
      from: dateformat(reservation.arrival, 'isoUtcDateTime'),
      to: dateformat(reservation.departure, 'isoUtcDateTime'),
    });
    // Zweiraum Apartment mit Küche, Zweiraum Apartment mit Küche und Balkon
    let upgradeUnitId;
    UPGRADE_UNIT_GROUPS.forEach(upgradeUnitGroup => {
      units.forEach(unit => {
        if (!upgradeUnitId) {
          if (unit.unitGroup.id === upgradeUnitGroup) {
            upgradeUnitId = unit.id;
          }
        }
      });
    });
    if (!upgradeUnitId) {
      logger.infoLog(`Could not upgrade reservation ${reservation.id}. No suitable upgrade found`);
      return reservation;
    }

    await this.apaleoClient.assignFixedUnitToReservation(reservation.id, upgradeUnitId);
    logger.infoLog(`Assigned unit ${upgradeUnitId} to reservation ${reservation.id}`);
    return reservation;
  }

  private isDoubleRoom(reservation: ReservationModel): boolean {
    // Belfort Category Doubleroom (98,99)
    return reservation.unitGroup.id === 'DE_BE_001-0004';
  }

  private shouldAssignRoom(reservation: ReservationModel) {
    return UPGRADE_UNIT_GROUPS.includes(reservation.unitGroup.id);
  }

  private isInDateRange(reservation: ReservationModel): boolean {
    const allowedMonth = ['01/2020', '02/2020'];
    if (allowedMonth.includes(dateformat(reservation.arrival, 'mm/yyyy'))) {
      return true;
    }
    return false;
  }
}
