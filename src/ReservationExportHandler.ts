import { Pool } from 'pg';
import dateformat from 'dateformat';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { ReservationEvent } from './types';
import logger from './LoggingService';
import { EnumReservationModelTravelPurpose, BookingModel } from './apaleo/generated/reservation';
import { generalConfig } from './config';
import { FolioItemModel, EnumChargeModelServiceType } from './apaleo/generated/finance';

interface ExportBooking {
  id: string;
  bookingId: string;
  bookingComPaymentCharge: number;
}

interface ExportReservation {
  id: string;
  bookingId: string;
  propertyId: string;
  bookedAt: Date;
  checkIn: Date;
  checkOut: Date;
  guests: number;
  children: number;
  status: string;
  total: number;
  ratePlan: string;
  channel: string;
  source: string;
  travelPurpose: string;
  roomType: string;
  unitGroupId: string;
  unitId?: string;
  comissionAmount?: number;
  comissionCurrency?: string;
  cancelledAt?: Date;
}

interface ExportReservationDetails {
  id: string;
  reservationId: string;
  from: Date;
  to: Date;
  serviceDate: Date;
  rate: number;
}

interface ExportReservationFolios {
  id: string;
  reservationId: string;
  folioId: string;
  serviceDate: Date;
  serviceType: string;
  serviceReceipt: string;
  grossAmount: number;
  netAmount: number;
  vatType: string;
  currency: string;
  unitId: string;
  unitGroupId: string;
}

interface ExportReservationPayments {
  id: string;
  reservationId: string;
  folioId: string;
  paymentId: string;
  method: string;
  date: Date;
  amount: number;
  currency: string;
  merchantReference: string;
  pspReference: string;
  receipt: string;
  sourcePaymentId: string;
}

interface ExportUnit {
  id: string;
  unitId: string;
  unitGroupId: string;
  propertyId: string;
  unitGroupName: string;
  unitName: string;
  maxPersons: number;
  maintenance: string;
}

interface ExportProperty {
  id: string;
  propertyId: string;
  propertyName: string;
  addressLine1: string;
  postalCode: string;
  city: string;
  countryCode: string;
}

interface NightInfos {
  [key: string]: {
    unitId: string;
    unitGroupId: string;
  };
}

interface UpsertQueries {
  select: string;
  update: string;
  insert: string;
}

interface UpsertParams<T> {
  select: (x: T) => any;
  update: (x: T, y: T) => any;
  insert: (x: T) => any;
}

interface DbRow {
  id: string;
}

export class ReservationExportHandler {
  apaleoClient: ApaleoClient;
  database: Pool;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.database = new Pool(generalConfig.exportDatabase);
  }

  async exportAllReservations() {
    const { reservations = [] } = await this.apaleoClient.getReservations({ expand: ['timeSlices', 'assignedUnits'] });
    const { folios = [] } = await this.apaleoClient.getFolios({ expand: ['reservation'] });
    logger.infoLog(`Got ${reservations.length} reservations and ${folios.length} folios`);
    const foliosByReservation: any = {};
    folios.forEach(folio => {
      if (folio.reservation) {
        const resId = folio.reservation.id;
        const { [resId]: x = [] } = foliosByReservation;
        x.push(folio);
        foliosByReservation[resId] = x;
      }
    });
    const formattedReservations: ExportReservation[] = reservations.map(reservation => ({
      id: reservation.id,
      bookingId: reservation.channelCode,
      propertyId: reservation.property.id,
      bookedAt: reservation.created,
      checkIn: reservation.arrival,
      checkOut: reservation.departure,
      guests: reservation.adults,
      children: reservation.childrenAges ? reservation.childrenAges.length : 0,
      status: reservation.status,
      total: reservation.totalGrossAmount.amount,
      ratePlan: reservation.ratePlan.code,
      channel: reservation.channelCode,
      source: reservation.source,
      travelPurpose: reservation.travelPurpose || EnumReservationModelTravelPurpose.Leisure,
      roomType: reservation.unitGroup.name,
      unitGroupId: reservation.unitGroup.id,
      unitId: reservation.assignedUnits ? reservation.assignedUnits[0].unit.id : undefined,
      comissionAmount: reservation.commission ? reservation.commission.commissionAmount.amount : 0,
      comissionCurrency: reservation.commission ? reservation.commission.commissionAmount.currency : '',
      cancelledAt: reservation.cancellationTime || undefined,
    }));

    const details: ExportReservationDetails[] = [];
    const formattedFolios: ExportReservationFolios[] = [];
    const payments: ExportReservationPayments[] = [];
    const bookings: ExportBooking[] = [];
    const nights: NightInfos = {};
    const sleep = (ms: number) => new Promise(res => setTimeout(() => res(), ms));
    for (const reservation of reservations) {
      const booking = await this.apaleoClient.getBookingById(reservation.bookingId);
      bookings.push({
        id: undefined,
        bookingId: booking.id,
        bookingComPaymentCharge: this.getBookingComPaymentCharge(booking),
      });
      // Format details
      reservation.timeSlices.map(night => {
        details.push({
          id: undefined,
          reservationId: reservation.id,
          from: night.from,
          to: night.to,
          serviceDate: night.serviceDate,
          rate: night.baseAmount.grossAmount,
        });
        if (night.unit && night.unitGroup) {
          nights[this.formatServiceDate(night.serviceDate)] = {
            unitId: night.unit.id,
            unitGroupId: night.unitGroup.id,
          };
        }
      });
      // Load folios
      const { folioData, paymentData } = await this.getFolioData(foliosByReservation[reservation.id], reservation.id, nights);
      await sleep(250);
      formattedFolios.push(...folioData);
      payments.push(...paymentData);
    }

    await this.upsertReservations(formattedReservations);
    await this.upsertBookings(bookings);
    await this.upsertReservationDetails(details);
    await this.upsertReservationFolios(formattedFolios, formattedReservations);
    await this.upsertReservationPayments(payments);
  }

  async exportChangedReservation(reservation: ReservationEvent) {
    const { data } = reservation;
    const exportReservation: ExportReservation = {
      id: data.reservationId,
      bookingId: data.bookingId,
      propertyId: data.property.id,
      checkIn: data.checkIn,
      checkOut: data.checkOut,
      guests: data.adults,
      children: data.children,
      status: data.status,
      roomType: data.roomType,
      total: data.totalGrossAmount,
      ratePlan: data.ratePlanCode,
      channel: data.channelCode,
      source: data.source,
      travelPurpose: data.travelPurpose || EnumReservationModelTravelPurpose.Leisure,
      bookedAt: data.createdAt,
      unitId: reservation.data.unit.unitId,
      unitGroupId: reservation.data.unit.unitGroupId,
      comissionAmount: data.comissionAmount,
      comissionCurrency: data.comissionCurrency,
      cancelledAt: data.cancellationTime || undefined,
    };

    const details: ExportReservationDetails[] = data.nights.map(night => ({
      id: undefined,
      reservationId: data.reservationId,
      from: night.from,
      to: night.to,
      serviceDate: night.serviceDate,
      rate: night.rate,
    }));

    const nights: NightInfos = {};
    data.nights.forEach(night => {
      nights[this.formatServiceDate(night.serviceDate)] = {
        unitGroupId: night.unitGroupId,
        unitId: night.unitId,
      };
    });

    const booking = await this.apaleoClient.getBookingById(data.bookingId);
    const exportBooking: ExportBooking = {
      id: undefined,
      bookingId: booking.id,
      bookingComPaymentCharge: this.getBookingComPaymentCharge(booking),
    };

    const { folios = [] } = await this.apaleoClient.getFoliosByReservationId(data.reservationId);
    const { folioData, paymentData } = await this.getFolioData(folios, data.reservationId, nights);
    await this.upsertReservations([exportReservation]);
    await this.upsertBookings([exportBooking]);
    await this.upsertReservationDetails(details);
    await this.upsertReservationFolios(folioData, [exportReservation]);
    await this.upsertReservationPayments(paymentData);
  }

  async exportAllUnits() {
    const { units = [] } = await this.apaleoClient.getUnits({ expand: ['unitGroup'] });
    const exportUnits = units
      .filter(u => u.unitGroup)
      .map(unit => ({
        id: undefined,
        unitId: unit.id,
        unitGroupId: unit.unitGroup.id,
        propertyId: unit.property.id,
        unitGroupName: unit.unitGroup.name,
        unitName: unit.name,
        maxPersons: unit.maxPersons,
        maintenance: unit.status.maintenance ? unit.status.maintenance.type : undefined,
      }));

    await this.upsertUnits(exportUnits);
  }

  async exportAllProperties() {
    const { properties = [] } = await this.apaleoClient.getProperties();

    const exportProperties = properties.map(property => {
      return {
        id: undefined,
        propertyId: property.id,
        propertyName: property.name,
        addressLine1: property.location.addressLine1 ? property.location.addressLine1 : undefined,
        postalCode: property.location.postalCode ? property.location.postalCode : undefined,
        city: property.location.city ? property.location.city : undefined,
        countryCode: property.location.countryCode ? property.location.countryCode : undefined,
      };
    });

    await this.upsertProperties(exportProperties);
  }

  private formatServiceDate(date: Date) {
    return dateformat(date, 'ddmmyyyy');
  }

  private async getFolioData(folios: FolioItemModel[], reservationId: string, nights: NightInfos) {
    const folioData: ExportReservationFolios[] = [];
    const paymentData: ExportReservationPayments[] = [];
    logger.infoLog(`Got ${folios.length} folios for reservation ${reservationId}`);
    for (const folio of folios) {
      const { charges = [], allowances = [], payments = [] } = await this.apaleoClient.getFolioById(folio.id);
      charges.forEach(charge => {
        // Calculate amount (Charge - Allowances)
        let grossAmount = charge.amount.grossAmount;
        let netAmount = charge.amount.netAmount;
        allowances
          .filter(a => a.sourceChargeId === charge.id)
          .forEach(allowance => {
            grossAmount -= allowance.amount.grossAmount;
            netAmount -= allowance.amount.netAmount;
          });
        const isAccommodation = charge.serviceType === EnumChargeModelServiceType.Accommodation;
        const unitInfo = nights[this.formatServiceDate(charge.serviceDate)] || false;
        const unitId = isAccommodation && unitInfo ? unitInfo.unitId : undefined;
        const unitGroupId = isAccommodation && unitInfo ? unitInfo.unitGroupId : undefined;
        folioData.push({
          id: undefined,
          reservationId,
          folioId: folio.id,
          serviceDate: charge.serviceDate,
          serviceType: charge.serviceType,
          serviceReceipt: charge.receipt,
          currency: charge.amount.currency,
          vatType: charge.amount.vatType,
          grossAmount,
          netAmount,
          unitId,
          unitGroupId,
        });
      });
      payments.forEach(payment => {
        const { externalReference } = payment;
        paymentData.push({
          id: undefined,
          reservationId,
          folioId: folio.id,
          paymentId: payment.id,
          date: payment.paymentDate,
          amount: payment.amount.amount,
          currency: payment.amount.currency,
          merchantReference: externalReference ? externalReference.merchantReference : '',
          pspReference: externalReference ? externalReference.pspReference : '',
          method: payment.method,
          receipt: payment.receipt,
          sourcePaymentId: payment.sourcePaymentId,
        });
      });
    }
    return { folioData, paymentData };
  }

  private async upsertReservations(reservations: ExportReservation[]) {
    logger.infoLog(`Upserting ${reservations.length} reservations`);
    const queries = {
      select: 'SELECT id from reservations WHERE reservation_id = $1',
      update:
        'UPDATE reservations SET check_in = $1, check_out = $2, guests = $3, status = $4, total = $5, rateplan = $6, channel = $7, source = $8, travelpurpose = $9, roomtype = $10, unit_group_id = $11, unit_id = $12, comission_amount = $13, comission_currency = $14, children = $15, cancelled_at = $16, updated_at = now() WHERE id = $17',
      insert: `INSERT INTO reservations (reservation_id, booking_id, property_id, booked_at, check_in, check_out, guests, status, total, rateplan, channel, source, travelpurpose, roomtype, unit_group_id, unit_id, comission_amount, comission_currency, children, cancelled_at)
  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportReservation> = {
      select: reservation => [reservation.id],
      update: (reservation, dbEntry) => [
        reservation.checkIn,
        reservation.checkOut,
        reservation.guests,
        reservation.status,
        reservation.total,
        reservation.ratePlan,
        reservation.channel,
        reservation.source,
        reservation.travelPurpose,
        reservation.roomType,
        reservation.unitGroupId,
        reservation.unitId,
        reservation.comissionAmount,
        reservation.comissionCurrency,
        reservation.children,
        reservation.cancelledAt,
        dbEntry.id,
      ],
      insert: reservation => [
        reservation.id,
        reservation.bookingId,
        reservation.propertyId,
        reservation.bookedAt,
        reservation.checkIn,
        reservation.checkOut,
        reservation.guests,
        reservation.status,
        reservation.total,
        reservation.ratePlan,
        reservation.channel,
        reservation.source,
        reservation.travelPurpose,
        reservation.roomType,
        reservation.unitGroupId,
        reservation.unitId,
        reservation.comissionAmount,
        reservation.comissionCurrency,
        reservation.children,
        reservation.cancelledAt,
      ],
    };
    await this.upsertData(reservations, queries, queryParams);
    logger.infoLog('Upserted reservations');
  }

  private getBookingComPaymentCharge(booking: BookingModel) {
    const regExp = /Payment charge is EUR ([0-9]*\.[0-9]*)?[0-9]*/;
    const comment = booking.bookerComment || '';
    const match = comment.match(regExp);
    if (match && match[0]) {
      return (match[0].replace('Payment charge is EUR', '') as unknown) as number;
    }
    return 0;
  }

  private async upsertReservationDetails(reservationDetails: ExportReservationDetails[]) {
    logger.infoLog(`Upserting ${reservationDetails.length} reservationDetails`);
    const queries = {
      select: 'SELECT id FROM reservation_details WHERE reservation_id = $1 AND service_date = $2',
      update: 'UPDATE reservation_details SET "from" = $1, "to" = $2, service_date = $3, rate = $4, updated_at = now() WHERE id = $5',
      insert: `INSERT INTO reservation_details (reservation_id, "from", "to", service_date, rate) VALUES ($1, $2, $3, $4, $5) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportReservationDetails> = {
      select: details => [details.reservationId, details.serviceDate],
      update: (details, dbEntry) => [details.from, details.to, details.serviceDate, details.rate, dbEntry.id],
      insert: details => [details.reservationId, details.from, details.to, details.serviceDate, details.rate],
    };
    await this.upsertData(reservationDetails, queries, queryParams);
    logger.infoLog('Upserted reservationDetails');
  }

  private async upsertReservationFolios(folios: ExportReservationFolios[], reservations: ExportReservation[]) {
    logger.infoLog(`Upserting ${folios.length} folios`);
    const queries = {
      select: 'SELECT id FROM reservation_folios WHERE folio_id = $1 AND service_date = $2 AND service_type = $3',
      update:
        'UPDATE reservation_folios SET service_date = $1, service_receipt = $2, gross_amount = $3, net_amount = $4, vat_type = $5, currency = $6, unit_id = $7, unit_group_id = $8, updated_at = now() WHERE id = $9',
      insert: `INSERT INTO reservation_folios (reservation_id, folio_id, service_date, service_type, service_receipt, gross_amount, net_amount, vat_type, currency, unit_id, unit_group_id)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportReservationFolios> = {
      select: folio => [folio.folioId, folio.serviceDate, folio.serviceType],
      update: (folio, dbEntry) => [
        folio.serviceDate,
        folio.serviceReceipt,
        folio.grossAmount,
        folio.netAmount,
        folio.vatType,
        folio.currency,
        folio.unitId,
        folio.unitGroupId,
        dbEntry.id,
      ],
      insert: folio => [
        folio.reservationId,
        folio.folioId,
        folio.serviceDate,
        folio.serviceType,
        folio.serviceReceipt,
        folio.grossAmount,
        folio.netAmount,
        folio.vatType,
        folio.currency,
        folio.unitId,
        folio.unitGroupId,
      ],
    };
    const upsertedIds = await this.upsertData(folios, queries, queryParams);
    upsertedIds.push('0');
    const reservationIds = reservations.map(x => x.id);
    const reservationIdParams = Object.keys(reservationIds).map((x, i) => `$${i + 1}`);

    // Delete unused rows
    const idParams = upsertedIds.map((x, i) => `$${i + 1 + reservationIdParams.length}`);

    await this.database.connect();
    await this.database.query(
      `DELETE FROM reservation_folios WHERE reservation_id IN (${reservationIdParams.join(',')}) AND id NOT IN (${idParams.join(',')})`,
      [...reservationIds, ...upsertedIds],
    );
    logger.infoLog('Upserted folios');
  }

  private async upsertReservationPayments(payments: ExportReservationPayments[]) {
    logger.infoLog(`Upserting ${payments.length} payments`);
    const queries = {
      select: 'SELECT id FROM reservation_payments WHERE folio_id = $1 AND payment_id = $2',
      update: `UPDATE reservation_payments SET method = $1, date = $2, amount = $3, currency = $4, merchant_reference = $5,
        psp_reference = $6, receipt = $7, source_payment_id = $8, updated_at = now() WHERE id = $9`,
      insert: `INSERT INTO reservation_payments (reservation_id, folio_id, method, date, amount, currency, merchant_reference,
        psp_reference, receipt, payment_id, source_payment_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportReservationPayments> = {
      select: payment => [payment.folioId, payment.paymentId],
      update: (payment, dbEntry) => [
        payment.method,
        payment.date,
        payment.amount,
        payment.currency,
        payment.merchantReference,
        payment.pspReference,
        payment.receipt,
        payment.sourcePaymentId,
        dbEntry.id,
      ],
      insert: payment => [
        payment.reservationId,
        payment.folioId,
        payment.method,
        payment.date,
        payment.amount,
        payment.currency,
        payment.merchantReference,
        payment.pspReference,
        payment.receipt,
        payment.paymentId,
        payment.sourcePaymentId,
      ],
    };
    await this.upsertData(payments, queries, queryParams);
    logger.infoLog('Upserted payments');
  }

  private async upsertBookings(bookings: ExportBooking[]) {
    logger.infoLog(`Upserting ${bookings.length} bookings`);
    const queries = {
      select: 'SELECT id FROM bookings WHERE booking_id = $1',
      update: 'UPDATE bookings SET booking_com_payment_charge = $1 WHERE id = $2',
      insert: `INSERT INTO bookings (booking_id, booking_com_payment_charge)
      VALUES ($1, $2) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportBooking> = {
      select: booking => [booking.bookingId],
      update: (booking, dbBooking) => [booking.bookingComPaymentCharge, dbBooking.id],
      insert: booking => [booking.bookingId, booking.bookingComPaymentCharge],
    };
    await this.upsertData<ExportBooking>(bookings, queries, queryParams);
    logger.infoLog('Upserted bookings');
  }

  private async upsertUnits(units: ExportUnit[]) {
    logger.infoLog(`Upserting ${units.length} units`);
    const queries = {
      select: "SELECT id FROM units WHERE unit_id = $1 AND export_date = date_trunc('day',now())",
      update:
        "UPDATE units SET unit_group_id = $1, property_id = $2, unit_group_name = $3, unit_name = $4, max_persons = $5, maintenance = $6 WHERE id = $7  AND export_date = date_trunc('day',now())",
      insert: `INSERT INTO units (unit_id, unit_group_id, property_id, unit_group_name, unit_name, max_persons, maintenance, export_date)
      VALUES ($1, $2, $3, $4, $5, $6, $7 , NOW()) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportUnit> = {
      select: unit => [unit.unitId],
      update: (unit, dbUnit) => [unit.unitGroupId, unit.propertyId, unit.unitGroupName, unit.unitName, unit.maxPersons, unit.maintenance, dbUnit.id],
      insert: unit => [unit.unitId, unit.unitGroupId, unit.propertyId, unit.unitGroupName, unit.unitName, unit.maxPersons, unit.maintenance],
    };
    await this.upsertData<ExportUnit>(units, queries, queryParams);
    logger.infoLog('Upserted units');
  }

  private async upsertProperties(properties: ExportProperty[]) {
    logger.infoLog(`Upserting ${properties.length} properties`);
    const queries = {
      select: 'SELECT id FROM properties WHERE property_id = $1',
      update:
        'UPDATE properties SET property_id = $1, property_name = $2, address_line_1 = $3, postal_code = $4, city = $5, country_code = $6 WHERE id = $7',
      insert: `INSERT INTO properties (property_id, property_name, address_line_1, postal_code, city, country_code)
      VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
    };
    const queryParams: UpsertParams<ExportProperty> = {
      select: property => [property.propertyId],
      update: (property, dbProperty) => [
        property.propertyId,
        property.propertyName,
        property.addressLine1,
        property.postalCode,
        property.city,
        property.countryCode,
        dbProperty.id,
      ],
      insert: property => [
        property.propertyId,
        property.propertyName,
        property.addressLine1,
        property.postalCode,
        property.city,
        property.countryCode,
      ],
    };
    await this.upsertData<ExportProperty>(properties, queries, queryParams);
    logger.infoLog('Upserted properties');
  }

  private async upsertData<T extends DbRow>(data: T[], queries: UpsertQueries, queryParams: UpsertParams<T>): Promise<string[]> {
    await this.database.connect();
    const idList = [];
    const promises = data.map(async entry => {
      try {
        const result = await this.database.query<T>(queries.select, queryParams.select(entry));
        if (result.rowCount > 0) {
          await this.database.query(queries.update, queryParams.update(entry, result.rows[0]));
          idList.push(result.rows[0].id);
        } else {
          const insert = await this.database.query<T>(queries.insert, queryParams.insert(entry));
          idList.push(insert.rows[0].id);
        }
      } catch (e) {
        logger.errorLog(`Could not upsert reservation folio`, { error: e, errorMessage: e.message, errorTrace: e.stack, entry });
      }
    });
    await Promise.all(promises);
    return idList;
  }
}
