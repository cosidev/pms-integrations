import Axios, { AxiosInstance, Method } from 'axios';
import { generalConfig } from './config';
import logger from './LoggingService';

interface RecieveCustomMessage {
  sender: {
    contact_id?: string;
    name?: string;
    handle: string;
  };
  subject?: string;
  body: string;
  body_format?: string;
}

export interface Handle {
  handle: string;
  source: string;
}

export interface Contact {
  name: string;
  description: string;
  handles: Handle[];
}

export class FrontClient {
  axios: AxiosInstance;

  constructor() {
    this.axios = Axios.create({
      baseURL: 'https://api2.frontapp.com/',
      headers: {
        'Authorization': `Bearer ${generalConfig.frontApiToken}`,
        'Content-Type': 'application/json',
      },
    });
  }

  async receiveCustomMessage(channelId: string, params: RecieveCustomMessage) {
    const path = `channels/${channelId}/incoming_messages`;
    return this.request('POST', path, params);
  }

  async createContact(params: Contact) {
    const path = 'contacts';
    return this.request('POST', path, params);
  }

  private async request(method: Method, path: string, params: any) {
    try {
      const response = await this.axios.request({
        method,
        url: path,
        data: params,
      });
      return response.data;
    } catch (e) {
      logger.errorLog(`Could not get response from front api`, { e });
      throw e;
    }
  }
}
