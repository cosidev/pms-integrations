import { UnitGroupAvailabilityItemModel } from '../apaleo/generated/availability';

export interface IndexSignature {
  [key: string]: string;
}

export interface OverbookingsPropertyModel {
  id: string;
  name: string;
}

export interface ReservationProps {
  arrival: string;
  departure: string;
  reservationId: string;
}

export interface OverbookingUpdateData {
  id: string;
  from: string;
  to: string;
  allowedOverbooking: number;
}

export interface OverbookingUpdateTracker {
  [Key: string]: OverbookingUpdateData;
}

export interface OverbookingDataSlice {
  availableCount: number;
  sellableCount: number;
  allowedOverbookingCount: number;
  unitGroupId: string;
}

export interface OverbookingTimeSlices {
  [key: string]: {
    [key: string]: OverbookingDataSlice,
  };
}

export interface OverbookingWarningTimeSlices {
  [key: string]: UnitGroupAvailabilityItemModel[];
}

interface ActionConfig {
  sourceData: OverbookingDataSlice;
  virtualAvailabilityUpdateTracker: OverbookingUpdateTracker;
  timeStamp: string;
}

export interface TargetResizeConfig extends ActionConfig {
  targetResizeList: OverbookingDataSlice[];
  totalDeductedCount: number;
}

export interface SourceAvailabilityActionConfig extends ActionConfig {
  virtualAvailabilityCount: number;
}
