import { Test, TestingModule } from '@nestjs/testing';
import { OverbookingsService } from './overbookings.service';
import { AppModule } from '../app.module';
import {
  APALEO_UNIT_GROUPS, APALEO_UNIT_GROUPS_D2, APALEO_UNIT_GROUPS_D3, FAMILY_CRASH_PAD,
} from './__mocks__/overbookingsVirtualAvailability.mock';
import { UnitGroupAvailabilityTimeSliceItemModel } from '../apaleo/generated/availability';
import { OverbookingUpdateData } from './overbookings.interface';
import { OverbookingsAvailability } from './OverbookingsAvailability';

const OVERBOOKING_CONFIG = {
  'DE_DEV_003-INTIMATE': 'DE_DEV_003-DOUBLEROOM',
  'DE_DEV_003-STUDIO': 'DE_DEV_003-DOUBLEROOM',
};
const SOURCE_TARGET_CONFIG = [
  'DE_DEV_003-STUDIO',
  'DE_DEV_003-DOUBLEROOM',
  'DE_DEV_003-INTIMATE',
];

function getTotalBookings(
  physicalCount: number,
  availableCount: number,
) {
  if (availableCount === physicalCount) { return 0; }
  if (availableCount > 0) { return physicalCount - availableCount; }
  return Math.abs(availableCount) + physicalCount;
}

function getSellableCount(
  physicalCount: number,
  availableCount: number,
  allowedOverbooking: number,
) {
  const totalBookings = getTotalBookings(physicalCount, availableCount);
  return (physicalCount + allowedOverbooking) - totalBookings;
}

function simulateBooking(
  updatedUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
  updateValues: Array<{ id: string; bookingsCount: number }>,
) {
  updateValues.forEach(value => {
    updatedUnitGroups[0].unitGroups.map(unitGroup => {
      if (unitGroup.unitGroup.id === value.id) {
        unitGroup.availableCount = unitGroup.availableCount - value.bookingsCount;
        unitGroup.sellableCount = getSellableCount(
          unitGroup.physicalCount,
          unitGroup.availableCount,
          unitGroup.allowedOverbookingCount,
        );
        return unitGroup;
      }
    });
  });

  return updatedUnitGroups;
}

function simulateAvailabilityUpdate(
  availableUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
  updateValues: OverbookingUpdateData[],
) {
  updateValues.forEach(timeSlice => {
    availableUnitGroups[0].unitGroups.map(unitGroup => {
      if (unitGroup.unitGroup.id === timeSlice.id) {
        unitGroup.allowedOverbookingCount = timeSlice.allowedOverbooking;
        unitGroup.sellableCount = getSellableCount(
          unitGroup.physicalCount,
          unitGroup.availableCount,
          timeSlice.allowedOverbooking,
        );

        const totalBookings = getTotalBookings(
          unitGroup.physicalCount,
          unitGroup.availableCount,
        );
        unitGroup.availableCount = unitGroup.physicalCount - totalBookings;
      }
      return unitGroup;
    });
  });

  return availableUnitGroups;
}

function triggerTestAssertion(
  updatedUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
  expectedResult: Array<{
    unitGroupId: string,
    allowedOverbookingCount: number,
    availableCount?: number,
  }>,
) {
  for (const assertionConfig of expectedResult) {
    const { unitGroupId, allowedOverbookingCount, availableCount } = assertionConfig;
    updatedUnitGroups[0].unitGroups.forEach(unitGroup => {
      if (unitGroup.unitGroup.id === unitGroupId) {
        expect(unitGroup.allowedOverbookingCount).toBe(allowedOverbookingCount);

        if (availableCount) {
          expect(unitGroup.availableCount).toBe(availableCount);
        }
      }
    });
  }
}

describe('Overbookings Virtual Availability', () => {
  let overbookingsService: OverbookingsService;
  let overbookingsAvailability: OverbookingsAvailability;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    overbookingsService = module.get<OverbookingsService>(OverbookingsService);
    overbookingsAvailability = module.get<OverbookingsAvailability>(OverbookingsAvailability);
  });

  function getVirtualAvailability(
    unitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
  ) {
    const formattedData = overbookingsService.availabilityFormat(unitGroups, SOURCE_TARGET_CONFIG);
    return overbookingsAvailability.getVirtualAvailability(formattedData, OVERBOOKING_CONFIG);
  }

  describe('One-to-One source-target system', () => {
    let availableUnitGroupsD1: UnitGroupAvailabilityTimeSliceItemModel[];

    beforeAll(() => {
      availableUnitGroupsD1 = APALEO_UNIT_GROUPS;
    });

    it('should simulate virtual availability seed', async () => {
      const updateValues = getVirtualAvailability(availableUnitGroupsD1);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD1, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
          },
        ],
      );
    });

    it('should drop allowedOverbookingCount on DOUBLEROOM if availability of STUDIO is less than one', async () => {
      const newUnitGroupData = simulateBooking(availableUnitGroupsD1, [{ id: 'DE_DEV_003-STUDIO', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(newUnitGroupData);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD1, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 0,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
          },
        ],
      );
    });

    it('should add allowedOverbookingCount on DOUBLEROOM if availability of STUDIO is free and greater than one', async () => {
      const restoredStudioAvailability = simulateBooking(availableUnitGroupsD1, [{ id: 'DE_DEV_003-STUDIO', bookingsCount: -1 }]);
      const updateValues = getVirtualAvailability(restoredStudioAvailability);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD1, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
          },
        ],
      );
    });

    it('should decrease studio availability if DOUBLEROOM is maxed out', async () => {
      const maxedOutDoubleRoom = simulateBooking(availableUnitGroupsD1, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 3 }]);
      const updateValues = getVirtualAvailability(maxedOutDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD1, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
          },
        ],
      );
    });

    it('should increment studio availability if DOUBLEROOM cancellation increases availability', async () => {
      availableUnitGroupsD1 = simulateBooking(availableUnitGroupsD1, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: -1 }]);
      const updateValues = getVirtualAvailability(availableUnitGroupsD1);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD1, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 1,
            availableCount: 0,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
        ],
      );
    });
  });

  describe('Dynamic virtual availability calculator', () => {
    let availableUnitGroupsD2: UnitGroupAvailabilityTimeSliceItemModel[];

    beforeAll(() => {
      availableUnitGroupsD2 = APALEO_UNIT_GROUPS_D2;
    });

    it('should simulate virtual availability seed', async () => {
      const updateValues = getVirtualAvailability(availableUnitGroupsD2);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should maintain allowedOverbookingCount on DOUBLEROOM if availability of STUDIO is the same', async () => {
      const newUnitGroupData = simulateBooking(availableUnitGroupsD2, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(newUnitGroupData);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: 1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should decrement allowedOverbookingCount on DOUBLEROOM if availability of STUDIO reduces', async () => {
      const restoredStudioAvailability = simulateBooking(availableUnitGroupsD2, [{ id: 'DE_DEV_003-STUDIO', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(restoredStudioAvailability);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 1,
            availableCount: 1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
        ],
      );
    });

    it('should increment virtual availability of DOUBLEROOM if STUDIO reservation is cancelled', async () => {
      const maxedOutDoubleRoom = simulateBooking(availableUnitGroupsD2, [{ id: 'DE_DEV_003-STUDIO', bookingsCount: -1 }]);
      const updateValues = getVirtualAvailability(maxedOutDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: 1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should decrement studio availability if DOUBLEROOM virtual unit is booked', async () => {
      const availableDoubleRoom = simulateBooking(availableUnitGroupsD2, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 2 }]);
      const updateValues = getVirtualAvailability(availableDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: -1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
        ],
      );
    });

    it('should decrement studio availability if DOUBLEROOM virtual unit is booked', async () => {
      const availableDoubleRoom = simulateBooking(availableUnitGroupsD2, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(availableDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD2, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: -2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -2,
          },
        ],
      );
    });

    it('should restore STUDIO availability if DOUBLEROOM reservation is cancelled', async () => {
      const NUMBER_OF_DOUBLEROOM_BOOKINGS = 4;
      let availableDoubleRoom: UnitGroupAvailabilityTimeSliceItemModel[];
      let updateValues: OverbookingUpdateData[];
      let updatedGroups: UnitGroupAvailabilityTimeSliceItemModel[] = availableUnitGroupsD2;

      for (let bookingCount = 1; bookingCount <= NUMBER_OF_DOUBLEROOM_BOOKINGS; ++bookingCount) {
        availableDoubleRoom = simulateBooking(updatedGroups, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: -1 }]);
        updateValues = getVirtualAvailability(availableDoubleRoom);
        updatedGroups = simulateAvailabilityUpdate(updatedGroups, updateValues);
      }

      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 2,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
          },
        ],
      );
    });
  });

  describe('Multi-categories system', () => {
    let availableUnitGroupsD3: UnitGroupAvailabilityTimeSliceItemModel[];

    beforeAll(() => {
      availableUnitGroupsD3 = APALEO_UNIT_GROUPS_D3;
    });

    it('should simulate virtual availability seed', async () => {
      const updateValues = getVirtualAvailability(availableUnitGroupsD3);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should maintain allowedOverbookingCount on DOUBLEROOM if availability of targets remain unchanged', async () => {
      const newUnitGroupData = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 2 }]);
      const updateValues = getVirtualAvailability(newUnitGroupData);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: 0,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
        ],
      );
    });

    it('should decrement allowedOverbookingCount on largest target if DOUBLEROOM virtual unit is booked', async () => {
      const bookedDoubleRoom = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(bookedDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: -1,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
        ],
      );
    });

    it('should decrement by deletion order if all targets share the same availableCount', async () => {
      const bookedDoubleRoom = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(bookedDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: -2,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -2,
            availableCount: 3,
          },
        ],
      );
    });

    it('should decrement allowedOverbookingCount on largest target if DOUBLEROOM virtual unit is booked', async () => {
      const bookedDoubleRoom = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(bookedDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: -3,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: -1,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -2,
            availableCount: 3,
          },
        ],
      );
    });

    it('should upgrade reduced availability on all targets if DOUBLEROOM reservation is cancelled', async () => {
      const NUMBER_OF_DOUBLEROOM_BOOKINGS = 3;
      let availableDoubleRoom: UnitGroupAvailabilityTimeSliceItemModel[];
      let updateValues: OverbookingUpdateData[];
      let updatedGroups: UnitGroupAvailabilityTimeSliceItemModel[] = availableUnitGroupsD3;
      const assertionList = [
        [
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: -1,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
          },
        ],
        [
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
          },
        ],
        [
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
          },
        ],
      ];

      for (let bookingCount = 0; bookingCount < NUMBER_OF_DOUBLEROOM_BOOKINGS; ++bookingCount) {
        availableDoubleRoom = simulateBooking(updatedGroups, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: -1 }]);
        updateValues = getVirtualAvailability(availableDoubleRoom);
        updatedGroups = simulateAvailabilityUpdate(updatedGroups, updateValues);
        triggerTestAssertion(
          updatedGroups,
          assertionList[bookingCount],
        );
      }

      updateValues = getVirtualAvailability(updatedGroups);
      updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should reduce allowedOverbookingCount on largest target if availability of source drops below zero', async () => {
      // restore available count on source
      let newUnitGroupData = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: -2 }]);
      // add 3 new bookings
      newUnitGroupData = simulateBooking(newUnitGroupData, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 3 }]);
      const updateValues = getVirtualAvailability(newUnitGroupData);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 3,
            availableCount: -1,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
        ],
      );
    });

    it('should accurately calculate allowedOverbookingCount on source if a new target is added', async () => {
      OVERBOOKING_CONFIG['DE_DEV_003-DEV3'] = 'DE_DEV_003-DOUBLEROOM';
      SOURCE_TARGET_CONFIG.push('DE_DEV_003-DEV3');

      availableUnitGroupsD3 = [
        {
          ...availableUnitGroupsD3[0],
          unitGroups: [
            ...availableUnitGroupsD3[0].unitGroups,
            FAMILY_CRASH_PAD,
          ],
        },
      ];

      const updateValues = getVirtualAvailability(availableUnitGroupsD3);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 5,
            availableCount: -1,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
          {
            unitGroupId: 'DE_DEV_003-DEV3',
            allowedOverbookingCount: 0,
            availableCount: 3,
          },
        ],
      );
    });

    it('should enforce deletion order last target in the config first(if the greater targets are equal)', async () => {
      const bookedDoubleRoom = simulateBooking(availableUnitGroupsD3, [{ id: 'DE_DEV_003-DOUBLEROOM', bookingsCount: 1 }]);
      const updateValues = getVirtualAvailability(bookedDoubleRoom);
      const updatedGroups = simulateAvailabilityUpdate(availableUnitGroupsD3, updateValues);
      triggerTestAssertion(
        updatedGroups,
        [
          {
            unitGroupId: 'DE_DEV_003-DOUBLEROOM',
            allowedOverbookingCount: 5,
            availableCount: -2,
          },
          {
            unitGroupId: 'DE_DEV_003-INTIMATE',
            allowedOverbookingCount: 0,
            availableCount: 2,
          },
          {
            unitGroupId: 'DE_DEV_003-STUDIO',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
          {
            unitGroupId: 'DE_DEV_003-DEV3',
            allowedOverbookingCount: -1,
            availableCount: 3,
          },
        ],
      );
    });
  });
});
