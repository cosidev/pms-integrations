import { Test, TestingModule } from '@nestjs/testing';
import { addDays, addMonths, format } from 'date-fns';
import { OverbookingsService } from './overbookings.service';
import { AppModule } from '../app.module';
import { SlackService } from '../SlackService';
import {
  AVAILABLE_UNIT_GROUPS,
  GROUPS_WITH_ALLOWED_OVERBOOKING,
  PROPERTIES,
  OVERBOOKED_GROUP,
  VIRTURL_AVAILABILITY_DATA_FORMAT,
  AVAILABLE_UNIT_GROUPS_SEED,
  DISABLED_AVAILABILITY_MOCK,
  RESET_OVERBOOKINGS_MOCK,
} from './__mocks__/overbookings.mock';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import { ReservationModel } from '../apaleo/generated/reservation';
import { OverbookingsReport } from './OverbookingsReport';

function formatDate(date: Date): string {
  return format(date, 'yyyy-MM-dd');
}

jest.mock('../LoggingService.ts');

describe('OverbookingsService', () => {
  let overbookingsService: OverbookingsService;
  let overbookingsReport: OverbookingsReport;
  let apaleoClient: ApaleoClient;
  let slack: SlackService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    slack = module.get<SlackService>(SlackService);
    overbookingsService = module.get<OverbookingsService>(OverbookingsService);
    overbookingsReport = module.get<OverbookingsReport>(OverbookingsReport);
  });

  it('should send notification if unit groups occupancy is not maxed out', async () => {
    jest.spyOn(apaleoClient, 'getProperties')
      .mockImplementation(() => new Promise(resolve => resolve({
        properties: PROPERTIES,
        count: 0,
      })));
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve(AVAILABLE_UNIT_GROUPS)));
    jest.spyOn(slack, 'sendOverbookingsReport')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.sendDailyOverbookingsReport();
    expect(apaleoClient.getProperties).toBeCalled();
    expect(slack.sendOverbookingsReport).toBeCalledWith(`
    :warning: *OVERBOOKINGS WARNING ALERT* :warning: \n

    No overbookings found
    `);
  });

  it('should send not found notification if virtual availability exists', async () => {
    jest.spyOn(apaleoClient, 'getProperties')
      .mockImplementation(() => new Promise(resolve => resolve({
        properties: PROPERTIES,
        count: 0,
      })));
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve({
        timeSlices: [{
          ...OVERBOOKED_GROUP.timeSlices[0],
          unitGroups: [{
            ...OVERBOOKED_GROUP.timeSlices[0].unitGroups[0],
            allowedOverbookingCount: 1,
          }],
        }],
        count: 1,
      })));
    jest.spyOn(slack, 'sendOverbookingsReport')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.sendDailyOverbookingsReport();
    expect(apaleoClient.getProperties).toBeCalled();
    expect(slack.sendOverbookingsReport).toBeCalledWith(`
    :warning: *OVERBOOKINGS WARNING ALERT* :warning: \n

    No overbookings found
    `);
  });

  it('should send out a slack notification if unit group is overbooked', async () => {
    jest.spyOn(apaleoClient, 'getProperties')
      .mockImplementation(() => new Promise(resolve => resolve({
        properties: PROPERTIES,
        count: 1,
      })));
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve(OVERBOOKED_GROUP)));
    jest.spyOn(slack, 'sendOverbookingsReport')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.sendDailyOverbookingsReport();
    expect(slack.sendOverbookingsReport).toBeCalled();
  });

  it('should trigger overbookings handler for new reservations', async () => {
    jest.spyOn(apaleoClient, 'getProperties')
      .mockImplementation(() => new Promise(resolve => resolve({
        properties: PROPERTIES,
        count: 1,
      })));
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve(OVERBOOKED_GROUP)));
    jest.spyOn(slack, 'sendOverbookingsReport')
      .mockImplementation(() => new Promise(resolve => resolve()));
    jest.spyOn(apaleoClient, 'updateOverbooking')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.handleNewReservation({
      id: 'BHV-1',
      arrival: new Date(),
      departure: addDays(new Date(), 5),
      property: PROPERTIES[0],
      unitGroup: {
        id: 'DE_BE_001',
      },
    } as unknown as ReservationModel);
    expect(slack.sendOverbookingsReport).toBeCalled();
    expect(apaleoClient.getProperties).not.toBeCalled();
  });

  it('should correctly format availableUnitGroups to virtual availability standard', async () => {
    const availableUnitGroups = overbookingsService.availabilityFormat(
      [GROUPS_WITH_ALLOWED_OVERBOOKING],
    );
    expect(availableUnitGroups).toEqual(VIRTURL_AVAILABILITY_DATA_FORMAT);
  });

  it('should re-adjust virtual availability of unit groups upon reservation cancellation', async () => {
    jest.spyOn(apaleoClient, 'getReservationById')
      .mockImplementation(() => new Promise(resolve => resolve({
        id: 'BHV-1',
        arrival: new Date(),
        departure: addDays(new Date(), 5),
        property: PROPERTIES[0],
        unitGroup: {
          id: 'DE_BE_002',
        },
      } as unknown as ReservationModel)));
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve({
        timeSlices: [GROUPS_WITH_ALLOWED_OVERBOOKING],
        count: 1,
      })));
    jest.spyOn(apaleoClient, 'updateOverbooking')
      .mockImplementation(() => new Promise(resolve => resolve()));
    jest.spyOn(overbookingsService, 'availabilityFormat');
    jest.spyOn(overbookingsService, 'setVirtualAvailability');

    await overbookingsService.processCancelledReservation('BHV-1');
    expect(overbookingsService.availabilityFormat).toBeCalled();
    expect(apaleoClient.updateOverbooking).toBeCalledTimes(1);
    expect(apaleoClient.updateOverbooking).toBeCalledWith('DE_BE_002-MIC', '2020-06-12', '2020-06-12', 0);
  });

  it('should manage virtual availability seed', async () => {
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve(AVAILABLE_UNIT_GROUPS_SEED)));
    jest.spyOn(apaleoClient, 'updateOverbooking')
      .mockImplementation(() => new Promise(resolve => resolve()));
    jest.spyOn(overbookingsService, 'availabilityFormat');

    const from = new Date();
    const to = addMonths(from, 12);
    await overbookingsService.seedVirtualAvailability();
    expect(apaleoClient.getUnitGroupsAvailabilty).toBeCalledWith(
      'DE_BE_002',
      formatDate(from),
      formatDate(to),
    );
    expect(overbookingsService.availabilityFormat).toBeCalled();
    expect(apaleoClient.updateOverbooking).toBeCalledTimes(1);
    expect(apaleoClient.updateOverbooking).toBeCalledWith('DE_BE_002-MIC', '2020-06-20', '2020-06-20', 1);
  });

  it('should drop availability on specified property to 0 on specified day', async () => {
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve({
        timeSlices: [GROUPS_WITH_ALLOWED_OVERBOOKING],
        count: 1,
      })));
    jest.spyOn(apaleoClient, 'updateOverbooking')
      .mockImplementation(() => new Promise(resolve => resolve()));
    jest.spyOn(overbookingsService, 'disableUnitGroupsAvailability');
    jest.spyOn(overbookingsService, 'setVirtualAvailability');
    jest.spyOn(slack, 'sendDisabledAvailabilityNotification')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.disableUnitGroupsAvailability();
    expect(apaleoClient.getUnitGroupsAvailabilty).toBeCalledWith(
      'DE_BE_001',
      formatDate(new Date()),
      formatDate(new Date()),
    );
    expect(overbookingsService.disableUnitGroupsAvailability).toBeCalled();
    expect(apaleoClient.updateOverbooking).toBeCalledTimes(2);
    expect(overbookingsService.setVirtualAvailability).toBeCalledWith(DISABLED_AVAILABILITY_MOCK);
    expect(slack.sendDisabledAvailabilityNotification).toBeCalled();
  });

  it('should drop allowedOverbooking three days before arrival', async () => {
    jest.spyOn(apaleoClient, 'getUnitGroupsAvailabilty')
      .mockImplementation(() => new Promise(resolve => resolve({
        timeSlices: [GROUPS_WITH_ALLOWED_OVERBOOKING],
        count: 1,
      })));
    jest.spyOn(apaleoClient, 'updateOverbooking')
      .mockImplementation(() => new Promise(resolve => resolve()));
    jest.spyOn(overbookingsService, 'disableOverbookingsBeforeArrival');
    jest.spyOn(overbookingsService, 'setVirtualAvailability');
    jest.spyOn(slack, 'sendResetOverbookingsNotification')
      .mockImplementation(() => new Promise(resolve => resolve()));

    await overbookingsReport.disableOverbookingsBeforeArrival();
    expect(apaleoClient.getUnitGroupsAvailabilty).toBeCalledWith(
      'DE_BE_002',
      formatDate(new Date()),
      formatDate(addDays(new Date(), 2)),
    );
    expect(apaleoClient.getUnitGroupsAvailabilty).toBeCalledTimes(2);
    expect(overbookingsService.disableOverbookingsBeforeArrival).toBeCalled();
    expect(apaleoClient.updateOverbooking).toBeCalledTimes(2);
    expect(overbookingsService.setVirtualAvailability).toBeCalledWith(RESET_OVERBOOKINGS_MOCK);
    expect(slack.sendResetOverbookingsNotification).toBeCalled();
  });
});
