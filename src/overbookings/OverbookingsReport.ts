import { Injectable } from '@nestjs/common';
import { addMonths } from 'date-fns';
import { SlackService } from '../SlackService';
import { UnitGroupAvailabilityTimeSliceItemModel } from '../apaleo/generated/availability';
import {
  IndexSignature,
  OverbookingsPropertyModel,
  ReservationProps,
  OverbookingWarningTimeSlices,
} from './overbookings.interface';
import logger from '../LoggingService';
import { ReservationModel } from '../apaleo/generated/reservation';
import { OverbookingsService } from './overbookings.service';
import { OverbookingsAvailability } from './OverbookingsAvailability';

@Injectable()
export class OverbookingsReport {
  constructor(
    private readonly overbookingsService: OverbookingsService,
    private readonly slackService: SlackService,
    private readonly overbookingsAvailability: OverbookingsAvailability,
  ) { }

  public async handleNewReservation(reservation: ReservationModel): Promise<void> {
    const { property, id: reservationId, arrival, departure } = reservation;
    const from = new Date(arrival);
    const to = new Date(departure);

    const properties = [{
      id: property.id,
      name: property.name,
    }];

    const reservationProps: ReservationProps = {
      arrival: this.overbookingsService.formatDate(from),
      departure: this.overbookingsService.formatDate(to),
      reservationId,
    };
    const availableUnitGroups = await this.overbookingsService.getUnitGroupAvailabilty(from, to, properties);
    const formattedUnitGroups = this.overbookingsService.availabilityFormat(availableUnitGroups);
    logger.infoLog('Processing overbookings for new reservation', reservationId);
    const updateValues = this.overbookingsAvailability.getVirtualAvailability(formattedUnitGroups);
    await this.overbookingsService.setVirtualAvailability(updateValues);
    return await this.triggerOverbookingsReport(
      availableUnitGroups, properties, reservationProps,
    );
  }

  public async disableUnitGroupsAvailability(): Promise<void> {
    await this.overbookingsService.disableUnitGroupsAvailability();
    logger.infoLog('Sending disabled unit groups availability report to slack');
    const today = this.overbookingsService.formatDate(new Date());
    const text = `
      All available unitgroups on Belfort for ${today} have been disabled
    `;
    await this.slackService.sendDisabledAvailabilityNotification(text);
    logger.infoLog('Disabled unit groups availability notification sent to slack');
  }

  public async disableOverbookingsBeforeArrival(): Promise<void> {
    await this.overbookingsService.disableOverbookingsBeforeArrival();
    logger.infoLog('Sending slack notification for reset overbookings');
    const text = `
      Restored overbookings to default on all properties with active overbooking
      for the next three days
    `;
    await this.slackService.sendResetOverbookingsNotification(text);
  }

  public async sendDailyOverbookingsReport(): Promise<void> {
    const from = new Date();
    const to = addMonths(from, 2);

    const properties = await this.overbookingsService.getProperties();
    const availableUnitGroups = await this.overbookingsService.getUnitGroupAvailabilty(from, to, properties);
    return await this.triggerOverbookingsReport(availableUnitGroups, properties);
  }

  private async triggerOverbookingsReport(
    availableUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
    properties: OverbookingsPropertyModel[],
    reservationProps?: ReservationProps,
  ): Promise<void> {
    const propertiesMap: IndexSignature = properties.reduce((accumulator, { id, name }) => {
      accumulator[id] = name;
      return accumulator;
    }, {});

    const unitGroups = this.overbookingsService.reportFormat(availableUnitGroups);
    const text = this.processOverbookingsResponse(
      unitGroups, propertiesMap, reservationProps,
    );
    if (!text) {
      return;
    }
    logger.infoLog('Sending overbooking warning report to slack');
    await this.slackService.sendOverbookingsReport(text);
    logger.infoLog('Overbooking warning report sent to slack');
  }

  private processOverbookingsResponse(
    availableUnitGroups: OverbookingWarningTimeSlices,
    propertiesMap: IndexSignature,
    reservationProps?: ReservationProps,
  ): string {
    const MAX_OCCUPANCY = 100;
    let hasOverbookedUnitGroups = false;

    let text = `:warning: *OVERBOOKINGS WARNING ALERT* :warning: \n`;
    if (reservationProps?.reservationId) {
      const { arrival, departure, reservationId } = reservationProps;
      text = `${text.trim()} \n
    *New Reservation* \n
    *Reservation ID:* ${reservationId}
    *Arrival:* ${arrival}
    *Departure:* ${departure} \n`;
    }

    Object.keys(availableUnitGroups).forEach(timeSlice => {
      const [from, to] = timeSlice.split('___');
      const overbookedTimeslices = `"colorizer"Overbooked Timeslices: ${from} --- ${to}"colorizer"`;
      const unitGroups = availableUnitGroups[timeSlice];
      unitGroups.forEach(({ unitGroup, occupancy, sellableCount, allowedOverbookingCount }) => {
        if (occupancy > MAX_OCCUPANCY && !allowedOverbookingCount) {
          const splitUnitGroupName = unitGroup.id.split(`-${unitGroup.code}`)[0] || '';
          const propertyName = propertiesMap[splitUnitGroupName];

          hasOverbookedUnitGroups = true;
          if (!text.includes(overbookedTimeslices)) {
            text = `${text}
              \n ${overbookedTimeslices} \n`;
          }

          text = `${text}
          "colorizer"Unit Group:"colorizer" ${unitGroup.name} (${propertyName}):::::::"colorizer"Occupancy:"colorizer" ${occupancy}:::::::"colorizer"Sellable Count:"colorizer" ${sellableCount}`;
        }
      });
    });

    logger.infoLog('Overbooking warning report generation complete');
    if (hasOverbookedUnitGroups) {
      return text.replace(/"colorizer"/gi, '`');
    }

    if (reservationProps?.reservationId) {
      return;
    }

    return `
    :warning: *OVERBOOKINGS WARNING ALERT* :warning: \n

    No overbookings found
    `;
  }
}
