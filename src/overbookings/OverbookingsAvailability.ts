import { Injectable } from '@nestjs/common';
import {
  OverbookingUpdateData, OverbookingTimeSlices, TargetResizeConfig,
  SourceAvailabilityActionConfig, OverbookingUpdateTracker, OverbookingDataSlice,
} from './overbookings.interface';
import { OVERBOOKING_CONFIG } from './overbookings.config';

@Injectable()
export class OverbookingsAvailability {
  public getVirtualAvailability(
    availableUnitGroups: OverbookingTimeSlices,
    overbookingConfig: { [key: string]: string } = OVERBOOKING_CONFIG,
  ): OverbookingUpdateData[] {
    const SECURITY_THRESHOLD = 1;
    const virtualAvailabilityUpdateTracker: OverbookingUpdateTracker = {};
    for (const timeDefinition in availableUnitGroups) {
      if (!availableUnitGroups.hasOwnProperty(timeDefinition)) {
        continue;
      }

      const targetResizeList = [];
      const unitGroups = availableUnitGroups[timeDefinition];
      let virtualAvailabilityCount = 0;
      let totalDeductedCount = 0;
      let sourceData: OverbookingDataSlice = {} as OverbookingDataSlice;
      for (const target in overbookingConfig) {
        if (!overbookingConfig.hasOwnProperty(target)) {
          continue;
        }

        const source = overbookingConfig[target];
        const sourceAvailability = unitGroups[source];
        const targetAvailability = unitGroups[target];
        if (!sourceAvailability || !targetAvailability) {
          continue;
        }
        sourceData = sourceAvailability;

        if (
          targetAvailability.availableCount > 0 &&
          targetAvailability.sellableCount > 0
        ) {
          virtualAvailabilityCount += targetAvailability.availableCount - SECURITY_THRESHOLD;
        }

        if (sourceAvailability.availableCount <= 0) {
          targetResizeList.push(targetAvailability);
          totalDeductedCount += Math.abs(targetAvailability.allowedOverbookingCount);
        }
      }

      const timeStamp = timeDefinition.split('___')[0];
      this.handleTargetDeduction({
        targetResizeList,
        timeStamp,
        sourceData,
        virtualAvailabilityUpdateTracker,
        totalDeductedCount,
      });

      this.handleSourceVirtualAvailability({
        virtualAvailabilityCount,
        virtualAvailabilityUpdateTracker,
        timeStamp,
        sourceData,
      });
    }

    return Object.values(virtualAvailabilityUpdateTracker);
  }

  private handleSourceVirtualAvailability(config: SourceAvailabilityActionConfig): void {
    const {
      virtualAvailabilityCount, virtualAvailabilityUpdateTracker, timeStamp, sourceData,
    } = config;
    const { allowedOverbookingCount, unitGroupId } = sourceData;

    if (allowedOverbookingCount === virtualAvailabilityCount) {
      return;
    }

    virtualAvailabilityUpdateTracker[`${timeStamp}-${unitGroupId}`] = {
      id: unitGroupId,
      from: timeStamp,
      to: timeStamp,
      allowedOverbooking: virtualAvailabilityCount,
    };
  }

  private handleTargetDeduction(config: TargetResizeConfig): void {
    const {
      targetResizeList, sourceData, virtualAvailabilityUpdateTracker, timeStamp,
      totalDeductedCount,
    } = config;

    if (!targetResizeList.length) {
      return;
    }

    const isUpgrade = Math.abs(sourceData.availableCount) < totalDeductedCount;
    const deductionCount = Math.abs(sourceData.availableCount) === 0 ? 0 : 1;
    const unitGroup = targetResizeList.reduce((prevTarget, current) => {
      if (deductionCount === 0 && current.allowedOverbookingCount < 0) {
        virtualAvailabilityUpdateTracker[`${timeStamp}-${current.unitGroupId}`] = {
          id: current.unitGroupId,
          from: timeStamp,
          to: timeStamp,
          allowedOverbooking: 0,
        };
      }

      if (isUpgrade) {
        return (prevTarget.sellableCount < current.sellableCount) ? prevTarget : current;
      }

      return (prevTarget.sellableCount > current.sellableCount) ? prevTarget : current;
    }, targetResizeList[0]);

    if (deductionCount === 0 || Math.abs(sourceData.availableCount) === totalDeductedCount) {
      return;
    }

    const { unitGroupId, allowedOverbookingCount } = unitGroup;
    const allowedOverbooking = isUpgrade ?
      allowedOverbookingCount + 1 :
      -(Math.abs(allowedOverbookingCount) + 1);

    virtualAvailabilityUpdateTracker[`${timeStamp}-${unitGroupId}`] = {
      id: unitGroupId,
      from: timeStamp,
      to: timeStamp,
      allowedOverbooking,
    };
  }
}
