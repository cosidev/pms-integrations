import { Module } from '@nestjs/common';
import { SlackService } from '../SlackService';
import { OverbookingsService } from './overbookings.service';
import { ApaleoModule } from '../apaleo/apaleo.module';
import { OverbookingsReport } from './OverbookingsReport';
import { OverbookingsAvailability } from './OverbookingsAvailability';

@Module({
  imports: [ApaleoModule],
  providers: [
    OverbookingsService,
    SlackService,
    OverbookingsReport,
    OverbookingsAvailability,
  ],
})
export class OverbookingsManagerModule { }
