import { Injectable } from '@nestjs/common';
import { format, addMonths, addDays } from 'date-fns';
import flatten from 'lodash.flatten';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import { UnitGroupAvailabilityTimeSliceItemModel } from '../apaleo/generated/availability';
import {
  OverbookingsPropertyModel,
  OverbookingUpdateData,
  OverbookingTimeSlices,
  OverbookingWarningTimeSlices,
} from './overbookings.interface';
import logger from '../LoggingService';
import {
  SOURCE_TARGET_CONFIG,
  VIRTUALLY_AVAILABLE_PROPERTIES,
  OVERBOOKING_FORMAT_CONFIG,
} from './overbookings.config';
import { OverbookingsAvailability } from './OverbookingsAvailability';

@Injectable()
export class OverbookingsService {
  constructor(
    private readonly apaleoClient: ApaleoClient,
    private readonly overbookingsAvailability: OverbookingsAvailability,
  ) { }

  public async getProperties(): Promise<OverbookingsPropertyModel[]> {
    logger.infoLog('Fetching properties list');
    const { properties = [] } = await this.apaleoClient.getProperties();

    return properties.map((property): any => {
      const { id, name } = property;
      return {
        id,
        name,
      };
    });
  }

  public async getUnitGroupAvailabilty(from: Date, to: Date, properties: OverbookingsPropertyModel[]) {
    const parsedFrom = this.formatDate(from);
    const parsedTo = this.formatDate(to);

    logger.infoLog(`Fetching unitgroups availability from ${parsedFrom} to ${parsedTo} for`, properties);
    const timeSliceList: UnitGroupAvailabilityTimeSliceItemModel[][] = [];
    for (const { id } of properties) {
      const { timeSlices } = await this.apaleoClient.getUnitGroupsAvailabilty(
        id, parsedFrom, parsedTo,
      );
      timeSliceList.push(timeSlices);
    }

    logger.infoLog(`Retrieval of unitgroups availability from ${parsedFrom} to ${parsedTo} complete`);
    return flatten(timeSliceList);
  }

  public availabilityFormat(
    availableUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
    sourceTargetConfig: string[] = SOURCE_TARGET_CONFIG,
  ): OverbookingTimeSlices {
    const timeSlices: OverbookingTimeSlices = {};
    for (const { from, unitGroups } of availableUnitGroups) {
      const parsedFrom = this.formatDate(new Date(from));
      let currentSource = '';
      for (const overbookingGroup of unitGroups) {
        const {
          unitGroup, availableCount, sellableCount, allowedOverbookingCount,
        } = overbookingGroup;

        if (!sourceTargetConfig.includes(unitGroup.id)) {
          continue;
        }

        const targets = OVERBOOKING_FORMAT_CONFIG[unitGroup.id];
        if (targets) {
          currentSource = unitGroup.id;
        }

        const formatted = {
          [unitGroup.id]: {
            availableCount,
            sellableCount,
            allowedOverbookingCount,
            unitGroupId: unitGroup.id,
          },
        };

        timeSlices[`${parsedFrom}___${currentSource}`] = {
          ...timeSlices[`${parsedFrom}___${currentSource}`],
          ...formatted,
        };
      }
    }

    return timeSlices;
  }

  public reportFormat(
    availableUnitGroups: UnitGroupAvailabilityTimeSliceItemModel[],
  ): OverbookingWarningTimeSlices {
    const formatted: OverbookingWarningTimeSlices = {};

    for (const { from, to, unitGroups } of availableUnitGroups) {
      const parsedFrom = this.formatDate(new Date(from));
      const parsedTo = this.formatDate(new Date(to));
      const timeDefinition = `${parsedFrom}___${parsedTo}`;

      if (formatted[timeDefinition]) {
        formatted[timeDefinition] = [
          ...formatted[timeDefinition],
          ...unitGroups,
        ];
      } else {
        formatted[timeDefinition] = [
          ...unitGroups,
        ];
      }
    }

    logger.infoLog('Overbookings warning data formatting complete');
    return formatted;
  }

  public async seedVirtualAvailability(
    properties: OverbookingsPropertyModel[] = VIRTUALLY_AVAILABLE_PROPERTIES,
  ): Promise<void> {
    const from = new Date();
    const to = addMonths(from, 12);

    const availableUnitGroups = await this.getUnitGroupAvailabilty(from, to, properties);
    const formattedUnitGroups = this.availabilityFormat(availableUnitGroups);
    const updateValues = this.overbookingsAvailability.getVirtualAvailability(formattedUnitGroups);
    await this.setVirtualAvailability(updateValues);
  }

  public async setVirtualAvailability(updateValues: OverbookingUpdateData[]): Promise<void> {
    if (!updateValues.length) {
      return;
    }
    logger.infoLog('Updating overbooking count for', updateValues);
    for (const { id, from, to, allowedOverbooking } of updateValues) {
      try {
        await this.apaleoClient.updateOverbooking(id, from, to, allowedOverbooking);
      } catch (error) {
        // tslint:disable-next-line:no-console
      }
    }
    logger.infoLog('Virtual availability update complete');
  }

  public formatDate(date: Date): string {
    return format(date, 'yyyy-MM-dd');
  }

  public async processCancelledReservation(reservationId: string): Promise<void> {
    const reservation = await this.apaleoClient.getReservationById(reservationId);
    const { property, arrival, departure } = reservation;

    const from = new Date(arrival);
    const to = new Date(departure);

    const properties = [{
      id: property.id,
      name: property.name,
    }];

    logger.infoLog('Processing overbookings for cancelled reservation', reservationId);
    const availableUnitGroups = await this.getUnitGroupAvailabilty(from, to, properties);
    const formattedUnitGroups = this.availabilityFormat(availableUnitGroups);
    const updateValues = this.overbookingsAvailability.getVirtualAvailability(formattedUnitGroups);
    await this.setVirtualAvailability(updateValues);
  }

  public async disableUnitGroupsAvailability(): Promise<void> {
    const today = new Date();
    const properties: OverbookingsPropertyModel[] = [
      { id: 'DE_BE_001', name: 'Belfort' },
    ];
    const availableUnitGroups = await this.getUnitGroupAvailabilty(today, today, properties);
    const formattedGroups = this.reportFormat(availableUnitGroups);
    const flattenedGroups = flatten(Object.values(formattedGroups));

    const updateData: OverbookingUpdateData[] = flattenedGroups.map(({
      unitGroup, allowedOverbookingCount, sellableCount,
    }) => {
      return {
        id: unitGroup.id,
        allowedOverbooking: allowedOverbookingCount - sellableCount,
        from: this.formatDate(today),
        to: this.formatDate(today),
      };
    });
    logger.infoLog('Dropping all available units on properties 0', properties);
    await this.setVirtualAvailability(updateData);
    logger.infoLog(`Availability of unit groups dropped to 0 on`, properties);
  }

  public async disableOverbookingsBeforeArrival(): Promise<void> {
    const from = new Date();
    const to = addDays(from, 2);

    const availableUnitGroups = await this.getUnitGroupAvailabilty(
      from, to, VIRTUALLY_AVAILABLE_PROPERTIES,
    );
    const formattedGroups = this.reportFormat(availableUnitGroups);

    const updateData: OverbookingUpdateData[] = [];
    Object.keys(formattedGroups).forEach(timeDefinition => {
      const unitGroups = formattedGroups[timeDefinition];
      const [timeSlice] = timeDefinition.split('___');
      const timeStamp = this.formatDate(new Date(timeSlice));
      unitGroups.forEach(({ unitGroup, availableCount, allowedOverbookingCount }) => {
        let allowedOverbooking = 0;
        if (availableCount < 0) {
          allowedOverbooking = Math.abs(availableCount);
        }
        if (OVERBOOKING_FORMAT_CONFIG[unitGroup.id] && allowedOverbooking !== allowedOverbookingCount) {
          updateData.push({
            id: unitGroup.id,
            allowedOverbooking,
            from: timeStamp,
            to: timeStamp,
          });
        }
      });
    });

    logger.infoLog('Resetting allowed overbookings for the next 3 days on', VIRTUALLY_AVAILABLE_PROPERTIES);
    await this.setVirtualAvailability(updateData);
  }
}
