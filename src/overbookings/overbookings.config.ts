import { OverbookingsPropertyModel } from './overbookings.interface';

export const OVERBOOKING_CONFIG = {
  // TARGET:SOURCE configuration
  'DE_BE_002-BR4': 'DE_BE_002-MIC',
  'DE_BE_002-BR5': 'DE_BE_002-MIC',
  'DE_BE_001-0002': 'DE_BE_001-0004',
  'DE_BE_001-0006': 'DE_BE_001-0004',
};

export const SOURCE_TARGET_CONFIG = [
  ...Object.keys(OVERBOOKING_CONFIG),
  ...Object.values(OVERBOOKING_CONFIG),
];

export const OVERBOOKING_FORMAT_CONFIG = {
  'DE_BE_002-MIC': ['DE_BE_002-BR4', 'DE_BE_002-BR5'],
  'DE_BE_001-0004': ['DE_BE_001-0002', 'DE_BE_001-0006'],
};

export const VIRTUALLY_AVAILABLE_PROPERTIES: OverbookingsPropertyModel[] = [
  { id: 'DE_BE_001', name: 'Belfort' },
  { id: 'DE_BE_002', name: 'Kater' },
];
