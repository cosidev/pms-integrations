import { format } from 'date-fns';
import { AvailableUnitGroupListModel, UnitGroupAvailabilityTimeSliceItemModel } from '../../apaleo/generated/availability';
import { PropertyItemModel } from '../../apaleo/generated/property';

export const AVAILABLE_UNIT_GROUPS: AvailableUnitGroupListModel = {
  timeSlices: [
    {
      from: new Date('2020-06-08T15:47:05+02:00'),
      to: new Date('2020-06-09T11:00:00+02:00'),
      property: {
        physicalCount: 14,
        houseCount: 14,
        soldCount: 14,
        occupancy: 100,
        sellableCount: 1,
        allowedOverbookingCount: 1,
        maintenance: {
          outOfService: 0,
          outOfOrder: 0,
          outOfInventory: 0,
        },
        block: {
          definite: 0,
          tentative: 0,
          picked: 0,
          remaining: 0,
        },
      },
      unitGroups: [
        {
          unitGroup: {
            id: 'DE_BE_002-MIC',
            code: '0004',
            name: 'Intimate',
            description: 'Our Intimate room is a small apartment on the ground floor near the inner garden. Designed over 18 square meters, the Intimate sleeps 2 people in a queen sized bed and features a private bathroom. It is the smallest room at the Belfort, located on the ground floor and accessable from the private courtyard.\n\n18 m2\n\n2 Adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom / Refrigerator',
          },
          physicalCount: 2,
          houseCount: 2,
          soldCount: 2,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 1,
          allowedOverbookingCount: 1,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_002-BR5',
            code: '0002',
            name: 'Deluxe',
            description: 'Our deluxe room is 30 square meters and comfortably sleeps 2 in a queen sized bed. It has a working space and a small coffee area, and is the perfect choice for travelling couples or friends.\n\n30 m2\n\n2 Adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom with a tub / Refrigerator',
          },
          physicalCount: 2,
          houseCount: 2,
          soldCount: 2,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 0,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_001-0001',
            code: '0001',
            name: 'Deluxe Suite',
            description: 'Our deluxe suite spans over 36 square meters and sleeps up to 4 guests. It has one queen bed in the bedroom and a sofa bed placed in the living area, with a fully equipped kitchen. It’s a space ideal for small families or travelling couples or friends.\n\n36m2\n\n3 Adults + 1 Child\n\n1 Queen size bed, 1 Sofa bed\n\n1 Bedroom / Bathroom / Kitchen',
          },
          physicalCount: 3,
          houseCount: 3,
          soldCount: 3,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 0,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_001-0006',
            code: '0006',
            name: 'Deluxe Terrace Suite',
            description: 'Our terrace suite is 36 square meters and sleeps two in a queen sized bed. It features one bedroom and a fully equipped kitchen. A beautiful balcony, the perfect place to relax faces the street.\n\n36m2\n\n2 adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom / Kitchen',
          },
          physicalCount: 3,
          houseCount: 3,
          soldCount: 3,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 0,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
      ],
    },
  ],
  count: 1,
};

export const GROUPS_WITH_ALLOWED_OVERBOOKING: UnitGroupAvailabilityTimeSliceItemModel = {
  from: new Date('2020-06-12T15:00:00+02:00'),
  to: new Date('2020-06-13T12:00:00+02:00'),
  property:
  {
    physicalCount: 4,
    houseCount: 4,
    soldCount: 1,
    occupancy: 25,
    sellableCount: 3,
    allowedOverbookingCount: 0,
    maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
    block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
  },
  unitGroups: [
    {
      unitGroup:
      {
        id: 'DE_BE_002-MIC',
        code: 'DOUBLEROOM',
        name: 'Doubleroom',
        description: 'tbd',
      },
      physicalCount: 2,
      houseCount: 2,
      soldCount: 1,
      occupancy: 50,
      availableCount: 1,
      sellableCount: 2,
      allowedOverbookingCount: 1,
      maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
      block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    },
    {
      unitGroup:
      {
        id: 'DE_BE_002-BR5',
        code: 'STUDIO',
        name: 'Studio',
        description: 'Studio',
      },
      physicalCount: 2,
      houseCount: 2,
      soldCount: 0,
      occupancy: 0,
      availableCount: 1,
      sellableCount: 1,
      allowedOverbookingCount: 0,
      maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
      block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    },
  ],
};

export const PROPERTIES = [
  {
    id: 'DE_BE_001',
    name: 'Dev Hotel',
    code: 'DE_B_PR_01',
    currencyCode: 'EUR',
  } as PropertyItemModel,
];

export const OVERBOOKED_GROUP = {
  timeSlices: [
    {
      from: new Date('2020-06-08T15:47:05+02:00'),
      to: new Date('2020-06-09T11:00:00+02:00'),
      property: {
        physicalCount: 14,
        houseCount: 14,
        soldCount: 14,
        occupancy: 100,
        sellableCount: 1,
        allowedOverbookingCount: 1,
        maintenance: {
          outOfService: 0,
          outOfOrder: 0,
          outOfInventory: 0,
        },
        block: {
          definite: 0,
          tentative: 0,
          picked: 0,
          remaining: 0,
        },
      },
      unitGroups: [
        {
          unitGroup: {
            id: 'DE_BE_002-MIC',
            code: '0004',
            name: 'Intimate',
            description: 'Our Intimate room is a small apartment on the ground floor near the inner garden. Designed over 18 square meters, the Intimate sleeps 2 people in a queen sized bed and features a private bathroom. It is the smallest room at the Belfort, located on the ground floor and accessable from the private courtyard.\n\n18 m2\n\n2 Adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom / Refrigerator',
          },
          physicalCount: 2,
          houseCount: 2,
          soldCount: 2,
          occupancy: 110,
          availableCount: 0,
          sellableCount: -1,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
      ],
    },
  ],
  count: 1,
};

export const VIRTURL_AVAILABILITY_DATA_FORMAT = {
  '2020-06-12___DE_BE_002-MIC': {
    'DE_BE_002-MIC':
    {
      availableCount: 1,
      sellableCount: 2,
      allowedOverbookingCount: 1,
      unitGroupId: 'DE_BE_002-MIC',
    },
    'DE_BE_002-BR5':
    {
      availableCount: 1,
      sellableCount: 1,
      allowedOverbookingCount: 0,
      unitGroupId: 'DE_BE_002-BR5',
    },
  },
};

export const AVAILABLE_UNIT_GROUPS_SEED: AvailableUnitGroupListModel = {
  timeSlices: [
    {
      from: new Date('2020-06-20T15:47:05+02:00'),
      to: new Date('2020-06-21T11:00:00+02:00'),
      property: {
        physicalCount: 14,
        houseCount: 14,
        soldCount: 14,
        occupancy: 100,
        sellableCount: 1,
        allowedOverbookingCount: 1,
        maintenance: {
          outOfService: 0,
          outOfOrder: 0,
          outOfInventory: 0,
        },
        block: {
          definite: 0,
          tentative: 0,
          picked: 0,
          remaining: 0,
        },
      },
      unitGroups: [
        {
          unitGroup: {
            id: 'DE_BE_002-MIC',
            code: 'DOUBLEROOM',
            name: 'Doubleroom',
            description: 'Our double room is a small apartment on the ground floor near the inner garden. Designed over 18 square meters, the Intimate sleeps 2 people in a queen sized bed and features a private bathroom. It is the smallest room at the Belfort, located on the ground floor and accessable from the private courtyard.\n\n18 m2\n\n2 Adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom / Refrigerator',
          },
          physicalCount: 2,
          houseCount: 2,
          soldCount: 2,
          occupancy: 0,
          availableCount: 2,
          sellableCount: 2,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_002-BR5',
            code: 'STUDIO',
            name: 'Studio',
            description: 'Our studio is 30 square meters and comfortably sleeps 2 in a queen sized bed. It has a working space and a small coffee area, and is the perfect choice for travelling couples or friends.\n\n30 m2\n\n2 Adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom with a tub / Refrigerator',
          },
          physicalCount: 2,
          houseCount: 2,
          soldCount: 2,
          occupancy: 0,
          availableCount: 2,
          sellableCount: 2,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_001-0001',
            code: '0001',
            name: 'Deluxe Suite',
            description: 'Our deluxe suite spans over 36 square meters and sleeps up to 4 guests. It has one queen bed in the bedroom and a sofa bed placed in the living area, with a fully equipped kitchen. It’s a space ideal for small families or travelling couples or friends.\n\n36m2\n\n3 Adults + 1 Child\n\n1 Queen size bed, 1 Sofa bed\n\n1 Bedroom / Bathroom / Kitchen',
          },
          physicalCount: 3,
          houseCount: 3,
          soldCount: 3,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 0,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
        {
          unitGroup: {
            id: 'DE_BE_001-0006',
            code: '0006',
            name: 'Deluxe Terrace Suite',
            description: 'Our terrace suite is 36 square meters and sleeps two in a queen sized bed. It features one bedroom and a fully equipped kitchen. A beautiful balcony, the perfect place to relax faces the street.\n\n36m2\n\n2 adults\n\n1 Queen size bed\n\n1 Bedroom / Bathroom / Kitchen',
          },
          physicalCount: 3,
          houseCount: 3,
          soldCount: 3,
          occupancy: 100,
          availableCount: 0,
          sellableCount: 0,
          allowedOverbookingCount: 0,
          maintenance: {
            outOfService: 0,
            outOfOrder: 0,
            outOfInventory: 0,
          },
          block: {
            definite: 0,
            tentative: 0,
            picked: 0,
            remaining: 0,
          },
        },
      ],
    },
  ],
  count: 1,
};

export const DISABLED_AVAILABILITY_MOCK = [
  {
    id: 'DE_BE_002-MIC',
    allowedOverbooking: -1,
    from: format(new Date(), 'yyyy-MM-dd'),
    to: format(new Date(), 'yyyy-MM-dd'),
  },
  {
    id: 'DE_BE_002-BR5',
    allowedOverbooking: -1,
    from: format(new Date(), 'yyyy-MM-dd'),
    to: format(new Date(), 'yyyy-MM-dd'),
  },
];

export const RESET_OVERBOOKINGS_MOCK = [
  {
    allowedOverbooking: 0,
    from: '2020-06-12',
    id: 'DE_BE_002-MIC',
    to: '2020-06-12',
  },
  {
    allowedOverbooking: 0,
    from: '2020-06-12',
    id: 'DE_BE_002-MIC',
    to: '2020-06-12',
  },
];
