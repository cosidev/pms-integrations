import { UnitGroupAvailabilityTimeSliceItemModel } from './../../apaleo/generated/availability';

export const APALEO_UNIT_GROUPS: UnitGroupAvailabilityTimeSliceItemModel[] = [
  {
    from: new Date('2020-06-12T15:00:00+02:00'),
    to: new Date('2020-06-13T12:00:00+02:00'),
    property:
    {
      physicalCount: 4,
      houseCount: 4,
      soldCount: 0,
      occupancy: 0,
      sellableCount: 4,
      allowedOverbookingCount: 0,
      maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
      block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    },
    unitGroups: [
      {
        unitGroup:
        {
          id: 'DE_DEV_003-DOUBLEROOM',
          code: 'DOUBLEROOM',
          name: 'Doubleroom',
          description: 'tbd',
        },
        physicalCount: 2,
        houseCount: 2,
        soldCount: 0,
        occupancy: 0,
        availableCount: 2,
        sellableCount: 2,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
      {
        unitGroup:
        {
          id: 'DE_DEV_003-STUDIO',
          code: 'STUDIO',
          name: 'Studio',
          description: 'Studio',
        },
        physicalCount: 2,
        houseCount: 2,
        soldCount: 0,
        occupancy: 0,
        availableCount: 2,
        sellableCount: 2,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
    ],
  },
];

export const APALEO_UNIT_GROUPS_D2: UnitGroupAvailabilityTimeSliceItemModel[] = [
  {
    from: new Date('2020-06-12T15:00:00+02:00'),
    to: new Date('2020-06-13T12:00:00+02:00'),
    property:
    {
      physicalCount: 4,
      houseCount: 4,
      soldCount: 0,
      occupancy: 0,
      sellableCount: 4,
      allowedOverbookingCount: 0,
      maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
      block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    },
    unitGroups: [
      {
        unitGroup:
        {
          id: 'DE_DEV_003-DOUBLEROOM',
          code: 'DOUBLEROOM',
          name: 'Doubleroom',
          description: 'tbd',
        },
        physicalCount: 2,
        houseCount: 2,
        soldCount: 0,
        occupancy: 0,
        availableCount: 2,
        sellableCount: 2,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
      {
        unitGroup:
        {
          id: 'DE_DEV_003-STUDIO',
          code: 'STUDIO',
          name: 'Studio',
          description: 'Studio',
        },
        physicalCount: 3,
        houseCount: 3,
        soldCount: 0,
        occupancy: 0,
        availableCount: 3,
        sellableCount: 3,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
    ],
  },
];

export const APALEO_UNIT_GROUPS_D3: UnitGroupAvailabilityTimeSliceItemModel[] = [
  {
    from: new Date('2020-06-12T15:00:00+02:00'),
    to: new Date('2020-06-13T12:00:00+02:00'),
    property:
    {
      physicalCount: 4,
      houseCount: 4,
      soldCount: 0,
      occupancy: 0,
      sellableCount: 4,
      allowedOverbookingCount: 0,
      maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
      block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
    },
    unitGroups: [
      {
        unitGroup:
        {
          id: 'DE_DEV_003-DOUBLEROOM',
          code: 'DOUBLEROOM',
          name: 'Doubleroom',
          description: 'tbd',
        },
        physicalCount: 2,
        houseCount: 2,
        soldCount: 0,
        occupancy: 0,
        availableCount: 2,
        sellableCount: 2,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
      {
        unitGroup:
        {
          id: 'DE_DEV_003-INTIMATE',
          code: 'INTIMATE',
          name: 'Intimate',
          description: 'Intimate',
        },
        physicalCount: 2,
        houseCount: 2,
        soldCount: 0,
        occupancy: 0,
        availableCount: 2,
        sellableCount: 2,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
      {
        unitGroup:
        {
          id: 'DE_DEV_003-STUDIO',
          code: 'STUDIO',
          name: 'Studio',
          description: 'Studio',
        },
        physicalCount: 3,
        houseCount: 3,
        soldCount: 0,
        occupancy: 0,
        availableCount: 3,
        sellableCount: 3,
        allowedOverbookingCount: 0,
        maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
        block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
      },
    ],
  },
];

export const FAMILY_CRASH_PAD = {
  unitGroup:
  {
    id: 'DE_DEV_003-DEV3',
    code: 'DEV3',
    name: 'Family Crashpad',
    description: 'Family Crashpad',
  },
  physicalCount: 3,
  houseCount: 3,
  soldCount: 0,
  occupancy: 0,
  availableCount: 3,
  sellableCount: 3,
  allowedOverbookingCount: 0,
  maintenance: { outOfService: 0, outOfOrder: 0, outOfInventory: 0 },
  block: { definite: 0, tentative: 0, picked: 0, remaining: 0 },
};
