import { generalConfig } from './config';
import axios from 'axios';
import logger from './LoggingService';
import { DesiredStayDetailsModel, DesiredTimeSliceModel, ReservationModel } from './apaleo/generated/reservation';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { SlackService } from './SlackService';
import { DynamoClient } from './common/aws/dynamoClient';

interface CurrencyLayerResponse {
  success: boolean;
  quotes?: {
    [key: string]: number;
  };
}

interface PragueReservationData {
  oldGrossAmount: number;
  newGrossAmount: number;
}

/*
  Recalculates reservation and commission amount for Merchants Avenue reservations made via booking.com
*/
export default class PragueCurrencyHandler {
  constructor(
    private readonly dynamoClient: DynamoClient,
    private readonly apaleoClient: ApaleoClient,
    private readonly slackService: SlackService,
  ) {}

  async handleReservation(reservation: ReservationModel) {
    if (!this.shouldHandle(reservation)) {
      return reservation;
    }

    const sKey = 'prague_currency_conversion';
    const dataTable = generalConfig.aws.dataDynamoTableName;
    const response = await this.dynamoClient.getItem<PragueReservationData>(dataTable, { dataKey: reservation.id, sKey });
    if (response) {
      return reservation;
    }

    await this.convertCurrency(reservation);

    // Reload reservation
    const newReservation = await this.apaleoClient.getReservationById(reservation.id, ['booker', 'assignedUnits', 'timeSlices']);
    await this.dynamoClient.putItem(dataTable, {
      dataKey: reservation.id,
      sKey,
      oldGrossAmount: reservation.totalGrossAmount.amount,
      newGrossAmount: newReservation.totalGrossAmount.amount,
    });

    return newReservation;
  }

  async handleAmmendedReservation(reservation: ReservationModel) {
    if (!this.shouldHandle(reservation)) {
      return reservation;
    }

    const sKey = 'prague_currency_conversion';
    const dataTable = generalConfig.aws.dataDynamoTableName;
    const response = await this.dynamoClient.getItem<PragueReservationData>(dataTable, { dataKey: reservation.id, sKey });
    if (!response) {
      return reservation;
    }

    const { newGrossAmount } = response;
    if (newGrossAmount !== reservation.totalGrossAmount.amount) {
      await this.slackService.sendAmmendedGrossAmount(
        `${reservation.id} (Merchants Avenue) has a new total gross amount. Old = ${newGrossAmount} Current: ${reservation.totalGrossAmount.amount}`,
      );
    }

    return reservation;
  }

  private async convertCurrency(reservation: ReservationModel) {
    logger.infoLog(`Updating prices from EUR to CZK for reservation ${reservation.id}`);
    const rate = await this.getCzkRate();
    logger.infoLog(`Got CZK rate ${rate}`);
    const timeSlices: DesiredTimeSliceModel[] = [];
    reservation.timeSlices.forEach(slice => {
      timeSlices.push({
        ratePlanId: slice.ratePlan.id,
        totalGrossAmount: {
          amount: slice.totalGrossAmount.amount * rate,
          currency: slice.totalGrossAmount.currency,
        },
      });
    });

    const request: DesiredStayDetailsModel = {
      arrival: (reservation.arrival as unknown) as string,
      departure: (reservation.departure as unknown) as string,
      adults: reservation.adults,
      childrenAges: reservation.childrenAges,
      requote: true,
      timeSlices,
    };

    logger.infoLog(`Ammending reservation ${reservation.id}`, { ammedRequest: request });
    await this.apaleoClient.forceAmmendReservation(reservation.id, request);

    const convertedCommission = reservation.commission?.commissionAmount.amount * rate;
    if (convertedCommission) {
      const commissionUpdateValues: any[] = [
        {
          op: 'add',
          path: '/commission/commissionAmount',
          value: {
            amount: convertedCommission,
            currency: 'CZK',
          },
        },
      ];
      logger.infoLog(`Updating the commission on ${reservation.id}`, { commissionAmendRequest: commissionUpdateValues });
      await this.apaleoClient.updateReservationbyId(reservation.id, commissionUpdateValues);
    }
  }

  private shouldHandle(reservation: ReservationModel) {
    const propertyId = reservation.property.id;
    const pragueProperties = ['CZ_PR_002', 'CZ_PR_003', 'CZ_PR_004'];
    if (!pragueProperties.includes(propertyId)) {
      return false;
    }

    if (reservation.source !== 'Booking.com') {
      return false;
    }

    return true;
  }

  private async getCzkRate() {
    const response = await axios.get<CurrencyLayerResponse>(
      'https://www.apilayer.net/api/live?access_key=5e8bfab3107f440d31ab4ad44f41efe4&source=EUR&currencies=CZK',
    );
    const pair = 'EURCZK';
    if (!response.data.success || !response.data.quotes || !response.data.quotes[pair]) {
      logger.errorLog('Could not get CZK rate from currency layer', { currencyLayer: response.data });
      throw new Error('Could not get CZK rate from currency layer');
    }
    return response.data.quotes[pair];
  }
}
