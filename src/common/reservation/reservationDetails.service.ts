import { Injectable } from '@nestjs/common';
import { DynamoClient } from '../aws';
import { generalConfig } from '../../config';
import { ReservationDetails } from '../types/reservation';
import { ReservationRefundDetailsService } from './reservationRefundDetails.service';

const table = generalConfig.aws.dataDynamoTableName;
const detailsKey = 'reservation_details';

@Injectable()
export class ReservationDetailsService {
  constructor(private readonly dynamoClient: DynamoClient, private readonly reservationRefundDetailsService: ReservationRefundDetailsService) {}

  async getDetails(id: string): Promise<ReservationDetails> {
    const details = await this.dynamoClient.getItem<ReservationDetails>(table, { dataKey: id, sKey: detailsKey });
    if (!details) {
      if (generalConfig.env === 'dev') {
        return this.createDetails(id, id.slice(0, -2));
      }
      throw new Error(`Could not find reservation details ${id}`);
    }
    details.refundDetails = await this.reservationRefundDetailsService.getDetails(id);
    return details;
  }

  async createDetails(id: string, bookingId: string, initial?: Partial<ReservationDetails>): Promise<ReservationDetails> {
    const details: ReservationDetails = {
      id,
      bookingId,
      bookingConfirmationSent: false,
      cancelConfirmationSent: false,
      checkoutInfosSent: false,
      checkinInfosSent: false,
      checkinDayInfosSent: false,
      invoiceSent: false,
      pinCodes: {},
      breakfastSelection: [],
      specialWishes: [],
      ...initial,
    };
    await this.dynamoClient.putItem(table, {
      dataKey: id,
      sKey: detailsKey,
      ...details,
    });
    return details;
  }

  async updateDetails(id: string, values: Partial<ReservationDetails>): Promise<void> {
    const key = {
      dataKey: id,
      sKey: detailsKey,
    };
    await this.dynamoClient.updateItem(table, key, values);
  }
}
