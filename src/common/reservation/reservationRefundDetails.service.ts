import { Injectable } from '@nestjs/common';
import { DynamoClient } from '../aws';
import { generalConfig } from '../../config';
import { ReservationRefundDetails } from '../types/reservation';

const table = generalConfig.aws.dataDynamoTableName;
const sKey = 'refund_status';

@Injectable()
export class ReservationRefundDetailsService {
  constructor(private readonly dynamoClient: DynamoClient) { }

  async getDetails(id: string): Promise<ReservationRefundDetails | undefined> {
    const details = await this.dynamoClient.getItem(table, { dataKey: id, sKey });
    return details as ReservationRefundDetails;
  }

  async updateDetails(id: string, values: Partial<ReservationRefundDetails>): Promise<void> {
    values.reservationId = id;
    await this.dynamoClient.updateItem(table, { dataKey: id, sKey }, values);
  }
}
