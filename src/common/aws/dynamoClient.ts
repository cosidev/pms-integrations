import { Injectable } from '@nestjs/common';
import { DynamoDB } from 'aws-sdk';
import { generalConfig } from '../../config';

interface Key {
  [key: string]: string;
}

interface FormattedUpdate {
  updateExpression: string;
  updateValues: {
    [key: string]: any;
  };
}

@Injectable()
export class DynamoClient extends DynamoDB.DocumentClient {
  constructor() {
    const { aws } = generalConfig;
    super({
      region: aws.dynamoRegion,
      endpoint: aws.dynamoEndpoint,
      convertEmptyValues: true,
    });
  }

  async getItem<T>(table: string, key: Key, removeKeys: boolean = true): Promise<T | undefined> {
    const response = await this.get({ TableName: table, Key: key }).promise();
    const item = response.Item;
    if (!item) {
      return undefined;
    }
    if (removeKeys) {
      Object.keys(key).forEach(k => delete item[k]);
    }
    return item as T;
  }

  async batchGetItems<T>(table: string, keys: Key[]): Promise<T[] | undefined> {
    const RequestItems = {
      [table]: {
        Keys: keys,
      },
    };
    const response = await this.batchGet({ RequestItems }).promise();
    const items = response.Responses;

    if (!items || !items[table].length) {
      return undefined;
    }

    return items[table] as Partial<T[]>;
  }

  async putItem<T>(table: string, item: Partial<T>): Promise<void> {
    await this.put({ TableName: table, Item: item }).promise();
  }

  async updateItem<T>(table: string, key: Key, values: Partial<T>): Promise<void> {
    const { updateExpression, updateValues } = this.formatUpdateExpression(values);
    await this.update({
      TableName: table,
      Key: key,
      UpdateExpression: updateExpression,
      ExpressionAttributeValues: updateValues,
      ReturnValues: 'NONE',
    }).promise();
  }

  formatUpdateExpression(values: any): FormattedUpdate {
    let updateExpression = 'set ';
    const updateValues = {};
    const alphabet = [...'abcdefghijklmnopqrstuvwxyz'];
    const updateEntries = Object.keys(values).length;
    if (updateEntries === 0 || updateEntries > alphabet.length) {
      return { updateExpression: '', updateValues: {} };
    }
    Object.keys(values).forEach((key, i) => {
      const index = alphabet[i];
      (updateExpression += `${key} = :${index},`), (updateValues[`:${index}`] = values[key]);
    });
    updateExpression = updateExpression.slice(0, -1);
    return { updateExpression, updateValues };
  }
}
