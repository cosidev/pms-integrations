import { Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { generalConfig } from '../config';

@Injectable()
export class S3Service {
  s3: S3;

  constructor() {
    this.s3 = new S3({
      s3ForcePathStyle: true,
      endpoint: generalConfig.aws.s3Endpoint,
      accessKeyId: generalConfig.aws.s3AccessKey,
      secretAccessKey: generalConfig.aws.s3AccessKey,
      signatureVersion: 'v4',
    });
  }

  getPresignedUrl(bucket: string, key: string, expires: number, contentType: string, acl: string): Promise<string> {
    const s3Params = {
      Bucket: bucket,
      Key: key,
      Expires: expires,
      ContentType: contentType,
      ACL: acl,
    };
    return this.s3.getSignedUrlPromise('putObject', s3Params);
  }

  getFile(bucket: string, key: string) {
    return this.s3.getObject({
      Bucket: bucket,
      Key: key,
    }).promise();
  }
}
