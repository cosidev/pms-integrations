import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { SiteClient } from 'datocms-client';
import { generalConfig } from '../../config';
import logger from '../../LoggingService';

@Injectable()
export class DatoCmsService {
  public async graphqlRequest(query: string) {
    const response = await axios.post(
      'https://graphql.datocms.com',
      { query },
      {
        headers: {
          Authorization: `Beaerer ${generalConfig.datoApiKey}`,
        },
      },
    );
    if (!response.data || response.data.errors) {
      logger.errorLog(`Could not fetch datocms graphql api`, { query, errors: response.data?.errors });
      throw new Error(`Could not fetch datocms graphql api`);
    }
    return response.data;
  }
}
