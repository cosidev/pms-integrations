import { Module } from '@nestjs/common';
import JWTService from '../JWTService';
import { S3Service } from './s3.service';
import { ReservationDetailsService } from './reservation/reservationDetails.service';
import { PropertyDetailsService } from './propertyDetails.service';
import { DynamoClient } from './aws';
import { ReservationRefundDetailsService } from './reservation/reservationRefundDetails.service';
import { PinCodeHandler } from '../PinCodeHandler';
import CmsClient from '../CmsClient';
import { SlackService } from '../SlackService';
import { DatoCmsService } from './datoCms/DatoCms.service';

@Module({
  providers: [
    JWTService,
    S3Service,
    ReservationDetailsService,
    ReservationRefundDetailsService,
    DynamoClient,
    PinCodeHandler,
    CmsClient,
    SlackService,
    PropertyDetailsService,
    DatoCmsService,
  ],
  exports: [
    JWTService,
    S3Service,
    ReservationDetailsService,
    ReservationRefundDetailsService,
    DynamoClient,
    PinCodeHandler,
    CmsClient,
    SlackService,
    DatoCmsService,
    PropertyDetailsService,
  ],
})
export class CommonModule {}
