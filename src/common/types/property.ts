import { Translated } from '../../types';

export interface PropertyDetails {
  title: string;
  propertyId: string;
  bookingMail: string;
  bookingPage: string;
  checkInAndOutDetails: Translated<string>;
  additionalInformation: Translated<string>;
  parking: Translated<string>;
  breakfastRecommendations: Translated<string>;
  specialHints: Translated<string>;
  recommendations: Translated<string>;
  emailFooter: Translated<string>;
  directions: Translated<string[]>;
  rooms: Translated<string[]>;
  signature: Translated<string>;
  availableServices?: string[];
}
