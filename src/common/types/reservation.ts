import { SpecialWish, BreakfastSelection } from '../../types';

export interface ReservationDetails {
  id: string;
  bookingId: string;
  bookingConfirmationSent: boolean;
  invoiceSent: boolean;
  checkinInfosSent: boolean;
  checkinDayInfosSent: boolean;
  checkoutInfosSent: boolean;
  cancelConfirmationSent: boolean;
  pinCodes: {
    [key: string]: number;
  };
  specialWishes: SpecialWish[];
  breakfastSelection: BreakfastSelection[];
  refundDetails?: ReservationRefundDetails;
  urls?: { [key: string]: string };
}

export enum CommissionReturnStatus {
  Requested = 'requested',
  Committed = 'committed',
  None = 'none',
}

export interface ReservationRefundDetails {
  reservationId: string;
  forcedRefundByOta: boolean;
  guestRebooked: boolean;
  guestVoucher: boolean;
  rebookingReservationId?: string;
  refundedByUs: boolean;
  commissionReturnStatus: string;
}
