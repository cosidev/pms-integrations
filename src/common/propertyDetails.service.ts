import { Injectable } from '@nestjs/common';
import { DynamoClient } from './aws';
import { generalConfig } from '../config';
import { PropertyDetails } from './types/property';

const table = generalConfig.aws.dataDynamoTableName;
const detailsKey = 'property_details';

@Injectable()
export class PropertyDetailsService {
  constructor(private readonly dynamoClient: DynamoClient) {}

  async getDetails(id: string): Promise<PropertyDetails> {
    const details = await this.dynamoClient.getItem<PropertyDetails>(table, { dataKey: id, sKey: detailsKey });

    if (!details) {
      throw new Error(`Could not find property details ${id}`);
    }

    return details;
  }

  async createOrUpdateDetails(values: PropertyDetails): Promise<void> {
    const id = values.propertyId;

    await this.dynamoClient.putItem(table, {
      dataKey: id,
      sKey: detailsKey,
      ...values,
    });
  }
}
