import { Injectable } from '@nestjs/common';
import axios from 'axios';
import dateformat from 'dateformat';
import { generalConfig } from './config';
import logger from './LoggingService';
import { ApaleoReservation } from './types';

export const headers = {
  'Content-type': 'application/json',
};

export const webhookMap = {
  bookingChannel: 'https://hooks.slack.com/services/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',
  pragueExport: 'https://hooks.slack.com/services/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',
  lucaDirect: 'https://hooks.slack.com/services/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',
  marijanaDirect: 'https://hooks.slack.com/services/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',
  slackDevelopment: 'https://hooks.slack.com/services/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',
  ptAutomationLogs: 'https://hooks.slack.com/services/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',
};

@Injectable()
export class SlackService {
  sendNewReservationNotification(reservation: ApaleoReservation) {
    const nights = reservation.timeSlices.length;
    const { currency, amount } = reservation.totalGrossAmount;
    const adr = amount / nights;
    const childrenCount = reservation.childrenAges ? reservation.childrenAges.length : 0;
    const persons = `Erwachsene: ${reservation.adults}${childrenCount > 0 ? ` Kinder: ${childrenCount}` : ''}`;
    const rateplan = reservation.ratePlan.name;
    const text = `:tada: New reservation ${reservation.id} (${reservation.booker.firstName} ${reservation.booker.lastName}) :tada: \n
    Stay = ${dateformat(reservation.arrival, 'dd.mm.yyyy')} - ${dateformat(reservation.departure, 'dd.mm.yyyy')} \n
    Nights = ${nights}  ||   ADR = ${adr}${currency}   ||   Total = ${amount}${currency} \n
    ${persons} \n
    Rateplan: ${rateplan} \n
    Property = ${reservation.property.name} \n
    Category = ${reservation.unitGroup.name} \n
    Channel = ${reservation.source || reservation.channelCode} \n`;

    if (generalConfig.prod) {
      const attachments = [
        {
          fallback: `View in apaleo https://app.apaleo.com/${reservation.property.id}/reservations/${reservation.id}`,
          actions: [
            {
              type: 'button',
              text: 'View in apaleo',
              url: `https://app.apaleo.com/${reservation.property.id}/reservations/${reservation.id}`,
            },
          ],
        },
      ];
      return this.sendMessage(webhookMap.bookingChannel, text, attachments);
    }
    logger.infoLog('Not sending slack msg due to dev env');
  }

  async sendPragueExportData(content: string, propertyId: string, ubyportIdd: string) {
    const token = 'xoxp-650561129729-727126661284-844642177191-cdfa2212f8ebe9b5c343448b01218a73';
    const channels = 'GQGUKA6HY'; // DMD3QKYTE
    const filename = `${ubyportIdd}_${dateformat(new Date(), 'yymmddhhMM')}.unl`;
    const filetype = 'unl';
    const title = `Exportfile ${propertyId}`;
    const serialize = obj => {
      const str = [];
      for (const p in obj) {
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
        }
      }
      return str.join('&');
    };
    return axios.post('https://slack.com/api/files.upload', serialize({ token, channels, filename, filetype, title, content }), {
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    });
  }

  sendPragueMessage(text: string) {
    return this.sendMessage(webhookMap.pragueExport, text);
  }

  sendOperationsTask(text: string, photos?: string[]) {
    const attachments = photos.map((photo, index) => {
      return {
        fallback: 'Report photos.',
        type: 'image',
        text: `Picture ${index + 1}`,
        image_url: photo,
      };
    });
    // ToDo: Create operations task channel
    return this.sendMessage(webhookMap.lucaDirect, text, attachments);
  }

  sendAmmendedGrossAmount(text: string) {
    return this.sendMessage(webhookMap.lucaDirect, text);
  }

  sendTravelValidationMetrics(reservationIds: string[]) {
    let text = `*Travel purpose validation complete* :tada: \n`;
    if (reservationIds.length > 0) {
      text += `*Reservations with travel purpose modified to Business* = ${reservationIds.join(', ') || 0}`;
    } else {
      text += `*No suitable reservations found*`;
    }

    return this.sendMessage(webhookMap.ptAutomationLogs, text);
  }

  sendAutomaticCheckInNotification(reservationId: string, propertyId: string) {
    const attachments = [
      {
        fallback: `View in apaleo https://app.apaleo.com/${propertyId}/reservations/${reservationId}/COSIGROUPLIVE-RESERVATIONDETAILS`,
        actions: [
          {
            type: 'button',
            text: 'View in apaleo',
            url: `https://app.apaleo.com/${propertyId}/reservations/${reservationId}/COSIGROUPLIVE-RESERVATIONDETAILS`,
          },
        ],
      },
    ];

    const text = `*Automatic checkin process complete* :tada: \n
    *Reservation*: ${reservationId} \n
    *Property*: ${propertyId} \n`;

    return this.sendMessage(webhookMap.ptAutomationLogs, text, attachments);
  }

  sendOverbookingsReport(text: string) {
    return this.sendMessage(webhookMap.ptAutomationLogs, text);
  }

  sendDisabledAvailabilityNotification(text: string) {
    return this.sendMessage(webhookMap.ptAutomationLogs, text);
  }

  sendResetOverbookingsNotification(text: string) {
    return this.sendMessage(webhookMap.ptAutomationLogs, text);
  }

  sendMessage(hook: string, text: string, attachments?: any) {
    if (generalConfig.prod) {
      return axios.post(hook, { text, attachments }, { headers });
    }
    return axios.post(webhookMap.slackDevelopment, { text, attachments }, { headers });
  }
}
