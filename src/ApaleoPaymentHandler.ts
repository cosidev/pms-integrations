import { isBefore, isToday } from 'date-fns';
import { ApaleoClient } from './apaleo/ApaleoClient';
import { EnumChargeModelServiceType, FolioModel, EnumCreateCustomPaymentRequestMethod } from './apaleo/generated/finance';
import { ApaleoReservation } from './types';
import logger from './LoggingService';
import { ReservationModel } from './apaleo/generated/reservation';

export class ApaleoPaymentHandler {
  private apaleoClient: ApaleoClient;

  constructor() {
    this.apaleoClient = new ApaleoClient();
  }

  async addCheckOutPayments() {
    const { reservations = [] } = await this.apaleoClient.getTodayDepartures();
    for (const reservation of reservations) {
      await this.addPaymentToReservation(reservation);
    }
  }

  async addCheckInPayments() {
    const { reservations: arrivals = [] } = await this.apaleoClient.getThreeDaysArrivals([]);
    for (const arrival of arrivals) {
      await this.addPaymentToReservation(arrival);
    }
  }

  async addPaymentToReservation(reservation: ApaleoReservation): Promise<ReservationModel> {
    try {
      // Get main folio
      const { folios } = await this.apaleoClient.getFoliosByReservationId(reservation.id, { onlyMain: true });
      const folio = await this.apaleoClient.getFolioById(folios[0].id);

      const accBalance = this.getAccomondationBalance(folio);
      if (accBalance === 0) {
        logger.infoLog(`No payment adding needed for reservation ${reservation.id}`);
        return this.reloadReservation(reservation.id);
      }

      const { isAirBnB, isBooking, isCheckoutDay, isCreditCard } = this.getInfos(reservation);

      const amount = {
        amount: accBalance,
        currency: folio.balance.currency,
      };
      if (isAirBnB && isCheckoutDay) {
        const payment = {
          amount,
          receipt: 'AirBnB',
          method: EnumCreateCustomPaymentRequestMethod.Airbnb,
        };
        logger.infoLog(`Adding airbnb payment to reservation ${reservation.id}`, { airbnbPayment: payment });
        await this.apaleoClient.addPaymentToFolio(folio.id, payment);
        return this.reloadReservation(reservation.id);
      }

      if (isBooking && !isCreditCard && isCheckoutDay) {
        const booking = await this.apaleoClient.getBookingById(reservation.bookingId);
        const isPrePaid = booking.bookerComment && booking.bookerComment.includes('THIS RESERVATION HAS BEEN PRE-PAID');
        if (isPrePaid) {
          const payment = {
            amount,
            receipt: 'Booking.com',
            method: EnumCreateCustomPaymentRequestMethod.BookingCom,
          };
          logger.infoLog(`Adding booking.com payment to reservation ${reservation.id}`, { bookingPayment: payment });
          await this.apaleoClient.addPaymentToFolio(folio.id, payment);
          return this.reloadReservation(reservation.id);
        }
      }

      if (isCreditCard && reservation.paymentAccount.isActive) {
        logger.infoLog(`Adding payment account payment to reservation ${reservation.id}`);
        await this.apaleoClient.addPaymentByPaymentAccount(folio.id, { amount });
        return this.reloadReservation(reservation.id);
      }

      return this.reloadReservation(reservation.id);
    } catch (e) {
      logger.errorLog(`Could not add automatic payment to ${reservation.id}`, { error: e });
      return this.reloadReservation(reservation.id);
    }
  }

  async isReservationPaid(reservation: ApaleoReservation) {
    if (reservation.balance.amount >= 0) {
      return true;
    }

    const { isAirBnB, isBooking, isHomeLike, isCreditCard } = this.getInfos(reservation);

    if (isAirBnB || isHomeLike) {
      return true;
    }

    if (isBooking && !isCreditCard) {
      const booking = await this.apaleoClient.getBookingById(reservation.bookingId);
      const isPrePaid = booking.bookerComment && booking.bookerComment.includes('THIS RESERVATION HAS BEEN PRE-PAID');
      return isPrePaid;
    }

    if (!reservation.paymentAccount || (reservation.paymentAccount && !reservation.paymentAccount.isVirtual)) {
      return false;
    }
    return true;
  }

  private getInfos(reservation: ApaleoReservation) {
    return {
      isAirBnB: reservation.source === 'Airbnb',
      isHomeLike: reservation.source === 'HomeLike',
      isBooking: reservation.source === 'Booking.com' || reservation.channelCode === 'BookingCom',
      isCreditCard: !!reservation.paymentAccount,
      isCheckoutDay: isToday(new Date(reservation.departure)),
    };
  }

  private reloadReservation(reservationId: string): Promise<ReservationModel> {
    return this.apaleoClient.getReservationById(reservationId, ['booker', 'assignedUnits', 'timeSlices']);
  }

  private getAccomondationBalance(folio: FolioModel): number {
    if (folio.balance.amount === 0) {
      return 0;
    }

    let accomondationAmount = folio.charges.reduce<number>((sum, charge) => {
      if (charge.serviceType === EnumChargeModelServiceType.Accommodation) {
        return sum + charge.amount.grossAmount;
      }
      return sum;
    }, 0);
    accomondationAmount = +accomondationAmount.toFixed(2);
    if (folio.allowedPayment >= accomondationAmount) {
      return accomondationAmount;
    }

    return 0;
  }
}
