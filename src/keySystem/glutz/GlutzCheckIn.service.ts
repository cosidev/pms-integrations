import { Injectable } from '@nestjs/common';
import GlutzApiService from './GlutzApi.service';
import { ApaleoClient } from '../../apaleo/ApaleoClient';
import { glutzProperties } from '../../util/properties';
import logger from '../../LoggingService';
import { SlackService } from '../../SlackService';

@Injectable()
export class GlutzCheckinService {
  constructor(
    private readonly glutzApi: GlutzApiService,
    private readonly apaleoClient: ApaleoClient,
    private readonly slack: SlackService,
  ) { }

  public async checkInReservations() {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    yesterday.setUTCHours(0, 0, 0);
    const { reservations = [] } = await this.apaleoClient.getReservationsbyTime(['Confirmed'], 'Arrival', yesterday, new Date(), {
      propertyIds: glutzProperties,
    });
    await Promise.all(
      reservations.map(async reservation => {
        logger.infoLog(`Checking pin code usage for ${reservation.id}`);
        try {
          const glutzUser = await this.glutzApi.getUser({ reservationId: reservation.id });
          logger.infoLog(`Found glutz user for ${reservation.id} (${glutzUser})`);
          const logs = await this.glutzApi.fetchDeviceLogs({
            userIds: [glutzUser],
            entryTypes: [2],
          });
          logger.infoLog(`Found ${logs.length} for ${reservation.id}`);
          if (logs.length > 0) {
            // Guest has used his code
            await this.apaleoClient.checkInReservationById(reservation.id);
            logger.infoLog(`Auto checked in ${reservation.id}`);
            await this.slack.sendAutomaticCheckInNotification(reservation.bookingId, reservation.property.id);
          }
        } catch (e) {
          logger.errorLog(`Error on auto checkin`, { e });
        }
      }),
    );
  }
}
