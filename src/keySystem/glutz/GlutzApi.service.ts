import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import axios from 'axios';
import { generalConfig } from '../../config';
import logger from '../../LoggingService';

interface GlutzUser {
  reservationId?: string;
  backupUnitId?: string;
}

interface CodeConfig {
  id?: string;
  code?: number;
  description?: string;
  userId?: string;
  validFrom?: string;
  validTo?: string;
}

interface DeviceLogsFilter {
  deviceids?: string[];
  entryTypes?: number[];
  userIds?: number[];
  startDateTime?: Date;
  endDateTime?: Date;
}

const glutzSystemUrl = 'https://pin.cosi-group.com';

@Injectable()
export default class GlutzApiService {
  public async fetchDeviceLogs(filter?: DeviceLogsFilter) {
    const arrayFormat = (x?: any[]) => (x ? x.map(y => `${y},`) : '');
    const formatDate = (x?: Date) => (x ? dateformat(x, 'isoUtcDateTime') : undefined);
    const formatedFilter = {
      ...filter,
      deviceids: arrayFormat(filter.deviceids),
      userIds: arrayFormat(filter.userIds),
      entryTypes: arrayFormat(filter.entryTypes),
      startDateTime: formatDate(filter.startDateTime),
      endDateTime: formatDate(filter.endDateTime),
    };
    const logs: string = await this.apiRequest('export/devicelog.csv', formatedFilter);
    const rows = logs.split('\n');
    // Empty log list
    if (rows.length === 2) {
      return [];
    }
    const headlines = rows.shift().split(';');
    return rows.map(row => {
      const item = {};
      row.split(';').forEach((x, i) => (item[headlines[i]] = x));
      return item;
    });
  }

  public async createUser(label: string, user: GlutzUser) {
    return this.rpcRequest([{ label, ...user }], 'eAccess.createUser');
  }

  public setCode(codeConfig: CodeConfig) {
    return this.rpcRequest(['Codes', { actionProfileId: '2', ...codeConfig }], 'eAccess.setModel');
  }

  public getUserCodes(userId: string) {
    return this.rpcRequest(['Codes', { userId }, ['id', 'userId', 'code', 'validFrom', 'validTo']], 'eAccess.getModel');
  }

  public getAccessPointId(unitId) {
    return this.rpcRequest([{ unitId }], 'eAccess.getAccessPoint');
  }

  public addUserToAccesspoint(accessPointId, userId) {
    return this.rpcRequest([[accessPointId], userId], 'eAccess.addAccessPointsToUser');
  }

  public getUser(filter: GlutzUser) {
    return this.rpcRequest([filter], 'eAccess.getUser');
  }

  public deleteUser(id: string) {
    return this.rpcRequest([id], 'eAccess.deleteUser');
  }

  private apiRequest = async (path: string, params = {}) => {
    const auth = {
      username: 'cosi',
      password: generalConfig.glutzInterfacePass,
    };
    try {
      const data = await axios.get(`${glutzSystemUrl}/api/${path}`, { auth, params });
      if (data.status !== 200) {
        logger.debugLog('Invalid glutz server response', { glutzApiRequest: { path, data } });
        throw new Error(`Invalid glutz api respone ${path}`);
      }
      return data.data;
    } catch (e) {
      logger.debugLog('Invalid glutz server response', { glutzApiRequest: { path, e } });
      throw e;
    }
  };

  private rpcRequest = async (params, method) => {
    const body = {
      jsonrpc: '2.0',
      id: 1,
      params,
      method,
    };
    const auth = {
      username: 'cosi',
      password: generalConfig.glutzInterfacePass,
    };
    const conf = {
      auth,
      dataType: 'json',
      headers: { 'Content-Type': 'application/json-rpc' },
    };
    const { data } = await axios.post(`${glutzSystemUrl}/rpc`, JSON.stringify(body), conf);
    if (!data || data.result === '0') {
      logger.debugLog('Invalid glutz server response', { glutzRequest: { params, method, data } });
      throw new Error('Invalid response');
    }
    return data.result;
  };
}
