import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { ApaleoClient } from '../../apaleo/ApaleoClient';
import { GlutzCheckinService } from './GlutzCheckIn.service';
import GlutzApiService from './GlutzApi.service';

jest.mock('../../LoggingService.ts');

describe('GlutzCheckInService', () => {
  let apaleoClient: ApaleoClient;
  let glutzCheckInService: GlutzCheckinService;
  let glutzApiService: GlutzApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    apaleoClient = module.get<ApaleoClient>(ApaleoClient);
    glutzApiService = module.get<GlutzApiService>(GlutzApiService);
    glutzCheckInService = module.get<GlutzCheckinService>(GlutzCheckinService);
  });

  describe('checkInReservations', () => {
    it('should not perform any checkin if there are no suitable reservations', async () => {
      jest.spyOn(apaleoClient, 'getReservationsbyTime').mockImplementation(() => new Promise(resolve => resolve({ reservations: [], count: 0 })));
      jest.spyOn(glutzApiService, 'getUser');

      await glutzCheckInService.checkInReservations();

      expect(glutzApiService.getUser).toHaveBeenCalledTimes(0);
    });

    it('should not perform any checkin if there are no logs for a reservation', async () => {
      const testReservation: any = { id: 'XXX' };
      jest
        .spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve({ reservations: [testReservation], count: 1 })));
      jest.spyOn(glutzApiService, 'getUser').mockImplementation(() => new Promise(resolve => resolve(1)));
      jest.spyOn(glutzApiService, 'fetchDeviceLogs').mockImplementation(() => new Promise(resolve => resolve([])));
      jest.spyOn(apaleoClient, 'checkInReservationById');

      await glutzCheckInService.checkInReservations();

      expect(glutzApiService.getUser).toHaveBeenCalledTimes(1);
      expect(glutzApiService.fetchDeviceLogs).toHaveBeenCalledTimes(1);
      expect(apaleoClient.checkInReservationById).toHaveBeenCalledTimes(0);
    });

    it('should perform checkin if there are logs for a reservation', async () => {
      const testReservation: any = { id: 'XXX' };
      jest
        .spyOn(apaleoClient, 'getReservationsbyTime')
        .mockImplementation(() => new Promise(resolve => resolve({ reservations: [testReservation], count: 1 })));
      jest.spyOn(glutzApiService, 'getUser').mockImplementation(() => new Promise(resolve => resolve(1)));
      jest.spyOn(glutzApiService, 'fetchDeviceLogs').mockImplementation(() => new Promise(resolve => resolve([{}])));
      jest.spyOn(apaleoClient, 'checkInReservationById').mockImplementation((id: string) => new Promise(resolve => resolve()));

      await glutzCheckInService.checkInReservations();

      expect(glutzApiService.getUser).toHaveBeenCalledTimes(1);
      expect(glutzApiService.getUser).toHaveBeenCalledWith({ reservationId: testReservation.id });
      expect(glutzApiService.fetchDeviceLogs).toHaveBeenCalledTimes(1);
      expect(apaleoClient.checkInReservationById).toHaveBeenCalledTimes(1);
      expect(apaleoClient.checkInReservationById).toHaveBeenCalledWith(testReservation.id);
    });
  });
});
