import { SlackService } from './../SlackService';
import { Module } from '@nestjs/common';
import { ApaleoModule } from './../apaleo/apaleo.module';
import GlutzApiService from './glutz/GlutzApi.service';
import { GlutzCheckinService } from './glutz/GlutzCheckIn.service';
import RemoteLockService from './remoteLock/remoteLock.service';
import RemoteLockApiService from './remoteLock/remoteLockApi.service';

@Module({
  imports: [ApaleoModule],
  controllers: [],
  providers: [GlutzApiService, GlutzCheckinService, SlackService, RemoteLockService, RemoteLockApiService],
})
export class KeySystemModule {}
