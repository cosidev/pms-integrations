import { Injectable } from '@nestjs/common';
import JWTService from '../../JWTService';
import axios, { Method } from 'axios';
import { generalConfig } from '../../config';
import logger from '../../LoggingService';

interface TokenInfo {
  token: string;
  expiresIn: number;
  createdAt: number;
}

@Injectable()
export default class RemoteLockApiService {
  private tokenInfo: TokenInfo;

  async authenticate() {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    const result = await axios.post(
      `https://connect.remotelock.com/oauth/token?client_id=${generalConfig.remoteLockClientId}&client_secret=${generalConfig.remoteLockSecret}&grant_type=client_credentials`,
      {},
      { headers },
    );
    const token = result.data?.access_token;
    if (!token) {
      logger.errorLog(`Could not authenticate with remoteLock api`, { remoteLockResponse: { ...result.data, status: result.status } });
      throw new Error('Could not retrieve remoteLock api token');
    }
    this.tokenInfo = {
      token: result.data.access_token,
      expiresIn: result.data.expires_in,
      createdAt: result.data.created_at,
    };
  }

  private async request(path: string, urlParams?: any, data?: any, method: Method = 'GET') {
    if (!this.tokenInfo) {
      await this.authenticate();
    }
    const timestamp = Date.now() / 1000;
    if (this.tokenInfo.createdAt + this.tokenInfo.expiresIn >= timestamp) {
      await this.authenticate();
    }
    try {
      const result = await axios.request({
        method,
        baseURL: 'https://api.remotelock.com',
        url: `/${path}`,
        headers: {
          Authorization: `Bearer ${this.tokenInfo.token}`,
        },
        params: urlParams,
        data,
      });
      if (`${result.status}`.substr(0, 1) !== '2') {
        logger.errorLog(`Could not get api response ${path}`, { remoteLockResponse: result.data });
        throw new Error(`Could not get api response ${path}`);
      }
      return result.data.data;
    } catch (e) {
      const response = e.response?.data;
      logger.errorLog(`Could not get api response PATH = ${path}`, { message: e.message, remoteLockResponse: response, errors: response?.errors });
      throw new Error(e);
    }
  }

  createGuest(guest: any) {
    return this.request(
      'access_persons',
      {},
      {
        type: 'access_guest',
        attributes: {
          ...guest,
        },
      },
      'POST',
    );
  }

  grantAccess(accessPersonId: string, accessId: string) {
    return this.request(
      `access_persons/${accessPersonId}/accesses`,
      '',
      {
        attributes: {
          accessible_id: accessId,
          accessible_type: 'lock',
        },
      },
      'POST',
    );
  }

  getAccessGuestsByName(name: string) {
    return this.request(`access_persons?type=access_guest&attributes[name]=${name}`);
  }

  deleteAccessPerson(id: string) {
    return this.request(`access_persons/${id}`, undefined, undefined, 'DELETE');
  }
}
