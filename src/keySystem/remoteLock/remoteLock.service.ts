import { Injectable, Logger } from '@nestjs/common';
import dateformat from 'dateformat';
import logger from '../../LoggingService';
import RemoteLockApiService from './remoteLockApi.service';

interface RemoteLockReservation {
  id: string;
  unitName: string;
  arrival: string;
  departure: string;
}

// Strauss unitName => remotelock device id
const unitRemotelockMapping = {
  51: 'b60b767f-7a15-403d-a908-45b631b71e8f',
  13: 'd701c5e0-f3b5-4017-a14f-83009a0bc21c',
  32: 'b5f1f23d-6a70-4753-bc40-6aa954bad226',
  54: 'dfb6e579-8415-4218-b8dd-5b6089b587eb',
  53: 'e639e856-0858-4379-b3f0-1dfcc89fe075',
  52: '8ce02740-7ff9-43f9-94ec-9663e5a7f337',
  45: '38e52171-70d5-462a-8eae-cc1e62d21c7e',
  44: '2d20b5e8-a7bb-4094-aa58-2200839b7004',
  43: '5040aed5-85b3-4003-8788-e03ea1558ca1',
  42: 'ee2e3a47-7006-489a-9762-b1cc2e8664a4',
  41: '72ee700c-bb91-448c-8f57-1d8d3eadb444',
  11: '47ebdfab-4755-453c-b98d-66b7be74ae3a',
  12: 'a6e95988-d300-4789-b538-851ea8b92d67',
  21: 'f1d50db7-623e-414b-8095-4c51a4a6dccc',
  22: 'a1e28a0c-1ba5-4ac4-922d-07541db99668',
  23: '9e667df4-8310-4bfb-80f0-97ae3f1d6fcb',
  24: 'f9c2794e-4457-4a09-9c19-587149036825',
  25: 'edb6c11b-c933-4d4e-a278-6d421201f01d',
  31: '2ec82081-1a1f-4df7-af4a-966d8b7774f9',
  33: '09a9ae72-a40d-4d4b-a154-b062626936c3',
  34: '02ff4de1-f429-4ca1-beb3-42d95521fddb',
  35: '99afbd73-e23b-4b31-812c-a8ad67384dca',
};

@Injectable()
export default class RemoteLockService {
  constructor(private readonly remoteLockApiService: RemoteLockApiService) {}

  async generateCode(reservation: RemoteLockReservation) {
    try {
      const accessId = unitRemotelockMapping[reservation.unitName];
      if (!accessId) {
        throw new Error(`Cant find remote lock for unit ${reservation.unitName}`);
      }
      const guest = await this.remoteLockApiService.createGuest({
        name: reservation.id,
        starts_at: dateformat(reservation.arrival, `yyyy-mm-dd'T'15:00:00`),
        ends_at: dateformat(reservation.departure, `yyyy-mm-dd'T'11:00:00`),
        generate_pin: true,
      });
      await this.remoteLockApiService.grantAccess(guest.id, accessId);
      return guest.attributes.pin;
    } catch (e) {
      logger.errorLog(`Could not create remote lock code for ${reservation.id}`, { e, message: e.message });
      throw e;
    }
  }

  async removeCode(reservationId: string) {
    const accessGuests = await this.remoteLockApiService.getAccessGuestsByName(reservationId);
    await Promise.all(accessGuests.map(guest => this.remoteLockApiService.deleteAccessPerson(guest.id)));
  }
}
