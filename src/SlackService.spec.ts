import axios from 'axios';
import { Test, TestingModule } from '@nestjs/testing';
import dateformat from 'dateformat';
import { AppModule } from './app.module';
import { SlackService, headers, webhookMap } from './SlackService';
import { NEW_RESERVATION_SLACK as RESERVATION } from './apaleo/__mocks__/newReservation.mock';
import { generalConfig } from './config';
import { UNIT_GROUPS } from './SlackService.mock';

jest.mock('./LoggingService.ts');
jest.spyOn(axios, 'post').mockImplementation(() => new Promise(resolve => resolve()));

describe('SlackService', () => {
  let slack: SlackService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    slack = module.get<SlackService>(SlackService);
    generalConfig.prod = true;
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should handle sendNewReservationNotification', () => {
    const {
      id,
      property,
      booker,
      arrival,
      departure,
      timeSlices,
      totalGrossAmount,
      childrenAges,
      adults,
      ratePlan,
      unitGroup,
      source,
      channelCode,
    } = RESERVATION;
    jest.spyOn(slack, 'sendMessage');

    const nights = timeSlices.length;
    const { currency, amount } = totalGrossAmount;
    const adr = amount / nights;
    const childrenCount = childrenAges ? childrenAges.length : 0;
    const persons = `Erwachsene: ${adults}${childrenCount > 0 ? ` Kinder: ${childrenCount}` : ''}`;
    const rateplan = ratePlan.name;

    const slackText = `:tada: New reservation ${id} (${booker.firstName} ${booker.lastName}) :tada: \n
    Stay = ${dateformat(arrival, 'dd.mm.yyyy')} - ${dateformat(departure, 'dd.mm.yyyy')} \n
    Nights = ${nights}  ||   ADR = ${adr}${currency}   ||   Total = ${amount}${currency} \n
    ${persons} \n
    Rateplan: ${rateplan} \n
    Property = ${property.name} \n
    Category = ${unitGroup.name} \n
    Channel = ${source || channelCode} \n`;

    const attachments = [
      {
        fallback: `View in apaleo https://app.apaleo.com/${property.id}/reservations/${id}`,
        actions: [
          {
            type: 'button',
            text: 'View in apaleo',
            url: `https://app.apaleo.com/${property.id}/reservations/${id}`,
          },
        ],
      },
    ];

    slack.sendNewReservationNotification(RESERVATION);

    expect(slack.sendMessage).toHaveBeenCalledTimes(1);
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.bookingChannel, slackText, attachments);
    expect(axios.post).toHaveBeenCalledTimes(1);

    jest.resetAllMocks();
    generalConfig.prod = false;
    slack.sendNewReservationNotification(RESERVATION);
    expect(slack.sendMessage).not.toHaveBeenCalled();
    expect(axios.post).not.toHaveBeenCalled();
  });

  it('should correctly trigger sendMessage based on environment', () => {
    const PROD_HOOK = 'https://hooks.slack.com/services/cosi/prod';
    const TEXT = 'New automation was triggered';

    slack.sendMessage(PROD_HOOK, TEXT);

    expect(axios.post).toHaveBeenCalledTimes(1);
    expect(axios.post).toHaveBeenCalledWith(
      PROD_HOOK,
      {
        text: TEXT,
        attachments: undefined,
      },
      {
        headers,
      },
    );

    generalConfig.prod = false;
    slack.sendMessage(PROD_HOOK, TEXT);
    expect(axios.post).toHaveBeenCalledWith(
      webhookMap.slackDevelopment,
      {
        text: TEXT,
        attachments: undefined,
      },
      {
        headers,
      },
    );
  });

  it('should correctly trigger sendAmmendedGrossAmount', () => {
    jest.spyOn(slack, 'sendMessage');

    const TEXT = 'Reservation with ID BJTKLHDK-2 was amended';
    slack.sendAmmendedGrossAmount(TEXT);

    expect(axios.post).toHaveBeenCalled();
    expect(axios.post).toHaveBeenCalledWith(
      webhookMap.lucaDirect,
      {
        text: TEXT,
        attachments: undefined,
      },
      {
        headers,
      },
    );
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.lucaDirect, TEXT);
  });

  it('should correctly trigger sendPragueMessage', () => {
    jest.spyOn(slack, 'sendMessage');

    const TEXT = 'Export ops completed without errors';
    slack.sendPragueMessage(TEXT);

    expect(axios.post).toHaveBeenCalled();
    expect(axios.post).toHaveBeenCalledWith(
      webhookMap.pragueExport,
      {
        text: TEXT,
        attachments: undefined,
      },
      {
        headers,
      },
    );
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.pragueExport, TEXT);
  });

  it('should handle sendTravelValidationMetrics', async () => {
    jest.spyOn(slack, 'sendMessage');

    const updatedReservationIds = ['ABC-1', 'XYZ-1'];
    await slack.sendTravelValidationMetrics(updatedReservationIds);

    let slackText = `*Travel purpose validation complete* :tada: \n*Reservations with travel purpose modified to Business* = ${updatedReservationIds.join(
      ', ',
    ) || 0}`;

    expect(slack.sendMessage).toHaveBeenCalledTimes(1);
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.ptAutomationLogs, slackText);
    expect(axios.post).toHaveBeenCalledTimes(1);

    jest.resetAllMocks();

    slackText = `*Travel purpose validation complete* :tada: \n*No suitable reservations found*`;

    await slack.sendTravelValidationMetrics([]);
    expect(slack.sendMessage).toHaveBeenCalledTimes(1);
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.ptAutomationLogs, slackText);
  });

  it('should handle sendAutomaticCheckInNotification', async () => {
    jest.spyOn(slack, 'sendMessage');

    const RESERVATION_ID = 'BJTKLHDK-1';
    const PROPERTY_ID = 'DE_DEV_001';
    await slack.sendAutomaticCheckInNotification(RESERVATION_ID, PROPERTY_ID);

    const slackText = `*Automatic checkin process complete* :tada: \n
    *Reservation*: ${RESERVATION_ID} \n
    *Property*: ${PROPERTY_ID} \n`;

    const attachments = [
      {
        fallback: `View in apaleo https://app.apaleo.com/${PROPERTY_ID}/reservations/${RESERVATION_ID}/COSIGROUPLIVE-RESERVATIONDETAILS`,
        actions: [
          {
            type: 'button',
            text: 'View in apaleo',
            url: `https://app.apaleo.com/${PROPERTY_ID}/reservations/${RESERVATION_ID}/COSIGROUPLIVE-RESERVATIONDETAILS`,
          },
        ],
      },
    ];

    expect(slack.sendMessage).toHaveBeenCalledTimes(1);
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.ptAutomationLogs, slackText, attachments);
    expect(axios.post).toHaveBeenCalledTimes(1);
  });

  it('should handle sendOperationsTask', async () => {
    jest.spyOn(slack, 'sendMessage');

    const photos = ['http://unsplash.com/image/1'];
    const slackText = 'Lost items have been reported in Unit 304 CZ_PR_002';
    await slack.sendOperationsTask(slackText, photos);

    const attachments = [
      {
        fallback: 'Report photos.',
        type: 'image',
        text: 'Picture 1',
        image_url: photos[0],
      },
    ];

    expect(slack.sendMessage).toHaveBeenCalledTimes(1);
    expect(slack.sendMessage).toHaveBeenCalledWith(webhookMap.lucaDirect, slackText, attachments);
    expect(axios.post).toHaveBeenCalledTimes(1);
  });
});
