import { IsString, IsEnum } from 'class-validator';
import { MailType } from '../../types';

export default class ManualEventDto {
  @IsEnum(MailType)
  type: MailType;

  @IsString()
  reservationId: string;
}
