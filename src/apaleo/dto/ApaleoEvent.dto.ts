import { IsString, IsNumber, IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

class Data {
  @IsString()
  entityId: string;
}

// tslint:disable-next-line: max-classes-per-file
export default class ApaleoEventDto {
  @IsString()
  topic: string;

  @IsString()
  type: string;

  @IsString()
  id: string;

  @IsString()
  @IsOptional()
  accountId?: string;

  @IsString()
  @IsOptional()
  propertyId?: string;

  @ValidateNested()
  @Type(() => Data)
  data: Data;

  @IsNumber()
  @IsOptional()
  timestamp: number;
}
