/** Generate by swagger-axios-codegen */
// tslint:disable
/* eslint-disable */
import axiosStatic, { AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType,
  };
  return configs;
}

export class ReservationService {
  /**
   * Returns a list of all reservations, filtered by the specified parameters.
   */
  static bookingReservationsGet(
    params: {
      /** Filter result by booking id */
      bookingId?: string;
      /** Filter result by requested properties */
      propertyIds?: string[];
      /** Filter result by requested rate plans */
      ratePlanIds?: string[];
      /** Filter result by requested companies */
      companyIds?: string[];
      /** Filter result by assigned units */
      unitIds?: string[];
      /** Filter result by requested unit groups */
      unitGroupIds?: string[];
      /** Filter result by requested unit group types */
      unitGroupTypes?: string[];
      /** Filter result by requested blocks */
      blockIds?: string[];
      /** Filter result by reservation status */
      status?: string[];
      /** Filter by date and time attributes of reservation. Use in combination with the 'To' and 'From' attributes.
All filters will check if the date specified by the filter type is between from (included) and to (excluded).
The exception being filtering for 'stay', which will return all reservations that are overlapping with the interval specified by from and to. */
      dateFilter?: string;
      /** The start of the time interval. When filtering by date, at least one of 'from' and 'to' has to be specified<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      from?: string;
      /** The end of the time interval, must be larger than 'from'. When filtering by date, at least one of 'from' and 'to' has to be specified<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      to?: string;
      /** Filter result by the channel code */
      channelCode?: string[];
      /** Filter result by source */
      sources?: string[];
      /** Filter result by validation message category */
      validationMessageCategory?: string[];
      /** Filter result by the external code. The result set will contain all reservations that have an external code starting with the
provided value */
      externalCode?: string;
      /** This will filter all reservations where the provided text is contained in: booker first name or last name or email or company name,
primary guest first name or last name or email or company name, external code, reservation id, unit name. The search is case insensitive. */
      textSearch?: string;
      /** This will filter reservations based on their balance.<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      balanceFilter?: string[];
      /** If set to {true}, returns only reservations, in which all folios are closed and have an invoice.
If set to {false}, returns only reservations, in which some of the folios are open or don't have an invoice */
      allFoliosHaveInvoice?: boolean;
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all fields that can be used to sort the results. Possible values are: arrival:asc, arrival:desc, departure:asc, departure:desc, created:asc, created:desc, updated:asc, updated:desc, id:asc, id:desc, firstname:asc, firstname:desc, lastname:asc, lastname:desc, unitname:asc, unitname:desc. All other values will be silently ignored. */
      sort?: string[];
      /** List of all embedded resources that should be expanded in the response. Possible values are: booker, actions, timeSlices, services, assignedUnits, company. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<ReservationListModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservations';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        bookingId: params['bookingId'],
        propertyIds: params['propertyIds'],
        ratePlanIds: params['ratePlanIds'],
        companyIds: params['companyIds'],
        unitIds: params['unitIds'],
        unitGroupIds: params['unitGroupIds'],
        unitGroupTypes: params['unitGroupTypes'],
        blockIds: params['blockIds'],
        status: params['status'],
        dateFilter: params['dateFilter'],
        from: params['from'],
        to: params['to'],
        channelCode: params['channelCode'],
        sources: params['sources'],
        validationMessageCategory: params['validationMessageCategory'],
        externalCode: params['externalCode'],
        textSearch: params['textSearch'],
        balanceFilter: params['balanceFilter'],
        allFoliosHaveInvoice: params['allFoliosHaveInvoice'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        sort: params['sort'],
        expand: params['expand'],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Returns a specific reservation.
   */
  static bookingReservationsByIdGet(
    params: {
      /** Id of the reservation to be retrieved. */
      id: string;
      /** List of all embedded resources that should be expanded in the response. Possible values are: timeSlices, services, booker, actions, company. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<ReservationModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservations/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allows to modify certain reservation properties
   */
  static bookingReservationsByIdPatch(
    params: {
      /** Id of the reservation to be modified. */
      id: string;
      /** Define the list of operations to be applied to the resource. Learn more about JSON Patch here: http://jsonpatch.com/. */
      body: Operation[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservations/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a list of all available units for a reservation [DEPRECATED]
   */
  static bookingReservationsByIdAvailableUnitsGet(
    params: {
      /** The id of the reservation */
      id: string;
      /** The unit group id */
      unitGroupId?: string;
      /** The from date and time<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      from?: string;
      /** The to date and time<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      to?: string;
      /** Should units that are set OutOfService in the defined time period be returned as available. */
      includeOutOfService?: boolean;
      /** The unit condition */
      unitCondition?: string;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<AvailableUnitListModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservations/{id}/available-units';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        unitGroupId: params['unitGroupId'],
        from: params['from'],
        to: params['to'],
        includeOutOfService: params['includeOutOfService'],
        unitCondition: params['unitCondition'],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class BookingService {
  /**
   * Creates a booking for one or more reservations.
   */
  static bookingBookingsPost(
    params: {
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The list of reservations you want to create. */
      body: CreateBookingModel;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<BookingCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/bookings';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Creates a booking for one or more reservations regardless of availability or restrictions.
   */
  static bookingBookings$forcePost(
    params: {
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The list of reservations you want to create. */
      body: CreateBookingModel;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<BookingCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/bookings/$force';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Returns a specific booking.
   */
  static bookingBookingsByIdGet(
    params: {
      /** Id of the booking to be retrieved. */
      id: string;
      /** List of all embedded resources that should be expanded in the response. Possible values are: property, unitGroup, ratePlan, services, reservations, propertyValues. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<BookingModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/bookings/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allows to modify certain booking properties
   */
  static bookingBookingsByIdPatch(
    params: {
      /** Id of the booking to be modified. */
      id: string;
      /** Define the list of operations to be applied to the resource. Learn more about JSON Patch here: http://jsonpatch.com/. */
      body: Operation[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/bookings/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReservationActionsService {
  /**
   * Assign a unit to a reservation.
   */
  static bookingReservationActionsByIdAssignUnitPut(
    params: {
      /** Id of the reservation a unit should be assigned to. */
      id: string;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<AutoAssignedUnitListModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/assign-unit';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Assign a specific unit to a reservation.
   */
  static bookingReservationActionsByIdAssignUnitByUnitIdPut(
    params: {
      /** Id of the reservation the unit should be assigned to. */
      id: string;
      /** The id of the unit to be assigned. */
      unitId: string;
      /** The start date and optional time for the unit assignment. If not specified, the reservation's arrival will be used.<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      from?: string;
      /** The end date and optional time for the unit assignment. If not specified, the reservation's departure will be used.<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      to?: string;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<AssignedUnitModel> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/assign-unit/{unitId}';
      url = url.replace('{id}', params['id'] + '');
      url = url.replace('{unitId}', params['unitId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);
      configs.params = { from: params['from'], to: params['to'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Check-in of a reservation.
   */
  static bookingReservationActionsByIdCheckinPut(
    params: {
      /** Id of the reservation that should be processed. */
      id: string;
      /**  */
      withCityTax?: boolean;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/checkin';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);
      configs.params = { withCityTax: params['withCityTax'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Check-out of a reservation.
   */
  static bookingReservationActionsByIdCheckoutPut(
    params: {
      /** Id of the reservation that should be processed. */
      id: string;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/checkout';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cancel a reservation.
   */
  static bookingReservationActionsByIdCancelPut(
    params: {
      /** Id of the reservation that should be processed. */
      id: string;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/cancel';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allows you to amend the stay details of a reservation
   */
  static bookingReservationActionsByIdAmendPut(
    params: {
      /** Id of the reservation that should be modified */
      id: string;
      /** The new stay details that should be applied to the reservation. */
      body: DesiredStayDetailsModel;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/amend';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allows you to amend the stay details of a reservation regardless of availability or restrictions.
   */
  static bookingReservationActionsByIdAmend$forcePut(
    params: {
      /** Id of the reservation that should be modified */
      id: string;
      /** The new stay details that should be applied to the reservation. */
      body: DesiredStayDetailsModel;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/booking/v1/reservation-actions/{id}/amend/$force';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmbeddedPropertyModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The name for the property */

  name: string;

  /** The description for the property */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class EmbeddedUnitGroupModel {
  /** The unit group id */

  id: string;

  /** The code for the unit group that can be shown in reports and table views */

  code: string;

  /** The name for the unit group */

  name: string;

  /** The description for the unit group */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class AvailableUnitItemStatusModel {
  /**  */

  isOccupied: boolean;

  /**  */

  condition: EnumAvailableUnitItemStatusModelCondition;

  /**  */

  maintenanceType: EnumAvailableUnitItemStatusModelMaintenanceType;

  constructor(data: undefined | any = {}) {
    this['isOccupied'] = data['isOccupied'];
    this['condition'] = data['condition'];
    this['maintenanceType'] = data['maintenanceType'];
  }
}

export class AvailableUnitItemModel {
  /** The unit id */

  id: string;

  /** The name for the unit */

  name: string;

  /** The description for the unit */

  description: string;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  status: AvailableUnitItemStatusModel;

  /** Maximum number of persons for the unit */

  maxPersons: number;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['property'] = data['property'];
    this['unitGroup'] = data['unitGroup'];
    this['status'] = data['status'];
    this['maxPersons'] = data['maxPersons'];
  }
}

export class AvailableUnitListModel {
  /** List of units */

  units: AvailableUnitItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['units'] = data['units'];
    this['count'] = data['count'];
  }
}

export class MonetaryValueModel {
  /**  */

  amount: number;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['amount'] = data['amount'];
    this['currency'] = data['currency'];
  }
}

export class EmbeddedRatePlanModel {
  /** The rate plan id */

  id: string;

  /** The code for the rate plan that can be shown in reports and table views */

  code: string;

  /** The name for the rate plan */

  name: string;

  /** The description for the rate plan */

  description: string;

  /** Whether the rate plan is subject to city tax or not */

  isSubjectToCityTax: boolean;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['isSubjectToCityTax'] = data['isSubjectToCityTax'];
  }
}

export class AmountModel {
  /**  */

  grossAmount: number;

  /**  */

  netAmount: number;

  /**  */

  vatType: EnumAmountModelVatType;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['grossAmount'] = data['grossAmount'];
    this['netAmount'] = data['netAmount'];
    this['vatType'] = data['vatType'];
    this['currency'] = data['currency'];
  }
}

export class Operation {
  /**  */

  value: object;

  /**  */

  path: string;

  /**  */

  op: string;

  /**  */

  from: string;

  constructor(data: undefined | any = {}) {
    this['value'] = data['value'];
    this['path'] = data['path'];
    this['op'] = data['op'];
    this['from'] = data['from'];
  }
}

export class CreatePaymentAccountModel {
  /** The account number (e.g. masked credit card number or last 4 digits) */

  accountNumber: string;

  /** The account holder (e.g. card holder) */

  accountHolder: string;

  /** The credit card's expiration month */

  expiryMonth: string;

  /** The credit card's expiration year */

  expiryYear: string;

  /** The payment method (e.g. visa) */

  paymentMethod: string;

  /** The email address of the shopper / customer */

  payerEmail: string;

  /** The reference used to uniquely identify the shopper (e.g. user ID or account ID). Used for recurring payments */

  payerReference: string;

  /** The reference of a payment transaction. This should be set when a payment transaction has been already initiated and should be completed upon reservation creation. */

  transactionReference: string;

  /** Indicates if the payment account is a virtual credit card. If not specified it defaults to 'false' */

  isVirtual: boolean;

  constructor(data: undefined | any = {}) {
    this['accountNumber'] = data['accountNumber'];
    this['accountHolder'] = data['accountHolder'];
    this['expiryMonth'] = data['expiryMonth'];
    this['expiryYear'] = data['expiryYear'];
    this['paymentMethod'] = data['paymentMethod'];
    this['payerEmail'] = data['payerEmail'];
    this['payerReference'] = data['payerReference'];
    this['transactionReference'] = data['transactionReference'];
    this['isVirtual'] = data['isVirtual'];
  }
}

export class PersonAddressModel {
  /**  */

  addressLine1: string;

  /**  */

  addressLine2: string;

  /**  */

  postalCode: string;

  /**  */

  city: string;

  /**  */

  countryCode: string;

  constructor(data: undefined | any = {}) {
    this['addressLine1'] = data['addressLine1'];
    this['addressLine2'] = data['addressLine2'];
    this['postalCode'] = data['postalCode'];
    this['city'] = data['city'];
    this['countryCode'] = data['countryCode'];
  }
}

export class PersonCompanyModel {
  /** Name of the company */

  name: string;

  /** Tax or Vat ID of the company */

  taxId: string;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['taxId'] = data['taxId'];
  }
}

export class BookerModel {
  /** Title of the booker */

  title: EnumBookerModelTitle;

  /** Gender of the booker */

  gender: EnumBookerModelGender;

  /** First name of the booker */

  firstName: string;

  /** Middle initial of the booker */

  middleInitial: string;

  /** Last name of the booker */

  lastName: string;

  /** Email address of the booker */

  email: string;

  /** Phone number of the booker */

  phone: string;

  /**  */

  address: PersonAddressModel;

  /** The booker's nationality, in ISO 3166-1 alpha-2 code */

  nationalityCountryCode: string;

  /** The booker's identification number for the given identificationType. */

  identificationNumber: string;

  /** The type of the identificationNumber */

  identificationType: EnumBookerModelIdentificationType;

  /**  */

  company: PersonCompanyModel;

  /** Preferred contact two-letter language code (ISO Alpha-2) */

  preferredLanguage: string;

  /** Birth date */

  birthDate: Date;

  constructor(data: undefined | any = {}) {
    this['title'] = data['title'];
    this['gender'] = data['gender'];
    this['firstName'] = data['firstName'];
    this['middleInitial'] = data['middleInitial'];
    this['lastName'] = data['lastName'];
    this['email'] = data['email'];
    this['phone'] = data['phone'];
    this['address'] = data['address'];
    this['nationalityCountryCode'] = data['nationalityCountryCode'];
    this['identificationNumber'] = data['identificationNumber'];
    this['identificationType'] = data['identificationType'];
    this['company'] = data['company'];
    this['preferredLanguage'] = data['preferredLanguage'];
    this['birthDate'] = data['birthDate'];
  }
}

export class GuestModel {
  /** Title of the guest */

  title: EnumGuestModelTitle;

  /** Gender of the booker */

  gender: EnumGuestModelGender;

  /** First name of the guest */

  firstName: string;

  /** Middle initial of the guest */

  middleInitial: string;

  /** Last name of the guest */

  lastName: string;

  /** Email address of the guest */

  email: string;

  /** Phone number of the guest */

  phone: string;

  /**  */

  address: PersonAddressModel;

  /** The guest's nationality, in ISO 3166-1 alpha-2 code */

  nationalityCountryCode: string;

  /** The guest's identification number for the given identificationType. */

  identificationNumber: string;

  /** The type of the identificationNumber */

  identificationType: EnumGuestModelIdentificationType;

  /**  */

  company: PersonCompanyModel;

  /** Two-letter code (ISO Alpha-2) of a language preferred for contact */

  preferredLanguage: string;

  /** Guest's birthdate */

  birthDate: Date;

  constructor(data: undefined | any = {}) {
    this['title'] = data['title'];
    this['gender'] = data['gender'];
    this['firstName'] = data['firstName'];
    this['middleInitial'] = data['middleInitial'];
    this['lastName'] = data['lastName'];
    this['email'] = data['email'];
    this['phone'] = data['phone'];
    this['address'] = data['address'];
    this['nationalityCountryCode'] = data['nationalityCountryCode'];
    this['identificationNumber'] = data['identificationNumber'];
    this['identificationType'] = data['identificationType'];
    this['company'] = data['company'];
    this['preferredLanguage'] = data['preferredLanguage'];
    this['birthDate'] = data['birthDate'];
  }
}

export class CreateReservationTimeSliceModel {
  /** The rate plan id for this time slice */

  ratePlanId: string;

  /**  */

  totalAmount: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['ratePlanId'] = data['ratePlanId'];
    this['totalAmount'] = data['totalAmount'];
  }
}


export class BookReservationServiceModel {
  /** The id of the service you want to book */

  serviceId: string;

  /** The number of services to book for each service date. It defaults to the service offer count when not specified. */

  count: number;

  /**  */

  amount: MonetaryValueModel;

  /** The optional dates you want to book the service for; if not specified the default service pattern will be used (e.g. whole stay). */

  dates: Date[];

  constructor(data: undefined | any = {}) {
    this['serviceId'] = data['serviceId'];
    this['count'] = data['count'];
    this['amount'] = data['amount'];
    this['dates'] = data['dates'];
  }
}

export class CommissionModel {
  /**  */

  commissionAmount: MonetaryValueModel;

  /**  */

  beforeCommissionAmount: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['commissionAmount'] = data['commissionAmount'];
    this['beforeCommissionAmount'] = data['beforeCommissionAmount'];
  }
}

export class CreateReservationModel {
  /** Date and optional time of arrival<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: string;

  /** Date and optional time of departure. Cannot be more than 5 years after arrival.<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: string;

  /** Number of adults */

  adults: number;

  /** Ages of the children */

  childrenAges: number[];

  /** Additional information and comments */

  comment: string;

  /** Additional information and comments by the guest */

  guestComment: string;

  /** Code in some system */

  externalCode: string;

  /** Channel code */

  channelCode: EnumCreateReservationModelChannelCode;

  /** Source of the reservation */

  source: string;

  /**  */

  primaryGuest: GuestModel;

  /** Additional guests of the reservation. */

  additionalGuests: GuestModel[];

  /** The guarantee that has to be applied for this reservation. It has to be the same or stronger than
the minimum guarantee required by the selected rate plan */

  guaranteeType: EnumCreateReservationModelGuaranteeType;

  /** Purpose of the trip, leisure or business */

  travelPurpose: EnumCreateReservationModelTravelPurpose;

  /** Gross prices including services and taxes for each time slice. They will be applied to the reservation timeslices
in the order specified from arrival to departure */

  timeSlices: CreateReservationTimeSliceModel[];

  /** Additional services (extras, add-ons) that should be added to the reservation */

  services: BookReservationServiceModel[];

  /** Set this if this reservation belongs to a company */

  companyId: string;

  /** Corporate code provided during creation. Used to find offers during amend. */

  corporateCode: string;

  /**  */

  prePaymentAmount: MonetaryValueModel;

  /**  */

  commission: CommissionModel;

  /** The promo code associated with a certain special offer */

  promoCode: string;

  constructor(data: undefined | any = {}) {
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['comment'] = data['comment'];
    this['guestComment'] = data['guestComment'];
    this['externalCode'] = data['externalCode'];
    this['channelCode'] = data['channelCode'];
    this['source'] = data['source'];
    this['primaryGuest'] = data['primaryGuest'];
    this['additionalGuests'] = data['additionalGuests'];
    this['guaranteeType'] = data['guaranteeType'];
    this['travelPurpose'] = data['travelPurpose'];
    this['timeSlices'] = data['timeSlices'];
    this['services'] = data['services'];
    this['companyId'] = data['companyId'];
    this['corporateCode'] = data['corporateCode'];
    this['prePaymentAmount'] = data['prePaymentAmount'];
    this['commission'] = data['commission'];
    this['promoCode'] = data['promoCode'];
  }
}

export class CreateBookingModel {
  /**  */

  paymentAccount: CreatePaymentAccountModel;

  /**  */

  booker: BookerModel;

  /** Additional information and comments */

  comment: string;

  /** Additional information and comments by the booker */

  bookerComment: string;

  /** List of reservations to create */

  reservations: CreateReservationModel[];

  constructor(data: undefined | any = {}) {
    this['paymentAccount'] = data['paymentAccount'];
    this['booker'] = data['booker'];
    this['comment'] = data['comment'];
    this['bookerComment'] = data['bookerComment'];
    this['reservations'] = data['reservations'];
  }
}

export class ReservationCreatedModel {
  /** Id of the reservation */

  id: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
  }
}

export class BookingCreatedModel {
  /** Booking id */

  id: string;

  /** List of ids for newly created reservations */

  reservationIds: ReservationCreatedModel[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['reservationIds'] = data['reservationIds'];
  }
}

export class PaymentAccountModel {
  /** The account number (e.g. masked credit card number or last 4 digits) */

  accountNumber: string;

  /** The account holder (e.g. card holder) */

  accountHolder: string;

  /** The credit card's expiration month */

  expiryMonth: string;

  /** The credit card's expiration year */

  expiryYear: string;

  /** The payment method (e.g. visa) */

  paymentMethod: string;

  /** The email address of the shopper / customer */

  payerEmail: string;

  /** The payer reference. It is used to make recurring captures and its usage is allowed only in the scope of the booking.
For the reason above this is a write-only field. */

  payerReference: string;

  /** Indicates if the payment account is a virtual credit card. If not specified it defaults to 'false' */

  isVirtual: boolean;

  /** Indicates if the payment account can be used for capturing payments. A payment account is active, when it has a valid payer reference set */

  isActive: boolean;

  constructor(data: undefined | any = {}) {
    this['accountNumber'] = data['accountNumber'];
    this['accountHolder'] = data['accountHolder'];
    this['expiryMonth'] = data['expiryMonth'];
    this['expiryYear'] = data['expiryYear'];
    this['paymentMethod'] = data['paymentMethod'];
    this['payerEmail'] = data['payerEmail'];
    this['payerReference'] = data['payerReference'];
    this['isVirtual'] = data['isVirtual'];
    this['isActive'] = data['isActive'];
  }
}

export class ServiceModel {
  /** The service id */

  id: string;

  /** The code for the service */

  code: string;

  /** The name for the service */

  name: string;

  /** The description for the service */

  description: string;

  /** Defines the granularity (room, person) for which this item is offered and priced */

  pricingUnit: EnumServiceModelPricingUnit;

  /**  */

  defaultGrossPrice: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['pricingUnit'] = data['pricingUnit'];
    this['defaultGrossPrice'] = data['defaultGrossPrice'];
  }
}

export class ServiceDateItemModel {
  /** The date this service is delivered */

  serviceDate: Date;

  /** The count of booked services */

  count: number;

  /**  */

  amount: AmountModel;

  /** Rate plans can have additional services. When booking an offer for such rate plans, those services are automatically booked.
They are marked as mandatory and they cannot be removed. */

  isMandatory: boolean;

  constructor(data: undefined | any = {}) {
    this['serviceDate'] = data['serviceDate'];
    this['count'] = data['count'];
    this['amount'] = data['amount'];
    this['isMandatory'] = data['isMandatory'];
  }
}

export class ReservationServiceItemModel {
  /**  */

  service: ServiceModel;

  /**  */

  totalAmount: AmountModel;

  /** The dates the service will be delivered with its price */

  dates: ServiceDateItemModel[];

  constructor(data: undefined | any = {}) {
    this['service'] = data['service'];
    this['totalAmount'] = data['totalAmount'];
    this['dates'] = data['dates'];
  }
}

export class ReservationCancellationFeeModel {
  /** The id of the cancellation policy applied */

  id: string;

  /** The code of the cancellation policy applied */

  code: string;

  /** The name of the cancellation policy applied */

  name: string;

  /** The description of the cancellation policy applied */

  description: string;

  /** The date and time the cancellation fee will be due. After that time this fee will
be charged in case of cancellation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  dueDateTime: Date;

  /**  */

  fee: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['dueDateTime'] = data['dueDateTime'];
    this['fee'] = data['fee'];
  }
}

export class EmbeddedCompanyModel {
  /** The company ID */

  id: string;

  /** The code of the company */

  code: string;

  /** The name of the company */

  name: string;

  /** Whether or not the company can check out on AR */

  canCheckOutOnAr: boolean;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['canCheckOutOnAr'] = data['canCheckOutOnAr'];
  }
}

export class BookingReservationModel {
  /** Reservation id */

  id: string;

  /** Status of the reservation */

  status: EnumBookingReservationModelStatus;

  /** Code in external system */

  externalCode: string;

  /** Channel code */

  channelCode: EnumBookingReservationModelChannelCode;

  /** Source of the reservation (e.g Hotels.com, Orbitz, etc.) */

  source: string;

  /**  */

  paymentAccount: PaymentAccountModel;

  /** Date of arrival<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: Date;

  /** Date of departure<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: Date;

  /** Number of adults */

  adults: number;

  /** The ages of the children */

  childrenAges: number[];

  /**  */

  totalGrossAmount: MonetaryValueModel;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  ratePlan: EmbeddedRatePlanModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /** The list of additional services (extras, add-ons) reserved for the stay */

  services: ReservationServiceItemModel[];

  /** Additional information and comment by the guest */

  guestComment: string;

  /**  */

  cancellationFee: ReservationCancellationFeeModel;

  /**  */

  company: EmbeddedCompanyModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['status'] = data['status'];
    this['externalCode'] = data['externalCode'];
    this['channelCode'] = data['channelCode'];
    this['source'] = data['source'];
    this['paymentAccount'] = data['paymentAccount'];
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['property'] = data['property'];
    this['ratePlan'] = data['ratePlan'];
    this['unitGroup'] = data['unitGroup'];
    this['services'] = data['services'];
    this['guestComment'] = data['guestComment'];
    this['cancellationFee'] = data['cancellationFee'];
    this['company'] = data['company'];
  }
}

export class PropertyValueModel {
  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  totalGrossAmount: MonetaryValueModel;

  /**  */

  balance: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['property'] = data['property'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['balance'] = data['balance'];
  }
}

export class BookingModel {
  /** Booking id */

  id: string;

  /** Group id */

  groupId: string;

  /**  */

  booker: BookerModel;

  /**  */

  paymentAccount: PaymentAccountModel;

  /** Additional information and comments */

  comment: string;

  /** Additional information and comment by the booker */

  bookerComment: string;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Date of last modification<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  modified: Date;

  /** Property specific values like total amount and balance */

  propertyValues: PropertyValueModel[];

  /** Reservations within this booking */

  reservations: BookingReservationModel[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['groupId'] = data['groupId'];
    this['booker'] = data['booker'];
    this['paymentAccount'] = data['paymentAccount'];
    this['comment'] = data['comment'];
    this['bookerComment'] = data['bookerComment'];
    this['created'] = data['created'];
    this['modified'] = data['modified'];
    this['propertyValues'] = data['propertyValues'];
    this['reservations'] = data['reservations'];
  }
}

export class EmbeddedServiceModel {
  /** The service id */

  id: string;

  /** The code for the service */

  code: string;

  /** The name for the service */

  name: string;

  /** The description for the service */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class TaxDetailModel {
  /**  */

  vatType: EnumTaxDetailModelVatType;

  /**  */

  vatPercent: number;

  /**  */

  net: MonetaryValueModel;

  /**  */

  tax: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['vatType'] = data['vatType'];
    this['vatPercent'] = data['vatPercent'];
    this['net'] = data['net'];
    this['tax'] = data['tax'];
  }
}

export class EmbeddedUnitModel {
  /** The unit id */

  id: string;

  /** The name for the unit */

  name: string;

  /** The description for the unit */

  description: string;

  /** The unit group id */

  unitGroupId: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['unitGroupId'] = data['unitGroupId'];
  }
}

export class ReservationNoShowFeeModel {
  /** The id of the no-show policy applied */

  id: string;

  /** The code of the no-show policy applied */

  code: string;

  /**  */

  fee: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['fee'] = data['fee'];
  }
}

export class ReservationAssignedUnitTimeRangeModel {
  /** The start date and time of the period for which the unit is assigned to the reservation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** The end date and time of the period for which the unit is assigned to the reservation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  constructor(data: undefined | any = {}) {
    this['from'] = data['from'];
    this['to'] = data['to'];
  }
}

export class ReservationAssignedUnitModel {
  /**  */

  unit: EmbeddedUnitModel;

  /** The time ranges for which the unit is assigned to the reservation */

  timeRanges: ReservationAssignedUnitTimeRangeModel[];

  constructor(data: undefined | any = {}) {
    this['unit'] = data['unit'];
    this['timeRanges'] = data['timeRanges'];
  }
}

export class ReservationServiceModel {
  /**  */

  service: EmbeddedServiceModel;

  /** The date this service is delivered */

  serviceDate: Date;

  /** The count of booked services */

  count: number;

  /**  */

  amount: AmountModel;

  /** Whether this service is already booked as extra */

  bookedAsExtra: boolean;

  constructor(data: undefined | any = {}) {
    this['service'] = data['service'];
    this['serviceDate'] = data['serviceDate'];
    this['count'] = data['count'];
    this['amount'] = data['amount'];
    this['bookedAsExtra'] = data['bookedAsExtra'];
  }
}

export class ActionReasonModel_NotAllowedReservationTimeSliceActionReason {
  /**  */

  code: EnumActionReasonModel_NotAllowedReservationTimeSliceActionReasonCode;

  /**  */

  message: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['message'] = data['message'];
  }
}

export class ActionModel_ReservationTimeSliceAction_NotAllowedReservationTimeSliceActionReason {
  /**  */

  action: EnumActionModel_ReservationTimeSliceAction_NotAllowedReservationTimeSliceActionReasonAction;

  /**  */

  isAllowed: boolean;

  /**  */

  reasons: ActionReasonModel_NotAllowedReservationTimeSliceActionReason[];

  constructor(data: undefined | any = {}) {
    this['action'] = data['action'];
    this['isAllowed'] = data['isAllowed'];
    this['reasons'] = data['reasons'];
  }
}

export class TimeSliceModel {
  /** The start date and time for this time slice<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** The end date and time for this time slice<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  /** The service date for this time slice */

  serviceDate: Date;

  /**  */

  ratePlan: EmbeddedRatePlanModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  unit: EmbeddedUnitModel;

  /**  */

  baseAmount: AmountModel;

  /**  */

  totalGrossAmount: MonetaryValueModel;

  /** The list of services included in the rate plan (package elements) */

  includedServices: ReservationServiceModel[];

  /** The list of actions allowed for this time slice */

  actions: ActionModel_ReservationTimeSliceAction_NotAllowedReservationTimeSliceActionReason[];

  constructor(data: undefined | any = {}) {
    this['from'] = data['from'];
    this['to'] = data['to'];
    this['serviceDate'] = data['serviceDate'];
    this['ratePlan'] = data['ratePlan'];
    this['unitGroup'] = data['unitGroup'];
    this['unit'] = data['unit'];
    this['baseAmount'] = data['baseAmount'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['includedServices'] = data['includedServices'];
    this['actions'] = data['actions'];
  }
}

export class ReservationValidationMessageModel {
  /** The message category */

  category: EnumReservationValidationMessageModelCategory;

  /** The message Code */

  code: EnumReservationValidationMessageModelCode;

  /** The message description */

  message: string;

  constructor(data: undefined | any = {}) {
    this['category'] = data['category'];
    this['code'] = data['code'];
    this['message'] = data['message'];
  }
}

export class ActionReasonModel_NotAllowedReservationActionReason {
  /**  */

  code: EnumActionReasonModel_NotAllowedReservationActionReasonCode;

  /**  */

  message: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['message'] = data['message'];
  }
}

export class ActionModel_ReservationAction_NotAllowedReservationActionReason {
  /**  */

  action: EnumActionModel_ReservationAction_NotAllowedReservationActionReasonAction;

  /**  */

  isAllowed: boolean;

  /**  */

  reasons: ActionReasonModel_NotAllowedReservationActionReason[];

  constructor(data: undefined | any = {}) {
    this['action'] = data['action'];
    this['isAllowed'] = data['isAllowed'];
    this['reasons'] = data['reasons'];
  }
}

export class ReservationItemModel {
  /** Reservation id */

  id: string;

  /** Booking id */

  bookingId: string;

  /** Block id */

  blockId: string;

  /** Status of the reservation */

  status: EnumReservationItemModelStatus;

  /** Time of check-in<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkInTime: Date;

  /** Time of check-out<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkOutTime: Date;

  /** Time of cancellation, if the reservation was canceled<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  cancellationTime: Date;

  /** Time of setting no-show reservation status<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  noShowTime: Date;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  ratePlan: EmbeddedRatePlanModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  unit: EmbeddedUnitModel;

  /**  */

  totalGrossAmount: MonetaryValueModel;

  /** Date of arrival<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: Date;

  /** Date of departure<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: Date;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Date of last modification<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  modified: Date;

  /** Number of adults */

  adults: number;

  /** The ages of the children */

  childrenAges: number[];

  /** Additional information and comments */

  comment: string;

  /** Additional information and comment by the guest */

  guestComment: string;

  /** Code in external system */

  externalCode: string;

  /** Channel code */

  channelCode: EnumReservationItemModelChannelCode;

  /** Source of the reservation (e.g Hotels.com, Orbitz, etc.) */

  source: string;

  /**  */

  primaryGuest: GuestModel;

  /** Additional guests of the reservation. */

  additionalGuests: GuestModel[];

  /**  */

  booker: BookerModel;

  /**  */

  paymentAccount: PaymentAccountModel;

  /** The strongest guarantee for the rate plans booked in this reservation */

  guaranteeType: EnumReservationItemModelGuaranteeType;

  /**  */

  cancellationFee: ReservationCancellationFeeModel;

  /**  */

  noShowFee: ReservationNoShowFeeModel;

  /** The purpose of the trip, leisure or business */

  travelPurpose: EnumReservationItemModelTravelPurpose;

  /**  */

  balance: MonetaryValueModel;

  /** The list of units assigned to this reservation */

  assignedUnits: ReservationAssignedUnitModel[];

  /** The list of time slices with the reserved units / unit groups for the stay */

  timeSlices: TimeSliceModel[];

  /** The list of additional services (extras, add-ons) reserved for the stay */

  services: ReservationServiceItemModel[];

  /** Validation rules are applied to reservations during their lifetime.
For example a reservation that was created while the house or unit group is already fully booked.
Whenever a rule was or is currently violated, a validation message will be added to this list.
They can be deleted whenever the hotel staff worked them off. */

  validationMessages: ReservationValidationMessageModel[];

  /** The list of actions for this reservation */

  actions: ActionModel_ReservationAction_NotAllowedReservationActionReason[];

  /**  */

  company: EmbeddedCompanyModel;

  /** Corporate code provided during creation. Used to find offers during amend. */

  corporateCode: string;

  /** Whether all folios of a reservation have an invoice */

  allFoliosHaveInvoice: boolean;

  /** Whether the city tax has already been added to the reservation. Set to false, if the property does not have city tax configured */

  hasCityTax: boolean;

  /**  */

  commission: CommissionModel;

  /** The promo code associated with a certain special offer used to create the reservation */

  promoCode: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['bookingId'] = data['bookingId'];
    this['blockId'] = data['blockId'];
    this['status'] = data['status'];
    this['checkInTime'] = data['checkInTime'];
    this['checkOutTime'] = data['checkOutTime'];
    this['cancellationTime'] = data['cancellationTime'];
    this['noShowTime'] = data['noShowTime'];
    this['property'] = data['property'];
    this['ratePlan'] = data['ratePlan'];
    this['unitGroup'] = data['unitGroup'];
    this['unit'] = data['unit'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['created'] = data['created'];
    this['modified'] = data['modified'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['comment'] = data['comment'];
    this['guestComment'] = data['guestComment'];
    this['externalCode'] = data['externalCode'];
    this['channelCode'] = data['channelCode'];
    this['source'] = data['source'];
    this['primaryGuest'] = data['primaryGuest'];
    this['additionalGuests'] = data['additionalGuests'];
    this['booker'] = data['booker'];
    this['paymentAccount'] = data['paymentAccount'];
    this['guaranteeType'] = data['guaranteeType'];
    this['cancellationFee'] = data['cancellationFee'];
    this['noShowFee'] = data['noShowFee'];
    this['travelPurpose'] = data['travelPurpose'];
    this['balance'] = data['balance'];
    this['assignedUnits'] = data['assignedUnits'];
    this['timeSlices'] = data['timeSlices'];
    this['services'] = data['services'];
    this['validationMessages'] = data['validationMessages'];
    this['actions'] = data['actions'];
    this['company'] = data['company'];
    this['corporateCode'] = data['corporateCode'];
    this['allFoliosHaveInvoice'] = data['allFoliosHaveInvoice'];
    this['hasCityTax'] = data['hasCityTax'];
    this['commission'] = data['commission'];
    this['promoCode'] = data['promoCode'];
  }
}

export class ReservationListModel {
  /** List of reservations */

  reservations: ReservationItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['reservations'] = data['reservations'];
    this['count'] = data['count'];
  }
}

export class PayableAmountModel {
  /**  */

  guest: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['guest'] = data['guest'];
  }
}

export class ReservationModel {
  /** Reservation id */

  id: string;

  /** Booking id */

  bookingId: string;

  /** Block id */

  blockId: string;

  /** Status of the reservation */

  status: EnumReservationModelStatus;

  /** Time of check-in<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkInTime: Date;

  /** Time of check-out<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkOutTime: Date;

  /** Time of cancellation, if the reservation was canceled<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  cancellationTime: Date;

  /** Time of setting no-show reservation status<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  noShowTime: Date;

  /**  */

  unit: EmbeddedUnitModel;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  ratePlan: EmbeddedRatePlanModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  totalGrossAmount: MonetaryValueModel;

  /** Date of arrival<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: Date;

  /** Date of departure<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: Date;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Date of last modification<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  modified: Date;

  /** Number of adults */

  adults: number;

  /** The ages of the children */

  childrenAges: number[];

  /** Additional information and comments */

  comment: string;

  /** Additional information and comment by the guest */

  guestComment: string;

  /** Code in external system */

  externalCode: string;

  /** Channel code */

  channelCode: EnumReservationModelChannelCode;

  /** Source of the reservation (e.g Hotels.com, Orbitz, etc.) */

  source: string;

  /**  */

  primaryGuest: GuestModel;

  /** Additional guests of the reservation. */

  additionalGuests: GuestModel[];

  /**  */

  booker: BookerModel;

  /**  */

  paymentAccount: PaymentAccountModel;

  /** The list of time slices with the reserved units / unit groups for the stay */

  timeSlices: TimeSliceModel[];

  /** The list of additional services (extras, add-ons) reserved for the stay */

  services: ReservationServiceItemModel[];

  /** The strongest guarantee for the rate plans booked in this reservation */

  guaranteeType: EnumReservationModelGuaranteeType;

  /**  */

  cancellationFee: ReservationCancellationFeeModel;

  /**  */

  noShowFee: ReservationNoShowFeeModel;

  /** The purpose of the trip, leisure or business */

  travelPurpose: EnumReservationModelTravelPurpose;

  /**  */

  balance: MonetaryValueModel;

  /** Validation rules are applied to reservations during their lifetime.
For example a reservation that was created while the house or unit group is already fully booked.
Whenever a rule was or is currently violated, a validation message will be added to this list.
They can be deleted whenever the hotel staff worked them off. */

  validationMessages: ReservationValidationMessageModel[];

  /** The list of actions for this reservation */

  actions: ActionModel_ReservationAction_NotAllowedReservationActionReason[];

  /**  */

  company: EmbeddedCompanyModel;

  /** Corporate code provided during creation. Used to find offers during amend. */

  corporateCode: string;

  /** Whether all folios of a reservation have an invoice */

  allFoliosHaveInvoice: boolean;

  /** Tax breakdown, displaying net and tax amount for each VAT type */

  taxDetails: TaxDetailModel[];

  /** Whether the city tax has already been added to the reservation. Set to false, if the property does not have city tax configured */

  hasCityTax: boolean;

  /**  */

  commission: CommissionModel;

  /** The promo code associated with a certain special offer used to create the reservation */

  promoCode: string;

  /**  */

  payableAmount: PayableAmountModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['bookingId'] = data['bookingId'];
    this['blockId'] = data['blockId'];
    this['status'] = data['status'];
    this['checkInTime'] = data['checkInTime'];
    this['checkOutTime'] = data['checkOutTime'];
    this['cancellationTime'] = data['cancellationTime'];
    this['noShowTime'] = data['noShowTime'];
    this['unit'] = data['unit'];
    this['property'] = data['property'];
    this['ratePlan'] = data['ratePlan'];
    this['unitGroup'] = data['unitGroup'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['created'] = data['created'];
    this['modified'] = data['modified'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['comment'] = data['comment'];
    this['guestComment'] = data['guestComment'];
    this['externalCode'] = data['externalCode'];
    this['channelCode'] = data['channelCode'];
    this['source'] = data['source'];
    this['primaryGuest'] = data['primaryGuest'];
    this['additionalGuests'] = data['additionalGuests'];
    this['booker'] = data['booker'];
    this['paymentAccount'] = data['paymentAccount'];
    this['timeSlices'] = data['timeSlices'];
    this['services'] = data['services'];
    this['guaranteeType'] = data['guaranteeType'];
    this['cancellationFee'] = data['cancellationFee'];
    this['noShowFee'] = data['noShowFee'];
    this['travelPurpose'] = data['travelPurpose'];
    this['balance'] = data['balance'];
    this['validationMessages'] = data['validationMessages'];
    this['actions'] = data['actions'];
    this['company'] = data['company'];
    this['corporateCode'] = data['corporateCode'];
    this['allFoliosHaveInvoice'] = data['allFoliosHaveInvoice'];
    this['taxDetails'] = data['taxDetails'];
    this['hasCityTax'] = data['hasCityTax'];
    this['commission'] = data['commission'];
    this['promoCode'] = data['promoCode'];
    this['payableAmount'] = data['payableAmount'];
  }
}

export class AutoAssignedUnitItemModel {
  /**  */

  unit: EmbeddedUnitModel;

  /** The start date and time for this time slice<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** The end date and time for this time slice<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  constructor(data: undefined | any = {}) {
    this['unit'] = data['unit'];
    this['from'] = data['from'];
    this['to'] = data['to'];
  }
}

export class AutoAssignedUnitListModel {
  /** The list of time slices with the respective assigned unit */

  timeSlices: AutoAssignedUnitItemModel[];

  constructor(data: undefined | any = {}) {
    this['timeSlices'] = data['timeSlices'];
  }
}

export class AssignedUnitModel {
  /**  */

  unit: EmbeddedUnitModel;

  constructor(data: undefined | any = {}) {
    this['unit'] = data['unit'];
  }
}

export class DesiredTimeSliceModel {
  /** The rate plan id for this time slice */

  ratePlanId: string;

  /**  */

  totalGrossAmount: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['ratePlanId'] = data['ratePlanId'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
  }
}

export class DesiredStayDetailsModel {
  /** Date and optional time of arrival<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: string;

  /** Date and optional time of departure. Cannot be more than 5 years after arrival.<br />Specify either a pure date or a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: string;

  /** Number of adults */

  adults: number;

  /** Ages of the children */

  childrenAges: number[];

  /** Whether the prices for time slices with no change to the rate plan should be re-quoted based on current prices, or if
only additions like change of number of adults should be calculated. Defaults to 'false'. */

  requote: boolean;

  /** The list of time slices */

  timeSlices: DesiredTimeSliceModel[];

  constructor(data: undefined | any = {}) {
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['requote'] = data['requote'];
    this['timeSlices'] = data['timeSlices'];
  }
}
export enum EnumAvailableUnitItemStatusModelCondition {
  'Clean' = 'Clean',
  'CleanToBeInspected' = 'CleanToBeInspected',
  'Dirty' = 'Dirty',
}
export enum EnumAvailableUnitItemStatusModelMaintenanceType {
  'OutOfService' = 'OutOfService',
  'OutOfOrder' = 'OutOfOrder',
  'OutOfInventory' = 'OutOfInventory',
}
export enum EnumAmountModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special',
  'ReducedCovid19' = 'ReducedCovid19',
  'NormalCovid19' = 'NormalCovid19',
}
export enum EnumBookerModelTitle {
  'Mr' = 'Mr',
  'Ms' = 'Ms',
  'Dr' = 'Dr',
  'Prof' = 'Prof',
  'Other' = 'Other',
}
export enum EnumBookerModelGender {
  'Female' = 'Female',
  'Male' = 'Male',
  'Other' = 'Other',
}
export enum EnumBookerModelIdentificationType {
  'SocialInsuranceNumber' = 'SocialInsuranceNumber',
  'PassportNumber' = 'PassportNumber',
  'IdNumber' = 'IdNumber',
  'DriverLicenseNumber' = 'DriverLicenseNumber',
}
export enum EnumGuestModelTitle {
  'Mr' = 'Mr',
  'Ms' = 'Ms',
  'Dr' = 'Dr',
  'Prof' = 'Prof',
  'Other' = 'Other',
}
export enum EnumGuestModelGender {
  'Female' = 'Female',
  'Male' = 'Male',
  'Other' = 'Other',
}
export enum EnumGuestModelIdentificationType {
  'SocialInsuranceNumber' = 'SocialInsuranceNumber',
  'PassportNumber' = 'PassportNumber',
  'IdNumber' = 'IdNumber',
  'DriverLicenseNumber' = 'DriverLicenseNumber',
}
export enum EnumCreateReservationModelChannelCode {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia',
  'Homelike' = 'Homelike',
}
export enum EnumCreateReservationModelGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company',
}
export enum EnumCreateReservationModelTravelPurpose {
  'Business' = 'Business',
  'Leisure' = 'Leisure',
}
export enum EnumServiceModelPricingUnit {
  'Room' = 'Room',
  'Person' = 'Person',
}
export enum EnumBookingReservationModelStatus {
  'Confirmed' = 'Confirmed',
  'InHouse' = 'InHouse',
  'CheckedOut' = 'CheckedOut',
  'Canceled' = 'Canceled',
  'NoShow' = 'NoShow',
}
export enum EnumBookingReservationModelChannelCode {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia',
  'Homelike' = 'Homelike',
}
export enum EnumTaxDetailModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special',
  'ReducedCovid19' = 'ReducedCovid19',
  'NormalCovid19' = 'NormalCovid19',
}
export enum EnumActionReasonModel_NotAllowedReservationTimeSliceActionReasonCode {
  'AmendNotAllowedWhenTimeSliceIsInThePast' = 'AmendNotAllowedWhenTimeSliceIsInThePast',
  'AmendNotAllowedWhenTimeSliceIsAlreadyPosted' = 'AmendNotAllowedWhenTimeSliceIsAlreadyPosted',
  'AmendNotAllowedForReservationInFinalStatus' = 'AmendNotAllowedForReservationInFinalStatus',
}
export enum EnumActionModel_ReservationTimeSliceAction_NotAllowedReservationTimeSliceActionReasonAction {
  'Amend' = 'Amend',
}
export enum EnumReservationValidationMessageModelCategory {
  'OfferNotAvailable' = 'OfferNotAvailable',
  'AutoUnitAssignment' = 'AutoUnitAssignment',
}
export enum EnumReservationValidationMessageModelCode {
  'UnitGroupFullyBooked' = 'UnitGroupFullyBooked',
  'UnitGroupCapacityExceeded' = 'UnitGroupCapacityExceeded',
  'RatePlanRestrictionsViolated' = 'RatePlanRestrictionsViolated',
  'RatePlanSurchargesNotSet' = 'RatePlanSurchargesNotSet',
  'RateRestrictionsViolated' = 'RateRestrictionsViolated',
  'RatePlanChannelNotSet' = 'RatePlanChannelNotSet',
  'RatesNotSet' = 'RatesNotSet',
  'BlockFullyBooked' = 'BlockFullyBooked',
  'UnitMoved' = 'UnitMoved',
  'IncludedServicesAmountExceededRateAmount' = 'IncludedServicesAmountExceededRateAmount',
  'RatePlanCompanyRestrictionsViolated' = 'RatePlanCompanyRestrictionsViolated',
}
export enum EnumActionReasonModel_NotAllowedReservationActionReasonCode {
  'CheckInNotAllowedForReservationNotInStatusConfirmed' = 'CheckInNotAllowedForReservationNotInStatusConfirmed',
  'CheckInNotAllowedBeforeArrivalDate' = 'CheckInNotAllowedBeforeArrivalDate',
  'CheckInNotAllowedAfterDepartureDateTime' = 'CheckInNotAllowedAfterDepartureDateTime',
  'CheckInNotAllowedWithoutUnitAssignedForWholeStay' = 'CheckInNotAllowedWithoutUnitAssignedForWholeStay',
  'CheckOutNotAllowedForReservationNotInStatusInHouse' = 'CheckOutNotAllowedForReservationNotInStatusInHouse',
  'CheckOutNotAllowedForPastReservationNotInStatusConfirmedOrInHouse' = 'CheckOutNotAllowedForPastReservationNotInStatusConfirmedOrInHouse',
  'CheckOutNotAllowedWithDepartureDateMoreThanOneDayInTheFuture' = 'CheckOutNotAllowedWithDepartureDateMoreThanOneDayInTheFuture',
  'CancelNotAllowedForReservationNotInStatusConfirmed' = 'CancelNotAllowedForReservationNotInStatusConfirmed',
  'AmendNotAllowedForNotAmendableTimeSlices' = 'AmendNotAllowedForNotAmendableTimeSlices',
  'AmendArrivalNotAllowedForNotAmendableTimeSlices' = 'AmendArrivalNotAllowedForNotAmendableTimeSlices',
  'AmendArrivalNotAllowedForReservationNotInStatusConfirmed' = 'AmendArrivalNotAllowedForReservationNotInStatusConfirmed',
  'AmendDepartureNotAllowedForReservationNotInStatusConfirmedOrInHouse' = 'AmendDepartureNotAllowedForReservationNotInStatusConfirmedOrInHouse',
  'AmendDepartureNotAllowedForReservationDepartureDateTooFarInThePast' = 'AmendDepartureNotAllowedForReservationDepartureDateTooFarInThePast',
  'NoShowNotAllowedForReservationNotInStatusConfirmed' = 'NoShowNotAllowedForReservationNotInStatusConfirmed',
  'NoShowNotAllowedBeforeArrivalDate' = 'NoShowNotAllowedBeforeArrivalDate',
  'AssignUnitNotAllowedForReservationInThePast' = 'AssignUnitNotAllowedForReservationInThePast',
  'AssignUnitNotAllowedForReservationNotInStatusConfirmedOrInHouse' = 'AssignUnitNotAllowedForReservationNotInStatusConfirmedOrInHouse',
  'UnassignUnitNotAllowedForReservationInThePast' = 'UnassignUnitNotAllowedForReservationInThePast',
  'UnassignUnitNotAllowedForReservationNotInStatusConfirmed' = 'UnassignUnitNotAllowedForReservationNotInStatusConfirmed',
  'UnassignUnitNotAllowedForReservationWithoutUnit' = 'UnassignUnitNotAllowedForReservationWithoutUnit',
  'RemoveCityTaxNotAllowedForReservationNotInStatusConfirmedOrInHouse' = 'RemoveCityTaxNotAllowedForReservationNotInStatusConfirmedOrInHouse',
  'RemoveCityTaxNotAllowedForReservationWithPostedCharges' = 'RemoveCityTaxNotAllowedForReservationWithPostedCharges',
  'RemoveCityTaxNotAllowedForReservationWithoutCityTax' = 'RemoveCityTaxNotAllowedForReservationWithoutCityTax',
  'AddCityTaxNotAllowedForReservationNotInStatusConfirmedOrInHouse' = 'AddCityTaxNotAllowedForReservationNotInStatusConfirmedOrInHouse',
  'AddCityTaxNotAllowedForReservationWithCityTax' = 'AddCityTaxNotAllowedForReservationWithCityTax',
  'AddCityTaxNotAllowedForReservationForRatePlanNotSubjectToCityTax' = 'AddCityTaxNotAllowedForReservationForRatePlanNotSubjectToCityTax',
  'AddCityTaxNotAllowedForReservationWithPostedCharges' = 'AddCityTaxNotAllowedForReservationWithPostedCharges',
  'RemoveServiceNotAllowedForReservationNotInStatusConfirmedOrInHouse' = 'RemoveServiceNotAllowedForReservationNotInStatusConfirmedOrInHouse',
  'RemoveServiceNotAllowedForReservationInThePast' = 'RemoveServiceNotAllowedForReservationInThePast',
}
export enum EnumActionModel_ReservationAction_NotAllowedReservationActionReasonAction {
  'CheckIn' = 'CheckIn',
  'CheckOut' = 'CheckOut',
  'Cancel' = 'Cancel',
  'AmendTimeSlices' = 'AmendTimeSlices',
  'AmendArrival' = 'AmendArrival',
  'AmendDeparture' = 'AmendDeparture',
  'NoShow' = 'NoShow',
  'AssignUnit' = 'AssignUnit',
  'UnassignUnit' = 'UnassignUnit',
  'RemoveCityTax' = 'RemoveCityTax',
  'AddCityTax' = 'AddCityTax',
  'RemoveService' = 'RemoveService',
}
export enum EnumReservationItemModelStatus {
  'Confirmed' = 'Confirmed',
  'InHouse' = 'InHouse',
  'CheckedOut' = 'CheckedOut',
  'Canceled' = 'Canceled',
  'NoShow' = 'NoShow',
}
export enum EnumReservationItemModelChannelCode {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia',
  'Homelike' = 'Homelike',
}
export enum EnumReservationItemModelGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company',
  'Ota' = 'Ota',
}
export enum EnumReservationItemModelTravelPurpose {
  'Business' = 'Business',
  'Leisure' = 'Leisure',
}
export enum EnumReservationModelStatus {
  'Confirmed' = 'Confirmed',
  'InHouse' = 'InHouse',
  'CheckedOut' = 'CheckedOut',
  'Canceled' = 'Canceled',
  'NoShow' = 'NoShow',
}
export enum EnumReservationModelChannelCode {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia',
  'Homelike' = 'Homelike',
}
export enum EnumReservationModelGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company',
  'Ota' = 'Ota',
}
export enum EnumReservationModelTravelPurpose {
  'Business' = 'Business',
  'Leisure' = 'Leisure',
}
