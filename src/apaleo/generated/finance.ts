/** Generate by swagger-axios-codegen */
// tslint:disable
/* eslint-disable */
import axiosStatic, { AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export class FolioService {
  /**
   * Returns a list of all folios.
   */
  static financeFoliosGet(
    params: {
      /** Filter folio list by property IDs */
      propertyIds?: string[];
      /** Filter folio list by company IDs */
      companyIds?: string[];
      /** Filter folio list by reservation IDs */
      reservationIds?: string[];
      /** Filter folio list by booking IDs */
      bookingIds?: string[];
      /** If set to {true}, only return empty folios (no unmoved [transitory] charges, no unmoved payments, no allowances).
If set to {false}, only return non-empty folios */
      isEmpty?: boolean;
      /** If set to {true}, only return folios that have been checked out on accounts receivables
Otherwise, returns all. */
      checkedOutOnAccountsReceivable?: boolean;
      /** If set to {true}, closed folios are filtered out from the result collection */
      excludeClosed?: boolean;
      /** If set to {true}, only return folios that been invoices */
      hasInvoices?: boolean;
      /** The inclusive start time of the date of creation. Mostly useful for external folios<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      createdFrom?: string;
      /** The exclusive end time of the date of creation. Mostly useful for external folios<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */
      createdTo?: string;
      /** If set to {true}, only main folios are returned, otherwise all. */
      onlyMain?: boolean;
      /** The type of the folio */
      type?: string;
      /** Allows filtering external folios by code.
Useful when you use external folios with custom codes.
Specifying this parameter will ignore the <b>Type</b> parameter and treat as if it would be set to "External" instead. */
      externalFolioCode?: string;
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: charges, allowedActions, company, warnings. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FolioListModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        propertyIds: params['propertyIds'],
        companyIds: params['companyIds'],
        reservationIds: params['reservationIds'],
        bookingIds: params['bookingIds'],
        isEmpty: params['isEmpty'],
        checkedOutOnAccountsReceivable: params['checkedOutOnAccountsReceivable'],
        excludeClosed: params['excludeClosed'],
        hasInvoices: params['hasInvoices'],
        createdFrom: params['createdFrom'],
        createdTo: params['createdTo'],
        onlyMain: params['onlyMain'],
        type: params['type'],
        externalFolioCode: params['externalFolioCode'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Returns one folio.
   */
  static financeFoliosByIdGet(
    params: {
      /** The ID of the folio. */
      id: string;
      /** List of all embedded resources that should be expanded in the response. Possible values are: folios. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FolioModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allows to modify certain properties of a folio
   */
  static financeFoliosByIdPatch(
    params: {
      /** The folio ID. */
      id: string;
      /** Define the list of operations to be applied to the resource. Learn more about JSON Patch here: http://jsonpatch.com/.
            See the FolioDebitorModel in GET for values that can be changed. */
      body: Operation[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class InvoiceService {
  /**
   * Gets the list of all invoices (the metadata only, not the files).
   */
  static financeInvoicesGet(
    params: {
      /** The invoice number */
      number?: string;
      /** If set to {true}, only return invoices with an open balance (AR)
Otherwise, returns all. */
      checkedOutOnAccountsReceivable?: boolean;
      /** Filter for the outstanding balance for invoices<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      outstandingPaymentFilter?: string[];
      /** Filter by invoice date<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      dateFilter?: string[];
      /** The ID of the property the invoices were created in */
      propertyId?: string;
      /** The ID of the reservation the invoices were created for */
      reservationId?: string;
      /** The ID of the folio the invoices were created from */
      folioId?: string;
      /** Find invoices for a recipient name or company. Provide at least three characters. */
      nameSearch?: string;
      /** If set to {true}, returns only invoices having no outstanding payments or marked as settled.
If set to {false}, returns only invoices with outstanding payment and not marked as settled.
If not set, returns all invoices. */
      paymentSettled?: boolean;
      /** Filter by company IDs */
      companyIds?: string[];
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: allowedActions, company. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<InvoiceListModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoices';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        number: params['number'],
        checkedOutOnAccountsReceivable: params['checkedOutOnAccountsReceivable'],
        outstandingPaymentFilter: params['outstandingPaymentFilter'],
        dateFilter: params['dateFilter'],
        propertyId: params['propertyId'],
        reservationId: params['reservationId'],
        folioId: params['folioId'],
        nameSearch: params['nameSearch'],
        paymentSettled: params['paymentSettled'],
        companyIds: params['companyIds'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Creates an invoice for one specific folio.
   */
  static financeInvoicesPost(
    params: {
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The folio ID to create the invoice for. */
      body: CreateInvoicePdfRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<InvoiceCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoices';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Gets an invoice PDF file.
   */
  static financeInvoicesByIdPdfGet(
    params: {
      /** The invoice ID. */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoices/{id}/pdf';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Gets invoice data.
   */
  static financeInvoicesByIdGet(
    params: {
      /** The invoice ID. */
      id: string;
      /** List of all embedded resources that should be expanded in the response. Possible values are: company. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<InvoiceModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoices/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class InvoiceActionService {
  /**
   * Marks an invoice as paid.
   */
  static financeInvoiceActionsByIdPayPut(
    params: {
      /** The invoice ID */
      id: string;
      /** see class */
      body: PayInvoiceRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoice-actions/{id}/pay';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cancels an invoice
   */
  static financeInvoiceActionsByIdCancelPut(
    params: {
      /** The invoice ID */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/invoice-actions/{id}/cancel';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FolioActionsService {
  /**
 * Adds and directly posts a cancellation fee to the folio.
If there are any fees configured for the property, an additional charge for each configured fee will be added.
 */
  static financeFolioActionsByFolioIdCancellationFeePost(
    params: {
      /** The folio ID */
      folioId: string;
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The cancellation fee to be added */
      body: MonetaryValueModel;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AddedChargeModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folio-actions/{folioId}/cancellation-fee';
      url = url.replace('{folioId}', params['folioId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FolioPaymentsService {
  /**
   * Trigger a custom payment for the folio.
   */
  static financeFoliosByFolioIdPaymentsPost(
    params: {
      /**  */
      folioId: string;
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The definition of the payment. */
      body: CreateCustomPaymentRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PaymentCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios/{folioId}/payments';
      url = url.replace('{folioId}', params['folioId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Captures a specific amount from a pre-authorization and posts it to the folio.
   */
  static financeFoliosByFolioIdPaymentsByAuthorizationPost(
    params: {
      /**  */
      folioId: string;
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The definition of the payment. */
      body: CreateAuthorizationPaymentRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PaymentCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios/{folioId}/payments/by-authorization';
      url = url.replace('{folioId}', params['folioId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Trigger a payment using the payment account stored on the reservation.
   */
  static financeFoliosByFolioIdPaymentsByPaymentAccountPost(
    params: {
      /**  */
      folioId: string;
      /** Unique key for safely retrying requests without accidentally performing the same operation twice. 
We'll always send back the same response for requests made with the same key, 
and keys can't be reused with different request parameters. Keys expire after 24 hours. */
      idempotencyKey?: string;
      /** The definition of the payment. */
      body: CreateAccountPaymentRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PaymentCreatedModel> {
    return new Promise((resolve, reject) => {
      let url = '/finance/v1/folios/{folioId}/payments/by-payment-account';
      url = url.replace('{folioId}', params['folioId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class NonStrictAddressModel {
  /**  */

  addressLine1: string;

  /**  */

  addressLine2: string;

  /**  */

  postalCode: string;

  /**  */

  city: string;

  /**  */

  countryCode: string;

  constructor(data: undefined | any = {}) {
    this['addressLine1'] = data['addressLine1'];
    this['addressLine2'] = data['addressLine2'];
    this['postalCode'] = data['postalCode'];
    this['city'] = data['city'];
    this['countryCode'] = data['countryCode'];
  }
}

export class CompanyInfoModel {
  /** Name of the company */

  name: string;

  /** Tax or Vat ID of the company */

  taxId: string;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['taxId'] = data['taxId'];
  }
}

export class FolioDebitorModel {
  /** Title */

  title: EnumFolioDebitorModelTitle;

  /** First name */

  firstName: string;

  /** Last name */

  name: string;

  /**  */

  address: NonStrictAddressModel;

  /**  */

  company: CompanyInfoModel;

  /** Any additional information about the debitor that should be present on the invoice */

  reference: string;

  constructor(data: undefined | any = {}) {
    this['title'] = data['title'];
    this['firstName'] = data['firstName'];
    this['name'] = data['name'];
    this['address'] = data['address'];
    this['company'] = data['company'];
    this['reference'] = data['reference'];
  }
}

export class EmbeddedReservationModel {
  /** Reservation id */

  id: string;

  /** Booking id */

  bookingId: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['bookingId'] = data['bookingId'];
  }
}

export class EmbeddedCompanyModel {
  /** The company ID */

  id: string;

  /** The code of the company */

  code: string;

  /** The name of the company */

  name: string;

  /** Whether or not the company can check out on AR */

  canCheckOutOnAr: boolean;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['canCheckOutOnAr'] = data['canCheckOutOnAr'];
  }
}

export class MonetaryValueModel {
  /**  */

  amount: number;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['amount'] = data['amount'];
    this['currency'] = data['currency'];
  }
}

export class EmbeddedInvoiceModel {
  /** Invoice id */

  id: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
  }
}

export class EmbeddedFolioModel {
  /** Folio ID */

  id: string;

  /** Name of the debitor - the one who will pay the bill */

  debitor: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['debitor'] = data['debitor'];
  }
}

export class AmountModel {
  /**  */

  grossAmount: number;

  /**  */

  netAmount: number;

  /**  */

  vatType: EnumAmountModelVatType;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['grossAmount'] = data['grossAmount'];
    this['netAmount'] = data['netAmount'];
    this['vatType'] = data['vatType'];
    this['currency'] = data['currency'];
  }
}

export class ChargeModel {
  /** ID for charges. This is unique within one folio. */

  id: string;

  /** The type of the service or good */

  serviceType: EnumChargeModelServiceType;

  /** The name, article number, or other description of this charge */

  name: string;

  /** The name, article number, or other description of this charge
translated in different languages */

  translatedNames: object;

  /** Status: is this already posted? */

  isPosted: boolean;

  /** The day when the line item is (or was) due to be charged. */

  serviceDate: Date;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /**  */

  movedFrom: EmbeddedFolioModel;

  /**  */

  movedTo: EmbeddedFolioModel;

  /** A reason why move operation was performed */

  movedReason: string;

  /**  */

  amount: AmountModel;

  /** Receipt for this transaction */

  receipt: string;

  /** Identifier used for grouping related charges together */

  groupId: string;

  /** ID of the custom sub-account the charge has been posted to */

  subAccountId: string;

  /** The count of services provided */

  quantity: number;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['serviceType'] = data['serviceType'];
    this['name'] = data['name'];
    this['translatedNames'] = data['translatedNames'];
    this['isPosted'] = data['isPosted'];
    this['serviceDate'] = data['serviceDate'];
    this['created'] = data['created'];
    this['movedFrom'] = data['movedFrom'];
    this['movedTo'] = data['movedTo'];
    this['movedReason'] = data['movedReason'];
    this['amount'] = data['amount'];
    this['receipt'] = data['receipt'];
    this['groupId'] = data['groupId'];
    this['subAccountId'] = data['subAccountId'];
    this['quantity'] = data['quantity'];
  }
}

export class FolioItemModel {
  /** The id of the folio */

  id: string;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Date of update<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  updated: Date;

  /** The folio type */

  type: EnumFolioItemModelType;

  /**  */

  debitor: FolioDebitorModel;

  /** The date when the folio has been closed */

  closingDate: Date;

  /** Set to {true} if this is the main folio for the reservation */

  isMainFolio: boolean;

  /** Set to {true} if the folio has no unmoved [transitory] charges, unmoved payments, and allowances. */

  isEmpty: boolean;

  /**  */

  reservation: EmbeddedReservationModel;

  /**  */

  company: EmbeddedCompanyModel;

  /**  */

  balance: MonetaryValueModel;

  /** Set to true, if the folio has been checked out on accounts receivable */

  checkedOutOnAccountsReceivable: boolean;

  /** Depending on the state of the folio, certain warnings are shown.
This list includes all folio warnings. */

  folioWarnings: EnumFolioItemModelFolioWarnings[];

  /** Depending on the state of the folio, certain actions are allowed or not.
This list includes all actions you can perform on this folio. */

  allowedActions: EnumFolioItemModelAllowedActions[];

  /** All invoices that have been created for this folio. This is only set on folios of type 'guest' */

  relatedInvoices: EmbeddedInvoiceModel[];

  /** Status of the folio */

  status: EnumFolioItemModelStatus;

  /** The list of charges */

  charges: ChargeModel[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['created'] = data['created'];
    this['updated'] = data['updated'];
    this['type'] = data['type'];
    this['debitor'] = data['debitor'];
    this['closingDate'] = data['closingDate'];
    this['isMainFolio'] = data['isMainFolio'];
    this['isEmpty'] = data['isEmpty'];
    this['reservation'] = data['reservation'];
    this['company'] = data['company'];
    this['balance'] = data['balance'];
    this['checkedOutOnAccountsReceivable'] = data['checkedOutOnAccountsReceivable'];
    this['folioWarnings'] = data['folioWarnings'];
    this['allowedActions'] = data['allowedActions'];
    this['relatedInvoices'] = data['relatedInvoices'];
    this['status'] = data['status'];
    this['charges'] = data['charges'];
  }
}

export class FolioListModel {
  /** List of folios. */

  folios: FolioItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['folios'] = data['folios'];
    this['count'] = data['count'];
  }
}

export class EmbeddedPropertyModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The name for the property */

  name: string;

  /** The description for the property */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class TransitoryChargeModel {
  /** ID for transitory charge. This is unique within one folio. */

  id: string;

  /** The name, article number, or other description of this item */

  name: string;

  /**  */

  amount: MonetaryValueModel;

  /** The service type of this transitory charge. As revenue and VAT of transitory charges are not recorded for the hotel, this is just FYI. */

  serviceType: EnumTransitoryChargeModelServiceType;

  /** The VAT of this transitory charge. As revenue and VAT of transitory charges are not recorded for the hotel, this is just FYI. */

  vatType: EnumTransitoryChargeModelVatType;

  /** The date when this charge was added. */

  serviceDate: Date;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Receipt for this transaction */

  receipt: string;

  /**  */

  movedFrom: EmbeddedFolioModel;

  /**  */

  movedTo: EmbeddedFolioModel;

  /** A reason why move operation was performed */

  movedReason: string;

  /** The count of services provided */

  quantity: number;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['amount'] = data['amount'];
    this['serviceType'] = data['serviceType'];
    this['vatType'] = data['vatType'];
    this['serviceDate'] = data['serviceDate'];
    this['created'] = data['created'];
    this['receipt'] = data['receipt'];
    this['movedFrom'] = data['movedFrom'];
    this['movedTo'] = data['movedTo'];
    this['movedReason'] = data['movedReason'];
    this['quantity'] = data['quantity'];
  }
}

export class ExternalReference {
  /** The merchant reference ('order number') */

  merchantReference: string;

  /** The globally unique identifier of this payment in the reports of the payment service */

  pspReference: string;

  constructor(data: undefined | any = {}) {
    this['merchantReference'] = data['merchantReference'];
    this['pspReference'] = data['pspReference'];
  }
}

export class PaymentModel {
  /** Id of the payment. This is unique within one folio. */

  id: string;

  /** The Payment Method. */

  method: EnumPaymentModelMethod;

  /**  */

  amount: MonetaryValueModel;

  /**  */

  externalReference: ExternalReference;

  /** Receipt for the payment. For payments done by the payment service provider integration, this is the same as the pspReference. */

  receipt: string;

  /** The date when the payment was done<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  paymentDate: Date;

  /**  */

  movedFrom: EmbeddedFolioModel;

  /**  */

  movedTo: EmbeddedFolioModel;

  /** A reason why move operation was performed */

  movedReason: string;

  /** A link to the original payment in case of splitting payments */

  sourcePaymentId: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['method'] = data['method'];
    this['amount'] = data['amount'];
    this['externalReference'] = data['externalReference'];
    this['receipt'] = data['receipt'];
    this['paymentDate'] = data['paymentDate'];
    this['movedFrom'] = data['movedFrom'];
    this['movedTo'] = data['movedTo'];
    this['movedReason'] = data['movedReason'];
    this['sourcePaymentId'] = data['sourcePaymentId'];
  }
}

export class PendingPaymentModel {
  /** Id of the payment task. */

  id: string;

  /**  */

  amount: MonetaryValueModel;

  /** The terminal used for the payment. */

  terminalId: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['amount'] = data['amount'];
    this['terminalId'] = data['terminalId'];
  }
}

export class AllowanceModel {
  /** ID for allowances. This is unique within one folio. */

  id: string;

  /**  */

  amount: AmountModel;

  /** Reason why this allowance was posted */

  reason: string;

  /**  */

  serviceType: EnumAllowanceModelServiceType;

  /**  */

  serviceDate: Date;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /**  */

  movedFrom: EmbeddedFolioModel;

  /**  */

  movedTo: EmbeddedFolioModel;

  /** A reason why move operation was performed */

  movedReason: string;

  /** ID of a charge allowance posted for. `Null` if posted for folio */

  sourceChargeId: string;

  /** ID of the custom sub-account the allowance has been posted to */

  subAccountId: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['amount'] = data['amount'];
    this['reason'] = data['reason'];
    this['serviceType'] = data['serviceType'];
    this['serviceDate'] = data['serviceDate'];
    this['created'] = data['created'];
    this['movedFrom'] = data['movedFrom'];
    this['movedTo'] = data['movedTo'];
    this['movedReason'] = data['movedReason'];
    this['sourceChargeId'] = data['sourceChargeId'];
    this['subAccountId'] = data['subAccountId'];
  }
}

export class FolioModel {
  /** The id of the folio */

  id: string;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Date of update<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  updated: Date;

  /** The folio type */

  type: EnumFolioModelType;

  /**  */

  debitor: FolioDebitorModel;

  /** The date when the folio has been closed */

  closingDate: Date;

  /**  */

  reservation: EmbeddedReservationModel;

  /**  */

  company: EmbeddedCompanyModel;

  /**  */

  property: EmbeddedPropertyModel;

  /** The list of charges */

  charges: ChargeModel[];

  /** The list of charges */

  transitoryCharges: TransitoryChargeModel[];

  /** The list of payments - <b>DEPRECATED: This field will be removed on July 3rd 2020. Use GET /finance/v1/folios/{folioId}/payments or GET /finance/v1/folios/{folioId}/refunds instead.</b> */

  payments: PaymentModel[];

  /** The list of pending payments - <b>DEPRECATED: This field will be removed on July 3rd 2020. Use GET /finance/v1/folios/{folioId}/payments instead.</b> */

  pendingPayments: PendingPaymentModel[];

  /** The list of allowances */

  allowances: AllowanceModel[];

  /**  */

  balance: MonetaryValueModel;

  /** Set to {true}, if the folio has been checked out on accounts receivable.
If you create an invoice from this folio, it will display the outstanding payments */

  checkedOutOnAccountsReceivable: boolean;

  /** Set to {true} if this is a main folio for the reservation */

  isMainFolio: boolean;

  /** Set to {true} if the folio has no unmoved [transitory] charges, unmoved payments, and allowances. */

  isEmpty: boolean;

  /** All folios that are related to this folio. Either because they belong to the same reservation, or because charges where moved
between them. This is only set on folios of type 'guest' */

  relatedFolios: EmbeddedFolioModel[];

  /** All invoices that have been created for this folio. This is only set on folios of type 'guest' */

  relatedInvoices: EmbeddedInvoiceModel[];

  /** Depending on the state of the folio, certain warnings are shown.
This list includes all folio warnings. */

  folioWarnings: EnumFolioModelFolioWarnings[];

  /** Depending on the state of the folio, certain actions are allowed or not.
This list includes all actions you can perform on this folio. */

  allowedActions: EnumFolioModelAllowedActions[];

  /** The maximum payment that can be posted on this folio */

  allowedPayment: number;

  /** The maximum allowance (gross) that can be posted on this folio */

  maximumAllowance: number;

  /** Status of the folio */

  status: EnumFolioModelStatus;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['created'] = data['created'];
    this['updated'] = data['updated'];
    this['type'] = data['type'];
    this['debitor'] = data['debitor'];
    this['closingDate'] = data['closingDate'];
    this['reservation'] = data['reservation'];
    this['company'] = data['company'];
    this['property'] = data['property'];
    this['charges'] = data['charges'];
    this['transitoryCharges'] = data['transitoryCharges'];
    this['payments'] = data['payments'];
    this['pendingPayments'] = data['pendingPayments'];
    this['allowances'] = data['allowances'];
    this['balance'] = data['balance'];
    this['checkedOutOnAccountsReceivable'] = data['checkedOutOnAccountsReceivable'];
    this['isMainFolio'] = data['isMainFolio'];
    this['isEmpty'] = data['isEmpty'];
    this['relatedFolios'] = data['relatedFolios'];
    this['relatedInvoices'] = data['relatedInvoices'];
    this['folioWarnings'] = data['folioWarnings'];
    this['allowedActions'] = data['allowedActions'];
    this['allowedPayment'] = data['allowedPayment'];
    this['maximumAllowance'] = data['maximumAllowance'];
    this['status'] = data['status'];
  }
}

export class Operation {
  /**  */

  value: object;

  /**  */

  path: string;

  /**  */

  op: string;

  /**  */

  from: string;

  constructor(data: undefined | any = {}) {
    this['value'] = data['value'];
    this['path'] = data['path'];
    this['op'] = data['op'];
    this['from'] = data['from'];
  }
}

export class AddedChargeModel {
  /** The id of the added charge */

  id: string;

  /** The fee charges ids */

  feeChargeIds: string[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['feeChargeIds'] = data['feeChargeIds'];
  }
}

export class CreateCustomPaymentRequest {
  /** The payment method. Use 'CreditCard', if none of the specific credit cards types matches. 'Booking.com' only makes sense, if
the property (hotel) configured Booking.com > Finance to be 'Payments by Booking.com' */

  method: EnumCreateCustomPaymentRequestMethod;

  /**  */

  amount: MonetaryValueModel;

  /** The optional receipt you want to store for the payment. It defaults to the reservation or external folio id.
This field is required if you are adding payment to the house account */

  receipt: string;

  constructor(data: undefined | any = {}) {
    this['method'] = data['method'];
    this['amount'] = data['amount'];
    this['receipt'] = data['receipt'];
  }
}

export class PaymentCreatedModel {
  /**  */

  id: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
  }
}

export class CreateAuthorizationPaymentRequest {
  /** Reference to the original authorization transaction */

  transactionReference: string;

  /**  */

  amount: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['transactionReference'] = data['transactionReference'];
    this['amount'] = data['amount'];
  }
}

export class CreateAccountPaymentRequest {
  /**  */

  amount: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['amount'] = data['amount'];
  }
}

export class InvoiceRecipientModel {
  /**  */

  name: string;

  /**  */

  address: NonStrictAddressModel;

  /**  */

  companyName: string;

  /**  */

  companyTaxId: string;

  /**  */

  reference: string;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['address'] = data['address'];
    this['companyName'] = data['companyName'];
    this['companyTaxId'] = data['companyTaxId'];
    this['reference'] = data['reference'];
  }
}

export class AddressModel {
  /**  */

  addressLine1: string;

  /**  */

  addressLine2: string;

  /**  */

  postalCode: string;

  /**  */

  city: string;

  /**  */

  countryCode: string;

  constructor(data: undefined | any = {}) {
    this['addressLine1'] = data['addressLine1'];
    this['addressLine2'] = data['addressLine2'];
    this['postalCode'] = data['postalCode'];
    this['city'] = data['city'];
    this['countryCode'] = data['countryCode'];
  }
}

export class InvoiceSenderModel {
  /**  */

  name: string;

  /**  */

  address: AddressModel;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['address'] = data['address'];
  }
}

export class CommercialInfoModel {
  /**  */

  registerEntry: string;

  /**  */

  taxId: string;

  /**  */

  managingDirectors: string;

  constructor(data: undefined | any = {}) {
    this['registerEntry'] = data['registerEntry'];
    this['taxId'] = data['taxId'];
    this['managingDirectors'] = data['managingDirectors'];
  }
}

export class BankAccountModel {
  /**  */

  iban: string;

  /**  */

  bic: string;

  /**  */

  bank: string;

  constructor(data: undefined | any = {}) {
    this['iban'] = data['iban'];
    this['bic'] = data['bic'];
    this['bank'] = data['bank'];
  }
}

export class InvoiceLineItemModel {
  /** The date on which this item or service is delivered */

  date: Date;

  /** The description of the item or service */

  description: string;

  /**  */

  price: MonetaryValueModel;

  /** The applied VAT type.
Is null when the InvoiceLineItemGroupingType != (NoGrouping) */

  vatType: EnumInvoiceLineItemModelVatType;

  /** The applied VAT percent
Is null when the InvoiceLineItemGroupingType != (NoGrouping) */

  vatPercent: number;

  /** Whether this line item represents a no-show fee */

  isNoShowFee: boolean;

  /** Items which are included in the package, if there are any */

  subItems: string[];

  /** Guest who the service has been provided to. Is defined only when the invoice has charges from multiple folios with different guests. */

  guest: string;

  /** The count of services provided. Is defined only when the InvoiceLineItemGroupingType == NoGrouping */

  quantity: number;

  constructor(data: undefined | any = {}) {
    this['date'] = data['date'];
    this['description'] = data['description'];
    this['price'] = data['price'];
    this['vatType'] = data['vatType'];
    this['vatPercent'] = data['vatPercent'];
    this['isNoShowFee'] = data['isNoShowFee'];
    this['subItems'] = data['subItems'];
    this['guest'] = data['guest'];
    this['quantity'] = data['quantity'];
  }
}

export class InvoiceLineItemsModel {
  /**  */

  lineItems: InvoiceLineItemModel[];

  /**  */

  subTotal: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['lineItems'] = data['lineItems'];
    this['subTotal'] = data['subTotal'];
  }
}

export class InvoicePaymentModel {
  /** Id of the payment. This is unique within one folio. */

  id: string;

  /** The Payment Method type. */

  method: EnumInvoicePaymentModelMethod;

  /** The Payment Method name translated in the requested language of the invoice */

  methodName: string;

  /**  */

  amount: MonetaryValueModel;

  /** The date when the payment was done<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  paymentDate: Date;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['method'] = data['method'];
    this['methodName'] = data['methodName'];
    this['amount'] = data['amount'];
    this['paymentDate'] = data['paymentDate'];
  }
}

export class TaxDetailModel {
  /**  */

  vatType: EnumTaxDetailModelVatType;

  /**  */

  vatPercent: number;

  /**  */

  net: MonetaryValueModel;

  /**  */

  tax: MonetaryValueModel;

  constructor(data: undefined | any = {}) {
    this['vatType'] = data['vatType'];
    this['vatPercent'] = data['vatPercent'];
    this['net'] = data['net'];
    this['tax'] = data['tax'];
  }
}

export class StayInfoModel {
  /** Name of the primary guest */

  guestName: string;

  /** The arrival date */

  arrivalDate: Date;

  /** The departure date */

  departureDate: Date;

  /** ID of the reservation this invoice is for, if any. */

  reservationId: string;

  constructor(data: undefined | any = {}) {
    this['guestName'] = data['guestName'];
    this['arrivalDate'] = data['arrivalDate'];
    this['departureDate'] = data['departureDate'];
    this['reservationId'] = data['reservationId'];
  }
}

export class InvoiceItemModel {
  /** Invoice identifier */

  id: string;

  /** Invoice number */

  number: string;

  /** Invoice type */

  type: EnumInvoiceItemModelType;

  /** Language which was used to create the invoice */

  languageCode: string;

  /** The folio for this invoice */

  folioId: string;

  /** The reservation for this invoice */

  reservationId: string;

  /** If the invoice is related to another invoice, this field contains related invoice number
For example, if the invoice has Cancellation type,
this field contains the number of invoice which is being cancelled */

  relatedInvoiceNumber: string;

  /**  */

  outstandingPayment: MonetaryValueModel;

  /** True, if this invoice had no outstanding payments or was settled. */

  paymentSettled: boolean;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** Name of the guest */

  guestName: string;

  /** Company the guest specified */

  guestCompany: string;

  /** Depending on the state of the invoice, certain actions are allowed or not.
This list includes all actions you can perform on this invoice. */

  allowedActions: EnumInvoiceItemModelAllowedActions[];

  /**  */

  company: EmbeddedCompanyModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['number'] = data['number'];
    this['type'] = data['type'];
    this['languageCode'] = data['languageCode'];
    this['folioId'] = data['folioId'];
    this['reservationId'] = data['reservationId'];
    this['relatedInvoiceNumber'] = data['relatedInvoiceNumber'];
    this['outstandingPayment'] = data['outstandingPayment'];
    this['paymentSettled'] = data['paymentSettled'];
    this['created'] = data['created'];
    this['guestName'] = data['guestName'];
    this['guestCompany'] = data['guestCompany'];
    this['allowedActions'] = data['allowedActions'];
    this['company'] = data['company'];
  }
}

export class InvoiceListModel {
  /** List of invoices. */

  invoices: InvoiceItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['invoices'] = data['invoices'];
    this['count'] = data['count'];
  }
}

export class CreateInvoicePdfRequest {
  /** The language in which the invoice should be produced. */

  languageCode: string;

  /** The ID of the folio for which the invoice should be created. */

  folioId: string;

  /** Specifies whether the invoice is created for a company set in the reservation or not. Defaults to 'false'. */

  createForCompany: boolean;

  constructor(data: undefined | any = {}) {
    this['languageCode'] = data['languageCode'];
    this['folioId'] = data['folioId'];
    this['createForCompany'] = data['createForCompany'];
  }
}

export class InvoiceCreatedModel {
  /** The invoice id */

  id: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
  }
}

export class InvoiceModel {
  /**  */

  to: InvoiceRecipientModel;

  /** True, if this invoice had no outstanding payments or was settled. */

  paymentSettled: boolean;

  /**  */

  number: string;

  /** Depending on the state of the invoice, certain actions are allowed or not.
This list includes all actions you can perform on this invoice. */

  allowedActions: EnumInvoiceModelAllowedActions[];

  /** Date the invoice has been created */

  invoiceDate: Date;

  /** The folio this invoice was requested for */

  folioId: string;

  /**  */

  from: InvoiceSenderModel;

  /**  */

  commercialInformation: CommercialInfoModel;

  /**  */

  bankAccount: BankAccountModel;

  /** Specification of the payment terms, as defined in the property */

  paymentTerms: string;

  /**  */

  lineItems: InvoiceLineItemsModel;

  /** A list of all payments */

  payments: InvoicePaymentModel[];

  /**  */

  outstandingPayment: MonetaryValueModel;

  /** The subtotal, displaying net and tax amount for each VAT type */

  taxDetails: TaxDetailModel[];

  /**  */

  total: MonetaryValueModel;

  /**  */

  stayInfo: StayInfoModel;

  /** The ID of the property */

  propertyId: string;

  /** The country code of the property */

  propertyCountryCode: string;

  /** Language which was used to create the invoice */

  languageCode: string;

  /**  */

  company: EmbeddedCompanyModel;

  constructor(data: undefined | any = {}) {
    this['to'] = data['to'];
    this['paymentSettled'] = data['paymentSettled'];
    this['number'] = data['number'];
    this['allowedActions'] = data['allowedActions'];
    this['invoiceDate'] = data['invoiceDate'];
    this['folioId'] = data['folioId'];
    this['from'] = data['from'];
    this['commercialInformation'] = data['commercialInformation'];
    this['bankAccount'] = data['bankAccount'];
    this['paymentTerms'] = data['paymentTerms'];
    this['lineItems'] = data['lineItems'];
    this['payments'] = data['payments'];
    this['outstandingPayment'] = data['outstandingPayment'];
    this['taxDetails'] = data['taxDetails'];
    this['total'] = data['total'];
    this['stayInfo'] = data['stayInfo'];
    this['propertyId'] = data['propertyId'];
    this['propertyCountryCode'] = data['propertyCountryCode'];
    this['languageCode'] = data['languageCode'];
    this['company'] = data['company'];
  }
}

export class PayInvoiceRequest {
  /** The payment method used for paying the invoice. Used for accounting. */

  paymentMethod: EnumPayInvoiceRequestPaymentMethod;

  /** The receipt for the payment. Each transaction in accounting has a receipt set. */

  receipt: string;

  constructor(data: undefined | any = {}) {
    this['paymentMethod'] = data['paymentMethod'];
    this['receipt'] = data['receipt'];
  }
}
export enum EnumFolioDebitorModelTitle {
  'Mr' = 'Mr',
  'Ms' = 'Ms',
  'Dr' = 'Dr',
  'Prof' = 'Prof',
  'Other' = 'Other'
}
export enum EnumAmountModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
export enum EnumChargeModelServiceType {
  'Other' = 'Other',
  'Accommodation' = 'Accommodation',
  'FoodAndBeverages' = 'FoodAndBeverages',
  'CancellationFees' = 'CancellationFees',
  'NoShow' = 'NoShow',
  'CityTax' = 'CityTax'
}
export enum EnumFolioItemModelType {
  'House' = 'House',
  'Guest' = 'Guest',
  'External' = 'External'
}
export enum EnumFolioItemModelFolioWarnings {
  'IncompleteBillingAddress' = 'IncompleteBillingAddress'
}
export enum EnumFolioItemModelAllowedActions {
  'AddCharge' = 'AddCharge',
  'AddAllowance' = 'AddAllowance',
  'AddCancellationFee' = 'AddCancellationFee',
  'AddNoShowFee' = 'AddNoShowFee',
  'AddPayment' = 'AddPayment',
  'AddRefund' = 'AddRefund',
  'CheckoutOnAr' = 'CheckoutOnAr',
  'Close' = 'Close',
  'PostOpenCharges' = 'PostOpenCharges',
  'CorrectFolio' = 'CorrectFolio',
  'ChangeAddress' = 'ChangeAddress',
  'Delete' = 'Delete',
  'Reopen' = 'Reopen',
  'CreateInvoice' = 'CreateInvoice',
  'CancelLastInvoice' = 'CancelLastInvoice',
  'CreateInvoiceWithSimpleDebitor' = 'CreateInvoiceWithSimpleDebitor'
}
export enum EnumFolioItemModelStatus {
  'Open' = 'Open',
  'Closed' = 'Closed',
  'ClosedWithInvoice' = 'ClosedWithInvoice'
}
export enum EnumTransitoryChargeModelServiceType {
  'Other' = 'Other',
  'Accommodation' = 'Accommodation',
  'FoodAndBeverages' = 'FoodAndBeverages',
  'CancellationFees' = 'CancellationFees',
  'NoShow' = 'NoShow',
  'CityTax' = 'CityTax'
}
export enum EnumTransitoryChargeModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
export enum EnumPaymentModelMethod {
  'Cash' = 'Cash',
  'BankTransfer' = 'BankTransfer',
  'CreditCard' = 'CreditCard',
  'Invoice' = 'Invoice',
  'Amex' = 'Amex',
  'VisaCredit' = 'VisaCredit',
  'VisaDebit' = 'VisaDebit',
  'MasterCard' = 'MasterCard',
  'MasterCardDebit' = 'MasterCardDebit',
  'Maestro' = 'Maestro',
  'GiroCard' = 'GiroCard',
  'DiscoverCard' = 'DiscoverCard',
  'Diners' = 'Diners',
  'Jcb' = 'Jcb',
  'BookingCom' = 'BookingCom',
  'VPay' = 'VPay',
  'PayPal' = 'PayPal',
  'Postcard' = 'Postcard',
  'Reka' = 'Reka',
  'Twint' = 'Twint',
  'Lunchcheck' = 'Lunchcheck',
  'Voucher' = 'Voucher',
  'ChinaUnionPay' = 'ChinaUnionPay',
  'Other' = 'Other',
  'Cheque' = 'Cheque',
  'Airbnb' = 'Airbnb'
}
export enum EnumAllowanceModelServiceType {
  'Other' = 'Other',
  'Accommodation' = 'Accommodation',
  'FoodAndBeverages' = 'FoodAndBeverages',
  'CancellationFees' = 'CancellationFees',
  'NoShow' = 'NoShow',
  'CityTax' = 'CityTax'
}
export enum EnumFolioModelType {
  'House' = 'House',
  'Guest' = 'Guest',
  'External' = 'External'
}
export enum EnumFolioModelFolioWarnings {
  'IncompleteBillingAddress' = 'IncompleteBillingAddress'
}
export enum EnumFolioModelAllowedActions {
  'AddCharge' = 'AddCharge',
  'AddAllowance' = 'AddAllowance',
  'AddCancellationFee' = 'AddCancellationFee',
  'AddNoShowFee' = 'AddNoShowFee',
  'AddPayment' = 'AddPayment',
  'AddRefund' = 'AddRefund',
  'CheckoutOnAr' = 'CheckoutOnAr',
  'Close' = 'Close',
  'PostOpenCharges' = 'PostOpenCharges',
  'CorrectFolio' = 'CorrectFolio',
  'ChangeAddress' = 'ChangeAddress',
  'Delete' = 'Delete',
  'Reopen' = 'Reopen',
  'CreateInvoice' = 'CreateInvoice',
  'CancelLastInvoice' = 'CancelLastInvoice',
  'CreateInvoiceWithSimpleDebitor' = 'CreateInvoiceWithSimpleDebitor'
}
export enum EnumFolioModelStatus {
  'Open' = 'Open',
  'Closed' = 'Closed',
  'ClosedWithInvoice' = 'ClosedWithInvoice'
}
export enum EnumCreateCustomPaymentRequestMethod {
  'Cash' = 'Cash',
  'BankTransfer' = 'BankTransfer',
  'CreditCard' = 'CreditCard',
  'Amex' = 'Amex',
  'VisaCredit' = 'VisaCredit',
  'VisaDebit' = 'VisaDebit',
  'MasterCard' = 'MasterCard',
  'MasterCardDebit' = 'MasterCardDebit',
  'Maestro' = 'Maestro',
  'GiroCard' = 'GiroCard',
  'DiscoverCard' = 'DiscoverCard',
  'Diners' = 'Diners',
  'Jcb' = 'Jcb',
  'BookingCom' = 'BookingCom',
  'VPay' = 'VPay',
  'PayPal' = 'PayPal',
  'Postcard' = 'Postcard',
  'Reka' = 'Reka',
  'Twint' = 'Twint',
  'Lunchcheck' = 'Lunchcheck',
  'Voucher' = 'Voucher',
  'ChinaUnionPay' = 'ChinaUnionPay',
  'Other' = 'Other',
  'Cheque' = 'Cheque',
  'Airbnb' = 'Airbnb'
}
export enum EnumInvoiceLineItemModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
export enum EnumInvoicePaymentModelMethod {
  'Cash' = 'Cash',
  'BankTransfer' = 'BankTransfer',
  'CreditCard' = 'CreditCard',
  'Invoice' = 'Invoice',
  'Amex' = 'Amex',
  'VisaCredit' = 'VisaCredit',
  'VisaDebit' = 'VisaDebit',
  'MasterCard' = 'MasterCard',
  'MasterCardDebit' = 'MasterCardDebit',
  'Maestro' = 'Maestro',
  'GiroCard' = 'GiroCard',
  'DiscoverCard' = 'DiscoverCard',
  'Diners' = 'Diners',
  'Jcb' = 'Jcb',
  'BookingCom' = 'BookingCom',
  'VPay' = 'VPay',
  'PayPal' = 'PayPal',
  'Postcard' = 'Postcard',
  'Reka' = 'Reka',
  'Twint' = 'Twint',
  'Lunchcheck' = 'Lunchcheck',
  'Voucher' = 'Voucher',
  'ChinaUnionPay' = 'ChinaUnionPay',
  'Other' = 'Other',
  'Cheque' = 'Cheque',
  'Airbnb' = 'Airbnb'
}
export enum EnumTaxDetailModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
export enum EnumInvoiceItemModelType {
  'Initial' = 'Initial',
  'Cancellation' = 'Cancellation',
  'Correction' = 'Correction'
}
export enum EnumInvoiceItemModelAllowedActions {
  'CorrectAddress' = 'CorrectAddress',
  'CorrectCharges' = 'CorrectCharges',
  'MarkAsPaid' = 'MarkAsPaid',
  'Cancel' = 'Cancel'
}
export enum EnumInvoiceModelAllowedActions {
  'CorrectAddress' = 'CorrectAddress',
  'CorrectCharges' = 'CorrectCharges',
  'MarkAsPaid' = 'MarkAsPaid',
  'Cancel' = 'Cancel'
}
export enum EnumPayInvoiceRequestPaymentMethod {
  'Cash' = 'Cash',
  'BankTransfer' = 'BankTransfer',
  'CreditCard' = 'CreditCard',
  'Amex' = 'Amex',
  'VisaCredit' = 'VisaCredit',
  'VisaDebit' = 'VisaDebit',
  'MasterCard' = 'MasterCard',
  'MasterCardDebit' = 'MasterCardDebit',
  'Maestro' = 'Maestro',
  'GiroCard' = 'GiroCard',
  'DiscoverCard' = 'DiscoverCard',
  'Diners' = 'Diners',
  'Jcb' = 'Jcb',
  'BookingCom' = 'BookingCom',
  'VPay' = 'VPay',
  'PayPal' = 'PayPal',
  'Postcard' = 'Postcard',
  'Reka' = 'Reka',
  'Twint' = 'Twint',
  'Lunchcheck' = 'Lunchcheck',
  'Voucher' = 'Voucher',
  'ChinaUnionPay' = 'ChinaUnionPay',
  'Other' = 'Other',
  'Cheque' = 'Cheque',
  'Airbnb' = 'Airbnb'
}
