/** Generate by swagger-axios-codegen */
import axiosStatic, { AxiosPromise, AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export class FinanceLogsService {
  /**
   * Returns folio change log entries sorted by the timestamp.
   */
  static logsFinanceFolioGet(
    params: {
      /** Filter the log entries by folio IDs */
      folioIds?: string[];
      /** Filter the log entries by event types. */
      eventTypes?: string[];
      /** Filter the log entries by property IDs */
      propertyIds?: string[];
      /** Filter the log entries by subject IDs (which user or application triggered this event) */
      subjectIds?: string[];
      /** Filter by event date and time<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      dateFilter?: string[];
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FolioChangeLogListModel> {
    return new Promise((resolve, reject) => {
      let url = '/logs/v1/finance/folio';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        folioIds: params['folioIds'],
        eventTypes: params['eventTypes'],
        propertyIds: params['propertyIds'],
        subjectIds: params['subjectIds'],
        dateFilter: params['dateFilter'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FolioChangeLogListModel {
  /** List of the log entries. */

  logEntries: FolioChangeLogItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['logEntries'] = data['logEntries'];
    this['count'] = data['count'];
  }
}

export class FolioChangeLogItemModel {
  /** The ID of the folio */

  folioId: string;

  /** The type of the operation */

  eventType: EnumFolioChangeLogItemModelEventType;

  /** The ID of related entity, if there is any (e.g. the ID of the charge posted) */

  relatedEntityId: string;

  /** The description of related entity, if there is any (e.g. the name of the charge posted or the payment method) */

  relatedEntityDescription: string;

  /** The amount of the operation, if applicable */

  amount: MonetaryValueModel;

  /** The ID of the property */

  propertyId: string;

  /** Date and time when the operation has been executed<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** The ID of the user or client that triggered this event */

  subjectId: string;

  constructor(data: undefined | any = {}) {
    this['folioId'] = data['folioId'];
    this['eventType'] = data['eventType'];
    this['relatedEntityId'] = data['relatedEntityId'];
    this['relatedEntityDescription'] = data['relatedEntityDescription'];
    this['amount'] = data['amount'];
    this['propertyId'] = data['propertyId'];
    this['created'] = data['created'];
    this['subjectId'] = data['subjectId'];
  }
}

export class MonetaryValueModel {
  /**  */

  amount: number;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['amount'] = data['amount'];
    this['currency'] = data['currency'];
  }
}
export enum EnumFolioChangeLogItemModelEventType {
  'BalanceChanged' = 'BalanceChanged',
  'Created' = 'Created',
  'ChargePosted' = 'ChargePosted',
  'TransitoryChargePosted' = 'TransitoryChargePosted',
  'AllowancePosted' = 'AllowancePosted',
  'PaymentPosted' = 'PaymentPosted',
  'ChargeMovedFromFolio' = 'ChargeMovedFromFolio',
  'TransitoryChargeMovedFromFolio' = 'TransitoryChargeMovedFromFolio',
  'PaymentMovedFromFolio' = 'PaymentMovedFromFolio',
  'ChargeMovedToFolio' = 'ChargeMovedToFolio',
  'TransitoryChargeMovedToFolio' = 'TransitoryChargeMovedToFolio',
  'PaymentMovedToFolio' = 'PaymentMovedToFolio',
  'Closed' = 'Closed',
  'Reopened' = 'Reopened',
  'Deleted' = 'Deleted'
}
