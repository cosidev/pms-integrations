/** Generate by swagger-axios-codegen */
// tslint:disable
/* eslint-disable */
import axiosStatic, { AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export class RatePlanService {
  /**
   * Get a rate plan
   */
  static rateplanRatePlansByIdGet(
    params: {
      /** The id of the rate plan. */
      id: string;
      /** 'all' or comma separated list of two-letter language codes (ISO Alpha-2) */
      languages?: string[];
      /** List of all embedded resources that should be expanded in the response. Possible values are: property, cancellationPolicy. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RatePlanModel> {
    return new Promise((resolve, reject) => {
      let url = '/rateplan/v1/rate-plans/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { languages: params['languages'], expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }

  /**
   * Get a rate plan list
   */
  static rateplanRatePlansGet(
    params: {
      /** Return rate plans for the specific property */
      propertyId?: string;
      /** Return rate plans filtered by requested codes */
      ratePlanCodes?: string[];
      /** Return rate plans that have any of the requested included services */
      includedServiceIds?: string[];
      /** Return rate plans that are sold though any of the specified channels */
      channelCodes?: string[];
      /** Return rate plans that have any of the requested promo codes */
      promoCodes?: string[];
      /** Return rate plans filtered by requested companies */
      companyIds?: string[];
      /** Return rate plans derived from any of the specified rate plans */
      baseRatePlanIds?: string[];
      /** Return rate plans with any of the specified unit groups */
      unitGroupIds?: string[];
      /** Return rate plans with any of the specified time slice definitions */
      timeSliceDefinitionIds?: string[];
      /** Return rate plans with any of the specified unit group types */
      unitGroupTypes?: string[];
      /** The time slice template, defaults to 'over night' */
      timeSliceTemplate?: string;
      /** Return rate plans with any of the specified min guarantee types */
      minGuaranteeTypes?: string[];
      /** Return rate plans with any of the specified cancellation policies */
      cancellationPolicyIds?: string[];
      /** Return only derived or base rate plans */
      isDerived?: boolean;
      /** This will filter rate plans based on their derivation level.<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      derivationLevelFilter?: string[];
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: property, unitGroup, cancellationPolicy, services. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RatePlanListModel> {
    return new Promise((resolve, reject) => {
      let url = '/rateplan/v1/rate-plans';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        propertyId: params['propertyId'],
        ratePlanCodes: params['ratePlanCodes'],
        includedServiceIds: params['includedServiceIds'],
        channelCodes: params['channelCodes'],
        promoCodes: params['promoCodes'],
        companyIds: params['companyIds'],
        baseRatePlanIds: params['baseRatePlanIds'],
        unitGroupIds: params['unitGroupIds'],
        timeSliceDefinitionIds: params['timeSliceDefinitionIds'],
        unitGroupTypes: params['unitGroupTypes'],
        timeSliceTemplate: params['timeSliceTemplate'],
        minGuaranteeTypes: params['minGuaranteeTypes'],
        cancellationPolicyIds: params['cancellationPolicyIds'],
        isDerived: params['isDerived'],
        derivationLevelFilter: params['derivationLevelFilter'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RatePlanItemModel {
  /** The rate plan id */

  id: string;

  /** The code for the rate plan that can be shown in reports and table views */

  code: string;

  /** The name for the rate plan */

  name: string;

  /** The description for the rate plan */

  description: string;

  /** The channel codes the rate plan is sold through */

  channelCodes: EnumRatePlanItemModelChannelCodes[];

  /** The minimum guarantee to be provided when this rate plan is booked so
the reservation will be guaranteed to the guest */

  minGuaranteeType: EnumRatePlanItemModelMinGuaranteeType;

  /** The rate codes for promotional and hidden rates. If at least one code is set the rate will be not publicly visible
anymore and only be offered when one of the promo codes is given in the offer request.
For backward compatibility it is still not possible to set multiple promo codes. */

  promoCodes: string[];

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  cancellationPolicy: EmbeddedCancellationPolicyModel;

  /**  */

  timeSliceDefinition: EmbeddedTimeSliceDefinitionModel;

  /**  */

  pricingRule: PricingRuleModel;

  /** Indicates whether the rates for this rate plan are derived from another rate plan */

  isDerived: boolean;

  /** Indicates whether the rate plan has an active booking period */

  isBookable: boolean;

  /** Whether the rate plan is subject to city tax or not */

  isSubjectToCityTax: boolean;

  /** Indicates the derivation level of the rate plan. When zero, it is a rate plan with manually managed prices. */

  derivationLevel: number;

  /** ID of the custom sub-account, used by accounting to determine the correct revenue account */

  subAccountId: string;

  /**  */

  ratesRange: RatesRangeModel;

  /** Companies that can use this rate plan */

  companies: CompanyRatePlanModel[];

  /** Services that are included in the rate plan */

  includedServices: RatePlanServiceItemModel[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['channelCodes'] = data['channelCodes'];
    this['minGuaranteeType'] = data['minGuaranteeType'];
    this['promoCodes'] = data['promoCodes'];
    this['property'] = data['property'];
    this['unitGroup'] = data['unitGroup'];
    this['cancellationPolicy'] = data['cancellationPolicy'];
    this['timeSliceDefinition'] = data['timeSliceDefinition'];
    this['pricingRule'] = data['pricingRule'];
    this['isDerived'] = data['isDerived'];
    this['isBookable'] = data['isBookable'];
    this['isSubjectToCityTax'] = data['isSubjectToCityTax'];
    this['derivationLevel'] = data['derivationLevel'];
    this['subAccountId'] = data['subAccountId'];
    this['ratesRange'] = data['ratesRange'];
    this['companies'] = data['companies'];
    this['includedServices'] = data['includedServices'];
  }
}

export class RatePlanServiceItemModel {
  /**  */

  service: EmbeddedServiceModel;

  /**  */

  grossPrice: MonetaryValueModel;

  /** Whether the service price is included in or added to the base rate. The property defaults to `Included`. */

  pricingMode: EnumRatePlanServiceItemModelPricingMode;

  constructor(data: undefined | any = {}) {
    this['service'] = data['service'];
    this['grossPrice'] = data['grossPrice'];
    this['pricingMode'] = data['pricingMode'];
  }
}

export class EmbeddedServiceModel {
  /** The service id */

  id: string;

  /** The code for the service */

  code: string;

  /** The name for the service */

  name: string;

  /** The description for the service */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class RatePlanListModel {
  /** List of rate plans */

  ratePlans: RatePlanItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['ratePlans'] = data['ratePlans'];
    this['count'] = data['count'];
  }
}

export enum EnumRatePlanServiceItemModelPricingMode {
  'Included' = 'Included',
  'Additional' = 'Additional'
}
export enum EnumRatePlanItemModelChannelCodes {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia',
  'Homelike' = 'Homelike'
}
export enum EnumRatePlanItemModelMinGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company'
}

export class MonetaryValueModel {
  /**  */

  amount: number;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['amount'] = data['amount'];
    this['currency'] = data['currency'];
  }
}

export class EmbeddedPropertyModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The name for the property */

  name: string;

  /** The description for the property */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class EmbeddedUnitGroupModel {
  /** The unit group id */

  id: string;

  /** The code for the unit group that can be shown in reports and table views */

  code: string;

  /** The name for the unit group */

  name: string;

  /** The description for the unit group */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class PeriodModel {
  /** The number of hours within the period */

  hours: number;

  /** The number of days within the period */

  days: number;

  /** The number of months within the period */

  months: number;

  constructor(data: undefined | any = {}) {
    this['hours'] = data['hours'];
    this['days'] = data['days'];
    this['months'] = data['months'];
  }
}

export class EmbeddedCancellationPolicyModel {
  /** The cancellation policy id */

  id: string;

  /** The code for the cancellation policy that can be shown in reports and table views */

  code: string;

  /** The name for the cancellation policy */

  name: string;

  /** The description for the cancellation policy */

  description: string;

  /**  */

  periodPriorToArrival: PeriodModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['periodPriorToArrival'] = data['periodPriorToArrival'];
  }
}

export class EmbeddedTimeSliceDefinitionModel {
  /** The time slice definition id */

  id: string;

  /** The name for the time slice definition */

  name: string;

  /** The template used by the time slice defintion */

  template: EnumEmbeddedTimeSliceDefinitionModelTemplate;

  /** The check-in time for reservations based on this rate plan<br />A time (without fractional second part) as defined in the <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkInTime: string;

  /** The check-out time for reservations based on this rate plan<br />A time (without fractional second part) as defined in the <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  checkOutTime: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['template'] = data['template'];
    this['checkInTime'] = data['checkInTime'];
    this['checkOutTime'] = data['checkOutTime'];
  }
}

export class EmbeddedRatePlanModel {
  /** The rate plan id */

  id: string;

  /** The code for the rate plan that can be shown in reports and table views */

  code: string;

  /** The name for the rate plan */

  name: string;

  /** The description for the rate plan */

  description: string;

  /** Whether the rate plan is subject to city tax or not */

  isSubjectToCityTax: boolean;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['isSubjectToCityTax'] = data['isSubjectToCityTax'];
  }
}

export class PricingRuleModel {
  /**  */

  baseRatePlan: EmbeddedRatePlanModel;

  /** The type used to control the calculation of the difference to the rates of the defined base
rate plan */

  type: EnumPricingRuleModelType;

  /** The value used to control the calculation of the difference to the rates of the defined base
rate plan. It can be a positive and a negative value */

  value: number;

  constructor(data: undefined | any = {}) {
    this['baseRatePlan'] = data['baseRatePlan'];
    this['type'] = data['type'];
    this['value'] = data['value'];
  }
}

export class RatesRangeModel {
  /** The first date when the rate plan has rates set */

  from: Date;

  /** The last date when the rate plan has rates set */

  to: Date;

  constructor(data: undefined | any = {}) {
    this['from'] = data['from'];
    this['to'] = data['to'];
  }
}

export class CompanyRatePlanModel {
  /** Company ID that can use this rate plan */

  id: string;

  /** The code of the company that can book this rate plan */

  code: string;

  /** Company rate plan code, identifying the company + rate plan pair. Is used is offers. */

  corporateCode: string;

  /** The name of the company that can book this rate plan */

  name: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['corporateCode'] = data['corporateCode'];
    this['name'] = data['name'];
  }
}

export class BookingPeriodModel {
  /** Start of booking period<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** End of booking period<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  constructor(data: undefined | any = {}) {
    this['from'] = data['from'];
    this['to'] = data['to'];
  }
}

export class BookingRestrictionsModel {
  /**  */

  minAdvance: PeriodModel;

  /**  */

  maxAdvance: PeriodModel;

  /** Time of day until the late booking can be made for this rate plan
Restrictions:
- the time has to be between the check-in (excl.) and check-out time (excl.)
- can only be set, if MinAdvance is not set<br />A time (without fractional second part) as defined in the <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  lateBookingUntil: string;

  constructor(data: undefined | any = {}) {
    this['minAdvance'] = data['minAdvance'];
    this['maxAdvance'] = data['maxAdvance'];
    this['lateBookingUntil'] = data['lateBookingUntil'];
  }
}

export class SurchargeModel {
  /** The total numbers of adults */

  adults: number;

  /** Specifies how to interpret 'Value' */

  type: EnumSurchargeModelType;

  /** The percent or absolute value (in the rate plan's currency) of the surcharge */

  value: number;

  constructor(data: undefined | any = {}) {
    this['adults'] = data['adults'];
    this['type'] = data['type'];
    this['value'] = data['value'];
  }
}

export class AgeCategorySurchargeModel {
  /** The number of adults */

  adults: number;

  /** The absolute value in the currency of the rate plan as surcharge for each child accompanied by the given number of 'adults'.
The minimum value is 0. */

  value: number;

  constructor(data: undefined | any = {}) {
    this['adults'] = data['adults'];
    this['value'] = data['value'];
  }
}

export class RatePlanAgeCategoryModel {
  /** The id of the age category */

  id: string;

  /** Additional charges for the current age category. The absolute value will be added to the manually defined or calculated derived rates.
The values for 'adults' must be unique within the list, and starting from 1. Values equal or higher than
the maximum unit group occupancy will be silently ignored. */

  surcharges: AgeCategorySurchargeModel[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['surcharges'] = data['surcharges'];
  }
}

export class RatePlanServiceModel {
  /** Service id to be included in the rate plan */

  serviceId: string;

  /**  */

  grossPrice: MonetaryValueModel;

  /** Whether the service price is included in or added to the base rate. The property defaults to `Included`. */

  pricingMode: EnumRatePlanServiceModelPricingMode;

  constructor(data: undefined | any = {}) {
    this['serviceId'] = data['serviceId'];
    this['grossPrice'] = data['grossPrice'];
    this['pricingMode'] = data['pricingMode'];
  }
}

export class RatePlanModel {
  /** The rate plan  id */

  id: string;

  /** The code for the rate plan that can be shown in reports and table views */

  code: string;

  /** The name for the rate plan */

  name: object;

  /** The description for the rate plan */

  description: object;

  /** The minimum guarantee to be provided when this rate plan is booked so
the reservation will be guaranteed to the guest */

  minGuaranteeType: EnumRatePlanModelMinGuaranteeType;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  cancellationPolicy: EmbeddedCancellationPolicyModel;

  /** The channel codes the rate plan is sold through */

  channelCodes: EnumRatePlanModelChannelCodes[];

  /** The service type, used by accounting to determine the correct revenue account */

  serviceType: EnumRatePlanModelServiceType;

  /** The VAT type, used by accounting to determine the correct VAT amount and account */

  vatType: EnumRatePlanModelVatType;

  /** The rate codes for promotional and hidden rates. If at least one code is set the rate will be not publicly visible
anymore and only be offered when one of the promo codes is given in the offer request.
For backward compatibility it is still not possible to set multiple promo codes. */

  promoCodes: string[];

  /**  */

  timeSliceDefinition: EmbeddedTimeSliceDefinitionModel;

  /**  */

  restrictions: BookingRestrictionsModel;

  /** Time periods when the rate plan is bookable */

  bookingPeriods: BookingPeriodModel[];

  /** Indicates whether the rate plan has an active booking period */

  isBookable: boolean;

  /** Whether the rate plan is subject to city tax or not.
Default value is {true} */

  isSubjectToCityTax: boolean;

  /**  */

  pricingRule: PricingRuleModel;

  /** Indicates whether the rates for this rate plan are derived from another rate plan */

  isDerived: boolean;

  /** Indicates the derivation level of the rate plan. When zero, it is a rate plan with manually managed prices. */

  derivationLevel: number;

  /** Additional charges for more than single occupancy. The percent or absolute value will be added to the manually defined or calculated derived rates. */

  surcharges: SurchargeModel[];

  /** Additional charges per age category. */

  ageCategories: RatePlanAgeCategoryModel[];

  /** Services that are included in the rate plan */

  includedServices: RatePlanServiceModel[];

  /** Companies that can use this rate plan */

  companies: CompanyRatePlanModel[];

  /** ID of the custom sub-account, used by accounting to determine the correct revenue account */

  subAccountId: string;

  /**  */

  ratesRange: RatesRangeModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['minGuaranteeType'] = data['minGuaranteeType'];
    this['property'] = data['property'];
    this['unitGroup'] = data['unitGroup'];
    this['cancellationPolicy'] = data['cancellationPolicy'];
    this['channelCodes'] = data['channelCodes'];
    this['serviceType'] = data['serviceType'];
    this['vatType'] = data['vatType'];
    this['promoCodes'] = data['promoCodes'];
    this['timeSliceDefinition'] = data['timeSliceDefinition'];
    this['restrictions'] = data['restrictions'];
    this['bookingPeriods'] = data['bookingPeriods'];
    this['isBookable'] = data['isBookable'];
    this['isSubjectToCityTax'] = data['isSubjectToCityTax'];
    this['pricingRule'] = data['pricingRule'];
    this['isDerived'] = data['isDerived'];
    this['derivationLevel'] = data['derivationLevel'];
    this['surcharges'] = data['surcharges'];
    this['ageCategories'] = data['ageCategories'];
    this['includedServices'] = data['includedServices'];
    this['companies'] = data['companies'];
    this['subAccountId'] = data['subAccountId'];
    this['ratesRange'] = data['ratesRange'];
  }
}
export enum EnumEmbeddedTimeSliceDefinitionModelTemplate {
  'DayUse' = 'DayUse',
  'OverNight' = 'OverNight'
}
export enum EnumPricingRuleModelType {
  'Absolute' = 'Absolute',
  'Percent' = 'Percent'
}
export enum EnumSurchargeModelType {
  'Absolute' = 'Absolute',
  'Percent' = 'Percent'
}
export enum EnumRatePlanServiceModelPricingMode {
  'Included' = 'Included',
  'Additional' = 'Additional'
}
export enum EnumRatePlanModelMinGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company'
}
export enum EnumRatePlanModelChannelCodes {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia'
}
export enum EnumRatePlanModelServiceType {
  'Other' = 'Other',
  'Accommodation' = 'Accommodation',
  'FoodAndBeverages' = 'FoodAndBeverages'
}
export enum EnumRatePlanModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
