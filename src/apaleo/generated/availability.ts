/** Generate by swagger-axios-codegen */
// tslint:disable
/* eslint-disable */
import axiosStatic, { AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export class AvailabilityService {
  /**
   * Get a list of all available unit groups in a property
   */
  static availabilityUnitGroupsGet(
    params: {
      /** The property id */
      propertyId: string;
      /** First day of the requested time period. The given day will be included in the response. */
      from: string;
      /** Last day of the requested time period. The given day will be included in the response. */
      to: string;
      /** The time slice template, defaults to 'over night' */
      timeSliceTemplate?: string;
      /** Filter result by requested unit group types */
      unitGroupTypes?: string[];
      /** The time slice definition ids */
      timeSliceDefinitionIds?: string[];
      /** The unit group ids */
      unitGroupIds?: string[];
      /** The number of adults you want availability for, defaults to 1 */
      adults?: number;
      /** The ages of the children you want availability for */
      childrenAges?: number[];
      /** When set to 'true', only the unit groups sold by the specified time slice template and time slice definition ids are returned,
otherwise all unit groups are returned */
      onlySellable?: boolean;
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AvailableUnitGroupListModel> {
    return new Promise((resolve, reject) => {
      let url = '/availability/v1/unit-groups';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        propertyId: params['propertyId'],
        from: params['from'],
        to: params['to'],
        timeSliceTemplate: params['timeSliceTemplate'],
        unitGroupTypes: params['unitGroupTypes'],
        timeSliceDefinitionIds: params['timeSliceDefinitionIds'],
        unitGroupIds: params['unitGroupIds'],
        adults: params['adults'],
        childrenAges: params['childrenAges'],
        onlySellable: params['onlySellable'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }

  /**
   * Allows to modify the unit group availability
   */
  static availabilityUnitGroupsByIdPatch(
    params: {
      /** Id of the unit group to be modified. */
      id: string;
      /** First day of the time period from which availability will be modified */
      from: string;
      /** Last day of the time period until which availability will be modified */
      to: string;
      /** The time slice template */
      timeSliceTemplate: string;
      /** Define the list of operations to be applied to the resource. Learn more about JSON Patch here: http://jsonpatch.com/. */
      body: Operation[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/availability/v0-nsfw/unit-groups/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);
      configs.params = { from: params['from'], to: params['to'], timeSliceTemplate: params['timeSliceTemplate'] };
      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmbeddedUnitGroupModel {
  /** The unit group id */

  id: string;

  /** The code for the unit group that can be shown in reports and table views */

  code: string;

  /** The name for the unit group */

  name: string;

  /** The description for the unit group */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class MaintenanceModel {
  /** The number of units which are out of service */

  outOfService: number;

  /** The number of units which are out of order */

  outOfOrder: number;

  /** The number of units which are out of inventory */

  outOfInventory: number;

  constructor(data: undefined | any = {}) {
    this['outOfService'] = data['outOfService'];
    this['outOfOrder'] = data['outOfOrder'];
    this['outOfInventory'] = data['outOfInventory'];
  }
}

export class BlockUnitsModel {
  /** The number of units which are definitely blocked */

  definite: number;

  /** The number of units which are tentatively blocked */

  tentative: number;

  /** The number of units which are picked from blocked */

  picked: number;

  /** The number of units which are not yet picked */

  remaining: number;

  constructor(data: undefined | any = {}) {
    this['definite'] = data['definite'];
    this['tentative'] = data['tentative'];
    this['picked'] = data['picked'];
    this['remaining'] = data['remaining'];
  }
}

export class PropertyAvailabilityModel {
  /** The number of units physically existing on the property */

  physicalCount: number;

  /** The number of units physically existing excluding the ones which are out of inventory */

  houseCount: number;

  /** The number of sold units including units picked up from blocks */

  soldCount: number;

  /** The percent value indicating the occupancy */

  occupancy: number;

  /** The number of units available for selling. This is the house count excluding the out of order and the already sold units */

  sellableCount: number;

  /** The number of units which are allowed for overbooking */

  allowedOverbookingCount: number;

  /**  */

  maintenance: MaintenanceModel;

  /**  */

  block: BlockUnitsModel;

  constructor(data: undefined | any = {}) {
    this['physicalCount'] = data['physicalCount'];
    this['houseCount'] = data['houseCount'];
    this['soldCount'] = data['soldCount'];
    this['occupancy'] = data['occupancy'];
    this['sellableCount'] = data['sellableCount'];
    this['allowedOverbookingCount'] = data['allowedOverbookingCount'];
    this['maintenance'] = data['maintenance'];
    this['block'] = data['block'];
  }
}

export class UnitGroupAvailabilityItemModel {
  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /** The number of units physically existing on the property */

  physicalCount: number;

  /** The number of units physically existing excluding the ones which are out of inventory */

  houseCount: number;

  /** The number of sold units including units picked up from blocks */

  soldCount: number;

  /** The percent value indicating the occupancy */

  occupancy: number;

  /** The number of units still available. This is the house count excluding the out of order units minus
the already sold units. */

  availableCount: number;

  /** The number of units available for selling. This is the minimum of the sellable units on property level
and the available units of this unit group. If there are only 3 units available on the property level
but 5 units available for this unit group, the sellable count will be 3. This situation can occur if
another category or the whole house is overbooked. */

  sellableCount: number;

  /** The number of units allowed for overbooking. */

  allowedOverbookingCount: number;

  /**  */

  maintenance: MaintenanceModel;

  /**  */

  block: BlockUnitsModel;

  constructor(data: undefined | any = {}) {
    this['unitGroup'] = data['unitGroup'];
    this['physicalCount'] = data['physicalCount'];
    this['houseCount'] = data['houseCount'];
    this['soldCount'] = data['soldCount'];
    this['occupancy'] = data['occupancy'];
    this['availableCount'] = data['availableCount'];
    this['sellableCount'] = data['sellableCount'];
    this['allowedOverbookingCount'] = data['allowedOverbookingCount'];
    this['maintenance'] = data['maintenance'];
    this['block'] = data['block'];
  }
}

export class UnitGroupAvailabilityTimeSliceItemModel {
  /** Date and time the time slice begins<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** Date and time the time slice ends<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  /**  */

  property: PropertyAvailabilityModel;

  /** List of unit group availabilities for this time slice */

  unitGroups: UnitGroupAvailabilityItemModel[];

  constructor(data: undefined | any = {}) {
    this['from'] = data['from'];
    this['to'] = data['to'];
    this['property'] = data['property'];
    this['unitGroups'] = data['unitGroups'];
  }
}

export class AvailableUnitGroupListModel {
  /** List of time slices */

  timeSlices: UnitGroupAvailabilityTimeSliceItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['timeSlices'] = data['timeSlices'];
    this['count'] = data['count'];
  }
}

export class Operation {
  /**  */

  value: object;

  /**  */

  path: string;

  /**  */

  op: string;

  /**  */

  from: string;

  constructor(data: undefined | any = {}) {
    this['value'] = data['value'];
    this['path'] = data['path'];
    this['op'] = data['op'];
    this['from'] = data['from'];
  }
}
