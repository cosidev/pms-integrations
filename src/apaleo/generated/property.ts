/** Generate by swagger-axios-codegen */
// tslint:disable
/* eslint-disable */
import axiosStatic, { AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType,
  };
  return configs;
}

export class PropertyService {
  /**
   * Get a properties list
   */
  static inventoryPropertiesGet(
    params: {
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<PropertyListModel> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/properties';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { pageNumber: params['pageNumber'], pageSize: params['pageSize'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a property
   */
  static inventoryPropertiesByIdGet(
    params: {
      /** The id of the property. */
      id: string;
      /** 'all' or comma separated list of two-letter language codes (ISO Alpha-2) */
      languages?: string[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<PropertyModel> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/properties/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { languages: params['languages'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class UnitService {
  /**
   * Allows to patch unit
   */
  static inventoryUnitsByIdPatch(
    params: {
      /** The id of the unit. */
      id: string;
      /** Define the list of operations to be applied to the resource. Learn more about JSON Patch here: http://jsonpatch.com/. */
      body: Operation[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/units/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params['body'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a unit
   */
  static inventoryUnitsByIdGet(
    params: {
      /** The id of the unit. */
      id: string;
      /** 'all' or comma separated list of two-letter language codes (ISO Alpha-2) */
      languages?: string[];
      /** List of all embedded resources that should be expanded in the response. Possible values are: property, unitGroup. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UnitModel> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/units/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { languages: params['languages'], expand: params['expand'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a units list
   */
  static inventoryUnitsGet(
    params: {
      /** Return units for specific property */
      propertyId?: string;
      /** Return units for the specific unit group */
      unitGroupId?: string;
      /** Return only occupied or vacant units */
      isOccupied?: boolean;
      /** Return units with the specific maintenance type */
      maintenanceType?: string;
      /** Return units with a specific condition */
      condition?: string;
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: property, unitGroup. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {},
  ): Promise<UnitListModel> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/units';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        propertyId: params['propertyId'],
        unitGroupId: params['unitGroupId'],
        isOccupied: params['isOccupied'],
        maintenanceType: params['maintenanceType'],
        condition: params['condition'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand'],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class UnitGroupService {
  /**
   * Get all unit groups, or all unit groups for a property
   */
  static inventoryUnitGroupsGet(
    params: {
      /** Return unit groups for specific property */
      propertyId?: string;
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: property. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UnitGroupListModel> {
    return new Promise((resolve, reject) => {
      let url = '/inventory/v1/unit-groups';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        propertyId: params['propertyId'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class AddressModel {
  /**  */

  addressLine1: string;

  /**  */

  addressLine2: string;

  /**  */

  postalCode: string;

  /**  */

  city: string;

  /**  */

  countryCode: string;

  constructor(data: undefined | any = {}) {
    this['addressLine1'] = data['addressLine1'];
    this['addressLine2'] = data['addressLine2'];
    this['postalCode'] = data['postalCode'];
    this['city'] = data['city'];
    this['countryCode'] = data['countryCode'];
  }
}

export class BankAccountModel {
  /**  */

  iban: string;

  /**  */

  bic: string;

  /**  */

  bank: string;

  constructor(data: undefined | any = {}) {
    this['iban'] = data['iban'];
    this['bic'] = data['bic'];
    this['bank'] = data['bank'];
  }
}

export class PropertyItemModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The id of the property used as a template while creating the property */

  propertyTemplateId: string;

  /** Whether the property can be used as a template for other properties */

  isTemplate: boolean;

  /** The name for the property */

  name: string;

  /** The description for the property */

  description: string;

  /** The legal name of the company running the property. */

  companyName: string;

  /** The managing director(s) of the company, as they should appear on invoices */

  managingDirectors: string;

  /** The entry in the Commercial Register of the company running the property, as it should appear on invoices */

  commercialRegisterEntry: string;

  /** The Tax-ID of the company running the property, as it should appear on invoices */

  taxId: string;

  /**  */

  location: AddressModel;

  /**  */

  bankAccount: BankAccountModel;

  /** The payment terms used for all rate plans */

  paymentTerms: object;

  /** The time zone name of the property from the IANA Time Zone Database.
(see: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) */

  timeZone: string;

  /** The currency a property works with. */

  currencyCode: string;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['propertyTemplateId'] = data['propertyTemplateId'];
    this['isTemplate'] = data['isTemplate'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['companyName'] = data['companyName'];
    this['managingDirectors'] = data['managingDirectors'];
    this['commercialRegisterEntry'] = data['commercialRegisterEntry'];
    this['taxId'] = data['taxId'];
    this['location'] = data['location'];
    this['bankAccount'] = data['bankAccount'];
    this['paymentTerms'] = data['paymentTerms'];
    this['timeZone'] = data['timeZone'];
    this['currencyCode'] = data['currencyCode'];
    this['created'] = data['created'];
  }
}

export class PropertyListModel {
  /** List of properties */

  properties: PropertyItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['properties'] = data['properties'];
    this['count'] = data['count'];
  }
}

export class PropertyModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The id of the property used as a template while creating the property */

  propertyTemplateId: string;

  /** Whether the property can be used as a template for other properties */

  isTemplate: boolean;

  /** The name for the property */

  name: object;

  /** The description for the property */

  description: object;

  /** The legal name of the company running the property. */

  companyName: string;

  /** The managing director(s) of the company, as they should appear on invoices */

  managingDirectors: string;

  /** The entry in the Commercial Register of the company running the property, as it should appear on invoices */

  commercialRegisterEntry: string;

  /** The Tax-ID of the company running the property, as it should appear on invoices */

  taxId: string;

  /**  */

  location: AddressModel;

  /**  */

  bankAccount: BankAccountModel;

  /** The payment terms used for all rate plans */

  paymentTerms: object;

  /** The time zone name of the property from the IANA Time Zone Database.
(see: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) */

  timeZone: string;

  /** The currency a property works with. */

  currencyCode: string;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['propertyTemplateId'] = data['propertyTemplateId'];
    this['isTemplate'] = data['isTemplate'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['companyName'] = data['companyName'];
    this['managingDirectors'] = data['managingDirectors'];
    this['commercialRegisterEntry'] = data['commercialRegisterEntry'];
    this['taxId'] = data['taxId'];
    this['location'] = data['location'];
    this['bankAccount'] = data['bankAccount'];
    this['paymentTerms'] = data['paymentTerms'];
    this['timeZone'] = data['timeZone'];
    this['currencyCode'] = data['currencyCode'];
    this['created'] = data['created'];
  }
}

export class Operation {
  /**  */

  value: object;

  /**  */

  path: string;

  /**  */

  op: string;

  /**  */

  from: string;

  constructor(data: undefined | any = {}) {
    this['value'] = data['value'];
    this['path'] = data['path'];
    this['op'] = data['op'];
    this['from'] = data['from'];
  }
}

export class EmbeddedPropertyModel {
  /** The property id */

  id: string;

  /** The code for the property that can be shown in reports and table views */

  code: string;

  /** The name for the property */

  name: string;

  /** The description for the property */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class EmbeddedUnitGroupModel {
  /** The unit group id */

  id: string;

  /** The code for the unit group that can be shown in reports and table views */

  code: string;

  /** The name for the unit group */

  name: string;

  /** The description for the unit group */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
  }
}

export class UnitMaintenanceModel {
  /** The id for the scheduled maintenance */

  id: string;

  /** Date and time the scheduled maintenance window starts<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** Date and time the scheduled maintenance window ends<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  /** The type of maintenance that is planned for the unit. A small repair (OutOfService),
a bigger disfunction that does not allow to sell the unit (OutOfOrder) or is it
even under construction and should reduce the house count (OutOfInventory) */

  type: EnumUnitMaintenanceModelType;

  /** The description text for the maintenance */

  description: string;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['from'] = data['from'];
    this['to'] = data['to'];
    this['type'] = data['type'];
    this['description'] = data['description'];
  }
}

export class UnitStatusModel {
  /**  */

  isOccupied: boolean;

  /**  */

  condition: EnumUnitStatusModelCondition;

  /**  */

  maintenance: UnitMaintenanceModel;

  constructor(data: undefined | any = {}) {
    this['isOccupied'] = data['isOccupied'];
    this['condition'] = data['condition'];
    this['maintenance'] = data['maintenance'];
  }
}

export class UnitModel {
  /** The unit id */

  id: string;

  /** The name for the unit */

  name: string;

  /** The description for the unit */

  description: object;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  status: UnitStatusModel;

  /** Maximum number of persons for the unit */

  maxPersons: number;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['property'] = data['property'];
    this['unitGroup'] = data['unitGroup'];
    this['status'] = data['status'];
    this['maxPersons'] = data['maxPersons'];
    this['created'] = data['created'];
  }
}

export class UnitItemMaintenanceModel {
  /** The id for the scheduled maintenance */

  id: string;

  /** The type of maintenance that is planned for the unit. A small repair (OutOfService),
a bigger disfunction that does not allow to sell the unit (OutOfOrder) or is it
even under construction and should reduce the house count (OutOfInventory) */

  type: EnumUnitItemMaintenanceModelType;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['type'] = data['type'];
  }
}

export class UnitItemStatusModel {
  /**  */

  isOccupied: boolean;

  /**  */

  condition: EnumUnitItemStatusModelCondition;

  /**  */

  maintenance: UnitItemMaintenanceModel;

  constructor(data: undefined | any = {}) {
    this['isOccupied'] = data['isOccupied'];
    this['condition'] = data['condition'];
    this['maintenance'] = data['maintenance'];
  }
}

export class UnitItemModel {
  /** The unit id */

  id: string;

  /** The name for the unit */

  name: string;

  /** The description for the unit */

  description: string;

  /**  */

  property: EmbeddedPropertyModel;

  /**  */

  unitGroup: EmbeddedUnitGroupModel;

  /**  */

  status: UnitItemStatusModel;

  /** Maximum number of persons for the unit */

  maxPersons: number;

  /** Date of creation<br />A date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['property'] = data['property'];
    this['unitGroup'] = data['unitGroup'];
    this['status'] = data['status'];
    this['maxPersons'] = data['maxPersons'];
    this['created'] = data['created'];
  }
}

export class UnitListModel {
  /** List of units */

  units: UnitItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['units'] = data['units'];
    this['count'] = data['count'];
  }
}

export class UnitGroupItemModel {
  /** The unit group id */

  id: string;

  /** The code for the unit group that can be shown in reports and table views */

  code: string;

  /** The name for the unit group */

  name: string;

  /** The description for the unit group */

  description: string;

  /** Number of units in this group */

  memberCount: number;

  /** Maximum number of persons for the unit group */

  maxPersons: number;

  /** The unit group rank */

  rank: number;

  /** The unit group type */

  type: EnumUnitGroupItemModelType;

  /**  */

  property: EmbeddedPropertyModel;

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['description'] = data['description'];
    this['memberCount'] = data['memberCount'];
    this['maxPersons'] = data['maxPersons'];
    this['rank'] = data['rank'];
    this['type'] = data['type'];
    this['property'] = data['property'];
  }
}

export class UnitGroupListModel {
  /** List of unit groups */

  unitGroups: UnitGroupItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['unitGroups'] = data['unitGroups'];
    this['count'] = data['count'];
  }
}
export enum EnumUnitGroupItemModelType {
  'BedRoom' = 'BedRoom',
  'MeetingRoom' = 'MeetingRoom',
  'EventSpace' = 'EventSpace',
  'ParkingLot' = 'ParkingLot',
  'Other' = 'Other'
}

export enum EnumUnitMaintenanceModelType {
  'OutOfService' = 'OutOfService',
  'OutOfOrder' = 'OutOfOrder',
  'OutOfInventory' = 'OutOfInventory'
}
export enum EnumUnitStatusModelCondition {
  'Clean' = 'Clean',
  'CleanToBeInspected' = 'CleanToBeInspected',
  'Dirty' = 'Dirty',
}
export enum EnumUnitItemMaintenanceModelType {
  'OutOfService' = 'OutOfService',
  'OutOfOrder' = 'OutOfOrder',
  'OutOfInventory' = 'OutOfInventory',
}
export enum EnumUnitItemStatusModelCondition {
  'Clean' = 'Clean',
  'CleanToBeInspected' = 'CleanToBeInspected',
  'Dirty' = 'Dirty'
}
