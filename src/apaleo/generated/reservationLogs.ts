/** Generate by swagger-axios-codegen */
import axiosStatic, { AxiosPromise, AxiosInstance } from 'axios';

export interface IRequestOptions {
  headers?: any;
  baseURL?: string;
  responseType?: string;
}

interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void) {
  const req = serviceOptions.axios ? serviceOptions.axios.request(configs) : axiosStatic(configs);

  return req
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    });
}

function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export class BookingLogsService {
  /**
   * Returns reservation change log entries sorted by the timestamp.
   */
  static logsBookingReservationGet(
    params: {
      /** Filter the log entries by reservation IDs */
      reservationIds?: string[];
      /** Filter the log entries by event types. */
      eventTypes?: string[];
      /** Filter the log entries by property IDs */
      propertyIds?: string[];
      /** Filter the log entries by subject IDs (which user or application triggered this event) */
      subjectIds?: string[];
      /** Filter by event date and time<br />You can provide an array of string expressions which all need to apply.<br />Each expression has the form of 'OPERATION_VALUE' where VALUE needs to be of the valid format of the property type and OPERATION can be:<br />'eq' for equals<br />'neq' for not equals<br />'lt' for less than<br />'gt' for greater than<br />'lte' for less than or equals<br />'gte' for greater than or equals<br />For instance<br />'eq_5' would mean the value should equal 5<br />'lte_7' would mean the value should be less than or equal to 7 */
      dateFilter?: string[];
      /** Page number, starting from 1 and defaulting to 1. Results in 204 if there are no items on that page. */
      pageNumber?: number;
      /** Page size. If this is not set, the pageNumber will be ignored and all values returned. */
      pageSize?: number;
      /** List of all embedded resources that should be expanded in the response. Possible values are: changes. All other values will be silently ignored. */
      expand?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservationChangeLogListModel> {
    return new Promise((resolve, reject) => {
      let url = '/logs/v0-nsfw/booking/reservation';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        reservationIds: params['reservationIds'],
        eventTypes: params['eventTypes'],
        propertyIds: params['propertyIds'],
        subjectIds: params['subjectIds'],
        dateFilter: params['dateFilter'],
        pageNumber: params['pageNumber'],
        pageSize: params['pageSize'],
        expand: params['expand']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReservationChangeLogListModel {
  /** List of the log entries. */

  logEntries: ReservationChangeLogItemModel[];

  /** Total count of items */

  count: number;

  constructor(data: undefined | any = {}) {
    this['logEntries'] = data['logEntries'];
    this['count'] = data['count'];
  }
}

export class ReservationChangeLogItemModel {
  /** The ID of the reservation */

  reservationId: string;

  /** The type of the operation */

  eventType: EnumReservationChangeLogItemModelEventType;

  /** The list of changes */

  changes: ReservationChangeModel[];

  /** The ID of the property */

  propertyId: string;

  /** Date and time when the operation has been executed<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  created: Date;

  /** The ID of the user or client that triggered this event */

  subjectId: string;

  constructor(data: undefined | any = {}) {
    this['reservationId'] = data['reservationId'];
    this['eventType'] = data['eventType'];
    this['changes'] = data['changes'];
    this['propertyId'] = data['propertyId'];
    this['created'] = data['created'];
    this['subjectId'] = data['subjectId'];
  }
}

export class ReservationChangeModel {
  /** The type of the change */

  changeType: EnumReservationChangeModelChangeType;

  /** The reservation that was created */

  reservationAddedValue: ReservationAddedChangeModel;

  /** Updated reservation */

  newReservationChangedValue: ReservationChangedChangeModel;

  /** Previous reservation */

  oldReservationChangedValue: ReservationChangedChangeModel;

  /** Updated additional guest */

  newAdditionalGuestValue: ReservationPersonChangeModel;

  /** Previous additional guest */

  oldAdditionalGuestValue: ReservationPersonChangeModel;

  /** Updated time slice */

  newTimeSliceValue: ReservationTimeSliceChangeModel;

  /** Previous time slice */

  oldTimeSliceValue: ReservationTimeSliceChangeModel;

  /** Updated extra service */

  newExtraServiceValue: ReservationServiceChangeModel;

  /** Previous extra service */

  oldExtraServiceValue: ReservationServiceChangeModel;

  /** Updated validation message */

  newValidationMessageValue: ReservationValidationMessageChangeModel;

  /** Previous validation message */

  oldValidationMessageValue: ReservationValidationMessageChangeModel;

  constructor(data: undefined | any = {}) {
    this['changeType'] = data['changeType'];
    this['reservationAddedValue'] = data['reservationAddedValue'];
    this['newReservationChangedValue'] = data['newReservationChangedValue'];
    this['oldReservationChangedValue'] = data['oldReservationChangedValue'];
    this['newAdditionalGuestValue'] = data['newAdditionalGuestValue'];
    this['oldAdditionalGuestValue'] = data['oldAdditionalGuestValue'];
    this['newTimeSliceValue'] = data['newTimeSliceValue'];
    this['oldTimeSliceValue'] = data['oldTimeSliceValue'];
    this['newExtraServiceValue'] = data['newExtraServiceValue'];
    this['oldExtraServiceValue'] = data['oldExtraServiceValue'];
    this['newValidationMessageValue'] = data['newValidationMessageValue'];
    this['oldValidationMessageValue'] = data['oldValidationMessageValue'];
  }
}

export class ReservationAddedChangeModel {
  /** Date of arrival<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: Date;

  /** Date of departure<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: Date;

  /** Status of the reservation */

  status: EnumReservationAddedChangeModelStatus;

  /** Block id */

  blockId: string;

  /** Total amount */

  totalGrossAmount: number;

  /** Number of adults */

  adults: number;

  /** Additional information and comments */

  comment: string;

  /** Additional information and comment by the guest */

  guestComment: string;

  /** Code in external system */

  externalCode: string;

  /** Channel code */

  channelCode: EnumReservationAddedChangeModelChannelCode;

  /** Source of the reservation (e.g Hotels.com, Orbitz, etc.) */

  source: string;

  /** The primary guest of the reservation */

  primaryGuest: ReservationPersonChangeModel;

  /** The ages of the children */

  childrenAges: number[];

  /** Additional guests of the reservation. */

  additionalGuests: ReservationPersonChangeModel[];

  /** Payment information */

  paymentAccount: ReservationPaymentAccountChangeModel;

  /** The list of time slices */

  timeSlices: ReservationTimeSliceChangeModel[];

  /** The list of additional services (extras, add-ons) reserved for the stay */

  extraServices: ReservationServiceChangeModel[];

  /** The strongest guarantee for the rate plans booked in this reservation */

  guaranteeType: EnumReservationAddedChangeModelGuaranteeType;

  /** Details about the cancellation fee for this reservation */

  cancellationFee: ReservationCancellationFeeChangeModel;

  /** Details about the no-show fee for this reservation */

  noShowFee: ReservationNoShowFeeChangeModel;

  /** The purpose of the trip, leisure or business */

  travelPurpose: EnumReservationAddedChangeModelTravelPurpose;

  /** The amount that needs to be pre-paid. */

  prePaymentAmount: number;

  /** The list of validation messages */

  validationMessages: ReservationValidationMessageChangeModel[];

  /** Company id for this reservation */

  companyId: string;

  constructor(data: undefined | any = {}) {
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['status'] = data['status'];
    this['blockId'] = data['blockId'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['adults'] = data['adults'];
    this['comment'] = data['comment'];
    this['guestComment'] = data['guestComment'];
    this['externalCode'] = data['externalCode'];
    this['channelCode'] = data['channelCode'];
    this['source'] = data['source'];
    this['primaryGuest'] = data['primaryGuest'];
    this['childrenAges'] = data['childrenAges'];
    this['additionalGuests'] = data['additionalGuests'];
    this['paymentAccount'] = data['paymentAccount'];
    this['timeSlices'] = data['timeSlices'];
    this['extraServices'] = data['extraServices'];
    this['guaranteeType'] = data['guaranteeType'];
    this['cancellationFee'] = data['cancellationFee'];
    this['noShowFee'] = data['noShowFee'];
    this['travelPurpose'] = data['travelPurpose'];
    this['prePaymentAmount'] = data['prePaymentAmount'];
    this['validationMessages'] = data['validationMessages'];
    this['companyId'] = data['companyId'];
  }
}

export class ReservationChangedChangeModel {
  /** Date of arrival<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  arrival: Date;

  /** Date of departure<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  departure: Date;

  /** Number of adults */

  adults: number;

  /** Value of ages of the children */

  childrenAges: number[];

  /** Comment */

  comment: string;

  /** Guest comment */

  guestComment: string;

  /** Total amount */

  totalGrossAmount: number;

  /** Primary guest */

  primaryGuest: ReservationPersonChangeModel;

  /** Payment account */

  paymentAccount: ReservationPaymentAccountChangeModel;

  /** Guarantee type */

  guaranteeType: EnumReservationChangedChangeModelGuaranteeType;

  /** Travel purpose */

  travelPurpose: EnumReservationChangedChangeModelTravelPurpose;

  /** Company id for this reservation */

  companyId: string;

  /** The no-show fee for this reservation */

  noShowFee: number;

  /** The cancellation fee for this reservation */

  cancellationFee: ReservationCancellationFeeChangeModel;

  /** The travel agent commission amount. */

  commissionAmount: number;

  constructor(data: undefined | any = {}) {
    this['arrival'] = data['arrival'];
    this['departure'] = data['departure'];
    this['adults'] = data['adults'];
    this['childrenAges'] = data['childrenAges'];
    this['comment'] = data['comment'];
    this['guestComment'] = data['guestComment'];
    this['totalGrossAmount'] = data['totalGrossAmount'];
    this['primaryGuest'] = data['primaryGuest'];
    this['paymentAccount'] = data['paymentAccount'];
    this['guaranteeType'] = data['guaranteeType'];
    this['travelPurpose'] = data['travelPurpose'];
    this['companyId'] = data['companyId'];
    this['noShowFee'] = data['noShowFee'];
    this['cancellationFee'] = data['cancellationFee'];
    this['commissionAmount'] = data['commissionAmount'];
  }
}

export class ReservationPersonChangeModel {
  /** Title of the guest */

  title: EnumReservationPersonChangeModelTitle;

  /** Gender of the booker */

  gender: EnumReservationPersonChangeModelGender;

  /** First name of the guest */

  firstName: string;

  /** Middle initial of the guest */

  middleInitial: string;

  /** Last name of the guest */

  lastName: string;

  /** Email address of the guest */

  email: string;

  /** Phone number of the guest */

  phone: string;

  /** Address of the guest */

  address: ReservationAddressChangeModel;

  /** The guest's nationality, in ISO 3166-1 alpha-2 code */

  nationalityCountryCode: string;

  /** The guest's identification number for the given identificationType. */

  identificationNumber: string;

  /** The type of the identificationNumber */

  identificationType: EnumReservationPersonChangeModelIdentificationType;

  /** Two-letter code (ISO Alpha-2) of a language preferred for contact */

  preferredLanguage: string;

  /** Guest's birthdate */

  birthDate: Date;

  /** The company of the guest, which will be displayed on the invoice. */

  company: ReservationCompanyChangeModel;

  constructor(data: undefined | any = {}) {
    this['title'] = data['title'];
    this['gender'] = data['gender'];
    this['firstName'] = data['firstName'];
    this['middleInitial'] = data['middleInitial'];
    this['lastName'] = data['lastName'];
    this['email'] = data['email'];
    this['phone'] = data['phone'];
    this['address'] = data['address'];
    this['nationalityCountryCode'] = data['nationalityCountryCode'];
    this['identificationNumber'] = data['identificationNumber'];
    this['identificationType'] = data['identificationType'];
    this['preferredLanguage'] = data['preferredLanguage'];
    this['birthDate'] = data['birthDate'];
    this['company'] = data['company'];
  }
}

export class ReservationTimeSliceChangeModel {
  /** The rate plan id for this time slice */

  ratePlanId: string;

  /** The start date and time for this time slice<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  from: Date;

  /** The end date and time for this time slice<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  to: Date;

  /** The assigned unit id for this time slice */

  unitId: string;

  /** The price for this time slice without included services */

  amount: AmountModel;

  constructor(data: undefined | any = {}) {
    this['ratePlanId'] = data['ratePlanId'];
    this['from'] = data['from'];
    this['to'] = data['to'];
    this['unitId'] = data['unitId'];
    this['amount'] = data['amount'];
  }
}

export class ReservationServiceChangeModel {
  /** The id of the service booked */

  serviceId: string;

  /** The number of services booked for each service date */

  count: number;

  /** The amount for the service for each service date */

  amount: AmountModel;

  /** The total amount for this service for the whole stay */

  totalAmount: AmountModel;

  /** The service date */

  serviceDate: Date;

  constructor(data: undefined | any = {}) {
    this['serviceId'] = data['serviceId'];
    this['count'] = data['count'];
    this['amount'] = data['amount'];
    this['totalAmount'] = data['totalAmount'];
    this['serviceDate'] = data['serviceDate'];
  }
}

export class ReservationValidationMessageChangeModel {
  /** The message category */

  category: EnumReservationValidationMessageChangeModelCategory;

  /** The message code */

  code: EnumReservationValidationMessageChangeModelCode;

  /** The message description */

  message: string;

  constructor(data: undefined | any = {}) {
    this['category'] = data['category'];
    this['code'] = data['code'];
    this['message'] = data['message'];
  }
}

export class ReservationPaymentAccountChangeModel {
  /** The account number (e.g. masked credit card number or last 4 digits) */

  accountNumber: string;

  /** The account holder (e.g. card holder) */

  accountHolder: string;

  /** The credit card's expiration month */

  expiryMonth: string;

  /** The credit card's expiration year */

  expiryYear: string;

  /** The payment method (e.g. visa) */

  paymentMethod: string;

  /** The email address of the shopper / customer */

  payerEmail: string;

  /** Indicates if the payment account is a virtual credit card */

  isVirtual: boolean;

  /** Indicates if the payment account can be used for capturing payments. A payment account is active, when it has a valid payer reference set */

  isActive: boolean;

  constructor(data: undefined | any = {}) {
    this['accountNumber'] = data['accountNumber'];
    this['accountHolder'] = data['accountHolder'];
    this['expiryMonth'] = data['expiryMonth'];
    this['expiryYear'] = data['expiryYear'];
    this['paymentMethod'] = data['paymentMethod'];
    this['payerEmail'] = data['payerEmail'];
    this['isVirtual'] = data['isVirtual'];
    this['isActive'] = data['isActive'];
  }
}

export class ReservationCancellationFeeChangeModel {
  /** The date and time the cancellation fee will be due. After that time this fee will
be charged in case of cancellation<br />Specify a date and time (without fractional second part) in UTC or with UTC offset as defined in <a href="https://en.wikipedia.org/wiki/ISO_8601">ISO8601:2004</a> */

  dueDateTime: Date;

  /** The fee amount */

  fee: number;

  constructor(data: undefined | any = {}) {
    this['dueDateTime'] = data['dueDateTime'];
    this['fee'] = data['fee'];
  }
}

export class ReservationNoShowFeeChangeModel {
  /** The fee amount */

  fee: number;

  constructor(data: undefined | any = {}) {
    this['fee'] = data['fee'];
  }
}

export class ReservationAddressChangeModel {
  /**  */

  addressLine1: string;

  /**  */

  addressLine2: string;

  /**  */

  postalCode: string;

  /**  */

  city: string;

  /**  */

  countryCode: string;

  constructor(data: undefined | any = {}) {
    this['addressLine1'] = data['addressLine1'];
    this['addressLine2'] = data['addressLine2'];
    this['postalCode'] = data['postalCode'];
    this['city'] = data['city'];
    this['countryCode'] = data['countryCode'];
  }
}

export class ReservationCompanyChangeModel {
  /** Name of the company */

  name: string;

  /** Tax or Vat ID of the company */

  taxId: string;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['taxId'] = data['taxId'];
  }
}

export class AmountModel {
  /**  */

  grossAmount: number;

  /**  */

  netAmount: number;

  /**  */

  vatType: EnumAmountModelVatType;

  /**  */

  currency: string;

  constructor(data: undefined | any = {}) {
    this['grossAmount'] = data['grossAmount'];
    this['netAmount'] = data['netAmount'];
    this['vatType'] = data['vatType'];
    this['currency'] = data['currency'];
  }
}
export enum EnumReservationChangeLogItemModelEventType {
  'Created' = 'Created',
  'Amended' = 'Amended',
  'GuestDataModified' = 'GuestDataModified',
  'CheckedIn' = 'CheckedIn',
  'CheckedOut' = 'CheckedOut',
  'Canceled' = 'Canceled',
  'SetToNoShow' = 'SetToNoShow',
  'CityTaxAdded' = 'CityTaxAdded',
  'CityTaxRemoved' = 'CityTaxRemoved',
  'UnitAssigned' = 'UnitAssigned',
  'UnitUnassigned' = 'UnitUnassigned',
  'PaymentAccountSet' = 'PaymentAccountSet',
  'PaymentAccountRemoved' = 'PaymentAccountRemoved',
  'InvoiceStatusChanged' = 'InvoiceStatusChanged',
  'Changed' = 'Changed'
}
export enum EnumReservationChangeModelChangeType {
  'ReservationAdded' = 'ReservationAdded',
  'AdditionalGuestAdded' = 'AdditionalGuestAdded',
  'AdditionalGuestRemoved' = 'AdditionalGuestRemoved',
  'AdditionalGuestChanged' = 'AdditionalGuestChanged',
  'TimeSliceAdded' = 'TimeSliceAdded',
  'TimeSliceRemoved' = 'TimeSliceRemoved',
  'TimeSliceChanged' = 'TimeSliceChanged',
  'ExtraServiceAdded' = 'ExtraServiceAdded',
  'ExtraServiceRemoved' = 'ExtraServiceRemoved',
  'ExtraServiceChanged' = 'ExtraServiceChanged',
  'ValidationMessageAdded' = 'ValidationMessageAdded',
  'ValidationMessageRemoved' = 'ValidationMessageRemoved',
  'ReservationChanged' = 'ReservationChanged'
}
export enum EnumReservationAddedChangeModelStatus {
  'Confirmed' = 'Confirmed',
  'InHouse' = 'InHouse',
  'CheckedOut' = 'CheckedOut',
  'Canceled' = 'Canceled',
  'NoShow' = 'NoShow'
}
export enum EnumReservationAddedChangeModelChannelCode {
  'Direct' = 'Direct',
  'BookingCom' = 'BookingCom',
  'Ibe' = 'Ibe',
  'ChannelManager' = 'ChannelManager',
  'Expedia' = 'Expedia'
}
export enum EnumReservationAddedChangeModelGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company',
  'Ota' = 'Ota'
}
export enum EnumReservationAddedChangeModelTravelPurpose {
  'Business' = 'Business',
  'Leisure' = 'Leisure'
}
export enum EnumReservationChangedChangeModelGuaranteeType {
  'PM6Hold' = 'PM6Hold',
  'CreditCard' = 'CreditCard',
  'Prepayment' = 'Prepayment',
  'Company' = 'Company',
  'Ota' = 'Ota'
}
export enum EnumReservationChangedChangeModelTravelPurpose {
  'Business' = 'Business',
  'Leisure' = 'Leisure'
}
export enum EnumReservationPersonChangeModelTitle {
  'Mr' = 'Mr',
  'Ms' = 'Ms',
  'Dr' = 'Dr',
  'Prof' = 'Prof',
  'Other' = 'Other'
}
export enum EnumReservationPersonChangeModelGender {
  'Female' = 'Female',
  'Male' = 'Male',
  'Other' = 'Other'
}
export enum EnumReservationPersonChangeModelIdentificationType {
  'SocialInsuranceNumber' = 'SocialInsuranceNumber',
  'PassportNumber' = 'PassportNumber',
  'IdNumber' = 'IdNumber',
  'DriverLicenseNumber' = 'DriverLicenseNumber'
}
export enum EnumReservationValidationMessageChangeModelCategory {
  'OfferNotAvailable' = 'OfferNotAvailable',
  'AutoUnitAssignment' = 'AutoUnitAssignment'
}
export enum EnumReservationValidationMessageChangeModelCode {
  'UnitGroupFullyBooked' = 'UnitGroupFullyBooked',
  'UnitGroupCapacityExceeded' = 'UnitGroupCapacityExceeded',
  'RatePlanRestrictionsViolated' = 'RatePlanRestrictionsViolated',
  'RatePlanSurchargesNotSet' = 'RatePlanSurchargesNotSet',
  'RateRestrictionsViolated' = 'RateRestrictionsViolated',
  'RatePlanChannelNotSet' = 'RatePlanChannelNotSet',
  'RatesNotSet' = 'RatesNotSet',
  'BlockFullyBooked' = 'BlockFullyBooked',
  'UnitMoved' = 'UnitMoved',
  'IncludedServicesAmountExceededRateAmount' = 'IncludedServicesAmountExceededRateAmount',
  'RatePlanCompanyRestrictionsViolated' = 'RatePlanCompanyRestrictionsViolated'
}
export enum EnumAmountModelVatType {
  'Null' = 'Null',
  'VeryReduced' = 'VeryReduced',
  'Reduced' = 'Reduced',
  'Normal' = 'Normal',
  'Without' = 'Without',
  'Special' = 'Special'
}
