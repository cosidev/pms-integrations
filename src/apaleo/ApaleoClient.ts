import axios from 'axios';
import dateformat from 'dateformat';
import { Injectable } from '@nestjs/common';
import {
  ReservationService,
  serviceOptions as restaurantServiceOptions,
  ReservationActionsService,
  MonetaryValueModel,
  DesiredStayDetailsModel,
  ReservationListModel,
  AutoAssignedUnitListModel,
  ReservationModel,
  BookingModel,
  BookingService,
  CreateBookingModel,
  BookingCreatedModel,
} from './generated/reservation';
import {
  PropertyService,
  serviceOptions as propertyServiceOptions,
  UnitService,
  UnitListModel,
  UnitModel,
  UnitGroupService,
  UnitGroupListModel,
  PropertyListModel,
} from './generated/property';
import {
  InvoiceService,
  FolioService,
  serviceOptions as financeServiceOptions,
  FolioActionsService,
  Operation,
  FolioListModel,
  FolioModel,
  FolioPaymentsService,
  CreateCustomPaymentRequest,
  PaymentCreatedModel,
  CreateAccountPaymentRequest,
  CreateAuthorizationPaymentRequest,
} from './generated/finance';
import { RatePlanService, serviceOptions as rateplanServiceOptions, RatePlanModel, RatePlanListModel } from './generated/rateplan';
import { FinanceLogsService, serviceOptions as financeLogsServiceOptions } from './generated/logs';
import { BookingLogsService, serviceOptions as reservationLogsServiceOptions } from './generated/reservationLogs';
import { generalConfig } from '../config';
import JWTService from '../JWTService';
import logger from '../LoggingService';
import { AvailabilityService, AvailableUnitGroupListModel } from './generated/availability';

interface ApiConfig {
  headers: {
    Authorization: string;
    [key: string]: string;
  };
}

axios.defaults.baseURL = 'https://api.apaleo.com';
axios.defaults.paramsSerializer = (params: { [key: string]: any }) => {
  let queryString = '';
  Object.keys(params).forEach(key => {
    let value = params[key];
    if (!value) {
      return;
    }
    queryString += `${key}=`;
    if (Array.isArray(value)) {
      let listValue = '';
      value.forEach(val => (listValue += `${val},`));
      value = listValue.substr(0, listValue.length - 1);
    }
    queryString += `${value}&`;
  });
  queryString = queryString.substr(0, queryString.length - 1);
  return queryString;
};

restaurantServiceOptions.axios = axios;
propertyServiceOptions.axios = axios;
financeServiceOptions.axios = axios;
financeLogsServiceOptions.axios = axios;
reservationLogsServiceOptions.axios = axios;
rateplanServiceOptions.axios = axios;

@Injectable()
export class ApaleoClient {
  apiConfig?: ApiConfig;
  language: string = 'de';
  token: string;
  jwtService: JWTService;

  constructor() {
    this.jwtService = new JWTService();
  }

  async authenticate() {
    const credentials = Buffer.from(`${generalConfig.apaleo.clientId}:${generalConfig.apaleo.clientSecret}`).toString('base64');

    const headers = {
      Authorization: `Basic ${credentials}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    const result = await axios.post('https://identity.apaleo.com/connect/token', 'grant_type=client_credentials', { headers });
    return result.data.access_token;
  }

  async getApiConfig() {
    if (this.token) {
      const payload = this.jwtService.decodeToken(this.token);
      // Reset token if expired
      const timestamp = Date.now() / 1000;
      if (payload.exp && payload.exp < timestamp) {
        this.apiConfig = undefined;
      }
    }
    if (!this.apiConfig) {
      this.token = await this.authenticate();
      this.apiConfig = {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      };
    }
    this.apiConfig.headers['Accept-Language'] = this.language;
    return this.apiConfig;
  }

  async request(request: Promise<any>) {
    try {
      const result = await request;
      return result;
    } catch (e) {
      if (e.response) {
        logger.errorLog(`Could not get result from apaleo api`, { responseData: e.response?.data, apaleoResponse: e.response });
      }
      throw e;
    }
  }

  async getReservationById(id: string, expand?: string[]): Promise<ReservationModel> {
    const config = await this.getApiConfig();
    return this.request(ReservationService.bookingReservationsByIdGet({ id, expand }, config));
  }

  async getBookingById(id: string): Promise<BookingModel> {
    const config = await this.getApiConfig();
    return this.request(BookingService.bookingBookingsByIdGet({ id }, config));
  }

  async getAvailableUnitsForReservation(id: string, filter?: any) {
    const config = await this.getApiConfig();
    return this.request(ReservationService.bookingReservationsByIdAvailableUnitsGet({ id, ...filter }, config));
  }

  async getPropertyById(id: string) {
    const config = await this.getApiConfig();
    return this.request(PropertyService.inventoryPropertiesByIdGet({ id }, config));
  }

  async getProperties(filter?: any): Promise<PropertyListModel> {
    const config = await this.getApiConfig();
    return this.request(PropertyService.inventoryPropertiesGet(filter, config));
  }

  async markUnitAsCleaned(id: string) {
    const config = await this.getApiConfig();
    const body: any = [
      {
        op: 'replace',
        path: '/status/condition',
        value: 'Clean',
      },
    ];
    return this.request(UnitService.inventoryUnitsByIdPatch({ id, body }, config));
  }

  async getFolios(filter: any) {
    const config = await this.getApiConfig();
    return this.request(FolioService.financeFoliosGet(filter, config));
  }

  async getFoliosByReservationId(id: string, filter?: any): Promise<FolioListModel> {
    const config = await this.getApiConfig();
    return this.request(
      FolioService.financeFoliosGet(
        {
          reservationIds: [id],
          ...filter,
        },
        config,
      ),
    );
  }

  async getFolioById(id: string): Promise<FolioModel> {
    const config = await this.getApiConfig();
    return this.request(FolioService.financeFoliosByIdGet({ id }, config));
  }

  async createPDFInvoice(id: string, createForCompany: boolean, languageCode: string) {
    const config = await this.getApiConfig();
    return this.request(
      InvoiceService.financeInvoicesPost(
        {
          body: {
            folioId: id,
            createForCompany,
            languageCode,
          },
        },
        config,
      ),
    );
  }

  async getPDFInvoiceById(id: string) {
    const config = await this.getApiConfig();
    return this.request(
      InvoiceService.financeInvoicesByIdPdfGet(
        { id },
        {
          ...config,
          responseType: 'arraybuffer',
        },
      ),
    );
  }

  async getLastValidInvoicePDF(invoiceId: string): Promise<any> {
    const pdfInvoice = await this.getPDFInvoiceById(invoiceId);
    const pdfStringEncoded = new Buffer(pdfInvoice, 'binary').toString('base64');
    return { pdf: pdfStringEncoded };
  }

  async getLastValidInvoice(reservationId: string): Promise<any> {
    const config = await this.getApiConfig();

    const { invoices } = await this.request(
      InvoiceService.financeInvoicesGet(
        { reservationId },
        {
          ...config,
        },
      ),
    );

    if (invoices?.length > 0) {
      return invoices[invoices.length - 1];
    }

    return null;
  }

  async updateSimpleReservationbyId(id: string, path: string, value: any) {
    const request = [
      {
        from: '',
        op: 'add',
        path,
        value,
      },
    ];
    return this.request(this.updateReservationbyId(id, request));
  }

  async updateTravelPurposeByReservationId(id: string, purpose: string) {
    const request = [
      {
        from: '',
        op: 'replace',
        path: '/travelPurpose',
        value: purpose as any,
      },
    ];
    return this.request(this.updateReservationbyId(id, request));
  }

  async updateReservationbyId(id: string, body: Operation[]) {
    const config = await this.getApiConfig();
    return this.request(ReservationService.bookingReservationsByIdPatch({ id, body }, config));
  }

  async cancelReservation(id: string) {
    const config = await this.getApiConfig();
    return ReservationActionsService.bookingReservationActionsByIdCancelPut({ id }, config);
  }

  async updateBookingById(id: string, path: string, value: any) {
    const config = await this.getApiConfig();
    return this.request(
      BookingService.bookingBookingsByIdPatch(
        {
          id,
          body: [
            {
              from: '',
              op: 'add',
              path,
              value,
            },
          ],
        },
        config,
      ),
    );
  }

  async addCancellationFeeToFolio(folioId: string, fee: MonetaryValueModel) {
    const config = await this.getApiConfig();
    return this.request(
      FolioActionsService.financeFolioActionsByFolioIdCancellationFeePost(
        {
          folioId,
          body: fee,
        },
        config,
      ),
    );
  }

  async addPaymentToFolio(folioId: string, payment: CreateCustomPaymentRequest): Promise<PaymentCreatedModel> {
    const config = await this.getApiConfig();
    return this.request(FolioPaymentsService.financeFoliosByFolioIdPaymentsPost({ folioId, body: payment }, config));
  }

  async addPaymentByPaymentAccount(folioId: string, body: CreateAccountPaymentRequest): Promise<PaymentCreatedModel> {
    const config = await this.getApiConfig();
    const req = FolioPaymentsService.financeFoliosByFolioIdPaymentsByPaymentAccountPost(
      {
        folioId,
        body,
      },
      config,
    );
    return this.request(req);
  }

  async addPaymentByAuthorization(folioId: string, body: CreateAuthorizationPaymentRequest): Promise<PaymentCreatedModel> {
    const config = await this.getApiConfig();
    const req = FolioPaymentsService.financeFoliosByFolioIdPaymentsByAuthorizationPost(
      {
        folioId,
        body,
      },
      config,
    );
    return this.request(req);
  }

  async updateFolioById(id: string, request: Operation[]) {
    const config = await this.getApiConfig();
    return this.request(
      FolioService.financeFoliosByIdPatch(
        {
          id,
          body: request,
        },
        config,
      ),
    );
  }

  async assignRoomToReservation(id: string): Promise<AutoAssignedUnitListModel> {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdAssignUnitPut({ id }, config));
  }

  async assignFixedUnitToReservation(id: string, unitId: string) {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdAssignUnitByUnitIdPut({ id, unitId }, config));
  }

  async checkInReservationById(id: string) {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdCheckinPut({ id }, config));
  }

  async checkOutReservationById(id: string) {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdCheckoutPut({ id }, config));
  }

  async getThreeDaysArrivals(expand: string[]) {
    const threeDaysStart = new Date();
    threeDaysStart.setUTCHours(0, 0, 0);
    threeDaysStart.setDate(threeDaysStart.getDate() + 3);
    const threeDaysEnd = new Date();
    threeDaysEnd.setDate(threeDaysEnd.getDate() + 3);
    threeDaysEnd.setUTCHours(23, 59);
    return this.getReservationsbyTime(['Confirmed'], 'Arrival', threeDaysStart, threeDaysEnd, { expand });
  }

  async getRateplanById(id: string, filter?: any): Promise<RatePlanModel> {
    const config = await this.getApiConfig();
    return this.request(RatePlanService.rateplanRatePlansByIdGet({ id, ...filter }, config));
  }

  async getTodayDepartures(channelCode?: string[], expand?: string[]) {
    return this.getReservationsByDay(new Date(), ['InHouse', 'CheckedOut'], 'Departure', { channelCode, expand });
  }

  async getReservationsByDay(date: Date, status: string[], dateFilter: string, filter?: any): Promise<ReservationListModel> {
    const start = new Date(date.getTime());
    start.setUTCHours(0, 0, 0);
    const end = new Date(date.getTime());
    end.setUTCHours(23, 59);
    return this.getReservationsbyTime(status, dateFilter, start, end, filter);
  }

  async getReservations(params?: any): Promise<ReservationListModel> {
    const config = await this.getApiConfig();
    return this.request(ReservationService.bookingReservationsGet(params, config));
  }

  async getReservationsbyTime(status: string[], dateFilter: string, from: Date, to: Date, filter?: any): Promise<ReservationListModel> {
    const config = await this.getApiConfig();
    return this.request(
      ReservationService.bookingReservationsGet(
        {
          status,
          dateFilter,
          from: dateformat(from, 'isoUtcDateTime'),
          to: dateformat(to, 'isoUtcDateTime'),
          expand: ['booker'],
          ...filter,
        },
        config,
      ),
    );
  }

  async getUnits(params: object): Promise<UnitListModel> {
    const config = await this.getApiConfig();
    return this.request(UnitService.inventoryUnitsGet(params, config));
  }

  async getUnitById(id: string, filter?: any): Promise<UnitModel> {
    const config = await this.getApiConfig();
    return this.request(UnitService.inventoryUnitsByIdGet({ id, ...filter }, config));
  }

  async forceAmmendReservation(id: string, body: DesiredStayDetailsModel) {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdAmend$forcePut({ id, body }, config));
  }

  async ammendReservation(id: string, body: DesiredStayDetailsModel) {
    const config = await this.getApiConfig();
    return this.request(ReservationActionsService.bookingReservationActionsByIdAmendPut({ id, body }, config));
  }

  async getReservationLogs(id: string, filter?: any) {
    const config = await this.getApiConfig();
    return this.request(
      BookingLogsService.logsBookingReservationGet(
        {
          reservationIds: [id],
          ...filter,
        },
        config,
      ),
    );
  }

  async getFolioLogs(folioId: string, filter?: any) {
    const config = await this.getApiConfig();
    return this.request(
      FinanceLogsService.logsFinanceFolioGet(
        {
          folioIds: [folioId],
        },
        config,
      ),
    );
  }

  async addMidstayReservation(reservation: CreateBookingModel, force?: boolean): Promise<BookingCreatedModel> {
    const config = await this.getApiConfig();

    let BookingsCreator = BookingService.bookingBookingsPost;
    if (force) {
      BookingsCreator = BookingService.bookingBookings$forcePost;
    }

    return this.request(
      BookingsCreator(
        {
          body: {
            ...reservation,
          },
        },
        config,
      ),
    );
  }

  async getUnitGroupsByPropertyId(propertyId: string, expand?: string[]): Promise<UnitGroupListModel> {
    const config = await this.getApiConfig();
    return this.request(UnitGroupService.inventoryUnitGroupsGet({ propertyId, expand }, config));
  }

  async getRateplans(filter?: any): Promise<RatePlanListModel> {
    const config = await this.getApiConfig();
    return this.request(RatePlanService.rateplanRatePlansGet({ ...filter }, config));
  }

  async getUnitGroupsAvailabilty(propertyId: string, from: string, to: string, filter?: any): Promise<AvailableUnitGroupListModel> {
    const config = await this.getApiConfig();
    return this.request(
      AvailabilityService.availabilityUnitGroupsGet(
        {
          propertyId,
          from,
          to,
          timeSliceTemplate: 'OverNight',
          ...filter,
        },
        config,
      ),
    );
  }

  async updateOverbooking(unitGroupId: string, from: string, to: string, allowedOverbooking: number): Promise<BookingCreatedModel> {
    const config = await this.getApiConfig();
    const body: any = [
      {
        op: 'replace',
        path: '/allowedOverbooking',
        value: allowedOverbooking,
      },
    ];
    return this.request(
      AvailabilityService.availabilityUnitGroupsByIdPatch({ id: unitGroupId, from, to, timeSliceTemplate: 'OverNight', body }, config),
    );
  }
}
