import { Injectable } from '@nestjs/common';
import { SNS } from 'aws-sdk';
import { differenceInDays, startOfWeek, subDays } from 'date-fns';
import dateformat from 'dateformat';
import { ApaleoClient } from './ApaleoClient';
import {
  ReservationEvent,
  CmsProperty,
  ReservationCheckInEvent,
  Reservation,
  ReservationInvoiceEvent,
  ApaleoReservation,
  ReservationCreatedEvent,
  MailType,
  ReservationCheckInDayEvent,
  DashButtonEvent,
  ReservationGuest,
  ManualMailEvent,
  ReservationNeededGuestDataEvent,
  ReservationChangeInvoiceAddressEvent,
  ReservationBreakfastSelection,
  FlicButtonHeaders,
} from '../types';
import { PropertyModel } from './generated/property';
import {
  ReservationModel,
  EnumReservationModelChannelCode,
  Operation,
  EnumReservationItemModelTravelPurpose,
  PersonAddressModel,
  GuestModel,
} from './generated/reservation';
import { generalConfig } from '../config';
import CmsClient from '../CmsClient';
import JWTService from '../JWTService';
import ReservationService from '../ReservationService';
import logger from '../LoggingService';
import { SlackService } from '../SlackService';
import DashButtonExportHandler from '../DashButtonExportHandler';
import PragueCurrencyHandler from '../PragueCurrencyHandler';
import BelfortUpgradeHandler from '../BelfortUpgradeHandler';
import { ApaleoEvent, ManualEvent } from './dto';
import { PinCodeHandler } from '../PinCodeHandler';
import { UrlShortener } from '../UrlShortener';
import PragueBreakfastHandler from '../PragueBreakfastHandler';
import { AirBnBReservationUpdateHandler } from '../AirBnBReservationUpdateHandler';
import { BookingMultiRoomHandler } from '../BookingMultiRoomHandler';
import { ApaleoPaymentHandler } from '../ApaleoPaymentHandler';
import { ReservationDetailsService } from '../common/reservation/reservationDetails.service';
import { DynamoClient as Dynamo } from '../common/aws';
import { DynamoClient } from '../DynamoClient';
import { ReservationDetails } from '../common/types/reservation';
import { ReservationRefundDetailsService } from '../common/reservation/reservationRefundDetails.service';
import { HrsEmailHandler } from '../HrsEmailHandler';
import { pinProperties } from '../util/properties';
import { ReservationEventLogger, EnumReservationEvents } from '../ReservationEventLogger';
import { GuestExperienceService } from '../GuestExperienceService';
import { OverbookingsService } from '../overbookings/overbookings.service';
import { OverbookingsReport } from '../overbookings/OverbookingsReport';
import { OverbookingsAvailability } from '../overbookings/OverbookingsAvailability';

const defaultExpands = ['booker', 'assignedUnits', 'timeSlices'];

@Injectable()
export default class ApaleoEventHandler {
  apaleoClient: ApaleoClient;
  cmsClient: CmsClient;
  reservationDetailsService: ReservationDetailsService;
  reservationService: ReservationService;
  jwtService: JWTService;
  sns: SNS;
  dynamoClient: DynamoClient;
  slack: SlackService;
  pragueCurrencyHandler: PragueCurrencyHandler;
  airBnBReservationUpdateHandler: AirBnBReservationUpdateHandler;
  pragueBreakfastHandler: PragueBreakfastHandler;
  belfortUpgradeHandler: BelfortUpgradeHandler;
  bookingMultiRoomHandler: BookingMultiRoomHandler;
  hrsEmailHandler: HrsEmailHandler;
  apaleoPaymentHandler: ApaleoPaymentHandler;
  pinCodeHandler: PinCodeHandler;
  urlShortener: UrlShortener;
  reservationEventLogger: ReservationEventLogger;
  overbookingsService: OverbookingsService;
  overbookingsReport: OverbookingsReport;
  overbookingsAvailability: OverbookingsAvailability;

  constructor() {
    this.apaleoClient = new ApaleoClient();
    this.cmsClient = new CmsClient();
    const dynamo = new Dynamo();
    const refundDetailsService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundDetailsService);
    this.jwtService = new JWTService();
    this.reservationService = new ReservationService();
    this.urlShortener = new UrlShortener();
    this.sns = new SNS({
      endpoint: generalConfig.aws.snsEndpoint,
    });
    this.slack = new SlackService();
    this.pragueCurrencyHandler = new PragueCurrencyHandler(dynamo, this.apaleoClient, this.slack);
    this.airBnBReservationUpdateHandler = new AirBnBReservationUpdateHandler(dynamo, this.apaleoClient, this.slack);
    this.pragueBreakfastHandler = new PragueBreakfastHandler();
    this.hrsEmailHandler = new HrsEmailHandler(new GuestExperienceService());
    this.belfortUpgradeHandler = new BelfortUpgradeHandler();
    this.apaleoPaymentHandler = new ApaleoPaymentHandler();
    this.pinCodeHandler = new PinCodeHandler();
    this.bookingMultiRoomHandler = new BookingMultiRoomHandler(this.apaleoClient, new GuestExperienceService());
    this.dynamoClient = new DynamoClient();
    this.reservationEventLogger = new ReservationEventLogger(new Dynamo());
    this.overbookingsAvailability = new OverbookingsAvailability();
    this.overbookingsService = new OverbookingsService(
      this.apaleoClient, this.overbookingsAvailability,
    );
    this.overbookingsReport = new OverbookingsReport(
      this.overbookingsService, this.slack, this.overbookingsAvailability,
    );
  }

  async processWebhook(event: ApaleoEvent) {
    const allowedWebooks = {
      Reservation: {
        'created': true,
        'canceled': true,
        'invoice-status-changed': true,
        'amended': true,
      },
      Folio: {
        '*': true,
      },
    };
    if (!allowedWebooks[event.topic] || (!allowedWebooks[event.topic][event.type] && !allowedWebooks[event.topic]['*'])) {
      return;
    }

    // Deduplicate webhook
    const isDuplicate = await this.deduplicateWebhook(event);
    if (isDuplicate) {
      logger.infoLog(`Not processing ${event.topic} ${event.type} due to duplication (${event.id})`);
      return;
    }

    if (event.topic === 'Folio') {
      const folio = await this.apaleoClient.getFolioById(event.data.entityId);
      const res = await this.apaleoClient.getReservationById(folio.reservation.id, defaultExpands);
      return this.processGenericEvent(res, MailType.FolioChanged);
    }

    const extendExpands = ['actions', ...defaultExpands];
    let reservation = await this.apaleoClient.getReservationById(event.data.entityId, extendExpands);
    switch (event.type) {
      case 'created': {
        reservation = await this.pragueCurrencyHandler.handleReservation(reservation);
        reservation = await this.airBnBReservationUpdateHandler.handleNewReservation(reservation);
        reservation = await this.bookingMultiRoomHandler.handleNewReservation(reservation);
        reservation = await this.hrsEmailHandler.handleNewReservation(reservation);
        await this.reservationEventLogger.logEvent({
          event: EnumReservationEvents.ReservationCreated,
          reservationId: reservation.id,
          message: 'Reservation created',
          timeStamp: reservation.created,
        });
        // Disable automatic upgrades
        // reservation = await this.belfortUpgradeHandler.handleReservation(reservation);
        await this.slack.sendNewReservationNotification(reservation);
        await this.processCreatedReservation(reservation);
        await this.overbookingsReport.handleNewReservation(reservation);
        break;
      }
      case 'amended': {
        await this.pragueCurrencyHandler.handleAmmendedReservation(reservation);
        await this.airBnBReservationUpdateHandler.handleAmmendedReservation(reservation);
        break;
      }
      case 'canceled':
        await this.processCancelledReservation(reservation);
        break;
      case 'invoice-status-changed':
        await this.processChangedInvoiceReservation(reservation);
        break;
    }
  }

  async deduplicateWebhook(event: ApaleoEvent): Promise<boolean> {
    const key = `apaleoWebhook_${event.id}`;
    const response = await this.dynamoClient.getItem(event.data.entityId, key);
    if (response.Item) {
      return true;
    }
    const validUntilDate = new Date();
    validUntilDate.setDate(validUntilDate.getDate() + 7);
    const validUntil = Math.floor(validUntilDate.getTime() / 1000);
    await this.dynamoClient.addItem(event.data.entityId, key, { value: true, validUntil });
    return false;
  }

  async processManualEvent(event: ManualEvent) {
    const reservation = await this.apaleoClient.getReservationById(event.reservationId, defaultExpands);

    switch (event.type) {
      case MailType.Created:
        await this.processReservations([reservation], this.getCreatedEvent.bind(this));
        break;
      case MailType.CheckIn:
        await this.processReservations([reservation], this.getCheckInEvent.bind(this));
        break;
      case MailType.CheckInDay:
        await this.processReservations([reservation], this.getCheckInDayEvent.bind(this));
        break;
      case MailType.Invoice:
        await this.processReservations([reservation], this.getInvoiceEvent.bind(this));
        break;
      case MailType.NeededGuestData:
        await this.processReservations([reservation], this.getNeededGuestDataEvent.bind(this));
        break;
    }
  }

  async processManualMailEvent(event: ManualMailEvent) {
    const reservation = await this.apaleoClient.getReservationById(event.reservationId, defaultExpands);

    switch (event.type) {
      case MailType.Created:
        await this.processReservations([reservation], this.getCreatedEvent.bind(this));
        break;
      case MailType.CheckIn:
        await this.processReservations([reservation], this.getCheckInEvent.bind(this));
        break;
      case MailType.CheckInDay:
        await this.processReservations([reservation], this.getCheckInDayEvent.bind(this));
        break;
      case MailType.Invoice:
        await this.processReservations([reservation], this.getInvoiceEvent.bind(this));
        break;
      case MailType.ChangeInvoiceAddress:
        await this.processReservations([reservation], this.getChangeInvoiceAddressEvent.bind(this));
        break;
    }
  }

  async processTravelPurposeValidation() {
    logger.infoLog('Processing travel purpose validation');

    const beginningOfWeek = startOfWeek(new Date());
    const lastDayOfPreviousWeek = subDays(beginningOfWeek, 1);
    const firstDayOfPreviousWeek = startOfWeek(lastDayOfPreviousWeek);

    const status = ['Confirmed', 'InHouse', 'CheckedOut'];
    const { reservations = [] } = await this.apaleoClient.getReservationsbyTime(status, 'Departure', firstDayOfPreviousWeek, lastDayOfPreviousWeek);
    const updatedReservations = [];
    for (const reservation of reservations) {
      const { booker, travelPurpose } = reservation;
      if (travelPurpose === EnumReservationItemModelTravelPurpose.Leisure) {
        if (booker.company && booker.company.name !== '') {
          logger.infoLog(`Updating ${reservation.id} to Business due to company name ${booker.company.name}`);
          await this.apaleoClient.updateTravelPurposeByReservationId(reservation.id, EnumReservationItemModelTravelPurpose.Business);
          updatedReservations.push(reservation.id);
        }
      }
    }

    await this.slack.sendTravelValidationMetrics(updatedReservations);
  }

  async processPragueBreakfastEmails() {
    logger.infoLog('Processing prague breakfast emails');
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    const reservations = await this.pragueBreakfastHandler.getBreakfastReservations(tomorrow, { expand: defaultExpands });
    logger.infoLog(`Found ${reservations.length} reservations with breakfast option`);

    const getEvent = async (data: Reservation, details: ReservationDetails): Promise<ReservationBreakfastSelection> => {
      const token = this.jwtService.getToken({ reservationId: data.reservationId, bookingId: data.bookingId });
      const breakfastUrl = await this.shortenUrl(details, `https://cosi-trip.com/breakfast?key=${token}`, 'breakfast');
      return {
        type: MailType.BreakfastSelectionReminder,
        data,
        breakfastUrl,
      };
    };

    await this.processReservations(reservations, getEvent);
  }

  async processChangedInvoiceReservation(reservation: ReservationModel) {
    const getEvent = async (data: Reservation): Promise<ReservationEvent> => {
      return {
        type: MailType.InvoiceStatusChanged,
        data,
      };
    };
    return this.processReservations([reservation], getEvent);
  }

  async processCancelledReservation(reservation: ReservationModel) {
    // tslint:disable-next-line: max-line-length
    const getEvent = async (data: Reservation, details: ReservationDetails, _, apaleoReservation: ApaleoReservation): Promise<ReservationEvent> => {
      if (details.pinCodes.pinComfortCode) {
        await this.pinCodeHandler.removeCode(apaleoReservation);
      }
      return {
        type: MailType.Canceled,
        data,
        cancellationFee: apaleoReservation.cancellationFee.fee.amount,
        cancellationCurrency: apaleoReservation.cancellationFee.fee.currency,
      };
    };
    return this.processReservations([reservation], getEvent.bind(this));
  }

  async processCreatedReservation(reservation: ReservationModel) {
    await this.processReservations([reservation], this.getCreatedEvent.bind(this));

    const propertyCountryCode = reservation.property.id.substr(0, 2);
    if (propertyCountryCode === 'CZ') {
      await this.processReservations([reservation], this.getNeededGuestDataEvent.bind(this));
    }

    if (propertyCountryCode === 'DE') {
      const isBooking = reservation.source === 'Booking.com' || reservation.channelCode === 'BookingCom';
      if (!isBooking) {
        await this.processReservations([reservation], this.getNeededGuestDataEvent.bind(this));
      }
    }

    // Check if CheckIn is within the next 3 days
    const arrivalDate = new Date(reservation.arrival);
    const checkInInfoDate = new Date();
    const dayDiff = differenceInDays(arrivalDate, checkInInfoDate);
    // Cronjob runs at 10:00 UTC
    if (
      dayDiff <= 2 ||
      (dayDiff === 3 && (checkInInfoDate.getUTCHours() > 10 || (checkInInfoDate.getUTCHours() === 10 && checkInInfoDate.getUTCMinutes() > 0)))
    ) {
      return this.processReservations([reservation], this.getCheckInEvent.bind(this));
    }
  }

  async processCheckIns() {
    // Fetch three days ahead arrivals
    const { reservations: arrivals = [] } = await this.apaleoClient.getThreeDaysArrivals(defaultExpands);
    logger.infoLog(`Got ${arrivals.length} checkIns`);
    return this.processReservations(arrivals, this.getCheckInEvent.bind(this));
  }

  async processCheckInDayInfos() {
    const { reservations = [] } = await this.apaleoClient.getReservationsByDay(new Date(), ['Confirmed', 'InHouse'], 'Arrival', {
      expand: defaultExpands,
    });
    return this.processReservations(reservations, this.getCheckInDayEvent.bind(this));
  }

  async processTodayCheckOuts() {
    const { Ibe, BookingCom, ChannelManager, Expedia, Homelike, Direct } = EnumReservationModelChannelCode;
    const channelCodes = [Ibe, BookingCom, ChannelManager, Expedia, Homelike, Direct];
    const { reservations: departures = [] } = await this.apaleoClient.getTodayDepartures(channelCodes, defaultExpands);
    await this.processReservations(departures, this.getInvoiceEvent.bind(this));
    for (const departure of departures) {
      await this.pinCodeHandler.removeCode(departure);
    }
    return true;
  }

  async processCheckOutEvent(propertyId: string, unitId: string, extraInfo: DashButtonEvent | FlicButtonHeaders) {
    const exporter = new DashButtonExportHandler();
    logger.infoLog('Checkout button pressed', { extraInfo });

    if (Number(extraInfo['button-battery-level']) < 20) {
      logger.errorLog('Flic button low battery level', extraInfo);
    }

    const filter = {
      expands: defaultExpands,
      propertyIds: [propertyId],
      unitIds: [unitId],
    };

    const { reservations = [] } = await this.apaleoClient.getReservationsByDay(new Date(), ['InHouse'], 'Departure', filter);
    if (reservations.length === 0) {
      logger.errorLog(`Checkout pressed without valid checkout. propertyId: ${propertyId}, unitId: ${unitId} `, extraInfo);
      return;
    }
    const reservation = reservations[0];
    logger.infoLog(`Checking out reservation ${reservation.id} `);
    try {
      await this.apaleoClient.checkOutReservationById(reservation.id);
      await exporter.exportDashButtonCheckOut(reservation);
      logger.infoLog(`Checked out reservation ${reservation.id} `);
    } catch (e) {
      logger.errorLog(`Could not check out ${reservation.id} `, { e });
    }
  }

  async processGenericEvent(reservation: ReservationModel, type: any) {
    const getEvent = async (data: Reservation): Promise<ReservationEvent> => {
      return { type, data };
    };
    return this.processReservations([reservation], getEvent);
  }

  async getCreatedEvent(
    data: Reservation,
    details: ReservationDetails,
    cmsProperty: CmsProperty,
    reservation: ApaleoReservation,
  ): Promise<ReservationCreatedEvent> {
    const token = this.jwtService.getToken({ reservationId: data.reservationId, bookingId: data.bookingId });
    const tripManagementUrl = await this.shortenUrl(details, `https://cosi-trip.com/?key=${token}`, 'tripManagement');

    const nonOtaChannels: string[] = [EnumReservationModelChannelCode.Direct, EnumReservationModelChannelCode.Ibe];
    const showCancelationRules = nonOtaChannels.includes(data.channelCode);
    return {
      type: MailType.Created,
      data,
      tripManagementUrl,
      showCancelationRules,
    };
  }

  // tslint:disable-next-line: max-line-length
  async getCheckInEvent(
    data: Reservation,
    details: ReservationDetails,
    cmsProperty: CmsProperty,
    reservation: ApaleoReservation,
  ): Promise<ReservationCheckInEvent> {
    let resDetails = details;
    logger.infoLog('Processing checkIn event');

    const paidReservation = await this.apaleoPaymentHandler.addPaymentToReservation(reservation);

    // Auto assign unit
    const rooms = await this.assignRoomToReservation(data);
    const roomName = rooms.timeSlices[0].unit.name;
    const cmsRoom = cmsProperty.rooms[data.language][roomName];
    const directions = cmsRoom?.directions;
    const checkInOutInfos = cmsRoom?.checkInCheckOutDetailsPerRoom || cmsProperty?.checkInAndOutDetails[data.language];

    // PinCode generation
    if (pinProperties.includes(data.property.id)) {
      if (!details.pinCodes.pinComfortCode) {
        logger.infoLog(`No pin access code found ${data.property.id} ${reservation.id} `);
        resDetails = await this.pinCodeHandler.generateCodes(reservation, details);
      }
    }

    // Balance check
    const isPaid = await this.apaleoPaymentHandler.isReservationPaid(paidReservation);
    if (!isPaid && reservation.property.id !== 'AT_VI_001') {
      logger.infoLog(`Negative balance found for ${data.property.id} ${reservation.id} `);
      throw new Error(`Negative balance for ${data.property.id} ${data.reservationId} `);
    }

    const roomNumber = this.reservationService.getFormattedRoomName(roomName);
    // Format kater adresses
    if (reservation.property.id === 'DE_BE_002') {
      if (roomNumber.substr(0, 2) === '47') {
        data.property.addressLine1 = 'Warschauer Str. 47';
      }
    }

    const { pinCodes } = resDetails;
    return {
      type: MailType.CheckIn,
      pinAccessCode: pinCodes.pinAccessCode,
      pinComfortCode: pinCodes.pinComfortCode,
      mainDoorCode: pinCodes.mainDoorCode,
      earlyCheckInCode: pinCodes.earlyCheckInCode,
      roomDirection: directions,
      checkInOutInfos,
      roomNumber,
      data,
    };
  }

  async getCheckInDayEvent(
    data: Reservation,
    details: ReservationDetails,
    cmsProperty: CmsProperty,
    reservation: ApaleoReservation,
  ): Promise<ReservationCheckInDayEvent> {
    let resDetails = details;
    const rooms = await this.assignRoomToReservation(data);
    const roomName = rooms.timeSlices[0].unit.name;
    const cmsRoom = cmsProperty.rooms[data.language][roomName];
    const directions = cmsRoom?.directions;
    const checkInOutInfos = cmsRoom?.checkInCheckOutDetailsPerRoom || cmsProperty.checkInAndOutDetails[data.language];

    const paidReservation = await this.apaleoPaymentHandler.addPaymentToReservation(reservation);

    // PinCode generation
    if (pinProperties.includes(data.property.id)) {
      if (!resDetails.pinCodes.pinComfortCode) {
        logger.infoLog(`No pin access code found ${data.property.id} ${reservation.id} `);
        resDetails = await this.pinCodeHandler.generateCodes(reservation, resDetails);
      }
    }

    // Balance check
    const isPaid = await this.apaleoPaymentHandler.isReservationPaid(paidReservation);
    if (!isPaid && reservation.property.id !== 'AT_VI_001') {
      logger.infoLog(`Negative balance found for ${data.property.id} ${reservation.id} `);
      throw new Error(`Negative balance for ${data.property.id} ${data.reservationId} `);
    }

    const roomNumber = this.reservationService.getFormattedRoomName(roomName);
    // Format kater adresses
    if (reservation.property.id === 'DE_BE_002') {
      if (roomNumber.substr(0, 2) === '47') {
        data.property.addressLine1 = 'Warschauer Str. 47';
      }
    }

    const { pinCodes } = resDetails;
    return {
      type: MailType.CheckInDay,
      pinAccessCode: pinCodes.pinAccessCode,
      pinComfortCode: pinCodes.pinComfortCode,
      mainDoorCode: pinCodes.mainDoorCode,
      earlyCheckInCode: pinCodes.earlyCheckInCode,
      roomDirection: directions,
      checkInOutInfos,
      roomNumber,
      data,
    };
  }

  async assignRoomToReservation(reservation: Reservation) {
    const { reservationId, unit } = reservation;

    const rooms = await this.apaleoClient.assignRoomToReservation(reservationId);
    if (unit && unit.unitId) {
      // Unit was assigned already
      return rooms;
    }
    await this.reservationEventLogger.logEvent({
      event: EnumReservationEvents.UnitAssigned,
      reservationId,
      message: 'Unit assigned to reservation',
      additionalData: {
        rooms,
      },
    });
    return rooms;
  }

  async getNeededGuestDataEvent(data: Reservation, details: ReservationDetails): Promise<ReservationNeededGuestDataEvent> {
    const token = this.jwtService.getToken({ reservationId: data.reservationId, bookingId: data.bookingId });
    const neededDataUrl = await this.shortenUrl(details, `https://cosi-trip.com/guest-legal-info?key=${token}`, 'neededData');
    return {
      type: MailType.NeededGuestData,
      data,
      neededDataUrl,
    };
  }

  async getChangeInvoiceAddressEvent(data: Reservation, details: ReservationDetails): Promise<ReservationChangeInvoiceAddressEvent> {
    const token = this.jwtService.getToken({ reservationId: data.reservationId, bookingId: data.bookingId });
    const changeInvoiceAddressUrl = await this.shortenUrl(details, `https://cosi-trip.com/invoice?key=${token}`, 'changeInvoiceAddress');
    return {
      type: MailType.ChangeInvoiceAddress,
      data,
      changeInvoiceAddressUrl,
    };
  }

  async getInvoiceEvent(data: Reservation, y, z, reservation: ApaleoReservation): Promise<ReservationInvoiceEvent> {
    const { folios } = await this.apaleoClient.getFoliosByReservationId(data.reservationId);
    const invoices: string[] = [];
    for (const folio of folios) {
      const invoiceCount = folio.relatedInvoices ? folio.relatedInvoices.length : 0;
      let invoiceId;
      if (invoiceCount === 0) {
        // No invoice generated yet
        if (folio.isMainFolio) {
          // Copy booking data to folio
          const op: Operation[] = [
            {
              from: '',
              op: 'add',
              path: 'debitor',
              value: {
                ...reservation.booker,
                name: reservation.booker.lastName,
              },
            },
          ];
          logger.infoLog(`Copy booker data to folio ${folio.id}`, { folio, booker: reservation.booker });
          await this.apaleoClient.updateFolioById(folio.id, op);
        }
        logger.infoLog(`Generating invoice for ${reservation.id} (Folio ${folio.id})`);
        const invoice = await this.apaleoClient.createPDFInvoice(folio.id, false, data.language);
        await this.reservationEventLogger.logEvent({
          event: EnumReservationEvents.InvoiceGenerated,
          reservationId: reservation.id,
          message: `Generated invoice for ${reservation.id} (Folio ${folio.id})`,
        });
        invoiceId = invoice.id;
      } else {
        invoiceId = folio.relatedInvoices[invoiceCount - 1].id;
      }
      // Get PDF invoice
      const pdfInvoice = await this.apaleoClient.getPDFInvoiceById(invoiceId);
      invoices.push(new Buffer(pdfInvoice, 'binary').toString('base64'));
    }
    return {
      type: MailType.Invoice,
      data,
      invoices,
    };
  }

  async processReservations(
    reservations: ApaleoReservation[],
    getEvent: (x: Reservation, y: ReservationDetails, z?: CmsProperty, o?: ApaleoReservation) => Promise<ReservationEvent>,
  ) {
    const cmsProperties: { [key: string]: CmsProperty } = {};
    const apaleoProperties: { [key: string]: PropertyModel } = {};
    for (const reservation of reservations) {
      const logInfo = `${reservation.id} (${reservation.property.id})`;
      logger.infoLog(`Processing reservation ${logInfo}`);
      try {
        let reservationDetails;
        try {
          reservationDetails = await this.reservationDetailsService.getDetails(reservation.id);
        } catch (e) {
          reservationDetails = await this.reservationDetailsService.createDetails(reservation.id, reservation.bookingId);
        }
        logger.infoLog(`Loaded reservation details ${logInfo}`);
        const propertyId = reservation.property.id;
        if (!apaleoProperties[propertyId]) {
          apaleoProperties[propertyId] = await this.apaleoClient.getPropertyById(propertyId);
        }
        logger.infoLog(`Loaded apaleo property ${propertyId}`);
        if (!cmsProperties[propertyId]) {
          cmsProperties[propertyId] = await this.cmsClient.getPropertyInfos(propertyId);
        }
        logger.infoLog(`Loaded cms property ${propertyId}`);

        logger.infoLog(`Formatting reservation ${logInfo}`);
        // tslint:disable-next-line: max-line-length
        const reservationData = await this.formatReservation(
          reservation,
          reservationDetails,
          apaleoProperties[propertyId],
          cmsProperties[propertyId],
        );
        logger.infoLog(`Loading event`);
        const event = await getEvent(reservationData, reservationDetails, cmsProperties[propertyId], reservation);
        logger.infoLog(`Publishing event ${event.type} for reservation ${reservation.id}`);
        await this.publishSnsMessage(event);
      } catch (e) {
        logger.errorLog(`Could not process reservation ${reservation.id}`, { error: e, errorMessage: e.message, errorTrace: e.stack, reservation });
      }
    }
  }

  async publishSnsMessage(event: any) {
    const message = { default: JSON.stringify(event) };
    await this.sns
      .publish({
        MessageStructure: 'json',
        Message: JSON.stringify(message),
        TopicArn: generalConfig.aws.apaleoEventsTopic,
      })
      .promise();
  }

  private async shortenUrl(details: ReservationDetails, url: string, key: string) {
    if (details.urls?.[key]) {
      return details.urls?.[key];
    }
    const shortUrl = await this.urlShortener.shorten(url);
    const urls = {
      ...details.urls,
      [key]: shortUrl,
    };
    await this.reservationDetailsService.updateDetails(details.id, { urls });
    return shortUrl;
  }

  private formatGuest(model: GuestModel): ReservationGuest {
    return {
      title: model.title,
      firstName: model.firstName,
      lastName: model.lastName,
      fullName: `${model.title || ''} ${model.firstName || ''} ${model.lastName || ''}`,
      citizenship: model.nationalityCountryCode,
      idNumber: model.identificationNumber,
      address: this.formatAddress(model.address),
      email: model.email,
    };
  }

  private formatAddress(address?: PersonAddressModel): string {
    if (!address) {
      return '';
    }
    return `${address.addressLine1 || ''} ${address.addressLine2 || ''} ${address.postalCode || ''} ${address.city || ''} ${address.countryCode ||
      ''}`;
  }

  private async formatReservation(
    reservation: ApaleoReservation,
    reservationDetails: ReservationDetails,
    property: PropertyModel,
    cmsProperty: CmsProperty,
  ): Promise<Reservation> {
    const language = this.reservationService.getPreferredLanguage(reservation);
    let translatedReservation = reservation;
    if (language !== this.apaleoClient.language) {
      logger.infoLog(`Reloading reservaton in language = ${language}`);
      this.apaleoClient.language = language;
      translatedReservation = await this.apaleoClient.getReservationById(reservation.id, defaultExpands);
    }
    const { primaryGuest, additionalGuests = [] } = reservation;
    return {
      reservationId: reservation.id,
      bookingId: reservation.bookingId,
      channelCode: reservation.channelCode,
      source: reservation.source,
      status: reservation.status,
      createdAt: reservation.created,
      checkIn: reservation.arrival,
      checkOut: reservation.departure,
      checkInTime: dateformat(reservation.arrival, 'dd.mm.yyyy'),
      checkOutTime: dateformat(reservation.departure, 'dd.mm.yyyy'),
      adults: reservation.adults,
      children: reservation.childrenAges ? reservation.childrenAges.length : 0,
      language,
      cancelationRules: translatedReservation.cancellationFee.description,
      cancellationTime: reservation.cancellationTime || undefined,
      guestComment: reservation.guestComment,
      ratePlanCode: reservation.ratePlan.code,
      roomType: reservation.unitGroup.name,
      travelPurpose: reservation.travelPurpose,
      comissionAmount: reservation.commission ? reservation.commission.commissionAmount.amount : 0,
      comissionCurrency: reservation.commission ? reservation.commission.commissionAmount.currency : '',
      nights: reservation.timeSlices.map(slice => ({
        from: slice.from,
        to: slice.to,
        serviceDate: slice.serviceDate,
        rate: slice.baseAmount.grossAmount,
        unitId: slice.unit ? slice.unit.id : undefined,
        unitGroupId: slice.unitGroup ? slice.unitGroup.id : undefined,
      })),
      unit: {
        name: translatedReservation.unitGroup.name,
        description: translatedReservation.unitGroup.description,
        unitId: translatedReservation.unit ? translatedReservation.unit.id : '',
        unitGroupId: translatedReservation.unitGroup.id,
      },
      booker: {
        male: reservation.booker.title === 'Mr',
        female: reservation.booker.title === 'Ms',
        email: reservation.booker.email,
        firstName: reservation.booker.firstName,
        lastName: reservation.booker.lastName,
      },
      primaryGuest: this.formatGuest(primaryGuest),
      additionalGuests: additionalGuests.map(guest => this.formatGuest(guest)),
      property: {
        id: reservation.property.id,
        name: reservation.property.name,
        addressLine1: property.location.addressLine1,
        addressLine2: property.location.addressLine2,
        city: property.location.city,
        postalCode: property.location.postalCode,
        countryCode: property.location.countryCode,
        isBerlin: property.location.city === 'Berlin',
        bookingEmail: cmsProperty.bookingMail,
        bookingPage: cmsProperty.bookingPage,
        cmsId: cmsProperty.id,
        signature: cmsProperty.signature[language],
        additionalInformation: cmsProperty.additionalInformation[language],
        footer: cmsProperty.emailFooter[language],
        breakfastRecommendations: cmsProperty.breakfastRecommendations ? cmsProperty.breakfastRecommendations[language] : '',
        parking: cmsProperty.parking[language],
      },
      totalGrossAmount: reservation.totalGrossAmount.amount,
      totalGrossCurrency: reservation.totalGrossAmount.currency,
    };
  }
}
