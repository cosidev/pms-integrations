import { RESERVATION } from './reservation.mock';
import { ReservationModel } from '../generated/reservation';

export const OLD_RESERVATION: ReservationModel = ({
  ...RESERVATION,
  totalGrossAmount: {
    amount: 138.0, currency: 'EUR',
  },
  timeSlices: [
    {
      from: '2020-04-23T11:00:00+02:00',
      to: '2020-04-24T11:00:00+02:00',
      serviceDate: '2020-04-18',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 69.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 69.0, currency: 'EUR' },
    },
    {
      from: '2020-04-24T11:00:00+02:00',
      to: '2020-04-25T11:00:00+02:00',
      serviceDate: '2020-04-19',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 69.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 69.0, currency: 'EUR' },
    },
  ],
} as unknown) as ReservationModel;

export const NEW_RESERVATION: ReservationModel = ({
  ...OLD_RESERVATION,
  totalGrossAmount: {
    amount: 158.0, currency: 'EUR',
  },
  timeSlices: [
    {
      from: '2020-04-23T11:00:00+02:00',
      to: '2020-04-24T11:00:00+02:00',
      serviceDate: '2020-04-18',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    },
    {
      from: '2020-04-24T11:00:00+02:00',
      to: '2020-04-25T11:00:00+02:00',
      serviceDate: '2020-04-19',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    },
  ],
} as unknown) as ReservationModel;

export const NEW_RESERVATION_SLACK: ReservationModel = ({
  ...RESERVATION,
  childrenAges: [3, 5],
  totalGrossAmount: {
    amount: 158.0, currency: 'EUR',
  },
  timeSlices: [
    {
      from: '2020-04-23T11:00:00+02:00',
      to: '2020-04-24T11:00:00+02:00',
      serviceDate: '2020-04-18',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    },
    {
      from: '2020-04-24T11:00:00+02:00',
      to: '2020-04-25T11:00:00+02:00',
      serviceDate: '2020-04-19',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    },
  ],
} as unknown) as ReservationModel;
