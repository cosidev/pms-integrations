export const FUTURE_TIME_SLICES = [
  {
    from: '2020-04-20T11:00:00+02:00',
    to: '2020-04-21T11:00:00+02:00',
    serviceDate: '2020-04-20',
    ratePlan: {
      id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
      code: 'DE_DEV_001',
      name: 'Non refundable',
      description: 'DE_DEV_001 IBE',
      isSubjectToCityTax: true,
    },
    unitGroup: {
      id: 'DE_DEV_001-SUPERIOR',
      code: 'SUPERIOR',
      name: 'SUPERIOR',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    unit: {
      id: 'DE_DEV_001-DZF',
      name: '201',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      unitGroupId: 'DE_DEV_001-SUPERIOR',
    },
    baseAmount: { grossAmount: 100.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
    totalGrossAmount: { amount: 100.0, currency: 'EUR' },
    actions: [{ action: 'Amend', isAllowed: true }],
  },
  {
    from: '2020-04-21T11:00:00+02:00',
    to: '2020-04-22T15:00:00+02:00',
    serviceDate: '2020-04-21',
    ratePlan: {
      id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
      code: 'DE_DEV_001',
      name: 'Non refundable',
      description: 'DE_DEV_001 IBE',
      isSubjectToCityTax: true,
    },
    unitGroup: {
      id: 'DE_DEV_001-SUPERIOR',
      code: 'SUPERIOR',
      name: 'SUPERIOR',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    unit: {
      id: 'DE_DEV_001-DZF',
      name: '201',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      unitGroupId: 'DE_DEV_001-SUPERIOR',
    },
    baseAmount: { grossAmount: 100.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
    totalGrossAmount: { amount: 100.0, currency: 'EUR' },
    actions: [{ action: 'Amend', isAllowed: true }],
  },
];

export const PAST_TIME_SLICES = [
  {
    from: '2020-04-18T15:00:00+02:00',
    to: '2020-04-19T11:00:00+02:00',
    serviceDate: '2020-04-18',
    ratePlan: {
      id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
      code: 'DE_DEV_001',
      name: 'Non refundable',
      description: 'DE_DEV_001 IBE',
      isSubjectToCityTax: true,
    },
    unitGroup: {
      id: 'DE_DEV_001-SUPERIOR',
      code: 'SUPERIOR',
      name: 'SUPERIOR',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
    totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    actions: [
      {
        action: 'Amend',
        isAllowed: false,
        reasons: [
          {
            code: 'AmendNotAllowedWhenTimeSliceIsInThePast',
            message: 'Cannot amend this time slice because it is the past.',
          },
        ],
      },
    ],
  },
  {
    from: '2020-04-19T11:00:00+02:00',
    to: '2020-04-20T11:00:00+02:00',
    serviceDate: '2020-04-19',
    ratePlan: {
      id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
      code: 'DE_DEV_001',
      name: 'Non refundable',
      description: 'DE_DEV_001 IBE',
      isSubjectToCityTax: true,
    },
    unitGroup: {
      id: 'DE_DEV_001-SUPERIOR',
      code: 'SUPERIOR',
      name: 'SUPERIOR',
      description:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
    totalGrossAmount: { amount: 79.0, currency: 'EUR' },
    actions: [
      {
        action: 'Amend',
        isAllowed: false,
        reasons: [{ code: 'AmendNotAllowedWhenTimeSliceIsInThePast', message: 'Cannot amend this time slice because it is the past.' }],
      },
    ],
  },
];
