import { ReservationModel } from '../generated/reservation';

export const RESERVATION: ReservationModel = ({
  id: 'EJTKMHDK-1',
  bookingId: 'EJTKMHDK',
  status: 'InHouse',
  checkInTime: '2020-04-20T11:53:36+02:00',
  unit: {
    id: 'DE_DEV_001-DZF',
    name: '201',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    unitGroupId: 'DE_DEV_001-SUPERIOR',
  },
  property: { id: 'DE_DEV_001', code: 'DE_DEV_001', name: 'Dev Hotel #1', description: 'Dev Hotel #1' },
  ratePlan: {
    id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
    code: 'DE_DEV_001',
    name: 'Non refundable',
    description: 'DE_DEV_001 IBE',
    isSubjectToCityTax: true,
  },
  unitGroup: {
    id: 'DE_DEV_001-SUPERIOR',
    code: 'SUPERIOR',
    name: 'SUPERIOR',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
  },
  totalGrossAmount: { amount: 358.0, currency: 'EUR' },
  arrival: '2020-04-18T15:00:00+02:00',
  departure: '2020-04-22T15:00:00+02:00',
  created: '2020-04-17T19:36:21+02:00',
  modified: '2020-04-20T12:03:30+02:00',
  adults: 2,
  channelCode: 'ChannelManager',
  source: 'Airbnb',
  primaryGuest: {
    title: 'Mr',
    gender: 'Female',
    firstName: 'luc',
    middleInitial: 'S',
    lastName: 'schlicht',
    email: 'luc.schlicht@gmail.com',
    phone: '+49-698-5816936',
    address: { addressLine1: '2819 Mathes Courts', postalCode: '05624', city: 'Spiegelburgstadt', countryCode: 'GL' },
  },
  booker: {
    title: 'Dr',
    gender: 'Male',
    firstName: 'gino',
    middleInitial: 'H',
    lastName: 'holtfreter',
    email: 'gino.holtfreter@hotmail.com',
    phone: '+49-870-9727461',
    address: { addressLine1: '2384 Peer Extension', postalCode: '69653', city: 'Aurorastadt', countryCode: 'JM' },
  },
  paymentAccount: {
    accountNumber: '3047',
    accountHolder: 'luna lauinger',
    expiryMonth: '7',
    expiryYear: '2020',
    paymentMethod: 'visa',
    payerEmail: 'luna.lauinger@hotmail.com',
    isVirtual: true,
    isActive: true,
  },
  timeSlices: [
    {
      from: '2020-04-18T15:00:00+02:00',
      to: '2020-04-19T11:00:00+02:00',
      serviceDate: '2020-04-18',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
      actions: [
        {
          action: 'Amend',
          isAllowed: false,
          reasons: [
            {
              code: 'AmendNotAllowedWhenTimeSliceIsInThePast',
              message: 'Cannot amend this time slice because it is the past.',
            },
          ],
        },
      ],
    },
    {
      from: '2020-04-19T11:00:00+02:00',
      to: '2020-04-20T11:00:00+02:00',
      serviceDate: '2020-04-19',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      baseAmount: { grossAmount: 79.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 79.0, currency: 'EUR' },
      actions: [
        {
          action: 'Amend',
          isAllowed: false,
          reasons: [{ code: 'AmendNotAllowedWhenTimeSliceIsInThePast', message: 'Cannot amend this time slice because it is the past.' }],
        },
      ],
    },
    {
      from: '2020-04-20T11:00:00+02:00',
      to: '2020-04-21T11:00:00+02:00',
      serviceDate: '2020-04-20',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      unit: {
        id: 'DE_DEV_001-DZF',
        name: '201',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
        unitGroupId: 'DE_DEV_001-SUPERIOR',
      },
      baseAmount: { grossAmount: 100.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 100.0, currency: 'EUR' },
      actions: [{ action: 'Amend', isAllowed: true }],
    },
    {
      from: '2020-04-21T11:00:00+02:00',
      to: '2020-04-22T15:00:00+02:00',
      serviceDate: '2020-04-21',
      ratePlan: {
        id: 'DE_DEV_001-DE_DEV_001-SUPERIOR',
        code: 'DE_DEV_001',
        name: 'Non refundable',
        description: 'DE_DEV_001 IBE',
        isSubjectToCityTax: true,
      },
      unitGroup: {
        id: 'DE_DEV_001-SUPERIOR',
        code: 'SUPERIOR',
        name: 'SUPERIOR',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      },
      unit: {
        id: 'DE_DEV_001-DZF',
        name: '201',
        description:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
        unitGroupId: 'DE_DEV_001-SUPERIOR',
      },
      baseAmount: { grossAmount: 100.0, netAmount: 66.39, vatType: 'Normal', currency: 'EUR' },
      totalGrossAmount: { amount: 100.0, currency: 'EUR' },
      actions: [{ action: 'Amend', isAllowed: true }],
    },
  ],
  guaranteeType: 'PM6Hold',
  cancellationFee: {
    code: 'NONREF',
    name: 'Non Refundable',
    description: 'No free cancellation',
    dueDateTime: '2020-04-17T19:36:21+02:00',
    fee: { amount: 351.0, currency: 'EUR' },
  },
  noShowFee: { code: 'NOSHOW', fee: { amount: 351.0, currency: 'EUR' } },
  balance: { amount: -351.0, currency: 'EUR' },
  allFoliosHaveInvoice: false,
  taxDetails: [
    { vatType: 'Null', vatPercent: 0.0, net: { amount: 0.0, currency: 'EUR' }, tax: { amount: 0.0, currency: 'EUR' } },
    { vatType: 'Reduced', vatPercent: 7.0, net: { amount: 0.0, currency: 'EUR' }, tax: { amount: 0.0, currency: 'EUR' } },
    { vatType: 'Normal', vatPercent: 19.0, net: { amount: 294.96, currency: 'EUR' }, tax: { amount: 56.04, currency: 'EUR' } },
    { vatType: 'Without', vatPercent: 0.0, net: { amount: 0.0, currency: 'EUR' }, tax: { amount: 0.0, currency: 'EUR' } },
  ],
  hasCityTax: false,
  commission: { commissionAmount: { amount: 20.0, currency: 'EUR' }, beforeCommissionAmount: { amount: 100.0, currency: 'EUR' } },
  payableAmount: { guest: { amount: 351.0, currency: 'EUR' } },
} as unknown) as ReservationModel;
