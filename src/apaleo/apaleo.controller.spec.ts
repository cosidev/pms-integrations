import { ApaleoClient } from './ApaleoClient';
import { Test, TestingModule } from '@nestjs/testing';
import { ApaleoController } from './apaleo.controller';
import ApaleoEventHandler from './ApaleoEventHandler';

describe('Apaleo Controller', () => {
  let controller: ApaleoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApaleoController],
      providers: [ApaleoEventHandler, ApaleoClient],
    }).compile();

    controller = module.get<ApaleoController>(ApaleoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
