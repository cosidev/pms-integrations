import { Controller, Post, Body } from '@nestjs/common';
import ApaleoEventHandler from './ApaleoEventHandler';
import { KeyAuth } from '../common/auth/keyAuth.decorator';
import { generalConfig } from '../config';
import logger from '../LoggingService';
import { ApaleoEvent, ManualEvent } from './dto';

@Controller('apaleo')
export class ApaleoController {
  constructor(private readonly eventHandler: ApaleoEventHandler) { }

  @Post('event')
  @KeyAuth(generalConfig.apiKey)
  async event(@Body() eventData: ApaleoEvent) {
    logger.infoLog(`Apaleo Event ${eventData.topic} ${eventData.type}`, eventData);

    // Apaleo healthchecks
    if (eventData.topic === 'system') {
      return { message: `Processed` };
    }

    await this.eventHandler.processWebhook(eventData);
    return { message: `Processed` };
  }

  @Post('manualEvent')
  @KeyAuth(generalConfig.apiKey)
  async manualEvent(@Body() eventData: ManualEvent) {
    logger.infoLog('Manual event', eventData);
    await this.eventHandler.processManualEvent(eventData);
    logger.infoLog('Processed manual event');
    return { message: `Processed` };
  }
}
