import { Module } from '@nestjs/common';
import { ApaleoController } from './apaleo.controller';
import ApaleoEventHandler from './ApaleoEventHandler';
import { ApaleoClient } from './ApaleoClient';
import { CommonModule } from '../common/common.module';

@Module({
  controllers: [ApaleoController],
  providers: [ApaleoEventHandler, ApaleoClient],
  exports: [ApaleoClient],
  imports: [CommonModule],
})
export class ApaleoModule { }
