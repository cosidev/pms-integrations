import { ReservationEventLogger, EnumReservationEvents } from './ReservationEventLogger';
import { templateIds } from './sendGrid';
import {
  ReservationCreatedEvent,
  ReservationCheckInEvent,
  ReservationCheckOutEvent,
  ReservationEvent,
  ReservationCanceledEvent,
  ReservationInvoiceEvent,
  ReservationCheckInDayEvent,
  Reservation,
  ReservationGuest,
  ReservationNeededGuestDataEvent,
  ReservationChangeInvoiceAddressEvent,
  ReservationBreakfastSelection,
} from './types';
import CmsClient from './CmsClient';
import { EnumReservationModelChannelCode } from './apaleo/generated/reservation';
import logger from './LoggingService';
import { ReservationDetailsService } from './common/reservation/reservationDetails.service';
import { DynamoClient } from './common/aws';
import { ReservationRefundDetailsService } from './common/reservation/reservationRefundDetails.service';

class MailService {
  private sendGridClient;
  private reservationDetailsService: ReservationDetailsService;
  private reservationEventLogger: ReservationEventLogger;

  constructor(sendGridClient, cmsClient: CmsClient) {
    this.sendGridClient = sendGridClient;
    const dynamo = new DynamoClient();
    const refundService = new ReservationRefundDetailsService(dynamo);
    this.reservationDetailsService = new ReservationDetailsService(dynamo, refundService);
    this.reservationEventLogger = new ReservationEventLogger(dynamo);
  }

  async sendBookingConfirmation(event: ReservationCreatedEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].bookingConfirmation;
    const guestData = this.buildGuestData(event.data, language);
    const templateData = {
      ...event.data,
      guestData,
      showCancelationRules: event.showCancelationRules,
      tripManagementUrl: event.tripManagementUrl,
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', event);
    await this.reservationEventLogger.logEvent({
      event: EnumReservationEvents.BookingConfirmation,
      reservationId: event.data.reservationId,
      message: 'Booking confirmation mail sent',
    });
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { bookingConfirmationSent: true });
  }

  async sendCheckInInfos(event: ReservationCheckInEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].checkInInfos;
    const codes = {
      pinAccessCode: event.pinAccessCode,
      pinComfortCode: event.pinComfortCode,
      mainDoorCode: event.mainDoorCode,
      earlyCheckInCode: event.earlyCheckInCode,
    };
    const templateData = {
      ...event.data,
      pinAccessCode: event.pinAccessCode,
      pinComfortCode: event.pinComfortCode,
      roomNumber: event.roomNumber,
      roomDirection: await this.replaceVariables(event.roomDirection, codes),
      checkInAndOutInfos: await this.replaceVariables(event.checkInOutInfos, codes),
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', templateData);
    await this.reservationEventLogger.logEvent({
      event: EnumReservationEvents.CheckIn,
      reservationId: event.data.reservationId,
      message: 'CheckIn information mail sent',
      additionalData: templateData,
    });
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { checkinInfosSent: true });
  }

  async sendCheckInDayInfos(event: ReservationCheckInDayEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].checkInDayInfos;
    const codes = {
      pinAccessCode: event.pinAccessCode,
      pinComfortCode: event.pinComfortCode,
      mainDoorCode: event.mainDoorCode,
      earlyCheckInCode: event.earlyCheckInCode,
    };
    const templateData = {
      ...event.data,
      pinAccessCode: event.pinAccessCode,
      pinComfortCode: event.pinComfortCode,
      roomNumber: event.roomNumber,
      roomDirection: await this.replaceVariables(event.roomDirection, codes),
      checkInAndOutInfos: await this.replaceVariables(event.checkInOutInfos, codes),
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', templateData);
    await this.reservationEventLogger.logEvent({
      event: EnumReservationEvents.CheckInDay,
      reservationId: event.data.reservationId,
      message: 'CheckInDay information mail sent',
      additionalData: templateData,
    });
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { checkinInfosSent: true });
  }

  async sendCheckOutInfos(event: ReservationCheckOutEvent) {
    if (event.data.channelCode !== EnumReservationModelChannelCode.Ibe) {
      logger.infoLog(`Not sending checkout infos confirmation. Channel = ${event.data.channelCode}`);
      return;
    }
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].checkOutInfos;
    await this.sendMail(this.getMailData(templateId, event, event.data));
    logger.infoLog('Mail sent', event);
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { checkoutInfosSent: true });
  }

  async sendNeededGuestData(event: ReservationNeededGuestDataEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].neededGuestData;
    const templateData = {
      ...event.data,
      regulatoryLink: event.neededDataUrl,
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', templateData);
    return true;
  }

  async sendDailyBreakfastReminder(event: ReservationBreakfastSelection) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].breakfastSelection;
    const templateData = {
      ...event.data,
      breakfastUrl: event.breakfastUrl,
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog(`${event.type} Mail sent`, { mailData: event.data });
    return true;
  }

  async sendChangeInvoiceAddress(event: ReservationChangeInvoiceAddressEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].changeInvoiceAddress;
    const templateData = {
      ...event.data,
      invoiceChangeLink: event.changeInvoiceAddressUrl,
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', templateData);
    return true;
  }

  async sendCancelConfirmation(event: ReservationCanceledEvent) {
    if (event.data.channelCode !== EnumReservationModelChannelCode.Ibe) {
      logger.infoLog(`Not sending cancel confirmation. Channel = ${event.data.channelCode}`);
      return;
    }
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].bookingCanceled;
    const templateData = {
      ...event.data,
      cancellationFee: event.cancellationFee,
    };
    await this.sendMail(this.getMailData(templateId, event, templateData));
    logger.infoLog('Mail sent', event);
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { cancelConfirmationSent: true });
  }

  async sendInvoices(event: ReservationInvoiceEvent) {
    const { language = 'en' } = event.data;
    const templateId = templateIds[language].invoices;
    const mailData = this.getMailData(templateId, event, event.data, {
      attachments: event.invoices.map(invoice => ({
        content: invoice,
        type: 'application/pdf',
        filename: `${language === 'de' ? 'Rechnung' : 'Invoice'}.pdf`,
      })),
    });
    await this.sendMail(mailData);
    logger.infoLog('Mail sent', event);
    return this.reservationDetailsService.updateDetails(event.data.reservationId, { invoiceSent: true });
  }

  private async sendMail(mailData: any) {
    try {
      await this.sendGridClient.send(mailData);
    } catch (e) {
      logger.errorLog('Could not send mail', { e, mailData });
    }
  }

  private buildGuestData(reservation: Reservation, lang: string): string {
    const { primaryGuest, additionalGuests } = reservation;
    let content = `<p><strong>${lang === 'de' ? 'Gastdaten' : 'Guest information'}</strong></p>\n`;
    let foundValue = false;
    const translations = {
      fullName: { de: 'Name', en: 'Name' },
      citizenship: { de: 'Staatsangehörigkeit', en: 'Citizenship' },
      idNumber: { de: 'Ausweis Nummer', en: 'ID Number' },
      address: { de: 'Addresse', en: 'Address' },
    };
    const guestToHtml = (guest: ReservationGuest) => {
      Object.keys(translations).forEach(key => {
        if (guest[key] && guest[key] !== '') {
          foundValue = true;
          content += `<p><strong>${translations[key][lang]}: ${guest[key]}</strong></p>\n`;
        }
      });
    };
    guestToHtml(primaryGuest);
    content += '\n';
    additionalGuests.forEach(guest => {
      guestToHtml(guest);
      content += '\n';
    });
    if (!foundValue) { return ''; }
    content += `</p>`;
    return content;
  }

  async replaceVariables(content: string, data: object) {
    return content.replace(/{{([a-z]|[A-Z])*}}/g, (match: string) => {
      const key = match.replace(/{{|}}/g, '');
      return data[key] || key;
    });
  }

  private getMailData(templateId: string, event: ReservationEvent, dynamicTemplateData: any, other?: any) {
    const { booker, primaryGuest, additionalGuests } = event.data;
    const to = booker.email ? [booker.email] : [];

    if (primaryGuest.email && primaryGuest.email !== booker.email) {
      to.push(primaryGuest.email);
    }

    additionalGuests.forEach(eachAdditionalGuest => {
      if (eachAdditionalGuest.email && !(to.includes(eachAdditionalGuest.email))) {
        to.push(eachAdditionalGuest.email);
      }
    });

    if (to.length === 0) {
      logger.errorLog(`Cant send mail. No email found for ${event.data.reservationId} ${event.type}`, {});
      return;
    }

    return {
      templateId,
      dynamicTemplateData,
      from: {
        email: event.data.property.bookingEmail,
        name: event.data.property.name,
      },
      to,
      ...other,
    };
  }
}

export default MailService;
