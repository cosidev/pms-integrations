import { Injectable } from '@nestjs/common';
import { SiteClient } from 'datocms-client';
import { CmsProperty, CmsReservation, ApaleoReservation } from './types';
import { generalConfig } from './config';
import logger from './LoggingService';
import axios from 'axios';

const itemTypes = {
  reservation: '134532',
  property: '134530',
};

@Injectable()
export default class CmsClient {
  datoClient: any;

  constructor() {
    this.datoClient = new SiteClient(generalConfig.datoApiKey);
  }

  async getPropertyInfos(propertyId: string, fetchRooms: boolean = true): Promise<CmsProperty> {
    logger.infoLog(`Loading cms property ${propertyId}`);
    const query = (id: string, locale: string): string => `query x { property(filter: { propertyId: {eq: "${id}"} }, locale: ${locale}) {
      propertyId
      bookingMail
      bookingPage
      signature
      additionalInformation
      emailFooter
      checkInAndOutDetails
      rooms {
        id
        rid
        directions
        checkInCheckOutDetailsPerRoom
      }
      availableServices
      parking
      breakfastRecommendations
      id
      createdAt
      updatedAt
    }
  }`;
    const {
      data: { property: propertyEn },
    } = await this.graphqlRequest(query(propertyId, 'en'));
    const {
      data: { property: propertyDe },
    } = await this.graphqlRequest(query(propertyId, 'de'));
    const cmsProperty: CmsProperty = {
      propertyId,
      bookingMail: propertyEn.bookingMail,
      bookingPage: propertyEn.bookingPage,
      signature: {
        de: propertyDe.signature,
        en: propertyEn.signature,
      },
      additionalInformation: {
        de: propertyDe.additionalInformation,
        en: propertyEn.additionalInformation,
      },
      emailFooter: {
        de: propertyDe.emailFooter,
        en: propertyEn.emailFooter,
      },
      checkInAndOutDetails: {
        de: propertyDe.checkInAndOutDetails,
        en: propertyEn.checkInAndOutDetails,
      },
      rooms: {
        de: this.formatPropertyRooms(propertyDe.rooms),
        en: this.formatPropertyRooms(propertyDe.rooms),
      },
      availableServices: propertyEn.availableServices || [],
      parking: {
        de: propertyDe.parking,
        en: propertyEn.parking,
      },
      breakfastRecommendations: {
        de: propertyDe.breakfastRecommendations,
        en: propertyEn.breakfastRecommendations,
      },
      updatedAt: propertyEn.updatedAt,
      createdAt: propertyEn.createdAt,
      id: propertyEn.id,
    };
    return cmsProperty;
  }

  private async graphqlRequest(query: string) {
    const response = await axios.post(
      'https://graphql.datocms.com',
      { query },
      {
        headers: {
          Authorization: `Beaerer ${generalConfig.datoApiKey}`,
        },
      },
    );
    if (!response.data || response.data.errors) {
      logger.errorLog(`Could not fetch datocms graphql api`, { query, errors: response.data?.errors });
      throw new Error(`Could not fetch datocms graphql api`);
    }
    return response.data;
  }

  private formatPropertyRooms(rooms: any) {
    const formattedRooms = {};
    rooms.forEach(room => {
      if (room.rid.includes(',')) {
        // Some rooms share same infos, rid contains multiple room ids
        const allRoomIds = room.rid.split(',');
        allRoomIds.forEach(id => {
          formattedRooms[id] = room;
        });
        return;
      }
      formattedRooms[room.rid] = room;
    });
    return formattedRooms;
  }

  async getReservationInfos(reservationId: string): Promise<CmsReservation> {
    const reservation = await this.getEntry(itemTypes.reservation, 'reservation_id', reservationId);
    if (reservation) {
      reservation.specialWishes = reservation.specialWishes ? JSON.parse(reservation.specialWishes) : [];
      reservation.breakfastSelection = reservation.breakfastSelection ? JSON.parse(reservation.breakfastSelection) : [];
    }
    return reservation;
  }

  async createReservation(reservation: ApaleoReservation): Promise<CmsReservation> {
    const defaultReservation = {
      reservationId: reservation.id,
      bookingConfirmationSent: false,
      invoiceSent: false,
      cancelConfirmationSent: false,
      checkinInfosSent: false,
      checkoutInfosSent: false,
      pinAccessCode: '',
      pinComfortCode: '',
      allergies: '',
      bookerName: `${reservation.booker.firstName} ${reservation.booker.lastName}`,
      specialWishes: '[]',
      breakfastSelection: '[]',
    };
    return await this.createEntry(itemTypes.reservation, defaultReservation);
  }

  async createEntry(itemType: string, values: any) {
    const entry = await this.datoClient.items.create({
      itemType,
      ...values,
    });
    return entry;
  }

  updateEntry(itemId, values) {
    return this.datoClient.items.update(itemId, values);
  }

  async getEntry(itemType: string, itemIdPath: string, id: string) {
    logger.debugLog(`Loading datocms entry`, {
      datoParams: {
        itemType,
        itemIdPath,
        id,
      },
    });
    const filter = {
      'filter[type]': itemType,
      [`filter[fields][${itemIdPath}][eq]`]: id,
    };
    logger.debugLog(`Loading datocms records`, {
      datoFilterParams: filter,
    });
    const records = await this.datoClient.items.all(filter);
    logger.debugLog(`Got datocms response`, {
      datoResponse: records,
    });
    if (!records[0]) {
      return false;
    }
    return records[0];
  }
}
