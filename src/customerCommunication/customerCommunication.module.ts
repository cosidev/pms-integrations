import { Module } from '@nestjs/common';
import CustomerCommunicationService from './customerCommunication.service';
import MailService from './mail.service';
import MailCmsService from './mailCms/mailCms.service';
import { CommonModule } from '../common/common.module';
import MailReservationService from './mailReservation.service';
import { ApaleoModule } from '../apaleo/apaleo.module';
import { ReservationEventLogger } from '../ReservationEventLogger';

@Module({
  imports: [CommonModule, ApaleoModule],
  controllers: [],
  providers: [CustomerCommunicationService, MailService, ReservationEventLogger, MailCmsService, MailReservationService],
})
export class CustomerCommunicationModule {}
