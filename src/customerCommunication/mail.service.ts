import { Injectable } from '@nestjs/common';
import sendgrid from '@sendgrid/mail';
import mjml from 'mjml';
import { generalConfig } from '../config';
import logger from '../LoggingService';
import {
  ReservationEvent,
  ReservationCheckInEvent,
  ReservationCheckInDayEvent,
  ReservationInvoiceEvent,
  ReservationNeededGuestDataEvent,
} from '../types';
import MailCmsService from './mailCms/mailCms.service';
import { bookingConfirmation, checkIn, checkInDay, legal } from './templates';
import MailReservationService, { MailReservation } from './mailReservation.service';
import { ReservationEventLogger, EnumReservationEvents } from '../ReservationEventLogger';
import { ReservationDetailsService } from '../common/reservation/reservationDetails.service';
import { MailData, Mail } from './mailCms/types';
import invoice from './templates/invoice';

@Injectable()
export default class MailService {
  private sendGridClient: any;

  constructor(
    private readonly mailCmsService: MailCmsService,
    private readonly reservationEventLogger: ReservationEventLogger,
    private readonly reservationDetailsService: ReservationDetailsService,
    private readonly mailReservationService: MailReservationService,
  ) {
    this.sendGridClient = sendgrid;
    this.sendGridClient.setApiKey(generalConfig.sendGridApiToken);
  }

  async sendBookingConfirmation(event: ReservationEvent) {
    try {
      const data = await this.mailCmsService.getBookingConfirmationContent(event.data.property.id, event.data.language);
      const reservation = await this.mailReservationService.getMailReservation(event.data.reservationId);
      const template = bookingConfirmation(data, reservation);
      await this.sendMail(template, data, reservation);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.BookingConfirmation,
        reservationId: event.data.reservationId,
        message: 'Booking confirmation mail sent',
      });
      await this.reservationDetailsService.updateDetails(event.data.reservationId, { bookingConfirmationSent: true });
    } catch (e) {
      logger.errorLog(e.message, { e });
    }
  }

  async sendCheckInInfos(event: ReservationCheckInEvent) {
    try {
      const data = await this.mailCmsService.getCheckInContent(event.data.property.id, event.data.language);
      const reservation = await this.mailReservationService.getMailReservation(event.data.reservationId);

      const unit = reservation.unitNumber;
      if (!unit) {
        throw new Error(`Cant send mail. Missing assigend unit ${event.data.reservationId} (${event.data.property.id})`);
      }
      const replaceKey = (key: string) => {
        data.property[key] = data.property.units[unit]?.[key] || data.property[key];
      };
      const overwriteConfig = ['roomAccess1', 'roomAccess2', 'roomAccess3', 'roomDirections', 'wifiInformation'];
      overwriteConfig.forEach(replaceKey);

      if (event.data.property.id === 'DE_BE_002') {
        if (unit.substr(0, 2) === '47') {
          data.property.address = 'Warschauerstraße 47, 10243 Berlin';
        }
      }

      const template = checkIn(data, reservation);
      await this.sendMail(template, data, reservation);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.CheckIn,
        reservationId: event.data.reservationId,
        message: 'CheckIn information mail sent',
        additionalData: {},
      });
      await this.reservationDetailsService.updateDetails(event.data.reservationId, { checkinInfosSent: true });
    } catch (e) {
      logger.errorLog(e.message, { e });
    }
  }

  async sendCheckInDayInfos(event: ReservationCheckInDayEvent) {
    try {
      const data = await this.mailCmsService.getCheckInDayContent(event.data.property.id, event.data.language);
      const reservation = await this.mailReservationService.getMailReservation(event.data.reservationId);

      const unit = reservation.unitNumber;
      if (!unit) {
        throw new Error(`Cant send mail. Missing assigend unit ${event.data.reservationId} (${event.data.property.id})`);
      }
      const replaceKey = (key: string) => {
        data.property[key] = data.property.units[unit]?.[key] || data.property[key];
      };
      const overwriteConfig = ['roomAccess1', 'roomAccess2', 'roomAccess3', 'roomDirections', 'wifiInformation'];
      overwriteConfig.forEach(replaceKey);

      if (event.data.property.id === 'DE_BE_002') {
        if (unit.substr(0, 2) === '47') {
          data.property.address = 'Warschauerstraße 47, 10243 Berlin';
        }
      }

      const template = checkInDay(data, reservation);
      await this.sendMail(template, data, reservation);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.CheckInDay,
        reservationId: event.data.reservationId,
        message: 'CheckInDay information mail sent',
        additionalData: {},
      });
      await this.reservationDetailsService.updateDetails(event.data.reservationId, { checkinDayInfosSent: true });
    } catch (e) {
      logger.errorLog(e.message, { e });
    }
  }

  async sendInvoice(event: ReservationInvoiceEvent) {
    try {
      const data = await this.mailCmsService.getInvoiceContent(event.data.property.id, event.data.language);
      const reservation = await this.mailReservationService.getMailReservation(event.data.reservationId);

      const template = invoice(data, reservation);

      const attachments = event.invoices.map(invoice => ({
        content: invoice,
        type: 'application/pdf',
        filename: `${event.data.language === 'de' ? 'Rechnung' : 'Invoice'}.pdf`,
      }));
      await this.sendMail(template, data, reservation, attachments);
      await this.reservationEventLogger.logEvent({
        event: EnumReservationEvents.InvoiceGenerated,
        reservationId: event.data.reservationId,
        message: 'Invoice mail sent',
        additionalData: {},
      });

      await this.reservationDetailsService.updateDetails(event.data.reservationId, { invoiceSent: true });
    } catch (e) {
      logger.errorLog(e.message, { e });
    }
  }

  async sendLegalRequirementsMail(event: ReservationNeededGuestDataEvent) {
    try {
      const data = await this.mailCmsService.getLegalContent(event.data.property.id, event.data.language);
      const reservation = await this.mailReservationService.getMailReservation(event.data.reservationId);

      const template = legal(data, reservation);

      await this.sendMail(template, { ...data, buttonUrl: reservation.urls?.neededData }, reservation);
    } catch (e) {
      logger.errorLog(e.message, { e });
    }
  }

  private async sendMail<T extends Mail>(template: string, data: MailData<T>, reservation: MailReservation, attachments?: any) {
    try {
      let html = mjml(template, { minify: true }).html;
      html = this.replaceVariables(html, { ...data, reservation });

      if (reservation.recipients.length === 0) {
        throw new Error(`Cant send mail. No email found for ${reservation.id} (${reservation.propertyId})`);
      }

      const mailData = {
        to: reservation.recipients,
        from: {
          email: data.property.bookingMail,
          name: data.property.name,
        },
        subject: data.mail.emailSubject,
        html,
        attachments,
      };
      await this.sendGridClient.send(mailData);
      logger.infoLog('Mail sent', { reservationId: reservation.id, propertyId: reservation.propertyId });
    } catch (e) {
      logger.errorLog(`Could not send mail ${reservation.id} (${reservation.propertyId})`, { message: e.message, e });
      throw new Error(`Could not send mail ${reservation.id} (${reservation.propertyId})`);
    }
  }

  private replaceVariables(html: string, data: any): string {
    const variableSyntax = /\{{(.*?)\}}/g;
    return html.replace(variableSyntax, match => {
      const key = match.replace(/{{|}}/g, '');
      const path = key.split(/[\.\[\]\'\"]/).filter(p => p);
      const value = path.reduce((o, p) => (o ? o[p] : undefined), data) || key;
      return value;
    });
  }
}
