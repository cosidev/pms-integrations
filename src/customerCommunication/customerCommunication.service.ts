import { Injectable } from '@nestjs/common';
import { ReservationEvent, MailType } from '../types';
import MailService from './mail.service';

@Injectable()
export default class CustomerCommunicationService {
  constructor(private readonly mailService: MailService) {}

  async handleReservationEvent(event: ReservationEvent) {
    // Decide which email to send
    switch (event.type) {
      case MailType.Created:
        await this.mailService.sendBookingConfirmation(event);
        break;
    }
  }
}
