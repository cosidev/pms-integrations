interface MjmlTag {
  tagName: string;
  attributes: any;
  children: MjmlTag[];
  content?: string;
}

export const tag = (tagName: string, attributes: any = {}, children: MjmlTag[] = [], content: string = ''): MjmlTag => {
  const formattedAttributes = {};
  Object.keys(attributes).forEach(key => {
    const dashedKey = key.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    formattedAttributes[dashedKey] = attributes[key];
  });
  return {
    tagName,
    attributes: formattedAttributes,
    children,
    content,
  };
};

export const mjFont = (name: string, href: string): MjmlTag => tag('mj-font', { name, href });
export const mjWrapper = (attributes: any = {}, children = []): MjmlTag => tag('mj-wrapper', attributes, children);
export const mjGroup = (attributes: any = {}, children = []): MjmlTag => tag('mj-group', attributes, children);
export const mjSection = (attributes: any = {}, children = []): MjmlTag => tag('mj-section', attributes, children);
export const mjColumn = (children = [], attributes: any = {}): MjmlTag => tag('mj-column', attributes, children);
export const mjSocial = (attributes: any = {}, children = []): MjmlTag => tag('mj-social', attributes, children);
export const mjSocialElement = (attributes: any = {}): MjmlTag => tag('mj-social-element', attributes);
export const mjText = (content: string, attributes: any = {}): MjmlTag => tag('mj-text', attributes, [], content);
export const mjImage = (src: string, attributes: any = {}): MjmlTag => tag('mj-image', { src, ...attributes });
export const mjSpacer = (attributes: any = {}): MjmlTag => tag('mj-spacer', attributes);
