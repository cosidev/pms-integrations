import * as mjml2html from 'mjml';
import * as sgMail from '@sendgrid/mail';
import { mjFont, tag, mjWrapper, mjSection, mjSpacer, mjColumn, mjText, mjGroup, mjSocial, mjSocialElement } from './utils';

const assetBasePath = 'https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets';

const styles = {
  colors: {
    white: '#ffffff',
    textDark: '#2B2E34',
    textLight: '#6F7175',
    lightGrey: '#9A9B9F',
    orange: 'rgb(202, 87, 24)',
  },
  fonts: {
    text: 'Verdana',
    special: 'Amalfi Coast',
    brand: 'Bell Mt Std',
    proxmia: 'Proxima Nova',
  },
  fontSizes: {
    default: '13px',
    lineHeight: '16px',
  },
};

const text = (content: string, attributes: any) =>
  mjText(content, {
    fontFamily: styles.fonts.text,
    fontSize: styles.fontSizes.default,
    lineHeight: styles.fontSizes.lineHeight,
    color: styles.colors.textLight,
    ...attributes,
  });

const cosiHead = tag('mj-head', {}, [
  mjFont('Amalfi Coast', `${assetBasePath}/css/amalfi.css`),
  mjFont('Proxima Nova', `${assetBasePath}/css/proxima.css`),
  mjFont('Bell Mt Std', `${assetBasePath}/css/BellMTSdt.css`),
  mjFont('Optima', `${assetBasePath}/css/optima.css`),
]);

const cosiHeader = mjSection({}, [
  mjColumn([
    mjText('COSI', {
      paddingBottom: '0',
      align: 'center',
      letterSpacing: '4px',
      fontFamily: styles.fonts.brand,
      fontSize: '40px',
      color: styles.colors.textDark,
    }),
    mjText(
      `Changing the way guests <span style="color: #C95617; font-family: '${styles.fonts.special}'; font-size: 20px; margin: 0 5px;">feel</span> about travel`,
      {
        paddingBottom: '0',
        align: 'center',
        fontFamily: styles.fonts.proxmia,
        fontSize: '14px',
        color: '#595B60',
      },
    ),
  ]),
]);

const cosiFooter = [
  mjSection({ padding: 0 }, [
    mjColumn([
      text('Best regards,', { color: styles.colors.textDark, align: 'center' }),
      mjText('Cosi team', { fontFamily: styles.fonts.special, fontSize: '21px', align: 'center' }),
    ]),
  ]),
  mjSection({ padding: 0 }, [
    mjGroup({}, [
      mjColumn([
        mjSocial({ fontSize: '15px', iconHeight: '21px', mode: 'horizontal', innerPadding: '15px' }, [
          mjSocialElement({ iconHeight: '21px', src: `${assetBasePath}/images/footer-facebook.png?v=2` }),
          mjSocialElement({ iconHeight: '21px', src: `${assetBasePath}/images/footer-instagram.png?v=2` }),
          mjSocialElement({ iconHeight: '21px', src: `${assetBasePath}/images/footer-linkedin.png?v=2` }),
        ]),
      ]),
    ]),
  ]),
  mjSection({ padding: 0 }, [
    mjColumn([
      text('www.cosi-group.com', { align: 'center', paddingTop: 0 }),
      text(`View our <span style="text-decoration: underline; color: ${styles.colors.orange}">Terms & Conditions</span> for more information`, {
        align: 'center',
        color: styles.colors.textLight,
      }),
      mjSpacer({}),
      text(
        'COSI Berlin Betriebsgesellschaft GmbH | Oranienburger Straße 45 | 10117 Berlin Vertreten durch: Christian Gaiser, Inga Laudiero, Dimitri Chandogin, Gerhard Maringer',
        {
          fontSize: '11px',
          align: 'center',
          color: styles.colors.lightGrey,
        },
      ),
    ]),
  ]),
];

export const bodyWrapper = (children: any[]): any => ({
  tagName: 'mjml',
  attributes: {},
  children: [cosiHead, tag('mj-body', {}, [mjWrapper({ backgroundColor: styles.colors.white }, [cosiHeader, ...children, ...cosiFooter])])],
});

const sgApiKey = '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';
sgMail.setApiKey(sgApiKey);

const sendMail = async to => {
  const msg = {
    to,
    from: {
      email: 'booking@cosi-group.com',
      name: 'Belfort Berlin',
    },
    subject: 'Test',
    text: 'plain text version',
    html: mjml2html(bodyWrapper([])).html,
  };

  sgMail.send(msg).then(
    () => {
      console.log('Sent');
    },
    error => {
      console.error(error);

      if (error.response) {
        console.error(error.response.body);
      }
    },
  );
};

const to = ['luca.demmel@cosi-group.com'];
sendMail(to);
