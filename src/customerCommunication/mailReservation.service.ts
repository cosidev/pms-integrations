import { Injectable } from '@nestjs/common';
import dateformat from 'dateformat';
import { ApaleoClient } from '../apaleo/ApaleoClient';
import { ReservationDetailsService } from '../common/reservation/reservationDetails.service';
import ReservationService from '../ReservationService';
import { ReservationModel } from '../apaleo/generated/reservation';

export interface MailReservation {
  id: string;
  bookingId: string;
  propertyId: string;
  guestName: string;
  formattedCheckIn: string;
  formattedCheckOut: string;
  checkIn: string;
  checkOut: string;
  unitGroup: string;
  totalGross: string;
  formattedGuests: string;
  recipients: string[];
  formattedPaymentInformation?: string;
  unitNumber?: string;
  pinCode?: number;
  // Belfort
  roomCode?: number;
  oneTimeActivationCode?: number;
  urls: {
    [key: string]: string;
  };
}

@Injectable()
export default class MailReservationService {
  private reservationService: ReservationService;

  constructor(private readonly apaleoClient: ApaleoClient, private readonly reservationDetailsService: ReservationDetailsService) {
    this.reservationService = new ReservationService();
  }

  async getMailReservation(reservationId: string): Promise<MailReservation> {
    const expands = ['booker', 'assignedUnits'];
    let reservation = await this.apaleoClient.getReservationById(reservationId, expands);
    const language = this.reservationService.getPreferredLanguage(reservation);
    if (language !== this.apaleoClient.language) {
      this.apaleoClient.language = language;
      reservation = await this.apaleoClient.getReservationById(reservation.id, expands);
    }
    const reservationDetails = await this.reservationDetailsService.getDetails(reservationId);

    return {
      id: reservationId,
      bookingId: reservation.bookingId,
      propertyId: reservation.property.id,
      guestName: `${reservation.booker.firstName || ''} ${reservation.booker.lastName}`.trim(),
      formattedCheckIn: dateformat(reservation.arrival, `3:00p'm' | mmm dd, yyyy`),
      formattedCheckOut: dateformat(reservation.departure, `11:00a'm' | mmm dd, yyyy`),
      checkIn: dateformat(reservation.arrival, 'dd.mm.yyyy'),
      checkOut: dateformat(reservation.departure, 'dd.mm.yyyy'),
      totalGross: `${reservation.totalGrossAmount.amount}${reservation.totalGrossAmount.currency}`,
      unitGroup: reservation.unitGroup.name,
      unitNumber: reservation.unit ? this.reservationService.getFormattedRoomName(reservation.unit.name) : undefined,
      formattedGuests: this.getFormattedGuests(reservation, language),
      formattedPaymentInformation: undefined,
      oneTimeActivationCode: reservationDetails.pinCodes?.pinAccessCode,
      roomCode: reservationDetails.pinCodes?.pinComfortCode,
      pinCode: reservationDetails.pinCodes?.mainDoorCode || reservationDetails.pinCodes?.pinComfortCode,
      recipients: this.getRecipients(reservation),
      urls: reservationDetails.urls || {},
    };
  }

  private getFormattedGuests(reservation: ReservationModel, language: string) {
    return language === 'en'
      ? `${reservation.adults} Adults ${reservation.childrenAges || [].length > 0 ? `, ${reservation.childrenAges.length} Children` : ''}`
      : `${reservation.adults} Erwachsene ${reservation.childrenAges || [].length > 0 ? `, ${reservation.childrenAges.length} Kinder` : ''}`;
  }

  private getRecipients(reservation: ReservationModel) {
    const { booker, primaryGuest, additionalGuests = [] } = reservation;
    const recipients = booker.email ? [booker.email] : [];
    if (primaryGuest.email && primaryGuest.email !== booker.email) {
      recipients.push(primaryGuest.email);
    }

    additionalGuests.forEach(eachAdditionalGuest => {
      if (eachAdditionalGuest.email && !recipients.includes(eachAdditionalGuest.email)) {
        recipients.push(eachAdditionalGuest.email);
      }
    });
    return recipients;
  }
}
