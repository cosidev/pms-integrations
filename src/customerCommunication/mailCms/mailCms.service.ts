import { Injectable } from '@nestjs/common';
import { DatoCmsService } from '../../common/datoCms/DatoCms.service';
import { generalContent, bookingConfirmation, checkIn, checkInDay, invoice, legal, propertyContent } from './graphqlTemplates';
import { MailData, BookingConfirmationContent, CheckInContent, UnitContent, Mail, CheckInDayContent, InvoiceContent, LegalContent } from './types';
import ReservationService from '../../ReservationService';

@Injectable()
export default class MailCmsService {
  private readonly reservationService: ReservationService;
  constructor(private readonly datoService: DatoCmsService) {
    this.reservationService = new ReservationService();
  }

  async getBookingConfirmationContent(propertyId: string, locale: string): Promise<MailData<BookingConfirmationContent>> {
    const formatBookingConfirmationData = (data: any): BookingConfirmationContent => ({
      ...data,
      bookingConfirmationSrc: data.bookingConfirmationSrc.url,
    });
    return this.getEmailContent<BookingConfirmationContent>(
      bookingConfirmation,
      'bookingConfirmationEmail',
      propertyId,
      locale,
      formatBookingConfirmationData,
    );
  }

  async getCheckInContent(propertyId: string, locale: string): Promise<MailData<CheckInContent>> {
    const formatCheckInData = (data: any): CheckInContent => data;
    return this.getEmailContent<CheckInContent>(checkIn, 'checkInMail', propertyId, locale, formatCheckInData);
  }

  async getCheckInDayContent(propertyId: string, locale: string): Promise<MailData<CheckInDayContent>> {
    const formatCheckInData = (data: any): CheckInDayContent => data;
    return this.getEmailContent<CheckInDayContent>(checkInDay, 'checkInDayEmail', propertyId, locale, formatCheckInData);
  }

  async getInvoiceContent(propertyId: string, locale: string): Promise<MailData<InvoiceContent>> {
    const formatInvoiceData = (data: any): InvoiceContent => data;
    return this.getEmailContent<InvoiceContent>(invoice, 'invoiceEmail', propertyId, locale, formatInvoiceData);
  }

  async getLegalContent(propertyId: string, locale: string): Promise<MailData<LegalContent>> {
    const formatLegalContent = (data: any): LegalContent => ({
      ...data,
      greetingSrc: data.greetingSrc.url,
    });
    return this.getEmailContent<LegalContent>(legal, 'legalRequirementsEmail', propertyId, locale, formatLegalContent);
  }

  private async getEmailContent<T extends Mail>(
    email: string,
    emailName: string,
    propertyId: string,
    locale: string,
    formatMailData: (x: any) => T,
  ): Promise<MailData<T>> {
    const query = `
    query x {
        email(locale: ${locale}) {
          ${generalContent}
          ${email}
        }
        ${propertyContent(propertyId, locale)}
    }
  `;
    const {
      data: { email: mail, property },
    } = await this.datoService.graphqlRequest(query);
    const data = {
      general: {
        ...mail.generalContent[0],
        cosiTeamSrc: mail.generalContent[0].cosiTeam.url,
      },
      mail: formatMailData(mail[emailName][0]),
      property: {
        propertyId,
        ...property.emailContent[0],
        illustration: property.emailContent[0].illustration?.url,
        units: this.formatPropertyUnits(property.emailUnitContent),
      },
    };

    return this.replaceHtmlPTags(data);
  }

  private formatPropertyUnits(emailUnits: UnitContent[]) {
    const units = {};
    emailUnits.forEach(unit => {
      if (unit.unitName.includes(',')) {
        // Some rooms share same infos, rid contains multiple room ids
        const allUnitNames = unit.unitName.split(',');
        allUnitNames.forEach(name => {
          units[this.reservationService.getFormattedRoomName(name)] = unit;
        });
        return;
      }
      units[this.reservationService.getFormattedRoomName(unit.unitName)] = unit;
    });
    return units;
  }

  // Replaces the default <p></p> wrapper of datocms html editor fields
  private replaceHtmlPTags(data: any) {
    if (typeof data === 'string' && data.substr(0, 3) === '<p>') {
      // <p>content</p> => content
      return data.substr(3, data.length).substr(0, data.length - 7);
    }
    if (Array.isArray(data)) {
      return data.map(entry => {
        return this.replaceHtmlPTags(entry);
      });
    }
    if (typeof data === 'object' && data !== null) {
      const newData = {};
      Object.keys(data).map(key => {
        newData[key] = this.replaceHtmlPTags(data[key]);
      });
      return newData;
    }
    return data;
  }
}
