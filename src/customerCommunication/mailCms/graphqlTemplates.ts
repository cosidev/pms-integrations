export const propertyContent = (propertyId: string, locale: string) => `
  property(filter: { propertyId: { eq: "${propertyId}"}}, locale: ${locale}) {
    propertyId
    emailContent {
      name
      address
      city
      illustration {
        url
      }
      phoneNumber
      additionalInfo
      checkInNote
      earlyCheckinInfo
      bookingMail
      messengerUrl
      whatsappNumber
      directionsUrl
      wifiInformation
      houseRules
      houseRulesHeadline
      imprint
      luggageStorage
      luggageStorageShort
      luggageStorageDirections
      parkingInfo
      roomAccess1
      roomAccess2
      roomAccess3
    }
    emailUnitContent {
      unitName
      roomAccess1
      roomAccess2
      roomAccess3
      roomDirections
      wifiInformation
    }
  }
`;

export const generalContent = `
generalContent {
  checkInDate
  checkOutDate
  checkInSupport
  bookingId
  cosiTeam {
    url
  }
  directionsCta
  emailCta
  facebookUrl
  hotelAddress
  instagramUrl
  linkedinUrl
  luggageStorageDirectionsHeadline
  luggageStorageHeadline
  manualButton
  callCta
  messengerCta
  oneTimeActivationCode
  pinCode
  questionsHeadline
  questionsInfo
  roomAccessHeadline
  tncInfo
  totalPrice
  tripManagementCta
  tripManagementInfo
  unitGroup
  unitNumber
  websiteUrl
  whatsappCta
  wifiKeyHeadline
  bestRegards
  bookingDetails
}`;

export const bookingConfirmation = `
bookingConfirmationEmail {
  bookingConfirmationSrc {
    url
  }
  greeting
  intro
  numberOfGuests
  checkIn
  checkOut
  creditCardTransactionDetails
  earlyCheckinHeadline
  emailPreview
  emailSubject
  footerIntro
  guest
  parkingHeadline
  reservationId
  supportInfo
}
`;

export const checkIn = `
checkInMail {
  emailPreview
  emailSubject
  footerIntro
  greeting
  intro
  luggageStorageDirectionsHeadline
  roomDirectionsHeadline
  title
}
`;

export const checkInDay = `
checkInDayEmail {
  emailSubject
  emailPreview
  footerIntro
  greeting
  intro
  title
}
`;

export const invoice = `
invoiceEmail {
  emailPreview
  emailSubject
  footerIntro
  greeting
  intro
  invoiceAttached
  title
}`;

export const legal = `
legalRequirementsEmail {
  emailPreview
  emailSubject
  greetingSrc {
    url
  }
  intro
  linkAction
  provideInfoCta
  footerIntro
}`;
