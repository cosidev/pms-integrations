export interface Mail {
  emailSubject: string;
  emailPreview: string;
}

export interface MailData<T extends Mail> {
  general: GeneralEmailContent;
  property: PropertyEmailContent;
  mail: T;
  buttonUrl?: string;
}

export interface GeneralEmailContent {
  checkInDate: string;
  checkOutDate: string;
  bookingId: string;
  checkInSupport: string;
  cosiTeamSrc: string;
  callCta: string;
  directionsCta: string;
  emailCta: string;
  facebookUrl: string;
  hotelAddress: string;
  instagramUrl: string;
  linkedinUrl: string;
  luggageStorageDirectionsHeadline: string;
  luggageStorageHeadline: string;
  manualButton: string;
  messengerCta: string;
  oneTimeActivationCode: string;
  pinCode: string;
  questionsHeadline: string;
  questionsInfo: string;
  roomAccessHeadline: string;
  tncInfo: string;
  totalPrice: string;
  tripManagementCta: string;
  tripManagementInfo: string;
  unitGroup: string;
  unitNumber: string;
  websiteUrl: string;
  whatsappCta: string;
  wifiKeyHeadline: string;
  bestRegards: string;
  bookingDetails: string;
}

export interface PropertyEmailContent {
  propertyId: string;
  name: string;
  illustration?: string;
  city: string;
  address: string;
  phoneNumber: string;
  additionalInfo?: string;
  checkInNote?: string;
  earlyCheckinInfo?: string;
  bookingMail: string;
  messengerUrl?: string;
  whatsappNumber?: string;
  directionsUrl?: string;
  wifiInformation?: string;
  houseRules: string;
  houseRulesHeadline: string;
  imprint: string;
  luggageStorage?: string;
  luggageStorageShort?: string;
  luggageStorageDirections?: string;
  parkingInfo?: string;
  roomAccess1: string;
  roomAccess2: string;
  roomAccess3: string;
  roomDirections: string;
  units: {
    [unitName: string]: UnitContent;
  };
}

export interface UnitContent {
  unitName: string;
  roomAccess1?: string;
  roomAccess2?: string;
  roomAccess3?: string;
  roomDirections?: string;
  wifiInformation?: string;
}

export interface BookingConfirmationContent extends Mail {
  bookingConfirmationSrc: string;
  greeting: string;
  intro: string;
  numberOfGuests: string;
  checkIn: string;
  checkOut: string;
  creditCardTransactionDetails: string;
  earlyCheckinHeadline: string;
  footerIntro: string;
  guest: string;
  parkingHeadline: string;
  reservationId: string;
  supportInfo: string;
}

export interface CheckInContent extends Mail {
  title: string;
  greeting: string;
  intro: string;
  roomDirectionsHeadline: string;
  luggageStorageDirectionsHeadline: string;
  footerIntro: string;
}

export interface CheckInDayContent extends Mail {
  title: string;
  greeting: string;
  intro: string;
  footerIntro: string;
}

export interface InvoiceContent extends Mail {
  title: string;
  greeting: string;
  intro: string;
  invoiceAttached: string;
  footerIntro: string;
}

export interface LegalContent extends Mail {
  title: string;
  greeting: string;
  greetingSrc: string;
  linkAction: string;
  provideInfoCta: string;
  intro: string;
  invoiceAttached: string;
  footerIntro: string;
}
