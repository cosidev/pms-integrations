import { head, header, footer, divider } from './common';
import { MailData, CheckInDayContent } from '../mailCms/types';
import { MailReservation } from '../mailReservation.service';

export default ({ general, mail, property }: MailData<CheckInDayContent>, reservation: MailReservation) => `<mjml>
${head(mail.emailPreview)}
  <mj-body>
    <!-- COSI Header -->
    ${header}

    <!-- Illustration hero -->
    <mj-hero mode="fluid-height" background-width="600px" background-height="228px" background-url="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/checkin-hero.png" background-color="#ffffff" padding="0px 0px"></mj-hero>

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0px">
      <mj-column>
        <mj-text
          color="#153B4A"
          align="center"
          font-family="Optima"
          font-size="30px"
          line-height="200%"
        >
          ${mail.title}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Greeting -->
    <mj-section background-color="#ffffff" padding-top="9px">
      <mj-column>
        <mj-text
          color="#2B2E34"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="16px"
        >
          ${mail.greeting}
        </mj-text>
        <mj-text
          color="#6F7175"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="16px"
        >
          ${mail.intro}
        </mj-text>
        <mj-text
          color="#153B4A"
          font-family="Verdana"
          font-weight="bold"
          font-size="13px"
          align="center"
        >
            ${general.bookingDetails}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-wrapper background-color="#ffffff">
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.hotelAddress}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" line-height="16px" font-size="13px" color="#2B2E34">${property.address}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkInDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkIn}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkOutDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkOut}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.bookingId}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.bookingId}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
            <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.unitNumber}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.unitNumber}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
    </mj-wrapper>

    <!-- Divider -->
    ${divider}

    <mj-section background-color="#ffffff" padding-bottom="0">
      <mj-column width="20%" vertical-align="top">
        <mj-image width="64px" height="57px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/pin_access.png?v=2" alt="" />
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
          align="center"
        >
          ${general.roomAccessHeadline}
        </mj-text>
      </mj-column>
    </mj-section>

    <mj-section css-class="icon-left-section" background-color="#ffffff" padding="0">
      <mj-column width="80%" vertical-align="top">
        <mj-text
          color="#6F7175"
          padding-left="0"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
          align="center"
        >
          ${property.roomAccess1}
        </mj-text>
      </mj-column>
    </mj-section>

    ${
      property.checkInNote
        ? `<mj-section background-color="#ffffff" padding="0">
    <mj-column>
      <mj-text
        align="center"
        font-size="13px"
        color="#2B2E34"
        font-family="Verdana"
      >
        ${property.checkInNote}
      </mj-text>
    </mj-column>
  </mj-section>`
        : ''
    }

    <mj-section background-color="#ffffff" padding-bottom="0">
      <mj-column>
      ${
        reservation.pinCode
          ? `
      <mj-text
          padding-bottom="30px"
          font-family="Verdana"
          font-size="16px"
          align="center"
        >
          ${general.pinCode}
          <span style="font-weight: bold; font-size: 20px; border: 1px solid; border-color: '#153B4A'; padding: 6px 20px">${reservation.pinCode}</span>
        </mj-text>`
          : ''
      }

        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
          align="center"
        >
          ${property.roomAccess2 || ''}
        </mj-text>
        ${
          reservation.oneTimeActivationCode
            ? `<mj-text
        padding-top="30px"
        padding-bottom="30px"
        color="#153B4A"
        font-family="Verdana"
        font-size="15px"
        align="center"
      >
        <span class="activation-title">
          ${general.oneTimeActivationCode}
        </span>
        <span class="activation-code">${reservation.oneTimeActivationCode}</span>
      </mj-text>`
            : ''
        }
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
          align="center"
        >
          ${property.roomAccess3 || ''}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
          align="center"
        >
          ${general.checkInSupport}
        </mj-text>

      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0">
      <mj-column>
        <mj-text color="#2B2E34" align="center" font-family="Verdana" font-size="13px">${mail.footerIntro}</mj-text>
      </mj-column>
    </mj-section>

    <!-- Footer -->
    ${footer(general, property)}
  </mj-body>
</mjml>`;
