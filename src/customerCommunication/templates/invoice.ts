import { head, header, footer, divider } from './common';
import { MailData, InvoiceContent } from '../mailCms/types';
import { MailReservation } from '../mailReservation.service';

export default ({ general, mail, property }: MailData<InvoiceContent>, reservation: MailReservation) => `<mjml>
${head(mail.emailPreview)}
  <mj-body>
    <!-- COSI Header -->
    ${header}

    <mj-section background-color="#ffffff" padding-bottom="0">
      <mj-column >
        <mj-image width="151px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/invoice.png" alt="invoice" />
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding-top="0px" padding-bottom="0px">
      <mj-column>
        <mj-text
          color="#153B4A"
          align="center"
          font-family="Optima"
          font-size="30px"
          line-height="200%"
        >
          ${mail.title}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Greeting -->
    <mj-section background-color="#ffffff" padding-top="9px">
      <mj-column>
        <mj-text
          color="#2B2E34"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="16px"
        >
          ${mail.greeting}
        </mj-text>
        <mj-text
          color="#6F7175"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${mail.intro}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-wrapper background-color="#ffffff">
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkInDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkIn}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkOutDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkOut}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
        <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.unitGroup}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.unitGroup}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.bookingId}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.bookingId}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
            <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A" font-weight="bold">${general.totalPrice}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px" font-weight="bold">${reservation.totalGross}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
    </mj-wrapper>

    <!-- Divider -->
    ${divider}

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0">
      <mj-column>
        <mj-text color="#2B2E34" align="center" font-family="Verdana" font-size="13px">${mail.invoiceAttached}</mj-text>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0">
      <mj-column>
        <mj-text line-height="18px" color="#2B2E34" align="center" font-family="Verdana" font-size="13px">${mail.footerIntro}</mj-text>
      </mj-column>
    </mj-section>

    <!-- Footer -->
    ${footer(general, property)}
  </mj-body>
</mjml>`;
