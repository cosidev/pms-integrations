import { GeneralEmailContent, PropertyEmailContent } from '../../mailCms/types';

export default (general: GeneralEmailContent, property: PropertyEmailContent) => `<mj-section background-color="#ffffff" padding="0">
  <mj-column>
    <mj-text color="#2B2E34" align="center" font-family="Verdana" font-size="13px" padding-bottom="0">${general.bestRegards}</mj-text>
    <mj-image padding-top="0" width="136px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/cosi-team.png?v=2020" />
  </mj-column>
</mj-section>

<mj-section background-color="#ffffff" padding="0">
  <mj-group>
    <mj-column>
      <mj-social mode="horizontal" inner-padding="8px">
        <mj-social-element css-class="social-icon" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/footer-facebook.png?v=2" href="${general.facebookUrl}" />
        <mj-social-element css-class="social-icon" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/footer-instagram.png?v=2" href="${general.instagramUrl}" />
        <mj-social-element css-class="social-icon" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/footer-linkedin.png?v=2" href="${general.linkedinUrl}" />
      </mj-social>
    </mj-column>
  </mj-group>
</mj-section>

<mj-section background-color="#ffffff" padding="0">
  <mj-column>
    <mj-text font-family="Verdana" font-size="13px" align="center" padding-top="0">www.cosi-group.com</mj-text>
    <mj-text font-family="Verdana" font-size="13px" align="center" line-height="18px" color="rgb(77, 77, 77)">${general.tncInfo}</mj-text>
    <mj-spacer />
    <mj-text font-family="Verdana" line-height="16px" font-size="11px" align="center" color="rgb(128, 128, 128)">${property.imprint}</mj-text>
  </mj-column>
</mj-section>`;
