import { MailData } from '../../mailCms/types';

export default (preview: string) => `<mj-head>
<mj-font name="Proxima Nova" href="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/css/proxima.css" />
<mj-font name="Optima" href="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/css/optima.css" />
<mj-preview>${preview || ''}</mj-preview>
<mj-style>
  .social-icon img {
    height: 22px !important;
    width: auto !important;
    margin: 0 auto !important;
  }
</mj-style>
<mj-style>
  @media (max-width: 480px) {
    .icon-left-column td {
      margin-left: 0;
      text-align: left;
      padding-left: 0 !important;
    }
    .icon-left-section {
      padding: 0 25px !important;
    }
  }
</mj-style>
<mj-style>
  .contact-display {
    display: none;
  }
  @media (min-width:480px) and (max-width: 595px) { 
  .contact-display {
    display: block;
  }
}
</mj-style>
<mj-style>
  .activation-title {
    background: #EFF6F4;
    padding: 5px 0px 5px 20px;
    line-height: 20px;
  }
  .activation-code {
    background: #EFF6F4;
    font-weight: bold;
    padding: 5px 20px 5px 0px;
  }
  @media (max-width: 380px) {
    .activation-title {
      padding: 5px 20px 5px 20px;
      line-height: 23px;
    }
    .activation-code {
      width: 100%;
      padding: 0px 20px 5px 20px;
    }
  }
</mj-style>
</mj-head>
`;
