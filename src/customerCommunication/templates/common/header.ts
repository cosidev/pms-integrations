export default `<mj-section background-color="#ffffff" padding-bottom="0">
  <mj-column>
    <mj-image align="right" width="52px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/cosi-logo.png?v=3" />
  </mj-column>
</mj-section>`;
