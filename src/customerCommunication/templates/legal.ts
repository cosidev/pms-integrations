import { head, header, footer, divider } from './common';
import { MailData, LegalContent } from '../mailCms/types';
import { MailReservation } from '../mailReservation.service';

export default ({ general, mail, property }: MailData<LegalContent>, reservation: MailReservation) => `<mjml>
${head(mail.emailPreview)}
  <mj-body>
    <!-- COSI Header -->
    ${header}

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0px">
      <mj-column>
        <mj-image padding="0" width="128px" src="${mail.greetingSrc}" />
        <mj-text padding-top="0" color="#2B2E34" font-family="Verdana" font-size="13px" align="center">${reservation.guestName}</mj-text>
        <mj-text color="#6F7175" font-family="Verdana" line-height="18px" font-size="13px" align="center">${mail.intro}</mj-text>
        <mj-text color="#6F7175" font-family="Verdana" line-height="18px" font-size="13px" align="center">${mail.linkAction}</mj-text>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff">
      <mj-column padding="0">
        <mj-button
          href="${reservation.urls?.neededData}"
          font-family="Verdana"
          font-size="20px"
          background-color="#153B4A"
          color="#ffffff"
          padding="0 25px"
          inner-padding="13px"
          width="100%"
          border-radius="0"
        >
          ${mail.provideInfoCta}
        </mj-button>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-family="Verdana"
          font-weight="bold"
          font-size="15px"
          align="center"
          line-height="18px"
        >
            ${general.bookingDetails}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-wrapper background-color="#ffffff">
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.hotelAddress}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${property.address}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkInDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.checkIn}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkOutDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.checkOut}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.bookingId}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.bookingId}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
    </mj-wrapper>

    <!-- Divider -->
    ${divider}

    <mj-section background-color="#ffffff">
      <mj-column>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
          align="center"
        >
          ${general.manualButton}
        </mj-text>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text line-height="18px" color="#2B2E34" align="center" font-family="Verdana" font-size="13px">${mail.footerIntro}</mj-text>
      </mj-column>
    </mj-section>

    <!-- Footer -->
    ${footer(general, property)}
  </mj-body>
</mjml>`;
