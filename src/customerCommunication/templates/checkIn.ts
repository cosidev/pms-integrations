import { head, header, footer, divider } from './common';
import { MailData, CheckInContent } from '../mailCms/types';
import { MailReservation } from '../mailReservation.service';

export default ({ general, mail, property }: MailData<CheckInContent>, reservation: MailReservation) => `<mjml>
${head(mail.emailPreview)}
  <mj-body>
    <!-- COSI Header -->
    ${header}

    <!-- Illustration hero -->
    <mj-hero mode="fluid-height" background-width="600px" background-height="228px" background-url="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/checkin-hero.png" background-color="#ffffff" padding="0px 0px"></mj-hero>

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0px">
      <mj-column>
        <mj-text
          color="#153B4A"
          align="center"
          font-family="Optima"
          font-size="30px"
          line-height="200%"
        >
          ${mail.title}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Greeting -->
    <mj-section background-color="#ffffff" padding-top="9px">
      <mj-column>
        <mj-text
          color="#2B2E34"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="16px"
        >
          ${mail.greeting}
        </mj-text>
        <mj-text
          color="#6F7175"
          align="center"
          font-family="Verdana"
          font-size="13px"
          line-height="16px"
        >
          ${mail.intro}
        </mj-text>
        <mj-text
          color="#153B4A"
          font-family="Verdana"
          font-weight="bold"
          font-size="13px"
          align="center"
        >
            ${general.bookingDetails}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-wrapper background-color="#ffffff">
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.hotelAddress}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" line-height="16px" font-size="13px" color="#2B2E34">${property.address}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkInDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkIn}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.checkOutDate}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.checkOut}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
      <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.bookingId}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.bookingId}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
            <mj-section padding="0">
        <mj-group>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.unitNumber}</mj-text>
          </mj-column>
          <mj-column vertical-align="top">
            <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="16px">${reservation.unitNumber}</mj-text>
          </mj-column>
        </mj-group>
      </mj-section>
    </mj-wrapper>

    <!-- Divider -->
    ${divider}

    ${
      property.luggageStorage
        ? `
      <mj-section background-color="#ffffff" padding-bottom="0">
        <mj-column>
          <mj-text
            color="#153B4A"
            font-size="15px"
            font-weight="bold"
          >
            ${general.luggageStorageHeadline}
          </mj-text>
        </mj-column>
      </mj-section>

      <mj-section css-class="icon-left-section" background-color="#ffffff" padding="0">
        <mj-column css-class="icon-left-column" width="20%" vertical-align="top">
          <mj-image width="55px" height="49px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/luggage.png?v=2" alt="luggage" />
        </mj-column>
        <mj-column width="80%" vertical-align="top">
          <mj-text
            color="#6F7175"
            padding-left="0"
            font-family="Verdana"
            font-size="13px"
            line-height="18px"
          >
            ${property.luggageStorage}
          </mj-text>
        </mj-column>
      </mj-section>`
        : ''
    }

    <mj-section background-color="#ffffff" padding-bottom="0">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
        >
          ${general.roomAccessHeadline}
        </mj-text>
      </mj-column>
    </mj-section>

    ${
      property.roomAccess1
        ? `
    <mj-section css-class="icon-left-section" background-color="#ffffff" padding="0">
      <mj-column css-class="icon-left-column" width="20%" vertical-align="top">
        <mj-image width="64px" height="57px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/pin_access.png?v=2" alt="pin_access" />
      </mj-column>
      <mj-column width="80%" vertical-align="top">
        <mj-text
          color="#6F7175"
          padding-left="0"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.roomAccess1}
        </mj-text>
      </mj-column>
    </mj-section>
    `
        : ''
    }
    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        ${
          reservation.pinCode
            ? `
        <mj-text
            padding-bottom="30px"
            font-family="Verdana"
            font-size="16px"
            align="center"
          >
            ${general.pinCode}
            <span style="font-weight: bold; font-size: 20px; border: 1px solid; border-color: '#153B4A'; padding: 6px 20px">${reservation.pinCode}</span>
          </mj-text>
          `
            : ''
        }
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.roomAccess2 || ''}
        </mj-text>
        ${
          reservation.oneTimeActivationCode
            ? `
        <mj-text
          padding-top="30px"
          padding-bottom="30px"
          color="#153B4A"
          font-family="Verdana"
          font-size="15px"
          align="center"
        >
          <span class="activation-title">
            ${general.oneTimeActivationCode}
          </span>
          <span class="activation-code">${reservation.oneTimeActivationCode}</span>
        </mj-text>`
            : ''
        }
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.roomAccess3 || ''}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${general.checkInSupport || ''}
        </mj-text>

      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
        >
          ${mail.roomDirectionsHeadline}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.roomDirections}
        </mj-text>
        ${
          property.luggageStorageDirections
            ? `
          <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
        >
          ${mail.luggageStorageDirectionsHeadline}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.luggageStorageDirections}
        </mj-text>
        `
            : ''
        }
      </mj-column>
    </mj-section>

    <!-- Divider -->
    ${divider}

    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
        >
          ${general.wifiKeyHeadline}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${property.wifiInformation}
        </mj-text>
      </mj-column>
    </mj-section>

    <!-- Divider -->
    <mj-include path="./templates/divider.mjml" />

    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text
          color="#153B4A"
          font-size="15px"
          font-weight="bold"
        >
          ${general.questionsHeadline}
        </mj-text>
        <mj-text
          color="#6F7175"
          font-family="Verdana"
          font-size="13px"
          line-height="18px"
        >
          ${general.questionsInfo}
        </mj-text>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding-top="0" padding-bottom="0">
    <mj-column>
    ${
      property.phoneNumber
        ? `
      <mj-social align="left">
        <mj-social-element icon-height="30px" color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/phone-icon.png?v=2">
          <span style="color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.callCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.phoneNumber}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    ${
      property.whatsappNumber
        ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/whatsapp-icon.png?v=2">
          <span style="padding-left: 0px; color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.whatsappCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.whatsappNumber}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    ${
      property.messengerUrl
        ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/facebook-icon.png?v=2">
          <span style="padding-left: 0px; color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.messengerCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.messengerUrl}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    </mj-column>
    <mj-column>
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/mail-icon.png?v=2">
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.bookingMail}</span>
        </mj-social-element>
      </mj-social>
      ${
        property.directionsUrl
          ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/directions-icon.png?v=2">
          <a href="${property.directionsUrl}"><span style="color: rgb(56, 166, 158); text-decoration: underline; font-family: Verdana; font-size: 13px">${general.directionsCta}</span></a>
        </mj-social-element>
      </mj-social>
      `
          : ''
      }
    </mj-column>
  </mj-section>

    ${
      reservation.urls.tripManagement
        ? `
    <!-- Image Header -->
    <mj-section background-color="#ffffff" padding-bottom="0">
      <mj-column>
        <mj-image width="180px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/tripmanagement.png?v=678" padding-bottom="0" />
      </mj-column>
      <mj-column>
        <mj-text color="#2B2E34" line-height="20px" font-family="Verdana" font-size="13px">
          ${general.tripManagementInfo}
        </mj-text>
      </mj-column>
    </mj-section>

    <mj-section background-color="#ffffff" padding="0">
      <mj-column padding="0">
        <mj-button
          href="${reservation.urls.tripManagement}"
          font-family="Verdana"
          font-size="20px"
          background-color="#153B4A"
          color="#ffffff"
          inner-padding="13px"
          padding="0 25px"
          width="100%"
          border-radius="0"
        >
        ${general.tripManagementCta}
        </mj-button>
      </mj-column>
    </mj-section>`
        : ''
    }

    <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0">
      <mj-column>
        <mj-text color="#2B2E34" align="center" font-family="Verdana" font-size="13px">${mail.footerIntro}</mj-text>
      </mj-column>
    </mj-section>

    <!-- Footer -->
    ${footer(general, property)}
  </mj-body>
</mjml>`;
