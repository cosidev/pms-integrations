export { default as bookingConfirmation } from './bookingConfirmation';
export { default as checkIn } from './checkIn';
export { default as checkInDay } from './checkInDay';
export { default as invoice } from './invoice';
export { default as legal } from './legal';
