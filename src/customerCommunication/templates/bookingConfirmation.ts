import { head, header, footer, divider } from './common';
import { MailData, BookingConfirmationContent } from '../mailCms/types';
import { MailReservation } from '../mailReservation.service';

export default ({ general, mail, property }: MailData<BookingConfirmationContent>, reservation: MailReservation) => `<mjml>
${head(mail.emailPreview)}
<mj-body>
  <!-- COSI Header -->
  ${header}

  ${
    property.illustration
      ? `
  <!-- Property illustration hero -->
  <mj-hero mode="fluid-height" background-width="600px" background-height="311px" background-url="${property.illustration}" background-color="#ffffff" padding="0px 0px"></mj-hero>
  `
      : `    <!-- Illustration hero -->
      <mj-hero mode="fluid-height" background-width="600px" background-height="228px" background-url="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/checkin-hero.png" background-color="#ffffff" padding="0px 0px"></mj-hero>
  `
  }

  <!-- MAIL TITLE -->
  <mj-section background-color="#ffffff" padding-top="15px" padding-bottom="0px">
    <mj-column>
      <mj-image width="306px" src="${mail.bookingConfirmationSrc}" />
    </mj-column>
  </mj-section>

  <!-- Greeting -->
  <mj-section background-color="#ffffff" padding-top="9px">
    <mj-column>
      <mj-text
        color="#2B2E34"
        align="center"
        font-family="Verdana"
        font-size="13px"
        line-height="30px"
      >
        ${mail.greeting}
        <br />
        ${mail.intro}
      </mj-text>
    </mj-column>
  </mj-section>

  <!-- Divider -->
  ${divider}

  <!-- Booking information -->
  <mj-section background-color="#ffffff" padding-bottom="0px">
    <mj-column>
      <mj-text
        color="#153B4A"
        font-family="Optima"
        font-size="44px"
        padding-left="20px"
        padding-bottom="0px"
      >
      ${property.name}
      </mj-text>
      <mj-text
        color="rgb(148, 150, 153)"
        font-size="13px"
        font-family="Verdana"
        padding-top="5px"
      >
      ${property.address}
      </mj-text>
    </mj-column>
  </mj-section>

  <mj-section background-color="#ffffff" padding-top="10px">
    <mj-group>
      <mj-column>
        <mj-text color="rgb(148, 150, 153)" font-size="13px" font-family="Verdana">${mail.checkIn}</mj-text>
        <mj-text
          padding-top="0px"
          color="#2B2E34"
          font-size="16px"
          font-family="Verdana"
          line-height="18px"
        >
          ${reservation.formattedCheckIn}
        </mj-text>
      </mj-column>
      <mj-column>
        <mj-text color="rgb(148, 150, 153)" font-size="13px" font-family="Verdana">${mail.checkOut}</mj-text>
        <mj-text
          padding-top="0px"
          color="#2B2E34"
          font-size="16px"
          line-height="18px"
          font-family="Verdana"
        >
          ${reservation.formattedCheckOut}
        </mj-text>
      </mj-column>
    </mj-group>
  </mj-section>

  ${
    reservation.urls.tripManagement
      ? `
  <!-- Image Header -->
  <mj-section background-color="#ffffff" padding-bottom="0">
    <mj-column>
      <mj-image width="180px" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/tripmanagement.png?v=678" padding-bottom="0" />
    </mj-column>
    <mj-column>
      <mj-text color="#2B2E34" line-height="20px" font-family="Verdana" font-size="13px">
        ${general.tripManagementInfo}
      </mj-text>
    </mj-column>
  </mj-section>

  <mj-section background-color="#ffffff" padding="0">
    <mj-column padding="0">
      <mj-button
        href="${reservation.urls.tripManagement}"
        font-family="Verdana"
        font-size="20px"
        background-color="#153B4A"
        color="#ffffff"
        inner-padding="13px"
        padding="0 25px"
        width="100%"
        border-radius="0"
      >
      ${general.tripManagementCta}
      </mj-button>
    </mj-column>
  </mj-section>`
      : ''
  }

  <!-- Introduction Text -->

  <mj-wrapper background-color="#ffffff" padding-top="43px">
    <mj-section padding="0">
      <mj-group>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#85868A">${mail.guest}</mj-text>
        </mj-column>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.guestName}</mj-text>
        </mj-column>
      </mj-group>
    </mj-section>
    <mj-section padding="0">
      <mj-group>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#85868A">${general.unitGroup}</mj-text>
        </mj-column>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.unitGroup}</mj-text>
        </mj-column>
      </mj-group>
    </mj-section padding="0">
    <mj-section padding="0">
      <mj-group>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#85868A">${mail.numberOfGuests}</mj-text>
        </mj-column>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.formattedGuests}</mj-text>
        </mj-column>
      </mj-group>
    </mj-section>
    <mj-section padding="0">
      <mj-group>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#85868A">${mail.reservationId}</mj-text>
        </mj-column>
        <mj-column vertical-align="top">
          <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">${reservation.id}</mj-text>
        </mj-column>
      </mj-group>
    </mj-section>
  </mj-wrapper>

  ${
    reservation.formattedPaymentInformation
      ? `
    <mj-section background-color="#ffffff" padding="0">
      <mj-column>
        <mj-text font-family="Verdana" font-size="11px" line-height="18px" color="#85868A">${reservation.formattedPaymentInformation}</mj-text>
      </mj-column>
    </mj-section>
  `
      : ''
  }

  ${divider}

  <mj-section background-color="#ffffff" padding="0">
    <mj-column>
      <mj-text font-family="Verdana" font-size="13px" color="#2B2E34" line-height="18px">
        ${mail.supportInfo}
      </mj-text>
    </mj-column>
  </mj-section>

  <mj-section background-color="#ffffff" padding-top="0" padding-bottom="0">
    <mj-column>
    ${
      property.phoneNumber
        ? `
      <mj-social align="left">
        <mj-social-element icon-height="30px" color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/phone-icon.png?v=2">
          <span style="color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.callCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.phoneNumber}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    ${
      property.whatsappNumber
        ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/whatsapp-icon.png?v=2">
          <span style="padding-left: 0px; color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.whatsappCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.whatsappNumber}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    ${
      property.messengerUrl
        ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/facebook-icon.png?v=2">
          <span style="padding-left: 0px; color: rgb(110, 110, 110); font-family: Verdana; font-size: 13px">${general.messengerCta}</span>
          <mj-raw><div class="contact-display"></div></mj-raw>
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.messengerUrl}</span>
        </mj-social-element>
      </mj-social>
    `
        : ''
    }
    </mj-column>
    <mj-column>
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/mail-icon.png?v=2">
          <span style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px">${property.bookingMail}</span>
        </mj-social-element>
      </mj-social>
      ${
        property.directionsUrl
          ? `
      <mj-social align="left">
        <mj-social-element color="#fff" src="https://cosi-email-files.s3.eu-central-1.amazonaws.com/assets/images/directions-icon.png?v=2">
          <a href="${property.directionsUrl}"><span style="color: rgb(56, 166, 158); text-decoration: underline; font-family: Verdana; font-size: 13px">${general.directionsCta}</span></a>
        </mj-social-element>
      </mj-social>
      `
          : ''
      }
    </mj-column>
  </mj-section>

  ${
    property.additionalInfo
      ? `
  <mj-section background-color="#ffffff" padding="0">
    <mj-column>
      <mj-text font-family="Verdana" color="rgb(102, 102, 102)" font-size="12px" line-height="18px">
        ${property.additionalInfo}
      </mj-text>
    </mj-column>
  </mj-section>
  `
      : ''
  }

  ${divider}

  <mj-section background-color="#ffffff" padding="0">
    <mj-column>
    ${
      property.earlyCheckinInfo
        ? `
        <mj-text color="#153B4A" font-family="Verdana" font-weight="bold" font-size="15px">
          ${mail.earlyCheckinHeadline}
        </mj-text>
        <mj-text color="#6F7175" font-family="Verdana" font-size="12px" line-height="18px">
          ${property.earlyCheckinInfo}
        </mj-text>
        `
        : ''
    }
      ${
        property.luggageStorageShort
          ? `
      <mj-text color="#153B4A" font-family="Verdana" font-weight="bold" font-size="15px">
        ${general.luggageStorageHeadline}
      </mj-text>
      <mj-text color="#6F7175" font-family="Verdana" font-size="12px" line-height="18px">
        ${property.luggageStorageShort}
      </mj-text>`
          : ''
      }
      ${
        property.parkingInfo
          ? `
      <mj-text color="#153B4A" font-family="Verdana" font-weight="bold" font-size="15px">
        ${mail.parkingHeadline}
      </mj-text>
      <mj-text color="#6F7175" font-family="Verdana" font-size="12px" line-height="18px">
        ${property.parkingInfo}
      </mj-text>`
          : ''
      }
      <mj-text color="#153B4A" font-family="Verdana" font-weight="bold" font-size="15px">
        ${property.houseRulesHeadline}
      </mj-text>
      <mj-text color="#6F7175" font-family="Verdana" font-size="12px" line-height="18px">
        ${property.houseRules}
      </mj-text>
    </mj-column>
  </mj-section>

  ${divider}

  <mj-section background-color="#ffffff" padding="0">
    <mj-column>
      <mj-text color="#2B2E34" align="center" font-family="Verdana" font-size="13px" line-height="18px">${mail.footerIntro}</mj-text>
    </mj-column>
  </mj-section>

  ${footer(general, property)}
</mj-body>
</mjml>`;
